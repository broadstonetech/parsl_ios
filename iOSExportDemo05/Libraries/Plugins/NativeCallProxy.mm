//
//  NativeCallProxy.m
//  UnityNativeIos
//
//  Created by Mufaza on 05/07/2021.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "NativeCallProxy.h"

@implementation FrameworkLibAPI

id<NativeCallsProtocol> api = NULL;
+(void) registerAPIforNativeCalls:(id<NativeCallsProtocol>) aApi
{
    api = aApi;
}

@end

extern "C"
{   //when cart icon tapped in unity
    void sendMessageToMobileApp(const char* message)
    {
        return [api sendMessageToMobileApp:[NSString stringWithUTF8String:message]];
    }

    void destroyUnity(const char* message)
   {
        return [api destroyUnity:[NSString stringWithUTF8String:message]];
   }

    void showContainerView(const char* message)
   {
        return [api showContainerView:[NSString stringWithUTF8String:message]];
   }
    void sendMessageOnReload(const char* message)
   {
        return [api sendMessageOnReload:[NSString stringWithUTF8String:message]];
   }
     void modelProjectionDone(const char* message)
   {
        return [api modelProjectionDone:[NSString stringWithUTF8String:message]];
   }

}


