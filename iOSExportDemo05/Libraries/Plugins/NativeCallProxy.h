//
//  NativeCallProxy.h
//  UnityNativeIos
//
//  Created by Mufaza on 05/07/2021.
//

#ifndef NativeCallProxy_h
#define NativeCallProxy_h


#endif /* NativeCallProxy_h */

#import <Foundation/Foundation.h>

@protocol NativeCallsProtocol
@required
- (void) sendMessageToMobileApp:(NSString*)message;
- (void) destroyUnity:(NSString*)message;
- (void) showContainerView:(NSString*)message;
- (void) sendMessageOnReload:(NSString*)message;
- (void) modelProjectionDone:(NSString*)message;


// other methods
@end

__attribute__ ((visibility("default")))
@interface FrameworkLibAPI : NSObject
+(void) registerAPIforNativeCalls:(id<NativeCallsProtocol>) aApi;

@end
