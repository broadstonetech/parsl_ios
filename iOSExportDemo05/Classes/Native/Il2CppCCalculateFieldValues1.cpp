﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.IntPtr>
struct Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914;
// System.Action`2<System.IntPtr,UnityEngine.XR.ARKit.NSError>
struct Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem>
struct Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B;
// System.Collections.Generic.HashSet`1<UnityEngine.TextureFormat>
struct HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_tC941CBF342F522B4B7EED02680F598139724300E;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor>
struct List_1_tDED98C236097B36F9015B396398179A6F8A62E50;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor>
struct List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor>
struct List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor>
struct List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor>
struct List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor>
struct List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0;
// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor>
struct List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor>
struct List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor>
struct List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor>
struct List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor>
struct List_1_tAE84735071B78277703DB9996DE2E5C4456317C5;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor>
struct List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673;
// System.Collections.Generic.List`1<AddToCartScript/ItemsData>
struct List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F;
// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
struct UnityAction_1_tB7747736C32FCE47BB6B16972A8A252665A16A91;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// PlacementObject[]
struct PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E;
// UnityEngine.TextureFormat[]
struct TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE;
// API
struct API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2;
// UnityEngine.XR.ARKit.ARKitSessionDelegate
struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4;
// UnityEngine.XR.ARFoundation.ARPointCloudManager
struct ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF;
// UnityEngine.XR.ARFoundation.ARRaycastManager
struct ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E;
// ArCanvas
struct ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.XR.ARSubsystems.ConfigurationChooser
struct ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Lean.Touch.LeanSelectable
struct LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ObjectSpawner
struct ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388;
// PlacementIndicator
struct PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275;
// PlacementObject
struct PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary
struct RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B;
// System.String
struct String_t;
// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// TMPro.TextMeshPro
struct TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor
struct XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2;
// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor
struct XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243;
// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor
struct XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor
struct XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B;
// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor
struct XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472;
// UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor
struct XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB;
// UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor
struct XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor
struct XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483;
// UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor
struct XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C;
// UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary
struct XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor
struct XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider
struct Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6;
// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider
struct Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190;
// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider
struct Provider_t055C90C34B2BCE8D134DF44C12823E320519168C;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider
struct Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E;
// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider
struct Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E;
// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider
struct Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB;
// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider
struct Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider
struct Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12;
// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider
struct Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider
struct Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A;
// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032 
{
public:

public:
};


// System.Object


// UnityEngine.XR.ARKit.ARKitLoaderConstants
struct ARKitLoaderConstants_t1A46CABB17835CC85326A66CF3921059BB927B13  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitMeshSubsystemExtensions
struct ARKitMeshSubsystemExtensions_tA52932FCB3EF1A3607716616D0217B10B222329D  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionDelegate
struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE  : public RuntimeObject
{
public:

public:
};

struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields
{
public:
	// System.Action`2<System.IntPtr,UnityEngine.XR.ARKit.NSError> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_SessionDidFailWithError
	Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * ___s_SessionDidFailWithError_0;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_CoachingOverlayViewWillActivate
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_CoachingOverlayViewWillActivate_1;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_CoachingOverlayViewDidDeactivate
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_CoachingOverlayViewDidDeactivate_2;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_ConfigurationChanged
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_ConfigurationChanged_3;

public:
	inline static int32_t get_offset_of_s_SessionDidFailWithError_0() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_SessionDidFailWithError_0)); }
	inline Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * get_s_SessionDidFailWithError_0() const { return ___s_SessionDidFailWithError_0; }
	inline Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D ** get_address_of_s_SessionDidFailWithError_0() { return &___s_SessionDidFailWithError_0; }
	inline void set_s_SessionDidFailWithError_0(Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * value)
	{
		___s_SessionDidFailWithError_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SessionDidFailWithError_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_CoachingOverlayViewWillActivate_1() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_CoachingOverlayViewWillActivate_1)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_CoachingOverlayViewWillActivate_1() const { return ___s_CoachingOverlayViewWillActivate_1; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_CoachingOverlayViewWillActivate_1() { return &___s_CoachingOverlayViewWillActivate_1; }
	inline void set_s_CoachingOverlayViewWillActivate_1(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_CoachingOverlayViewWillActivate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CoachingOverlayViewWillActivate_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_CoachingOverlayViewDidDeactivate_2() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_CoachingOverlayViewDidDeactivate_2)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_CoachingOverlayViewDidDeactivate_2() const { return ___s_CoachingOverlayViewDidDeactivate_2; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_CoachingOverlayViewDidDeactivate_2() { return &___s_CoachingOverlayViewDidDeactivate_2; }
	inline void set_s_CoachingOverlayViewDidDeactivate_2(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_CoachingOverlayViewDidDeactivate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CoachingOverlayViewDidDeactivate_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigurationChanged_3() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_ConfigurationChanged_3)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_ConfigurationChanged_3() const { return ___s_ConfigurationChanged_3; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_ConfigurationChanged_3() { return &___s_ConfigurationChanged_3; }
	inline void set_s_ConfigurationChanged_3(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_ConfigurationChanged_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ConfigurationChanged_3), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions
struct ARWorldMapRequestStatusExtensions_tBA8B558AE95AA57A9FB348869D8F63494FDB5C1A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.EnvironmentProbeApi
struct EnvironmentProbeApi_t2B36D33D812E48CB4615B2FB0B41BD22D184BB19  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.NSErrorExtensions
struct NSErrorExtensions_t6DCF646D8E7E93886A929C22BC31AD2F85E5BDAB  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.NSObject
struct NSObject_tDE23F4BBB8E43766E6795164F9B80137E83E96C0  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary
struct RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemProvider::m_Running
	bool ___m_Running_0;

public:
	inline static int32_t get_offset_of_m_Running_0() { return static_cast<int32_t>(offsetof(SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9, ___m_Running_0)); }
	inline bool get_m_Running_0() const { return ___m_Running_0; }
	inline bool* get_address_of_m_Running_0() { return &___m_Running_0; }
	inline void set_m_Running_0(bool value)
	{
		___m_Running_0 = value;
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider
struct SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<running>k__BackingField
	bool ___U3CrunningU3Ek__BackingField_0;
	// UnityEngine.SubsystemsImplementation.SubsystemProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<providerBase>k__BackingField
	SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * ___U3CproviderBaseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CrunningU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E, ___U3CrunningU3Ek__BackingField_0)); }
	inline bool get_U3CrunningU3Ek__BackingField_0() const { return ___U3CrunningU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CrunningU3Ek__BackingField_0() { return &___U3CrunningU3Ek__BackingField_0; }
	inline void set_U3CrunningU3Ek__BackingField_0(bool value)
	{
		___U3CrunningU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CproviderBaseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E, ___U3CproviderBaseU3Ek__BackingField_1)); }
	inline SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * get_U3CproviderBaseU3Ek__BackingField_1() const { return ___U3CproviderBaseU3Ek__BackingField_1; }
	inline SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 ** get_address_of_U3CproviderBaseU3Ek__BackingField_1() { return &___U3CproviderBaseU3Ek__BackingField_1; }
	inline void set_U3CproviderBaseU3Ek__BackingField_1(SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * value)
	{
		___U3CproviderBaseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderBaseU3Ek__BackingField_1), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// API/<GetDisplayBundleRoutine>d__5
struct U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8  : public RuntimeObject
{
public:
	// System.Int32 API/<GetDisplayBundleRoutine>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<GetDisplayBundleRoutine>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String API/<GetDisplayBundleRoutine>d__5::assetName
	String_t* ___assetName_2;
	// API API/<GetDisplayBundleRoutine>d__5::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_3;
	// UnityEngine.Transform API/<GetDisplayBundleRoutine>d__5::bundleParent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___bundleParent_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject> API/<GetDisplayBundleRoutine>d__5::callback
	UnityAction_1_tB7747736C32FCE47BB6B16972A8A252665A16A91 * ___callback_5;
	// UnityEngine.Networking.UnityWebRequest API/<GetDisplayBundleRoutine>d__5::<www>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwwwU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_assetName_2() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___assetName_2)); }
	inline String_t* get_assetName_2() const { return ___assetName_2; }
	inline String_t** get_address_of_assetName_2() { return &___assetName_2; }
	inline void set_assetName_2(String_t* value)
	{
		___assetName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assetName_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___U3CU3E4__this_3)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_bundleParent_4() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___bundleParent_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_bundleParent_4() const { return ___bundleParent_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_bundleParent_4() { return &___bundleParent_4; }
	inline void set_bundleParent_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___bundleParent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bundleParent_4), (void*)value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___callback_5)); }
	inline UnityAction_1_tB7747736C32FCE47BB6B16972A8A252665A16A91 * get_callback_5() const { return ___callback_5; }
	inline UnityAction_1_tB7747736C32FCE47BB6B16972A8A252665A16A91 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(UnityAction_1_tB7747736C32FCE47BB6B16972A8A252665A16A91 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8, ___U3CwwwU3E5__2_6)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwwwU3E5__2_6() const { return ___U3CwwwU3E5__2_6; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwwwU3E5__2_6() { return &___U3CwwwU3E5__2_6; }
	inline void set_U3CwwwU3E5__2_6(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwwwU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwwwU3E5__2_6), (void*)value);
	}
};


// API/<WaitForNetwork>d__3
struct U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0  : public RuntimeObject
{
public:
	// System.Int32 API/<WaitForNetwork>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<WaitForNetwork>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// API API/<WaitForNetwork>d__3::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0, ___U3CU3E4__this_2)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// API/<WaitForValidAsset>d__4
struct U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4  : public RuntimeObject
{
public:
	// System.Int32 API/<WaitForValidAsset>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<WaitForValidAsset>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// API API/<WaitForValidAsset>d__4::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4, ___U3CU3E4__this_2)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitCpuImageApi/Native
struct Native_tF04A9C18AA77B29062CE0428C310D081D0D35E77  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem/NativeApi
struct NativeApi_tD8D7F6FC5A365EA4D3189425623AC3D23A7ED03B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitMeshSubsystemExtensions/NativeApi
struct NativeApi_tDB232E5FEE2CD4783CEB5C809F129E4A2A4E7EB5  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/NativeApi
struct NativeApi_tFF41350E0941271CB10B461CA357ABF510ACB223  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem/NativeApi
struct NativeApi_t62CE96BA0CF6D47C5FD80B91223590E60F948070  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi
struct NativeApi_t14734856858FD58426CAE2F6A09FA522E550C226  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/NativeApi
struct NativeApi_tB65E4D304D37EA8B8ED6329919AEE7845A85C55E  : public RuntimeObject
{
public:

public:
};


// Anim/<delay>d__3
struct U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79  : public RuntimeObject
{
public:
	// System.Int32 Anim/<delay>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Anim/<delay>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// ArCanvas/<WaitForDestruction>d__12
struct U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F  : public RuntimeObject
{
public:
	// System.Int32 ArCanvas/<WaitForDestruction>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ArCanvas/<WaitForDestruction>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ArCanvas ArCanvas/<WaitForDestruction>d__12::<>4__this
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F, ___U3CU3E4__this_2)); }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ArCanvas/<WaitForPanelOff>d__8
struct U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9  : public RuntimeObject
{
public:
	// System.Int32 ArCanvas/<WaitForPanelOff>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ArCanvas/<WaitForPanelOff>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ArCanvas ArCanvas/<WaitForPanelOff>d__8::<>4__this
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9, ___U3CU3E4__this_2)); }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ArCanvas/NativeAPI
struct NativeAPI_t0C0CF7D8A37491A6EE905628D637573B751EFBA9  : public RuntimeObject
{
public:

public:
};


// ObjectSpawner/<WaitForButtonOff>d__12
struct U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForButtonOff>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForButtonOff>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForButtonOff>d__12::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/<WaitForDownload>d__21
struct U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForDownload>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForDownload>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForDownload>d__21::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/<WaitForDownloadRedeem2>d__26
struct U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForDownloadRedeem2>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForDownloadRedeem2>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForDownloadRedeem2>d__26::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/<WaitForDownloadRedeem>d__24
struct U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForDownloadRedeem>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForDownloadRedeem>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForDownloadRedeem>d__24::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/NativeAPI
struct NativeAPI_tA82FCF52A5EFF585B109939C30419CF8D4259A65  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRCpuImage/Api
struct Api_t7C92F00C6416A2C636A44AAC833C3773C567DC3E  : public RuntimeObject
{
public:

public:
};


// Unity.Collections.NativeSlice`1<System.Byte>
struct NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B 
{
public:
	// System.Byte* Unity.Collections.NativeSlice`1::m_Buffer
	uint8_t* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Stride
	int32_t ___m_Stride_1;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Buffer_0)); }
	inline uint8_t* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline uint8_t** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(uint8_t* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Stride_1() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Stride_1)); }
	inline int32_t get_m_Stride_1() const { return ___m_Stride_1; }
	inline int32_t* get_address_of_m_Stride_1() { return &___m_Stride_1; }
	inline void set_m_Stride_1(int32_t value)
	{
		___m_Stride_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// Unity.Collections.NativeSlice`1<System.UInt32>
struct NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A 
{
public:
	// System.Byte* Unity.Collections.NativeSlice`1::m_Buffer
	uint8_t* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Stride
	int32_t ___m_Stride_1;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Buffer_0)); }
	inline uint8_t* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline uint8_t** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(uint8_t* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Stride_1() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Stride_1)); }
	inline int32_t get_m_Stride_1() const { return ___m_Stride_1; }
	inline int32_t* get_address_of_m_Stride_1() { return &___m_Stride_1; }
	inline void set_m_Stride_1(int32_t value)
	{
		___m_Stride_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystem>
struct SubsystemProvider_1_tBB539901FE99992CAA10A1EFDFA610E048498E98  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem>
struct SubsystemProvider_1_tC3DB99A11F9F3210CE2ABA9FE09C127C5B13FF80  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem>
struct SubsystemProvider_1_tBF37BFFB47314B7D87E24F4C4903C90930C0302C  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem>
struct SubsystemProvider_1_t3086BC462E1384FBB8137E64FA6C513FC002E440  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem>
struct SubsystemProvider_1_t71FE677A1A2F32123FE4C7868D5943892028AE12  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystem>
struct SubsystemProvider_1_t57D5C398A7A30AC3C3674CA126FAE612BC00F597  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystem>
struct SubsystemProvider_1_t3D6D4D936F16F3264F75D1BAB46A4A398F18F204  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystem>
struct SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystem>
struct SubsystemProvider_1_t7ACBE98539B067B19E2D5BCC2B852277F4A38875  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystem>
struct SubsystemProvider_1_tFA56F133FD9BCE90A1C4C7D15FFE2571963D8DE4  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRDepthSubsystem,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider>
struct SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider>
struct SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider>
struct SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t055C90C34B2BCE8D134DF44C12823E320519168C ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider>
struct SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider>
struct SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XROcclusionSubsystem,UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider>
struct SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRParticipantSubsystem,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider>
struct SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRPlaneSubsystem,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider>
struct SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRRaycastSubsystem,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider>
struct SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRSessionSubsystem,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider>
struct SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitCpuImageApi
struct ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59  : public Api_t7C92F00C6416A2C636A44AAC833C3773C567DC3E
{
public:

public:
};

struct ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitCpuImageApi UnityEngine.XR.ARKit.ARKitCpuImageApi::<instance>k__BackingField
	ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * ___U3CinstanceU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<UnityEngine.TextureFormat> UnityEngine.XR.ARKit.ARKitCpuImageApi::s_SupportedVideoConversionFormats
	HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * ___s_SupportedVideoConversionFormats_1;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields, ___U3CinstanceU3Ek__BackingField_0)); }
	inline ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * get_U3CinstanceU3Ek__BackingField_0() const { return ___U3CinstanceU3Ek__BackingField_0; }
	inline ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 ** get_address_of_U3CinstanceU3Ek__BackingField_0() { return &___U3CinstanceU3Ek__BackingField_0; }
	inline void set_U3CinstanceU3Ek__BackingField_0(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * value)
	{
		___U3CinstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinstanceU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_SupportedVideoConversionFormats_1() { return static_cast<int32_t>(offsetof(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields, ___s_SupportedVideoConversionFormats_1)); }
	inline HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * get_s_SupportedVideoConversionFormats_1() const { return ___s_SupportedVideoConversionFormats_1; }
	inline HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 ** get_address_of_s_SupportedVideoConversionFormats_1() { return &___s_SupportedVideoConversionFormats_1; }
	inline void set_s_SupportedVideoConversionFormats_1(HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * value)
	{
		___s_SupportedVideoConversionFormats_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SupportedVideoConversionFormats_1), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARWorldMap
struct ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMap::<nativeHandle>k__BackingField
	int32_t ___U3CnativeHandleU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnativeHandleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2, ___U3CnativeHandleU3Ek__BackingField_1)); }
	inline int32_t get_U3CnativeHandleU3Ek__BackingField_1() const { return ___U3CnativeHandleU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CnativeHandleU3Ek__BackingField_1() { return &___U3CnativeHandleU3Ek__BackingField_1; }
	inline void set_U3CnativeHandleU3Ek__BackingField_1(int32_t value)
	{
		___U3CnativeHandleU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequest
struct ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};


// UnityEngine.XR.ARKit.DefaultARKitSessionDelegate
struct DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639  : public ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE
{
public:
	// System.Int32 UnityEngine.XR.ARKit.DefaultARKitSessionDelegate::<retriesRemaining>k__BackingField
	int32_t ___U3CretriesRemainingU3Ek__BackingField_4;
	// System.Int32 UnityEngine.XR.ARKit.DefaultARKitSessionDelegate::<maxRetryCount>k__BackingField
	int32_t ___U3CmaxRetryCountU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CretriesRemainingU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639, ___U3CretriesRemainingU3Ek__BackingField_4)); }
	inline int32_t get_U3CretriesRemainingU3Ek__BackingField_4() const { return ___U3CretriesRemainingU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CretriesRemainingU3Ek__BackingField_4() { return &___U3CretriesRemainingU3Ek__BackingField_4; }
	inline void set_U3CretriesRemainingU3Ek__BackingField_4(int32_t value)
	{
		___U3CretriesRemainingU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CmaxRetryCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639, ___U3CmaxRetryCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CmaxRetryCountU3Ek__BackingField_5() const { return ___U3CmaxRetryCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CmaxRetryCountU3Ek__BackingField_5() { return &___U3CmaxRetryCountU3Ek__BackingField_5; }
	inline void set_U3CmaxRetryCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CmaxRetryCountU3Ek__BackingField_5 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.InteropServices.GCHandle
struct GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};


// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.XR.ARKit.MemoryLayout
struct MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::size
	int32_t ___size_0;
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::stride
	int32_t ___stride_1;
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::alignment
	int32_t ___alignment_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_stride_1() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___stride_1)); }
	inline int32_t get_stride_1() const { return ___stride_1; }
	inline int32_t* get_address_of_stride_1() { return &___stride_1; }
	inline void set_stride_1(int32_t value)
	{
		___stride_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___alignment_2)); }
	inline int32_t get_alignment_2() const { return ___alignment_2; }
	inline int32_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int32_t value)
	{
		___alignment_2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary
struct MutableRuntimeReferenceImageLibrary_t887376CE46B48DEEC6E8655D429BADCA6E3C7EAA  : public RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B
{
public:

public:
};


// UnityEngine.XR.ARKit.OSVersion
struct OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<major>k__BackingField
	int32_t ___U3CmajorU3Ek__BackingField_0;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<minor>k__BackingField
	int32_t ___U3CminorU3Ek__BackingField_1;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<point>k__BackingField
	int32_t ___U3CpointU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmajorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CmajorU3Ek__BackingField_0)); }
	inline int32_t get_U3CmajorU3Ek__BackingField_0() const { return ___U3CmajorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmajorU3Ek__BackingField_0() { return &___U3CmajorU3Ek__BackingField_0; }
	inline void set_U3CmajorU3Ek__BackingField_0(int32_t value)
	{
		___U3CmajorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CminorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CminorU3Ek__BackingField_1)); }
	inline int32_t get_U3CminorU3Ek__BackingField_1() const { return ___U3CminorU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CminorU3Ek__BackingField_1() { return &___U3CminorU3Ek__BackingField_1; }
	inline void set_U3CminorU3Ek__BackingField_1(int32_t value)
	{
		___U3CminorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CpointU3Ek__BackingField_2)); }
	inline int32_t get_U3CpointU3Ek__BackingField_2() const { return ___U3CpointU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CpointU3Ek__BackingField_2() { return &___U3CpointU3Ek__BackingField_2; }
	inline void set_U3CpointU3Ek__BackingField_2(int32_t value)
	{
		___U3CpointU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C__padding[32];
	};

public:
};


// AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27 
{
public:
	// System.String AddToCartScript/ItemsData::Name
	String_t* ___Name_0;
	// System.Int32 AddToCartScript/ItemsData::Quantity
	int32_t ___Quantity_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_Quantity_1() { return static_cast<int32_t>(offsetof(ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27, ___Quantity_1)); }
	inline int32_t get_Quantity_1() const { return ___Quantity_1; }
	inline int32_t* get_address_of_Quantity_1() { return &___Quantity_1; }
	inline void set_Quantity_1(int32_t value)
	{
		___Quantity_1 = value;
	}
};

// Native definition for P/Invoke marshalling of AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Quantity_1;
};
// Native definition for COM marshalling of AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Quantity_1;
};

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider>
struct TrackingSubsystem_4_t4CF696722E0C05A2C0234E78E673F4F17EEC1C94  : public SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider>
struct TrackingSubsystem_4_t3D5C3B3749ABE82CC258AD552288C51FAE67DA1A  : public SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRHumanBody,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider>
struct TrackingSubsystem_4_tB0BB38AE7B56DA9BE6D8463DD64E4766AD686B86  : public SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider>
struct TrackingSubsystem_4_tF2C9DD677702042D71E5050214FE516389400277  : public SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRDepthSubsystem,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider>
struct TrackingSubsystem_4_t52B43FDBB6E641E351193D790222EA1C68B2984E  : public SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRRaycast,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider>
struct TrackingSubsystem_4_t87A57AE1E1117ED73BBD3B84DD595F36FA975077  : public SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider>
struct TrackingSubsystem_4_tCE5EA1B7325877FD88C7CF41F681F25B1FC1717A  : public SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRTrackedObject,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider>
struct TrackingSubsystem_4_t1E41FDFF37B1529EED554D89481040B067E300EB  : public SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB
{
public:

public:
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50
	__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0;

public:
	inline static int32_t get_offset_of_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields, ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0)); }
	inline __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  get_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() const { return ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0; }
	inline __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C * get_address_of_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() { return &___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0; }
	inline void set_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0(__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  value)
	{
		___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitErrorCode
struct ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535 
{
public:
	// System.Int64 UnityEngine.XR.ARKit.ARKitErrorCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageDatabase
struct ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71  : public MutableRuntimeReferenceImageLibrary_t887376CE46B48DEEC6E8655D429BADCA6E3C7EAA
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase::<nativePtr>k__BackingField
	intptr_t ___U3CnativePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnativePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71, ___U3CnativePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CnativePtrU3Ek__BackingField_0() const { return ___U3CnativePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CnativePtrU3Ek__BackingField_0() { return &___U3CnativePtrU3Ek__BackingField_0; }
	inline void set_U3CnativePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CnativePtrU3Ek__BackingField_0 = value;
	}
};

struct ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields
{
public:
	// UnityEngine.TextureFormat[] UnityEngine.XR.ARKit.ARKitImageDatabase::k_SupportedFormats
	TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* ___k_SupportedFormats_1;

public:
	inline static int32_t get_offset_of_k_SupportedFormats_1() { return static_cast<int32_t>(offsetof(ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields, ___k_SupportedFormats_1)); }
	inline TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* get_k_SupportedFormats_1() const { return ___k_SupportedFormats_1; }
	inline TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE** get_address_of_k_SupportedFormats_1() { return &___k_SupportedFormats_1; }
	inline void set_k_SupportedFormats_1(TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* value)
	{
		___k_SupportedFormats_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_SupportedFormats_1), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARMeshClassification
struct ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8 
{
public:
	// System.Byte UnityEngine.XR.ARKit.ARMeshClassification::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldAlignment
struct ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatus
struct ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMappingStatus
struct ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMappingStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.CoreLocationErrorCode
struct CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E 
{
public:
	// System.Int64 UnityEngine.XR.ARKit.CoreLocationErrorCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.XR.ARSubsystems.Feature
struct Feature_t079F5923A4893A9E07B968C27F44AC5FCAC87C83 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.Feature::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Feature_t079F5923A4893A9E07B968C27F44AC5FCAC87C83, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ManagedReferenceImage
struct ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1 
{
public:
	// System.Guid UnityEngine.XR.ARKit.ManagedReferenceImage::guid
	Guid_t  ___guid_0;
	// System.Guid UnityEngine.XR.ARKit.ManagedReferenceImage::textureGuid
	Guid_t  ___textureGuid_1;
	// UnityEngine.Vector2 UnityEngine.XR.ARKit.ManagedReferenceImage::size
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___size_2;
	// System.IntPtr UnityEngine.XR.ARKit.ManagedReferenceImage::name
	intptr_t ___name_3;
	// System.IntPtr UnityEngine.XR.ARKit.ManagedReferenceImage::texture
	intptr_t ___texture_4;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}

	inline static int32_t get_offset_of_textureGuid_1() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___textureGuid_1)); }
	inline Guid_t  get_textureGuid_1() const { return ___textureGuid_1; }
	inline Guid_t * get_address_of_textureGuid_1() { return &___textureGuid_1; }
	inline void set_textureGuid_1(Guid_t  value)
	{
		___textureGuid_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___size_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_size_2() const { return ___size_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___name_3)); }
	inline intptr_t get_name_3() const { return ___name_3; }
	inline intptr_t* get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(intptr_t value)
	{
		___name_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___texture_4)); }
	inline intptr_t get_texture_4() const { return ___texture_4; }
	inline intptr_t* get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(intptr_t value)
	{
		___texture_4 = value;
	}
};


// UnityEngine.XR.ARKit.NSData
struct NSData_t17EC9EAA93403854EE122C95559948C024C209D0 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSData::m_NativePtr
	intptr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(NSData_t17EC9EAA93403854EE122C95559948C024C209D0, ___m_NativePtr_0)); }
	inline intptr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline intptr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(intptr_t value)
	{
		___m_NativePtr_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSError
struct NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSError::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSErrorDomain
struct NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.NSErrorDomain::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.NSMutableData
struct NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSMutableData::m_NativePtr
	intptr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F, ___m_NativePtr_0)); }
	inline intptr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline intptr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(intptr_t value)
	{
		___m_NativePtr_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSString
struct NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSString::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARKit.NativeChanges
struct NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NativeChanges::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode
struct OcclusionPreferenceMode_tB85530C1AF1BD2CD83770B19A90C6D3F781EADC1 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OcclusionPreferenceMode_tB85530C1AF1BD2CD83770B19A90C6D3F781EADC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Pose
struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields, ___k_Identity_2)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___k_Identity_2 = value;
	}
};


// UnityEngine.XR.ARKit.SetReferenceLibraryResult
struct SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.SetReferenceLibraryResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem
struct XROcclusionSubsystem_t7546B929F9B5B6EB13B975FE4DB1F4099EE533B8  : public SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job
struct ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78 
{
public:
	// Unity.Collections.NativeSlice`1<System.UInt32> UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job::rgbaImage
	NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  ___rgbaImage_0;
	// Unity.Collections.NativeSlice`1<System.UInt32> UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job::argbImage
	NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  ___argbImage_1;

public:
	inline static int32_t get_offset_of_rgbaImage_0() { return static_cast<int32_t>(offsetof(ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78, ___rgbaImage_0)); }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  get_rgbaImage_0() const { return ___rgbaImage_0; }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A * get_address_of_rgbaImage_0() { return &___rgbaImage_0; }
	inline void set_rgbaImage_0(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  value)
	{
		___rgbaImage_0 = value;
	}

	inline static int32_t get_offset_of_argbImage_1() { return static_cast<int32_t>(offsetof(ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78, ___argbImage_1)); }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  get_argbImage_1() const { return ___argbImage_1; }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A * get_address_of_argbImage_1() { return &___argbImage_1; }
	inline void set_argbImage_1(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  value)
	{
		___argbImage_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/ReleaseDatabaseJob
struct ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/ReleaseDatabaseJob::database
	intptr_t ___database_0;

public:
	inline static int32_t get_offset_of_database_0() { return static_cast<int32_t>(offsetof(ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1, ___database_0)); }
	inline intptr_t get_database_0() const { return ___database_0; }
	inline intptr_t* get_address_of_database_0() { return &___database_0; }
	inline void set_database_0(intptr_t value)
	{
		___database_0 = value;
	}
};


// Lean.Touch.LeanFingerFilter/FilterType
struct FilterType_t6E9F3441B69748423F13473EEA080A192D41B44E 
{
public:
	// System.Int32 Lean.Touch.LeanFingerFilter/FilterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterType_t6E9F3441B69748423F13473EEA080A192D41B44E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider
struct Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6  : public SubsystemProvider_1_tBB539901FE99992CAA10A1EFDFA610E048498E98
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider
struct Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190  : public SubsystemProvider_1_tC3DB99A11F9F3210CE2ABA9FE09C127C5B13FF80
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider
struct Provider_t055C90C34B2BCE8D134DF44C12823E320519168C  : public SubsystemProvider_1_tBF37BFFB47314B7D87E24F4C4903C90930C0302C
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider
struct Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E  : public SubsystemProvider_1_t3086BC462E1384FBB8137E64FA6C513FC002E440
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider
struct Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E  : public SubsystemProvider_1_t71FE677A1A2F32123FE4C7868D5943892028AE12
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider
struct Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB  : public SubsystemProvider_1_t57D5C398A7A30AC3C3674CA126FAE612BC00F597
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider
struct Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E  : public SubsystemProvider_1_t3D6D4D936F16F3264F75D1BAB46A4A398F18F204
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider
struct Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12  : public SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider
struct Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227  : public SubsystemProvider_1_t7ACBE98539B067B19E2D5BCC2B852277F4A38875
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider
struct Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A  : public SubsystemProvider_1_tFA56F133FD9BCE90A1C4C7D15FFE2571963D8DE4
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/Availability
struct Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/Availability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Quaternion>
struct NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector3>
struct NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector4>
struct NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem
struct ARKitOcclusionSubsystem_tE15296E017A597561D0C3B399ED52577B138F343  : public XROcclusionSubsystem_t7546B929F9B5B6EB13B975FE4DB1F4099EE533B8
{
public:

public:
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.ConfigurationDescriptor
struct ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78 
{
public:
	// System.IntPtr UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Identifier
	intptr_t ___m_Identifier_0;
	// UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Capabilities
	uint64_t ___m_Capabilities_1;
	// System.Int32 UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Rank
	int32_t ___m_Rank_2;

public:
	inline static int32_t get_offset_of_m_Identifier_0() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Identifier_0)); }
	inline intptr_t get_m_Identifier_0() const { return ___m_Identifier_0; }
	inline intptr_t* get_address_of_m_Identifier_0() { return &___m_Identifier_0; }
	inline void set_m_Identifier_0(intptr_t value)
	{
		___m_Identifier_0 = value;
	}

	inline static int32_t get_offset_of_m_Capabilities_1() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Capabilities_1)); }
	inline uint64_t get_m_Capabilities_1() const { return ___m_Capabilities_1; }
	inline uint64_t* get_address_of_m_Capabilities_1() { return &___m_Capabilities_1; }
	inline void set_m_Capabilities_1(uint64_t value)
	{
		___m_Capabilities_1 = value;
	}

	inline static int32_t get_offset_of_m_Rank_2() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Rank_2)); }
	inline int32_t get_m_Rank_2() const { return ___m_Rank_2; }
	inline int32_t* get_address_of_m_Rank_2() { return &___m_Rank_2; }
	inline void set_m_Rank_2(int32_t value)
	{
		___m_Rank_2 = value;
	}
};


// Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB 
{
public:
	// Lean.Touch.LeanFingerFilter/FilterType Lean.Touch.LeanFingerFilter::Filter
	int32_t ___Filter_0;
	// System.Boolean Lean.Touch.LeanFingerFilter::IgnoreStartedOverGui
	bool ___IgnoreStartedOverGui_1;
	// System.Int32 Lean.Touch.LeanFingerFilter::RequiredFingerCount
	int32_t ___RequiredFingerCount_2;
	// System.Int32 Lean.Touch.LeanFingerFilter::RequiredMouseButtons
	int32_t ___RequiredMouseButtons_3;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanFingerFilter::RequiredSelectable
	LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 * ___RequiredSelectable_4;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::fingers
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___fingers_5;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::filteredFingers
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___filteredFingers_6;

public:
	inline static int32_t get_offset_of_Filter_0() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___Filter_0)); }
	inline int32_t get_Filter_0() const { return ___Filter_0; }
	inline int32_t* get_address_of_Filter_0() { return &___Filter_0; }
	inline void set_Filter_0(int32_t value)
	{
		___Filter_0 = value;
	}

	inline static int32_t get_offset_of_IgnoreStartedOverGui_1() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___IgnoreStartedOverGui_1)); }
	inline bool get_IgnoreStartedOverGui_1() const { return ___IgnoreStartedOverGui_1; }
	inline bool* get_address_of_IgnoreStartedOverGui_1() { return &___IgnoreStartedOverGui_1; }
	inline void set_IgnoreStartedOverGui_1(bool value)
	{
		___IgnoreStartedOverGui_1 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_2() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___RequiredFingerCount_2)); }
	inline int32_t get_RequiredFingerCount_2() const { return ___RequiredFingerCount_2; }
	inline int32_t* get_address_of_RequiredFingerCount_2() { return &___RequiredFingerCount_2; }
	inline void set_RequiredFingerCount_2(int32_t value)
	{
		___RequiredFingerCount_2 = value;
	}

	inline static int32_t get_offset_of_RequiredMouseButtons_3() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___RequiredMouseButtons_3)); }
	inline int32_t get_RequiredMouseButtons_3() const { return ___RequiredMouseButtons_3; }
	inline int32_t* get_address_of_RequiredMouseButtons_3() { return &___RequiredMouseButtons_3; }
	inline void set_RequiredMouseButtons_3(int32_t value)
	{
		___RequiredMouseButtons_3 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_4() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___RequiredSelectable_4)); }
	inline LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 * get_RequiredSelectable_4() const { return ___RequiredSelectable_4; }
	inline LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 ** get_address_of_RequiredSelectable_4() { return &___RequiredSelectable_4; }
	inline void set_RequiredSelectable_4(LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 * value)
	{
		___RequiredSelectable_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RequiredSelectable_4), (void*)value);
	}

	inline static int32_t get_offset_of_fingers_5() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___fingers_5)); }
	inline List_1_tC941CBF342F522B4B7EED02680F598139724300E * get_fingers_5() const { return ___fingers_5; }
	inline List_1_tC941CBF342F522B4B7EED02680F598139724300E ** get_address_of_fingers_5() { return &___fingers_5; }
	inline void set_fingers_5(List_1_tC941CBF342F522B4B7EED02680F598139724300E * value)
	{
		___fingers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingers_5), (void*)value);
	}

	inline static int32_t get_offset_of_filteredFingers_6() { return static_cast<int32_t>(offsetof(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB, ___filteredFingers_6)); }
	inline List_1_tC941CBF342F522B4B7EED02680F598139724300E * get_filteredFingers_6() const { return ___filteredFingers_6; }
	inline List_1_tC941CBF342F522B4B7EED02680F598139724300E ** get_address_of_filteredFingers_6() { return &___filteredFingers_6; }
	inline void set_filteredFingers_6(List_1_tC941CBF342F522B4B7EED02680F598139724300E * value)
	{
		___filteredFingers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filteredFingers_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB_marshaled_pinvoke
{
	int32_t ___Filter_0;
	int32_t ___IgnoreStartedOverGui_1;
	int32_t ___RequiredFingerCount_2;
	int32_t ___RequiredMouseButtons_3;
	LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 * ___RequiredSelectable_4;
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___fingers_5;
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___filteredFingers_6;
};
// Native definition for COM marshalling of Lean.Touch.LeanFingerFilter
struct LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB_marshaled_com
{
	int32_t ___Filter_0;
	int32_t ___IgnoreStartedOverGui_1;
	int32_t ___RequiredFingerCount_2;
	int32_t ___RequiredMouseButtons_3;
	LeanSelectable_t5410D1E2638CCBA781EDCB3980E6BC089BDB8C89 * ___RequiredSelectable_4;
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___fingers_5;
	List_1_tC941CBF342F522B4B7EED02680F598139724300E * ___filteredFingers_6;
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.XR.ARKit.SerializedARCollaborationData
struct SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3 
{
public:
	// UnityEngine.XR.ARKit.NSData UnityEngine.XR.ARKit.SerializedARCollaborationData::m_NSData
	NSData_t17EC9EAA93403854EE122C95559948C024C209D0  ___m_NSData_0;

public:
	inline static int32_t get_offset_of_m_NSData_0() { return static_cast<int32_t>(offsetof(SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3, ___m_NSData_0)); }
	inline NSData_t17EC9EAA93403854EE122C95559948C024C209D0  get_m_NSData_0() const { return ___m_NSData_0; }
	inline NSData_t17EC9EAA93403854EE122C95559948C024C209D0 * get_address_of_m_NSData_0() { return &___m_NSData_0; }
	inline void set_m_NSData_0(NSData_t17EC9EAA93403854EE122C95559948C024C209D0  value)
	{
		___m_NSData_0 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRDepthSubsystem
struct XRDepthSubsystem_t808E21F0192095B08FA03AC535314FB5EF3B7E28  : public TrackingSubsystem_4_t52B43FDBB6E641E351193D790222EA1C68B2984E
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem
struct XREnvironmentProbeSubsystem_t0C60258F565400E7C5AF0E0B7FA933F2BCF83CB6  : public TrackingSubsystem_4_t3D5C3B3749ABE82CC258AD552288C51FAE67DA1A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem
struct XRHumanBodySubsystem_t71FBF94503DCE781657FA4F362464EA389CD9F2B  : public TrackingSubsystem_4_tB0BB38AE7B56DA9BE6D8463DD64E4766AD686B86
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem
struct XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE  : public TrackingSubsystem_4_tCE5EA1B7325877FD88C7CF41F681F25B1FC1717A
{
public:
	// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::m_ImageLibrary
	RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * ___m_ImageLibrary_4;

public:
	inline static int32_t get_offset_of_m_ImageLibrary_4() { return static_cast<int32_t>(offsetof(XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE, ___m_ImageLibrary_4)); }
	inline RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * get_m_ImageLibrary_4() const { return ___m_ImageLibrary_4; }
	inline RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B ** get_address_of_m_ImageLibrary_4() { return &___m_ImageLibrary_4; }
	inline void set_m_ImageLibrary_4(RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * value)
	{
		___m_ImageLibrary_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ImageLibrary_4), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem
struct XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881  : public TrackingSubsystem_4_t1E41FDFF37B1529EED554D89481040B067E300EB
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::m_Library
	XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * ___m_Library_4;

public:
	inline static int32_t get_offset_of_m_Library_4() { return static_cast<int32_t>(offsetof(XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881, ___m_Library_4)); }
	inline XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * get_m_Library_4() const { return ___m_Library_4; }
	inline XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C ** get_address_of_m_Library_4() { return &___m_Library_4; }
	inline void set_m_Library_4(XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * value)
	{
		___m_Library_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Library_4), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem
struct XRParticipantSubsystem_t7F710E46FC5A17967E7CAE126DE9443C752C36FC  : public TrackingSubsystem_4_tF2C9DD677702042D71E5050214FE516389400277
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem
struct XRPlaneSubsystem_t7C76F9D2C993B0DC38F0A7CDCE745EA7C12417EE  : public TrackingSubsystem_4_t4CF696722E0C05A2C0234E78E673F4F17EEC1C94
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem
struct XRRaycastSubsystem_t62FDAC9AA1BD44C4557AEE3FEF3D2FA24C71B6B8  : public TrackingSubsystem_4_t87A57AE1E1117ED73BBD3B84DD595F36FA975077
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/ARKitProvider
struct ARKitProvider_tBED347A85C19012D5B5F124BC8C0FB4876FBEACC  : public Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem/ARKitProvider
struct ARKitProvider_t5CC80E0D6F9A6EFFA70F54E8CDBEDB36D0CC4ED0  : public Provider_t055C90C34B2BCE8D134DF44C12823E320519168C
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob
struct AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5 
{
public:
	// Unity.Collections.NativeSlice`1<System.Byte> UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::image
	NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  ___image_0;
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::database
	intptr_t ___database_1;
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::validator
	intptr_t ___validator_2;
	// System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::width
	int32_t ___width_3;
	// System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::height
	int32_t ___height_4;
	// System.Single UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::physicalWidth
	float ___physicalWidth_5;
	// UnityEngine.TextureFormat UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::format
	int32_t ___format_6;
	// UnityEngine.XR.ARKit.ManagedReferenceImage UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::managedReferenceImage
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  ___managedReferenceImage_7;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___image_0)); }
	inline NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  get_image_0() const { return ___image_0; }
	inline NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B * get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  value)
	{
		___image_0 = value;
	}

	inline static int32_t get_offset_of_database_1() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___database_1)); }
	inline intptr_t get_database_1() const { return ___database_1; }
	inline intptr_t* get_address_of_database_1() { return &___database_1; }
	inline void set_database_1(intptr_t value)
	{
		___database_1 = value;
	}

	inline static int32_t get_offset_of_validator_2() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___validator_2)); }
	inline intptr_t get_validator_2() const { return ___validator_2; }
	inline intptr_t* get_address_of_validator_2() { return &___validator_2; }
	inline void set_validator_2(intptr_t value)
	{
		___validator_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_physicalWidth_5() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___physicalWidth_5)); }
	inline float get_physicalWidth_5() const { return ___physicalWidth_5; }
	inline float* get_address_of_physicalWidth_5() { return &___physicalWidth_5; }
	inline void set_physicalWidth_5(float value)
	{
		___physicalWidth_5 = value;
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___format_6)); }
	inline int32_t get_format_6() const { return ___format_6; }
	inline int32_t* get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(int32_t value)
	{
		___format_6 = value;
	}

	inline static int32_t get_offset_of_managedReferenceImage_7() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___managedReferenceImage_7)); }
	inline ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  get_managedReferenceImage_7() const { return ___managedReferenceImage_7; }
	inline ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1 * get_address_of_managedReferenceImage_7() { return &___managedReferenceImage_7; }
	inline void set_managedReferenceImage_7(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  value)
	{
		___managedReferenceImage_7 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/ARKitProvider
struct ARKitProvider_t6838013201F7369104C65DD65159D910B116E8AC  : public Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider
struct ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611  : public Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB
{
public:
	// UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_OcclusionPreferenceMode
	int32_t ___m_OcclusionPreferenceMode_14;

public:
	inline static int32_t get_offset_of_m_OcclusionPreferenceMode_14() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611, ___m_OcclusionPreferenceMode_14)); }
	inline int32_t get_m_OcclusionPreferenceMode_14() const { return ___m_OcclusionPreferenceMode_14; }
	inline int32_t* get_address_of_m_OcclusionPreferenceMode_14() { return &___m_OcclusionPreferenceMode_14; }
	inline void set_m_OcclusionPreferenceMode_14(int32_t value)
	{
		___m_OcclusionPreferenceMode_14 = value;
	}
};

struct ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureHumanStencilPropertyId
	int32_t ___k_TextureHumanStencilPropertyId_7;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureHumanDepthPropertyId
	int32_t ___k_TextureHumanDepthPropertyId_8;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureEnvironmentDepthPropertyId
	int32_t ___k_TextureEnvironmentDepthPropertyId_9;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureEnvironmentDepthConfidencePropertyId
	int32_t ___k_TextureEnvironmentDepthConfidencePropertyId_10;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_HumanEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_HumanEnabledMaterialKeywords_11;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_EnvironmentDepthEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_EnvironmentDepthEnabledMaterialKeywords_12;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_AllDisabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_AllDisabledMaterialKeywords_13;

public:
	inline static int32_t get_offset_of_k_TextureHumanStencilPropertyId_7() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureHumanStencilPropertyId_7)); }
	inline int32_t get_k_TextureHumanStencilPropertyId_7() const { return ___k_TextureHumanStencilPropertyId_7; }
	inline int32_t* get_address_of_k_TextureHumanStencilPropertyId_7() { return &___k_TextureHumanStencilPropertyId_7; }
	inline void set_k_TextureHumanStencilPropertyId_7(int32_t value)
	{
		___k_TextureHumanStencilPropertyId_7 = value;
	}

	inline static int32_t get_offset_of_k_TextureHumanDepthPropertyId_8() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureHumanDepthPropertyId_8)); }
	inline int32_t get_k_TextureHumanDepthPropertyId_8() const { return ___k_TextureHumanDepthPropertyId_8; }
	inline int32_t* get_address_of_k_TextureHumanDepthPropertyId_8() { return &___k_TextureHumanDepthPropertyId_8; }
	inline void set_k_TextureHumanDepthPropertyId_8(int32_t value)
	{
		___k_TextureHumanDepthPropertyId_8 = value;
	}

	inline static int32_t get_offset_of_k_TextureEnvironmentDepthPropertyId_9() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureEnvironmentDepthPropertyId_9)); }
	inline int32_t get_k_TextureEnvironmentDepthPropertyId_9() const { return ___k_TextureEnvironmentDepthPropertyId_9; }
	inline int32_t* get_address_of_k_TextureEnvironmentDepthPropertyId_9() { return &___k_TextureEnvironmentDepthPropertyId_9; }
	inline void set_k_TextureEnvironmentDepthPropertyId_9(int32_t value)
	{
		___k_TextureEnvironmentDepthPropertyId_9 = value;
	}

	inline static int32_t get_offset_of_k_TextureEnvironmentDepthConfidencePropertyId_10() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureEnvironmentDepthConfidencePropertyId_10)); }
	inline int32_t get_k_TextureEnvironmentDepthConfidencePropertyId_10() const { return ___k_TextureEnvironmentDepthConfidencePropertyId_10; }
	inline int32_t* get_address_of_k_TextureEnvironmentDepthConfidencePropertyId_10() { return &___k_TextureEnvironmentDepthConfidencePropertyId_10; }
	inline void set_k_TextureEnvironmentDepthConfidencePropertyId_10(int32_t value)
	{
		___k_TextureEnvironmentDepthConfidencePropertyId_10 = value;
	}

	inline static int32_t get_offset_of_m_HumanEnabledMaterialKeywords_11() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_HumanEnabledMaterialKeywords_11)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_HumanEnabledMaterialKeywords_11() const { return ___m_HumanEnabledMaterialKeywords_11; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_HumanEnabledMaterialKeywords_11() { return &___m_HumanEnabledMaterialKeywords_11; }
	inline void set_m_HumanEnabledMaterialKeywords_11(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_HumanEnabledMaterialKeywords_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HumanEnabledMaterialKeywords_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_EnvironmentDepthEnabledMaterialKeywords_12() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_EnvironmentDepthEnabledMaterialKeywords_12)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_EnvironmentDepthEnabledMaterialKeywords_12() const { return ___m_EnvironmentDepthEnabledMaterialKeywords_12; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_EnvironmentDepthEnabledMaterialKeywords_12() { return &___m_EnvironmentDepthEnabledMaterialKeywords_12; }
	inline void set_m_EnvironmentDepthEnabledMaterialKeywords_12(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_EnvironmentDepthEnabledMaterialKeywords_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EnvironmentDepthEnabledMaterialKeywords_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_AllDisabledMaterialKeywords_13() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_AllDisabledMaterialKeywords_13)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_AllDisabledMaterialKeywords_13() const { return ___m_AllDisabledMaterialKeywords_13; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_AllDisabledMaterialKeywords_13() { return &___m_AllDisabledMaterialKeywords_13; }
	inline void set_m_AllDisabledMaterialKeywords_13(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_AllDisabledMaterialKeywords_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AllDisabledMaterialKeywords_13), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitParticipantSubsystem/ARKitProvider
struct ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C  : public Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitParticipantSubsystem/ARKitProvider::m_Ptr
	intptr_t ___m_Ptr_1;

public:
	inline static int32_t get_offset_of_m_Ptr_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C, ___m_Ptr_1)); }
	inline intptr_t get_m_Ptr_1() const { return ___m_Ptr_1; }
	inline intptr_t* get_address_of_m_Ptr_1() { return &___m_Ptr_1; }
	inline void set_m_Ptr_1(intptr_t value)
	{
		___m_Ptr_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem/ARKitProvider
struct ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88  : public Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitRaycastSubsystem/ARKitProvider::m_Self
	intptr_t ___m_Self_1;

public:
	inline static int32_t get_offset_of_m_Self_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88, ___m_Self_1)); }
	inline intptr_t get_m_Self_1() const { return ___m_Self_1; }
	inline intptr_t* get_address_of_m_Self_1() { return &___m_Self_1; }
	inline void set_m_Self_1(intptr_t value)
	{
		___m_Self_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider
struct ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF  : public Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider::m_Self
	intptr_t ___m_Self_1;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider::m_SubsystemHandle
	GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  ___m_SubsystemHandle_2;

public:
	inline static int32_t get_offset_of_m_Self_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF, ___m_Self_1)); }
	inline intptr_t get_m_Self_1() const { return ___m_Self_1; }
	inline intptr_t* get_address_of_m_Self_1() { return &___m_Self_1; }
	inline void set_m_Self_1(intptr_t value)
	{
		___m_Self_1 = value;
	}

	inline static int32_t get_offset_of_m_SubsystemHandle_2() { return static_cast<int32_t>(offsetof(ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF, ___m_SubsystemHandle_2)); }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  get_m_SubsystemHandle_2() const { return ___m_SubsystemHandle_2; }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 * get_address_of_m_SubsystemHandle_2() { return &___m_SubsystemHandle_2; }
	inline void set_m_SubsystemHandle_2(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  value)
	{
		___m_SubsystemHandle_2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/ARKitProvider
struct ARKitProvider_tC6BBA25F076F6D64FA8E6336E6860D7DBD41A7A1  : public Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRObjectTrackingSubsystem/ARKitProvider
struct ARKitProvider_tDDBF5C474A3543A0F5FA7FB07A058B223F6DA8EC  : public Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider
struct ARKitProvider_tF9F620C89E671C028D10768C5300D3539661A2C1  : public Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem
struct ARKitEnvironmentProbeSubsystem_t628136AE15CA16280BA838B37E2B7CDF0473E9D2  : public XREnvironmentProbeSubsystem_t0C60258F565400E7C5AF0E0B7FA933F2BCF83CB6
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem
struct ARKitHumanBodySubsystem_t8BA2C528FDE109FBDDBD0ED2FF8BC1FD081E92B7  : public XRHumanBodySubsystem_t71FBF94503DCE781657FA4F362464EA389CD9F2B
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem
struct ARKitImageTrackingSubsystem_t0F98C12C3E36B9F3819FC43494A883B600401237  : public XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitLoaderSettings
struct ARKitLoaderSettings_t97CD3CA8B21183EC5C61B94BB7299FD217479B24  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitParticipantSubsystem
struct ARKitParticipantSubsystem_tFBFB76108F5069693F4F40F8DAA4B6A907A709C3  : public XRParticipantSubsystem_t7F710E46FC5A17967E7CAE126DE9443C752C36FC
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem
struct ARKitRaycastSubsystem_t8DF7A7F21C4610FBBEF6A240FD1790CD47BE9900  : public XRRaycastSubsystem_t62FDAC9AA1BD44C4557AEE3FEF3D2FA24C71B6B8
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem
struct ARKitXRDepthSubsystem_t50D82DC2115EED29E5F0EEBA8A0F4FA7876BD2D5  : public XRDepthSubsystem_t808E21F0192095B08FA03AC535314FB5EF3B7E28
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRObjectTrackingSubsystem
struct ARKitXRObjectTrackingSubsystem_tB611337EA1668BBCB3A7123F14117CB536707A6D  : public XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem
struct ARKitXRPlaneSubsystem_t03042D64756D81EC6B4657CD1323D7404EC7530B  : public XRPlaneSubsystem_t7C76F9D2C993B0DC38F0A7CDCE745EA7C12417EE
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.Configuration
struct Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0 
{
public:
	// UnityEngine.XR.ARSubsystems.ConfigurationDescriptor UnityEngine.XR.ARSubsystems.Configuration::<descriptor>k__BackingField
	ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  ___U3CdescriptorU3Ek__BackingField_0;
	// UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.Configuration::<features>k__BackingField
	uint64_t ___U3CfeaturesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0, ___U3CdescriptorU3Ek__BackingField_0)); }
	inline ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  get_U3CdescriptorU3Ek__BackingField_0() const { return ___U3CdescriptorU3Ek__BackingField_0; }
	inline ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78 * get_address_of_U3CdescriptorU3Ek__BackingField_0() { return &___U3CdescriptorU3Ek__BackingField_0; }
	inline void set_U3CdescriptorU3Ek__BackingField_0(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  value)
	{
		___U3CdescriptorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CfeaturesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0, ___U3CfeaturesU3Ek__BackingField_1)); }
	inline uint64_t get_U3CfeaturesU3Ek__BackingField_1() const { return ___U3CfeaturesU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CfeaturesU3Ek__BackingField_1() { return &___U3CfeaturesU3Ek__BackingField_1; }
	inline void set_U3CfeaturesU3Ek__BackingField_1(uint64_t value)
	{
		___U3CfeaturesU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.XR.Management.XRLoader
struct XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRReferenceObjectEntry
struct XRReferenceObjectEntry_t873762C96E954D47B49C3B36CABD423408DC72D2  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Quaternion> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob::positionsIn
	NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob::positionsOut
	NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468, ___positionsIn_0)); }
	inline NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468, ___positionsOut_1)); }
	inline NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  value)
	{
		___positionsOut_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/FlipBoundaryWindingJob::positions
	NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA, ___positions_0)); }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  get_positions_0() const { return ___positions_0; }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  value)
	{
		___positions_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector4> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob::positionsIn
	NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob::positionsOut
	NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE, ___positionsIn_0)); }
	inline NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE, ___positionsOut_1)); }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  value)
	{
		___positionsOut_1 = value;
	}
};


// System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration>
struct Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494 
{
public:
	// T System.Nullable`1::value
	Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494, ___value_0)); }
	inline Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  get_value_0() const { return ___value_0; }
	inline Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitReferenceObjectEntry
struct ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47  : public XRReferenceObjectEntry_t873762C96E954D47B49C3B36CABD423408DC72D2
{
public:
	// UnityEngine.Pose UnityEngine.XR.ARKit.ARKitReferenceObjectEntry::m_ReferenceOrigin
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___m_ReferenceOrigin_4;

public:
	inline static int32_t get_offset_of_m_ReferenceOrigin_4() { return static_cast<int32_t>(offsetof(ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47, ___m_ReferenceOrigin_4)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_m_ReferenceOrigin_4() const { return ___m_ReferenceOrigin_4; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_m_ReferenceOrigin_4() { return &___m_ReferenceOrigin_4; }
	inline void set_m_ReferenceOrigin_4(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___m_ReferenceOrigin_4 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.XR.Management.XRLoaderHelper
struct XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013  : public XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem> UnityEngine.XR.Management.XRLoaderHelper::m_SubsystemInstanceMap
	Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * ___m_SubsystemInstanceMap_4;

public:
	inline static int32_t get_offset_of_m_SubsystemInstanceMap_4() { return static_cast<int32_t>(offsetof(XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013, ___m_SubsystemInstanceMap_4)); }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * get_m_SubsystemInstanceMap_4() const { return ___m_SubsystemInstanceMap_4; }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B ** get_address_of_m_SubsystemInstanceMap_4() { return &___m_SubsystemInstanceMap_4; }
	inline void set_m_SubsystemInstanceMap_4(Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * value)
	{
		___m_SubsystemInstanceMap_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubsystemInstanceMap_4), (void*)value);
	}
};


// API
struct API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject API::ObjSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSpawner_4;

public:
	inline static int32_t get_offset_of_ObjSpawner_4() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___ObjSpawner_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSpawner_4() const { return ___ObjSpawner_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSpawner_4() { return &___ObjSpawner_4; }
	inline void set_ObjSpawner_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSpawner_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSpawner_4), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitLoader
struct ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27  : public XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013
{
public:

public:
};

struct ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_SessionSubsystemDescriptors
	List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * ___s_SessionSubsystemDescriptors_5;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_CameraSubsystemDescriptors
	List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * ___s_CameraSubsystemDescriptors_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_DepthSubsystemDescriptors
	List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * ___s_DepthSubsystemDescriptors_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_PlaneSubsystemDescriptors
	List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * ___s_PlaneSubsystemDescriptors_8;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_AnchorSubsystemDescriptors
	List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * ___s_AnchorSubsystemDescriptors_9;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_RaycastSubsystemDescriptors
	List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * ___s_RaycastSubsystemDescriptors_10;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_HumanBodySubsystemDescriptors
	List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * ___s_HumanBodySubsystemDescriptors_11;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_EnvironmentProbeSubsystemDescriptors
	List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * ___s_EnvironmentProbeSubsystemDescriptors_12;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_InputSubsystemDescriptors
	List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * ___s_InputSubsystemDescriptors_13;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ImageTrackingSubsystemDescriptors
	List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * ___s_ImageTrackingSubsystemDescriptors_14;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ObjectTrackingSubsystemDescriptors
	List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * ___s_ObjectTrackingSubsystemDescriptors_15;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_FaceSubsystemDescriptors
	List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * ___s_FaceSubsystemDescriptors_16;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_OcclusionSubsystemDescriptors
	List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * ___s_OcclusionSubsystemDescriptors_17;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ParticipantSubsystemDescriptors
	List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * ___s_ParticipantSubsystemDescriptors_18;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_MeshSubsystemDescriptors
	List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * ___s_MeshSubsystemDescriptors_19;

public:
	inline static int32_t get_offset_of_s_SessionSubsystemDescriptors_5() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_SessionSubsystemDescriptors_5)); }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * get_s_SessionSubsystemDescriptors_5() const { return ___s_SessionSubsystemDescriptors_5; }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 ** get_address_of_s_SessionSubsystemDescriptors_5() { return &___s_SessionSubsystemDescriptors_5; }
	inline void set_s_SessionSubsystemDescriptors_5(List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * value)
	{
		___s_SessionSubsystemDescriptors_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SessionSubsystemDescriptors_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_CameraSubsystemDescriptors_6() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_CameraSubsystemDescriptors_6)); }
	inline List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * get_s_CameraSubsystemDescriptors_6() const { return ___s_CameraSubsystemDescriptors_6; }
	inline List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC ** get_address_of_s_CameraSubsystemDescriptors_6() { return &___s_CameraSubsystemDescriptors_6; }
	inline void set_s_CameraSubsystemDescriptors_6(List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * value)
	{
		___s_CameraSubsystemDescriptors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CameraSubsystemDescriptors_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_DepthSubsystemDescriptors_7() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_DepthSubsystemDescriptors_7)); }
	inline List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * get_s_DepthSubsystemDescriptors_7() const { return ___s_DepthSubsystemDescriptors_7; }
	inline List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 ** get_address_of_s_DepthSubsystemDescriptors_7() { return &___s_DepthSubsystemDescriptors_7; }
	inline void set_s_DepthSubsystemDescriptors_7(List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * value)
	{
		___s_DepthSubsystemDescriptors_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DepthSubsystemDescriptors_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_PlaneSubsystemDescriptors_8() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_PlaneSubsystemDescriptors_8)); }
	inline List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * get_s_PlaneSubsystemDescriptors_8() const { return ___s_PlaneSubsystemDescriptors_8; }
	inline List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F ** get_address_of_s_PlaneSubsystemDescriptors_8() { return &___s_PlaneSubsystemDescriptors_8; }
	inline void set_s_PlaneSubsystemDescriptors_8(List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * value)
	{
		___s_PlaneSubsystemDescriptors_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PlaneSubsystemDescriptors_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_AnchorSubsystemDescriptors_9() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_AnchorSubsystemDescriptors_9)); }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * get_s_AnchorSubsystemDescriptors_9() const { return ___s_AnchorSubsystemDescriptors_9; }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 ** get_address_of_s_AnchorSubsystemDescriptors_9() { return &___s_AnchorSubsystemDescriptors_9; }
	inline void set_s_AnchorSubsystemDescriptors_9(List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * value)
	{
		___s_AnchorSubsystemDescriptors_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AnchorSubsystemDescriptors_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastSubsystemDescriptors_10() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_RaycastSubsystemDescriptors_10)); }
	inline List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * get_s_RaycastSubsystemDescriptors_10() const { return ___s_RaycastSubsystemDescriptors_10; }
	inline List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 ** get_address_of_s_RaycastSubsystemDescriptors_10() { return &___s_RaycastSubsystemDescriptors_10; }
	inline void set_s_RaycastSubsystemDescriptors_10(List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * value)
	{
		___s_RaycastSubsystemDescriptors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastSubsystemDescriptors_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_HumanBodySubsystemDescriptors_11() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_HumanBodySubsystemDescriptors_11)); }
	inline List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * get_s_HumanBodySubsystemDescriptors_11() const { return ___s_HumanBodySubsystemDescriptors_11; }
	inline List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 ** get_address_of_s_HumanBodySubsystemDescriptors_11() { return &___s_HumanBodySubsystemDescriptors_11; }
	inline void set_s_HumanBodySubsystemDescriptors_11(List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * value)
	{
		___s_HumanBodySubsystemDescriptors_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_HumanBodySubsystemDescriptors_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_EnvironmentProbeSubsystemDescriptors_12() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_EnvironmentProbeSubsystemDescriptors_12)); }
	inline List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * get_s_EnvironmentProbeSubsystemDescriptors_12() const { return ___s_EnvironmentProbeSubsystemDescriptors_12; }
	inline List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F ** get_address_of_s_EnvironmentProbeSubsystemDescriptors_12() { return &___s_EnvironmentProbeSubsystemDescriptors_12; }
	inline void set_s_EnvironmentProbeSubsystemDescriptors_12(List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * value)
	{
		___s_EnvironmentProbeSubsystemDescriptors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EnvironmentProbeSubsystemDescriptors_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_InputSubsystemDescriptors_13() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_InputSubsystemDescriptors_13)); }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * get_s_InputSubsystemDescriptors_13() const { return ___s_InputSubsystemDescriptors_13; }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 ** get_address_of_s_InputSubsystemDescriptors_13() { return &___s_InputSubsystemDescriptors_13; }
	inline void set_s_InputSubsystemDescriptors_13(List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * value)
	{
		___s_InputSubsystemDescriptors_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InputSubsystemDescriptors_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_ImageTrackingSubsystemDescriptors_14() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ImageTrackingSubsystemDescriptors_14)); }
	inline List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * get_s_ImageTrackingSubsystemDescriptors_14() const { return ___s_ImageTrackingSubsystemDescriptors_14; }
	inline List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 ** get_address_of_s_ImageTrackingSubsystemDescriptors_14() { return &___s_ImageTrackingSubsystemDescriptors_14; }
	inline void set_s_ImageTrackingSubsystemDescriptors_14(List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * value)
	{
		___s_ImageTrackingSubsystemDescriptors_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ImageTrackingSubsystemDescriptors_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_ObjectTrackingSubsystemDescriptors_15() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ObjectTrackingSubsystemDescriptors_15)); }
	inline List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * get_s_ObjectTrackingSubsystemDescriptors_15() const { return ___s_ObjectTrackingSubsystemDescriptors_15; }
	inline List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D ** get_address_of_s_ObjectTrackingSubsystemDescriptors_15() { return &___s_ObjectTrackingSubsystemDescriptors_15; }
	inline void set_s_ObjectTrackingSubsystemDescriptors_15(List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * value)
	{
		___s_ObjectTrackingSubsystemDescriptors_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ObjectTrackingSubsystemDescriptors_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_FaceSubsystemDescriptors_16() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_FaceSubsystemDescriptors_16)); }
	inline List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * get_s_FaceSubsystemDescriptors_16() const { return ___s_FaceSubsystemDescriptors_16; }
	inline List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E ** get_address_of_s_FaceSubsystemDescriptors_16() { return &___s_FaceSubsystemDescriptors_16; }
	inline void set_s_FaceSubsystemDescriptors_16(List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * value)
	{
		___s_FaceSubsystemDescriptors_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_FaceSubsystemDescriptors_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_OcclusionSubsystemDescriptors_17() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_OcclusionSubsystemDescriptors_17)); }
	inline List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * get_s_OcclusionSubsystemDescriptors_17() const { return ___s_OcclusionSubsystemDescriptors_17; }
	inline List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC ** get_address_of_s_OcclusionSubsystemDescriptors_17() { return &___s_OcclusionSubsystemDescriptors_17; }
	inline void set_s_OcclusionSubsystemDescriptors_17(List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * value)
	{
		___s_OcclusionSubsystemDescriptors_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OcclusionSubsystemDescriptors_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_ParticipantSubsystemDescriptors_18() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ParticipantSubsystemDescriptors_18)); }
	inline List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * get_s_ParticipantSubsystemDescriptors_18() const { return ___s_ParticipantSubsystemDescriptors_18; }
	inline List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF ** get_address_of_s_ParticipantSubsystemDescriptors_18() { return &___s_ParticipantSubsystemDescriptors_18; }
	inline void set_s_ParticipantSubsystemDescriptors_18(List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * value)
	{
		___s_ParticipantSubsystemDescriptors_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParticipantSubsystemDescriptors_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_MeshSubsystemDescriptors_19() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_MeshSubsystemDescriptors_19)); }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * get_s_MeshSubsystemDescriptors_19() const { return ___s_MeshSubsystemDescriptors_19; }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 ** get_address_of_s_MeshSubsystemDescriptors_19() { return &___s_MeshSubsystemDescriptors_19; }
	inline void set_s_MeshSubsystemDescriptors_19(List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * value)
	{
		___s_MeshSubsystemDescriptors_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MeshSubsystemDescriptors_19), (void*)value);
	}
};


// Activate
struct Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Activate::thissdas
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thissdas_4;

public:
	inline static int32_t get_offset_of_thissdas_4() { return static_cast<int32_t>(offsetof(Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88, ___thissdas_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thissdas_4() const { return ___thissdas_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thissdas_4() { return &___thissdas_4; }
	inline void set_thissdas_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thissdas_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thissdas_4), (void*)value);
	}
};


// AddToCartScript
struct AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text AddToCartScript::CartItemsCount
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___CartItemsCount_4;
	// System.Int32 AddToCartScript::CartItemsCounter
	int32_t ___CartItemsCounter_5;
	// System.Collections.Generic.List`1<AddToCartScript/ItemsData> AddToCartScript::MyItems
	List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * ___MyItems_6;

public:
	inline static int32_t get_offset_of_CartItemsCount_4() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___CartItemsCount_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_CartItemsCount_4() const { return ___CartItemsCount_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_CartItemsCount_4() { return &___CartItemsCount_4; }
	inline void set_CartItemsCount_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___CartItemsCount_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CartItemsCount_4), (void*)value);
	}

	inline static int32_t get_offset_of_CartItemsCounter_5() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___CartItemsCounter_5)); }
	inline int32_t get_CartItemsCounter_5() const { return ___CartItemsCounter_5; }
	inline int32_t* get_address_of_CartItemsCounter_5() { return &___CartItemsCounter_5; }
	inline void set_CartItemsCounter_5(int32_t value)
	{
		___CartItemsCounter_5 = value;
	}

	inline static int32_t get_offset_of_MyItems_6() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___MyItems_6)); }
	inline List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * get_MyItems_6() const { return ___MyItems_6; }
	inline List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F ** get_address_of_MyItems_6() { return &___MyItems_6; }
	inline void set_MyItems_6(List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * value)
	{
		___MyItems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MyItems_6), (void*)value);
	}
};


// Anim
struct Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// AnimateModel
struct AnimateModel_t5FA7BFBC700198F4FD54C33909FB2F40BDF02877  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ArCanvas
struct ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AndroidJavaObject ArCanvas::communicationBridge
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * ___communicationBridge_4;
	// UnityEngine.GameObject ArCanvas::CanvasHolder
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CanvasHolder_5;
	// UnityEngine.GameObject ArCanvas::CartPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CartPanel_6;
	// UnityEngine.GameObject ArCanvas::InfoButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___InfoButton_7;
	// UnityEngine.GameObject ArCanvas::CloseButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CloseButton_8;
	// UnityEngine.GameObject ArCanvas::ObjSelectionButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSelectionButton_9;
	// UnityEngine.GameObject ArCanvas::SelectionEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___SelectionEffect_10;

public:
	inline static int32_t get_offset_of_communicationBridge_4() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___communicationBridge_4)); }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * get_communicationBridge_4() const { return ___communicationBridge_4; }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E ** get_address_of_communicationBridge_4() { return &___communicationBridge_4; }
	inline void set_communicationBridge_4(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * value)
	{
		___communicationBridge_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___communicationBridge_4), (void*)value);
	}

	inline static int32_t get_offset_of_CanvasHolder_5() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CanvasHolder_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CanvasHolder_5() const { return ___CanvasHolder_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CanvasHolder_5() { return &___CanvasHolder_5; }
	inline void set_CanvasHolder_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CanvasHolder_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CanvasHolder_5), (void*)value);
	}

	inline static int32_t get_offset_of_CartPanel_6() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CartPanel_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CartPanel_6() const { return ___CartPanel_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CartPanel_6() { return &___CartPanel_6; }
	inline void set_CartPanel_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CartPanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CartPanel_6), (void*)value);
	}

	inline static int32_t get_offset_of_InfoButton_7() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___InfoButton_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_InfoButton_7() const { return ___InfoButton_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_InfoButton_7() { return &___InfoButton_7; }
	inline void set_InfoButton_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___InfoButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InfoButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_CloseButton_8() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CloseButton_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CloseButton_8() const { return ___CloseButton_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CloseButton_8() { return &___CloseButton_8; }
	inline void set_CloseButton_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CloseButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CloseButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_ObjSelectionButton_9() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___ObjSelectionButton_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSelectionButton_9() const { return ___ObjSelectionButton_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSelectionButton_9() { return &___ObjSelectionButton_9; }
	inline void set_ObjSelectionButton_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSelectionButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSelectionButton_9), (void*)value);
	}

	inline static int32_t get_offset_of_SelectionEffect_10() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___SelectionEffect_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_SelectionEffect_10() const { return ___SelectionEffect_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_SelectionEffect_10() { return &___SelectionEffect_10; }
	inline void set_SelectionEffect_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___SelectionEffect_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SelectionEffect_10), (void*)value);
	}
};


// CSharpscaling
struct CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSharpscaling::initialFingersDistance
	float ___initialFingersDistance_4;
	// UnityEngine.Vector3 CSharpscaling::initialScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___initialScale_5;

public:
	inline static int32_t get_offset_of_initialFingersDistance_4() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1, ___initialFingersDistance_4)); }
	inline float get_initialFingersDistance_4() const { return ___initialFingersDistance_4; }
	inline float* get_address_of_initialFingersDistance_4() { return &___initialFingersDistance_4; }
	inline void set_initialFingersDistance_4(float value)
	{
		___initialFingersDistance_4 = value;
	}

	inline static int32_t get_offset_of_initialScale_5() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1, ___initialScale_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_initialScale_5() const { return ___initialScale_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_initialScale_5() { return &___initialScale_5; }
	inline void set_initialScale_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___initialScale_5 = value;
	}
};

struct CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields
{
public:
	// UnityEngine.Transform CSharpscaling::ScaleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___ScaleTransform_6;

public:
	inline static int32_t get_offset_of_ScaleTransform_6() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields, ___ScaleTransform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_ScaleTransform_6() const { return ___ScaleTransform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_ScaleTransform_6() { return &___ScaleTransform_6; }
	inline void set_ScaleTransform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___ScaleTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ScaleTransform_6), (void*)value);
	}
};


// ContentController
struct ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// API ContentController::api
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___api_4;
	// UnityEngine.GameObject ContentController::ObjSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSpawner_5;

public:
	inline static int32_t get_offset_of_api_4() { return static_cast<int32_t>(offsetof(ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB, ___api_4)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_api_4() const { return ___api_4; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_api_4() { return &___api_4; }
	inline void set_api_4(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___api_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___api_4), (void*)value);
	}

	inline static int32_t get_offset_of_ObjSpawner_5() { return static_cast<int32_t>(offsetof(ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB, ___ObjSpawner_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSpawner_5() const { return ___ObjSpawner_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSpawner_5() { return &___ObjSpawner_5; }
	inline void set_ObjSpawner_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSpawner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSpawner_5), (void*)value);
	}
};


// DragObject1
struct DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 DragObject1::dist
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dist_4;
	// System.Single DragObject1::posX
	float ___posX_5;
	// System.Single DragObject1::posY
	float ___posY_6;

public:
	inline static int32_t get_offset_of_dist_4() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___dist_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_dist_4() const { return ___dist_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_dist_4() { return &___dist_4; }
	inline void set_dist_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___dist_4 = value;
	}

	inline static int32_t get_offset_of_posX_5() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___posX_5)); }
	inline float get_posX_5() const { return ___posX_5; }
	inline float* get_address_of_posX_5() { return &___posX_5; }
	inline void set_posX_5(float value)
	{
		___posX_5 = value;
	}

	inline static int32_t get_offset_of_posY_6() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___posY_6)); }
	inline float get_posY_6() const { return ___posY_6; }
	inline float* get_address_of_posY_6() { return &___posY_6; }
	inline void set_posY_6(float value)
	{
		___posY_6 = value;
	}
};


// Lean.Touch.LeanDragTranslate
struct LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Lean.Touch.LeanFingerFilter Lean.Touch.LeanDragTranslate::Use
	LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB  ___Use_4;
	// UnityEngine.Camera Lean.Touch.LeanDragTranslate::Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___Camera_5;
	// System.Single Lean.Touch.LeanDragTranslate::Sensitivity
	float ___Sensitivity_6;
	// System.Single Lean.Touch.LeanDragTranslate::Damping
	float ___Damping_7;
	// System.Single Lean.Touch.LeanDragTranslate::Inertia
	float ___Inertia_8;
	// UnityEngine.Vector3 Lean.Touch.LeanDragTranslate::remainingTranslation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___remainingTranslation_9;
	// System.Single Lean.Touch.LeanDragTranslate::Dampening
	float ___Dampening_10;
	// UnityEngine.Vector3 Lean.Touch.LeanDragTranslate::RemainingDelta
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___RemainingDelta_11;

public:
	inline static int32_t get_offset_of_Use_4() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Use_4)); }
	inline LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB  get_Use_4() const { return ___Use_4; }
	inline LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB * get_address_of_Use_4() { return &___Use_4; }
	inline void set_Use_4(LeanFingerFilter_t6CB26F067486A40E3F0F9B648AA87533C950CBCB  value)
	{
		___Use_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___RequiredSelectable_4), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___fingers_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___Use_4))->___filteredFingers_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_Camera_5() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Camera_5)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_Camera_5() const { return ___Camera_5; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_Camera_5() { return &___Camera_5; }
	inline void set_Camera_5(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___Camera_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Camera_5), (void*)value);
	}

	inline static int32_t get_offset_of_Sensitivity_6() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Sensitivity_6)); }
	inline float get_Sensitivity_6() const { return ___Sensitivity_6; }
	inline float* get_address_of_Sensitivity_6() { return &___Sensitivity_6; }
	inline void set_Sensitivity_6(float value)
	{
		___Sensitivity_6 = value;
	}

	inline static int32_t get_offset_of_Damping_7() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Damping_7)); }
	inline float get_Damping_7() const { return ___Damping_7; }
	inline float* get_address_of_Damping_7() { return &___Damping_7; }
	inline void set_Damping_7(float value)
	{
		___Damping_7 = value;
	}

	inline static int32_t get_offset_of_Inertia_8() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Inertia_8)); }
	inline float get_Inertia_8() const { return ___Inertia_8; }
	inline float* get_address_of_Inertia_8() { return &___Inertia_8; }
	inline void set_Inertia_8(float value)
	{
		___Inertia_8 = value;
	}

	inline static int32_t get_offset_of_remainingTranslation_9() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___remainingTranslation_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_remainingTranslation_9() const { return ___remainingTranslation_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_remainingTranslation_9() { return &___remainingTranslation_9; }
	inline void set_remainingTranslation_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___remainingTranslation_9 = value;
	}

	inline static int32_t get_offset_of_Dampening_10() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___Dampening_10)); }
	inline float get_Dampening_10() const { return ___Dampening_10; }
	inline float* get_address_of_Dampening_10() { return &___Dampening_10; }
	inline void set_Dampening_10(float value)
	{
		___Dampening_10 = value;
	}

	inline static int32_t get_offset_of_RemainingDelta_11() { return static_cast<int32_t>(offsetof(LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8, ___RemainingDelta_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_RemainingDelta_11() const { return ___RemainingDelta_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_RemainingDelta_11() { return &___RemainingDelta_11; }
	inline void set_RemainingDelta_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___RemainingDelta_11 = value;
	}
};


// LookAtCamera
struct LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject LookAtCamera::Cam
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Cam_4;

public:
	inline static int32_t get_offset_of_Cam_4() { return static_cast<int32_t>(offsetof(LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2, ___Cam_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Cam_4() const { return ___Cam_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Cam_4() { return &___Cam_4; }
	inline void set_Cam_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Cam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Cam_4), (void*)value);
	}
};


// MyDrag
struct MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MyDrag::holding
	bool ___holding_4;

public:
	inline static int32_t get_offset_of_holding_4() { return static_cast<int32_t>(offsetof(MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F, ___holding_4)); }
	inline bool get_holding_4() const { return ___holding_4; }
	inline bool* get_address_of_holding_4() { return &___holding_4; }
	inline void set_holding_4(bool value)
	{
		___holding_4 = value;
	}
};


// ObjectSpawner
struct ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ObjectSpawner::objectToSpawn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___objectToSpawn_4;
	// PlacementIndicator ObjectSpawner::placementIndicator
	PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * ___placementIndicator_5;
	// UnityEngine.GameObject ObjectSpawner::ContentCont
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ContentCont_6;
	// System.String ObjectSpawner::RedeemID1
	String_t* ___RedeemID1_7;
	// UnityEngine.GameObject ObjectSpawner::DownloadingText
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___DownloadingText_8;
	// UnityEngine.GameObject ObjectSpawner::ControlTransferButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ControlTransferButton_9;
	// System.String ObjectSpawner::RedeemID2
	String_t* ___RedeemID2_10;
	// UnityEngine.GameObject ObjectSpawner::RedeemRefContainer
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___RedeemRefContainer_11;
	// UnityEngine.GameObject ObjectSpawner::ResetButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ResetButton_12;
	// UnityEngine.GameObject ObjectSpawner::AlertDialog
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___AlertDialog_13;
	// System.String ObjectSpawner::ReturnModelId
	String_t* ___ReturnModelId_14;
	// System.Boolean ObjectSpawner::SingleCall
	bool ___SingleCall_15;

public:
	inline static int32_t get_offset_of_objectToSpawn_4() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___objectToSpawn_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_objectToSpawn_4() const { return ___objectToSpawn_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_objectToSpawn_4() { return &___objectToSpawn_4; }
	inline void set_objectToSpawn_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___objectToSpawn_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectToSpawn_4), (void*)value);
	}

	inline static int32_t get_offset_of_placementIndicator_5() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___placementIndicator_5)); }
	inline PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * get_placementIndicator_5() const { return ___placementIndicator_5; }
	inline PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 ** get_address_of_placementIndicator_5() { return &___placementIndicator_5; }
	inline void set_placementIndicator_5(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * value)
	{
		___placementIndicator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementIndicator_5), (void*)value);
	}

	inline static int32_t get_offset_of_ContentCont_6() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ContentCont_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ContentCont_6() const { return ___ContentCont_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ContentCont_6() { return &___ContentCont_6; }
	inline void set_ContentCont_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ContentCont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ContentCont_6), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemID1_7() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemID1_7)); }
	inline String_t* get_RedeemID1_7() const { return ___RedeemID1_7; }
	inline String_t** get_address_of_RedeemID1_7() { return &___RedeemID1_7; }
	inline void set_RedeemID1_7(String_t* value)
	{
		___RedeemID1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemID1_7), (void*)value);
	}

	inline static int32_t get_offset_of_DownloadingText_8() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___DownloadingText_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_DownloadingText_8() const { return ___DownloadingText_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_DownloadingText_8() { return &___DownloadingText_8; }
	inline void set_DownloadingText_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___DownloadingText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DownloadingText_8), (void*)value);
	}

	inline static int32_t get_offset_of_ControlTransferButton_9() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ControlTransferButton_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ControlTransferButton_9() const { return ___ControlTransferButton_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ControlTransferButton_9() { return &___ControlTransferButton_9; }
	inline void set_ControlTransferButton_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ControlTransferButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ControlTransferButton_9), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemID2_10() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemID2_10)); }
	inline String_t* get_RedeemID2_10() const { return ___RedeemID2_10; }
	inline String_t** get_address_of_RedeemID2_10() { return &___RedeemID2_10; }
	inline void set_RedeemID2_10(String_t* value)
	{
		___RedeemID2_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemID2_10), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemRefContainer_11() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemRefContainer_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_RedeemRefContainer_11() const { return ___RedeemRefContainer_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_RedeemRefContainer_11() { return &___RedeemRefContainer_11; }
	inline void set_RedeemRefContainer_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___RedeemRefContainer_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemRefContainer_11), (void*)value);
	}

	inline static int32_t get_offset_of_ResetButton_12() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ResetButton_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ResetButton_12() const { return ___ResetButton_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ResetButton_12() { return &___ResetButton_12; }
	inline void set_ResetButton_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ResetButton_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResetButton_12), (void*)value);
	}

	inline static int32_t get_offset_of_AlertDialog_13() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___AlertDialog_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_AlertDialog_13() const { return ___AlertDialog_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_AlertDialog_13() { return &___AlertDialog_13; }
	inline void set_AlertDialog_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___AlertDialog_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AlertDialog_13), (void*)value);
	}

	inline static int32_t get_offset_of_ReturnModelId_14() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ReturnModelId_14)); }
	inline String_t* get_ReturnModelId_14() const { return ___ReturnModelId_14; }
	inline String_t** get_address_of_ReturnModelId_14() { return &___ReturnModelId_14; }
	inline void set_ReturnModelId_14(String_t* value)
	{
		___ReturnModelId_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReturnModelId_14), (void*)value);
	}

	inline static int32_t get_offset_of_SingleCall_15() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___SingleCall_15)); }
	inline bool get_SingleCall_15() const { return ___SingleCall_15; }
	inline bool* get_address_of_SingleCall_15() { return &___SingleCall_15; }
	inline void set_SingleCall_15(bool value)
	{
		___SingleCall_15 = value;
	}
};


// PlaceContent
struct PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlaceContent::raycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___raycastManager_4;
	// UnityEngine.UI.GraphicRaycaster PlaceContent::raycaster
	GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * ___raycaster_5;

public:
	inline static int32_t get_offset_of_raycastManager_4() { return static_cast<int32_t>(offsetof(PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299, ___raycastManager_4)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_raycastManager_4() const { return ___raycastManager_4; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_raycastManager_4() { return &___raycastManager_4; }
	inline void set_raycastManager_4(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___raycastManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycastManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_raycaster_5() { return static_cast<int32_t>(offsetof(PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299, ___raycaster_5)); }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * get_raycaster_5() const { return ___raycaster_5; }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 ** get_address_of_raycaster_5() { return &___raycaster_5; }
	inline void set_raycaster_5(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * value)
	{
		___raycaster_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycaster_5), (void*)value);
	}
};


// PlacementIndicator
struct PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlacementIndicator::rayManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___rayManager_4;
	// UnityEngine.GameObject PlacementIndicator::visual
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___visual_5;

public:
	inline static int32_t get_offset_of_rayManager_4() { return static_cast<int32_t>(offsetof(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275, ___rayManager_4)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_rayManager_4() const { return ___rayManager_4; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_rayManager_4() { return &___rayManager_4; }
	inline void set_rayManager_4(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___rayManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rayManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_visual_5() { return static_cast<int32_t>(offsetof(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275, ___visual_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_visual_5() const { return ___visual_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_visual_5() { return &___visual_5; }
	inline void set_visual_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___visual_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___visual_5), (void*)value);
	}
};


// PlacementObject
struct PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean PlacementObject::IsSelected
	bool ___IsSelected_4;
	// System.Boolean PlacementObject::IsLocked
	bool ___IsLocked_5;
	// TMPro.TextMeshPro PlacementObject::OverlayText
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___OverlayText_6;
	// UnityEngine.Canvas PlacementObject::canvasComponent
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___canvasComponent_7;
	// System.String PlacementObject::OverlayDisplayText
	String_t* ___OverlayDisplayText_8;

public:
	inline static int32_t get_offset_of_IsSelected_4() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___IsSelected_4)); }
	inline bool get_IsSelected_4() const { return ___IsSelected_4; }
	inline bool* get_address_of_IsSelected_4() { return &___IsSelected_4; }
	inline void set_IsSelected_4(bool value)
	{
		___IsSelected_4 = value;
	}

	inline static int32_t get_offset_of_IsLocked_5() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___IsLocked_5)); }
	inline bool get_IsLocked_5() const { return ___IsLocked_5; }
	inline bool* get_address_of_IsLocked_5() { return &___IsLocked_5; }
	inline void set_IsLocked_5(bool value)
	{
		___IsLocked_5 = value;
	}

	inline static int32_t get_offset_of_OverlayText_6() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___OverlayText_6)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_OverlayText_6() const { return ___OverlayText_6; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_OverlayText_6() { return &___OverlayText_6; }
	inline void set_OverlayText_6(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___OverlayText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OverlayText_6), (void*)value);
	}

	inline static int32_t get_offset_of_canvasComponent_7() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___canvasComponent_7)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_canvasComponent_7() const { return ___canvasComponent_7; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_canvasComponent_7() { return &___canvasComponent_7; }
	inline void set_canvasComponent_7(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___canvasComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_OverlayDisplayText_8() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___OverlayDisplayText_8)); }
	inline String_t* get_OverlayDisplayText_8() const { return ___OverlayDisplayText_8; }
	inline String_t** get_address_of_OverlayDisplayText_8() { return &___OverlayDisplayText_8; }
	inline void set_OverlayDisplayText_8(String_t* value)
	{
		___OverlayDisplayText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OverlayDisplayText_8), (void*)value);
	}
};


// PlacementWithMultipleDraggingDroppingController
struct PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::placedPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placedPrefab_4;
	// UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::welcomePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___welcomePanel_5;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::dismissButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___dismissButton_6;
	// UnityEngine.Camera PlacementWithMultipleDraggingDroppingController::arCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___arCamera_7;
	// PlacementObject[] PlacementWithMultipleDraggingDroppingController::placedObjects
	PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* ___placedObjects_8;
	// UnityEngine.Vector2 PlacementWithMultipleDraggingDroppingController::touchPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___touchPosition_9;
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlacementWithMultipleDraggingDroppingController::arRaycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___arRaycastManager_10;
	// System.Boolean PlacementWithMultipleDraggingDroppingController::onTouchHold
	bool ___onTouchHold_11;
	// PlacementObject PlacementWithMultipleDraggingDroppingController::lastSelectedObject
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * ___lastSelectedObject_13;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::redButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___redButton_14;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::greenButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___greenButton_15;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::blueButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___blueButton_16;

public:
	inline static int32_t get_offset_of_placedPrefab_4() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___placedPrefab_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placedPrefab_4() const { return ___placedPrefab_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placedPrefab_4() { return &___placedPrefab_4; }
	inline void set_placedPrefab_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placedPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_welcomePanel_5() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___welcomePanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_welcomePanel_5() const { return ___welcomePanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_welcomePanel_5() { return &___welcomePanel_5; }
	inline void set_welcomePanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___welcomePanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___welcomePanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_dismissButton_6() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___dismissButton_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_dismissButton_6() const { return ___dismissButton_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_dismissButton_6() { return &___dismissButton_6; }
	inline void set_dismissButton_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___dismissButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dismissButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_arCamera_7() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___arCamera_7)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_arCamera_7() const { return ___arCamera_7; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_arCamera_7() { return &___arCamera_7; }
	inline void set_arCamera_7(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___arCamera_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCamera_7), (void*)value);
	}

	inline static int32_t get_offset_of_placedObjects_8() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___placedObjects_8)); }
	inline PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* get_placedObjects_8() const { return ___placedObjects_8; }
	inline PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E** get_address_of_placedObjects_8() { return &___placedObjects_8; }
	inline void set_placedObjects_8(PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* value)
	{
		___placedObjects_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placedObjects_8), (void*)value);
	}

	inline static int32_t get_offset_of_touchPosition_9() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___touchPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_touchPosition_9() const { return ___touchPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_touchPosition_9() { return &___touchPosition_9; }
	inline void set_touchPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___touchPosition_9 = value;
	}

	inline static int32_t get_offset_of_arRaycastManager_10() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___arRaycastManager_10)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_arRaycastManager_10() const { return ___arRaycastManager_10; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_arRaycastManager_10() { return &___arRaycastManager_10; }
	inline void set_arRaycastManager_10(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___arRaycastManager_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arRaycastManager_10), (void*)value);
	}

	inline static int32_t get_offset_of_onTouchHold_11() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___onTouchHold_11)); }
	inline bool get_onTouchHold_11() const { return ___onTouchHold_11; }
	inline bool* get_address_of_onTouchHold_11() { return &___onTouchHold_11; }
	inline void set_onTouchHold_11(bool value)
	{
		___onTouchHold_11 = value;
	}

	inline static int32_t get_offset_of_lastSelectedObject_13() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___lastSelectedObject_13)); }
	inline PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * get_lastSelectedObject_13() const { return ___lastSelectedObject_13; }
	inline PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 ** get_address_of_lastSelectedObject_13() { return &___lastSelectedObject_13; }
	inline void set_lastSelectedObject_13(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * value)
	{
		___lastSelectedObject_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastSelectedObject_13), (void*)value);
	}

	inline static int32_t get_offset_of_redButton_14() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___redButton_14)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_redButton_14() const { return ___redButton_14; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_redButton_14() { return &___redButton_14; }
	inline void set_redButton_14(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___redButton_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___redButton_14), (void*)value);
	}

	inline static int32_t get_offset_of_greenButton_15() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___greenButton_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_greenButton_15() const { return ___greenButton_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_greenButton_15() { return &___greenButton_15; }
	inline void set_greenButton_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___greenButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___greenButton_15), (void*)value);
	}

	inline static int32_t get_offset_of_blueButton_16() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___blueButton_16)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_blueButton_16() const { return ___blueButton_16; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_blueButton_16() { return &___blueButton_16; }
	inline void set_blueButton_16(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___blueButton_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blueButton_16), (void*)value);
	}
};

struct PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlacementWithMultipleDraggingDroppingController::hits
	List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * ___hits_12;

public:
	inline static int32_t get_offset_of_hits_12() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields, ___hits_12)); }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * get_hits_12() const { return ___hits_12; }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D ** get_address_of_hits_12() { return &___hits_12; }
	inline void set_hits_12(List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * value)
	{
		___hits_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_12), (void*)value);
	}
};


// ReturnCartList
struct ReturnCartList_tED3BCB6B9F26337331DB4F94802112BC76F9FB02  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ToggleAR
struct ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager ToggleAR::planeManager
	ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * ___planeManager_4;
	// UnityEngine.XR.ARFoundation.ARPointCloudManager ToggleAR::pointCloudManager
	ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * ___pointCloudManager_5;

public:
	inline static int32_t get_offset_of_planeManager_4() { return static_cast<int32_t>(offsetof(ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7, ___planeManager_4)); }
	inline ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * get_planeManager_4() const { return ___planeManager_4; }
	inline ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 ** get_address_of_planeManager_4() { return &___planeManager_4; }
	inline void set_planeManager_4(ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * value)
	{
		___planeManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___planeManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_pointCloudManager_5() { return static_cast<int32_t>(offsetof(ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7, ___pointCloudManager_5)); }
	inline ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * get_pointCloudManager_5() const { return ___pointCloudManager_5; }
	inline ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF ** get_address_of_pointCloudManager_5() { return &___pointCloudManager_5; }
	inline void set_pointCloudManager_5(ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * value)
	{
		___pointCloudManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointCloudManager_5), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem
struct XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD  : public SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F
{
public:
	// System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::<currentConfiguration>k__BackingField
	Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  ___U3CcurrentConfigurationU3Ek__BackingField_4;
	// UnityEngine.XR.ARSubsystems.ConfigurationChooser UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_DefaultConfigurationChooser
	ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * ___m_DefaultConfigurationChooser_5;
	// UnityEngine.XR.ARSubsystems.ConfigurationChooser UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_ConfigurationChooser
	ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * ___m_ConfigurationChooser_6;

public:
	inline static int32_t get_offset_of_U3CcurrentConfigurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___U3CcurrentConfigurationU3Ek__BackingField_4)); }
	inline Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  get_U3CcurrentConfigurationU3Ek__BackingField_4() const { return ___U3CcurrentConfigurationU3Ek__BackingField_4; }
	inline Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494 * get_address_of_U3CcurrentConfigurationU3Ek__BackingField_4() { return &___U3CcurrentConfigurationU3Ek__BackingField_4; }
	inline void set_U3CcurrentConfigurationU3Ek__BackingField_4(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  value)
	{
		___U3CcurrentConfigurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_m_DefaultConfigurationChooser_5() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___m_DefaultConfigurationChooser_5)); }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * get_m_DefaultConfigurationChooser_5() const { return ___m_DefaultConfigurationChooser_5; }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 ** get_address_of_m_DefaultConfigurationChooser_5() { return &___m_DefaultConfigurationChooser_5; }
	inline void set_m_DefaultConfigurationChooser_5(ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * value)
	{
		___m_DefaultConfigurationChooser_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultConfigurationChooser_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConfigurationChooser_6() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___m_ConfigurationChooser_6)); }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * get_m_ConfigurationChooser_6() const { return ___m_ConfigurationChooser_6; }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 ** get_address_of_m_ConfigurationChooser_6() { return &___m_ConfigurationChooser_6; }
	inline void set_m_ConfigurationChooser_6(ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * value)
	{
		___m_ConfigurationChooser_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ConfigurationChooser_6), (void*)value);
	}
};


// onClickForScaling
struct onClickForScaling_t4E6FED225317AC601426A8B8C57BDE93374439B8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// rotateController
struct rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single rotateController::_sensitivity
	float ____sensitivity_4;
	// UnityEngine.Vector3 rotateController::_mouseReference
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____mouseReference_5;
	// UnityEngine.Vector3 rotateController::_mouseOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____mouseOffset_6;
	// UnityEngine.Vector3 rotateController::_rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____rotation_7;
	// System.Boolean rotateController::_isRotating
	bool ____isRotating_8;

public:
	inline static int32_t get_offset_of__sensitivity_4() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____sensitivity_4)); }
	inline float get__sensitivity_4() const { return ____sensitivity_4; }
	inline float* get_address_of__sensitivity_4() { return &____sensitivity_4; }
	inline void set__sensitivity_4(float value)
	{
		____sensitivity_4 = value;
	}

	inline static int32_t get_offset_of__mouseReference_5() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____mouseReference_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__mouseReference_5() const { return ____mouseReference_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__mouseReference_5() { return &____mouseReference_5; }
	inline void set__mouseReference_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____mouseReference_5 = value;
	}

	inline static int32_t get_offset_of__mouseOffset_6() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____mouseOffset_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__mouseOffset_6() const { return ____mouseOffset_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__mouseOffset_6() { return &____mouseOffset_6; }
	inline void set__mouseOffset_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____mouseOffset_6 = value;
	}

	inline static int32_t get_offset_of__rotation_7() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____rotation_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__rotation_7() const { return ____rotation_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__rotation_7() { return &____rotation_7; }
	inline void set__rotation_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____rotation_7 = value;
	}

	inline static int32_t get_offset_of__isRotating_8() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____isRotating_8)); }
	inline bool get__isRotating_8() const { return ____isRotating_8; }
	inline bool* get_address_of__isRotating_8() { return &____isRotating_8; }
	inline void set__isRotating_8(bool value)
	{
		____isRotating_8 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem
struct ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216  : public XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::<sessionDelegate>k__BackingField
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * ___U3CsessionDelegateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CsessionDelegateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216, ___U3CsessionDelegateU3Ek__BackingField_7)); }
	inline ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * get_U3CsessionDelegateU3Ek__BackingField_7() const { return ___U3CsessionDelegateU3Ek__BackingField_7; }
	inline ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE ** get_address_of_U3CsessionDelegateU3Ek__BackingField_7() { return &___U3CsessionDelegateU3Ek__BackingField_7; }
	inline void set_U3CsessionDelegateU3Ek__BackingField_7(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * value)
	{
		___U3CsessionDelegateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsessionDelegateU3Ek__BackingField_7), (void*)value);
	}
};

struct ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::s_OnAsyncWorldMapCompleted
	OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * ___s_OnAsyncWorldMapCompleted_8;

public:
	inline static int32_t get_offset_of_s_OnAsyncWorldMapCompleted_8() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields, ___s_OnAsyncWorldMapCompleted_8)); }
	inline OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * get_s_OnAsyncWorldMapCompleted_8() const { return ___s_OnAsyncWorldMapCompleted_8; }
	inline OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF ** get_address_of_s_OnAsyncWorldMapCompleted_8() { return &___s_OnAsyncWorldMapCompleted_8; }
	inline void set_s_OnAsyncWorldMapCompleted_8(OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * value)
	{
		___s_OnAsyncWorldMapCompleted_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OnAsyncWorldMapCompleted_8), (void*)value);
	}
};


// Lean.Touch.LeanTranslateSmooth
struct LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7  : public LeanDragTranslate_tF676F7A9EEF251BD85703194303AD30FF1EE45E8
{
public:
	// System.Single Lean.Touch.LeanTranslateSmooth::Dampening
	float ___Dampening_12;
	// UnityEngine.Vector3 Lean.Touch.LeanTranslateSmooth::RemainingDelta
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___RemainingDelta_13;

public:
	inline static int32_t get_offset_of_Dampening_12() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7, ___Dampening_12)); }
	inline float get_Dampening_12() const { return ___Dampening_12; }
	inline float* get_address_of_Dampening_12() { return &___Dampening_12; }
	inline void set_Dampening_12(float value)
	{
		___Dampening_12 = value;
	}

	inline static int32_t get_offset_of_RemainingDelta_13() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7, ___RemainingDelta_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_RemainingDelta_13() const { return ___RemainingDelta_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_RemainingDelta_13() { return &___RemainingDelta_13; }
	inline void set_RemainingDelta_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___RemainingDelta_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3268[2] = 
{
	ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields::get_offset_of_U3CinstanceU3Ek__BackingField_0(),
	ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields::get_offset_of_s_SupportedVideoConversionFormats_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3271[20] = 
{
	ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3273[15] = 
{
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_SessionSubsystemDescriptors_5(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_CameraSubsystemDescriptors_6(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_DepthSubsystemDescriptors_7(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_PlaneSubsystemDescriptors_8(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_AnchorSubsystemDescriptors_9(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_RaycastSubsystemDescriptors_10(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_HumanBodySubsystemDescriptors_11(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_EnvironmentProbeSubsystemDescriptors_12(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_InputSubsystemDescriptors_13(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_ImageTrackingSubsystemDescriptors_14(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_ObjectTrackingSubsystemDescriptors_15(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_FaceSubsystemDescriptors_16(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_OcclusionSubsystemDescriptors_17(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_ParticipantSubsystemDescriptors_18(),
	ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields::get_offset_of_s_MeshSubsystemDescriptors_19(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3274[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3278[1] = 
{
	ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88::get_offset_of_m_Self_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3281[4] = 
{
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields::get_offset_of_s_SessionDidFailWithError_0(),
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields::get_offset_of_s_CoachingOverlayViewWillActivate_1(),
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields::get_offset_of_s_CoachingOverlayViewDidDeactivate_2(),
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields::get_offset_of_s_ConfigurationChanged_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3282[2] = 
{
	ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF::get_offset_of_m_Self_1(),
	ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF::get_offset_of_m_SubsystemHandle_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3283[3] = 
{
	Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3286[2] = 
{
	ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216::get_offset_of_U3CsessionDelegateU3Ek__BackingField_7(),
	ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields::get_offset_of_s_OnAsyncWorldMapCompleted_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3287[9] = 
{
	ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3288[4] = 
{
	ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3289[2] = 
{
	0,
	ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2::get_offset_of_U3CnativeHandleU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3290[5] = 
{
	ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3291[1] = 
{
	ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4::get_offset_of_m_RequestId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3292[8] = 
{
	ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3294[20] = 
{
	CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3295[2] = 
{
	DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639::get_offset_of_U3CretriesRemainingU3Ek__BackingField_4(),
	DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639::get_offset_of_U3CmaxRetryCountU3Ek__BackingField_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3297[2] = 
{
	TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3303[1] = 
{
	ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1::get_offset_of_database_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3304[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3305[2] = 
{
	ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78::get_offset_of_rgbaImage_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78::get_offset_of_argbImage_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3306[8] = 
{
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_image_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_database_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_validator_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_width_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_height_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_physicalWidth_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_format_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5::get_offset_of_managedReferenceImage_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3307[2] = 
{
	ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71::get_offset_of_U3CnativePtrU3Ek__BackingField_0(),
	ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields::get_offset_of_k_SupportedFormats_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3310[5] = 
{
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1::get_offset_of_guid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1::get_offset_of_textureGuid_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1::get_offset_of_size_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1::get_offset_of_name_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1::get_offset_of_texture_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3311[3] = 
{
	MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D::get_offset_of_size_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D::get_offset_of_stride_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D::get_offset_of_alignment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3312[1] = 
{
	NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76::get_offset_of_m_Ptr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3313[1] = 
{
	NSData_t17EC9EAA93403854EE122C95559948C024C209D0::get_offset_of_m_NativePtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3314[1] = 
{
	NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9::get_offset_of_m_Self_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3315[4] = 
{
	NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3316[1] = 
{
	NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F::get_offset_of_m_NativePtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3319[1] = 
{
	NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350::get_offset_of_m_Self_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3320[1] = 
{
	ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47::get_offset_of_m_ReferenceOrigin_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3323[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_k_TextureHumanStencilPropertyId_7(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_k_TextureHumanDepthPropertyId_8(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_k_TextureEnvironmentDepthPropertyId_9(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_k_TextureEnvironmentDepthConfidencePropertyId_10(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_m_HumanEnabledMaterialKeywords_11(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_m_EnvironmentDepthEnabledMaterialKeywords_12(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields::get_offset_of_m_AllDisabledMaterialKeywords_13(),
	ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611::get_offset_of_m_OcclusionPreferenceMode_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3326[3] = 
{
	OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C::get_offset_of_U3CmajorU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C::get_offset_of_U3CminorU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C::get_offset_of_U3CpointU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3327[1] = 
{
	ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C::get_offset_of_m_Ptr_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3329[1] = 
{
	FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA::get_offset_of_positions_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3330[2] = 
{
	TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3334[1] = 
{
	SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3::get_offset_of_m_NSData_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3335[4] = 
{
	SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3337[1] = 
{
	U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields::get_offset_of_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3340[2] = 
{
	ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27::get_offset_of_Quantity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3341[3] = 
{
	AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2::get_offset_of_CartItemsCount_4(),
	AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2::get_offset_of_CartItemsCounter_5(),
	AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2::get_offset_of_MyItems_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3344[3] = 
{
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3345[3] = 
{
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3346[7] = 
{
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_communicationBridge_4(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_CanvasHolder_5(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_CartPanel_6(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_InfoButton_7(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_CloseButton_8(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_ObjSelectionButton_9(),
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B::get_offset_of_SelectionEffect_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3347[3] = 
{
	DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5::get_offset_of_dist_4(),
	DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5::get_offset_of_posX_5(),
	DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5::get_offset_of_posY_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3348[1] = 
{
	LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2::get_offset_of_Cam_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3349[1] = 
{
	MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F::get_offset_of_holding_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3350[5] = 
{
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7::get_offset_of_IsSelected_4(),
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7::get_offset_of_IsLocked_5(),
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7::get_offset_of_OverlayText_6(),
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7::get_offset_of_canvasComponent_7(),
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7::get_offset_of_OverlayDisplayText_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3351[13] = 
{
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_placedPrefab_4(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_welcomePanel_5(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_dismissButton_6(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_arCamera_7(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_placedObjects_8(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_touchPosition_9(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_arRaycastManager_10(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_onTouchHold_11(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields::get_offset_of_hits_12(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_lastSelectedObject_13(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_redButton_14(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_greenButton_15(),
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9::get_offset_of_blueButton_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3353[3] = 
{
	CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1::get_offset_of_initialFingersDistance_4(),
	CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1::get_offset_of_initialScale_5(),
	CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields::get_offset_of_ScaleTransform_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3355[5] = 
{
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24::get_offset_of__sensitivity_4(),
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24::get_offset_of__mouseReference_5(),
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24::get_offset_of__mouseOffset_6(),
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24::get_offset_of__rotation_7(),
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24::get_offset_of__isRotating_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3356[3] = 
{
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3357[3] = 
{
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3358[7] = 
{
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_U3CU3E1__state_0(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_U3CU3E2__current_1(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_assetName_2(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_U3CU3E4__this_3(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_bundleParent_4(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_callback_5(),
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8::get_offset_of_U3CwwwU3E5__2_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3359[2] = 
{
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2::get_offset_of_ObjSpawner_4(),
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3360[2] = 
{
	ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB::get_offset_of_api_4(),
	ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB::get_offset_of_ObjSpawner_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3361[2] = 
{
	PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299::get_offset_of_raycastManager_4(),
	PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299::get_offset_of_raycaster_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3362[2] = 
{
	ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7::get_offset_of_planeManager_4(),
	ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7::get_offset_of_pointCloudManager_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3363[1] = 
{
	Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88::get_offset_of_thissdas_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3364[2] = 
{
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79::get_offset_of_U3CU3E1__state_0(),
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79::get_offset_of_U3CU3E2__current_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3367[3] = 
{
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3368[3] = 
{
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3369[3] = 
{
	U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForDownloadRedeemU3Ed__24_t29C5F44B656C8449DA393CD27DCA563EEA9BF148::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3370[3] = 
{
	U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForDownloadRedeem2U3Ed__26_t13B167165729CE762DF55C81AF9B7832DBFE069F::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3371[12] = 
{
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_objectToSpawn_4(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_placementIndicator_5(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_ContentCont_6(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_RedeemID1_7(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_DownloadingText_8(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_ControlTransferButton_9(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_RedeemID2_10(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_RedeemRefContainer_11(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_ResetButton_12(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_AlertDialog_13(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_ReturnModelId_14(),
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388::get_offset_of_SingleCall_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3372[2] = 
{
	PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275::get_offset_of_rayManager_4(),
	PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275::get_offset_of_visual_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3373[2] = 
{
	LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7::get_offset_of_Dampening_12(),
	LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7::get_offset_of_RemainingDelta_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
