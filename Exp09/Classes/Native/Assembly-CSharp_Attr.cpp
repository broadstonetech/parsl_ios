﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_0_0_0_var), NULL);
	}
}
static void ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_0_0_0_var), NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8__ctor_mF6B6D61ED9803BAA7972088214215CD50BA87DA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_IDisposable_Dispose_m878EE75B726025F5D9D841AD3C4FF97E0AD93A31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47D83E3B2E1D848C93B56942BA0221FF00148CA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_Reset_mDEAFCA4F8352195F9810D55DD864B6B429C3F31D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_get_Current_m438E91004CD053B214798E0365536CEF2A4D0B81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12__ctor_mDF11A65818476D125AAD342CD06AC559289159F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_IDisposable_Dispose_m989B47B6D360334473E353EDB842D3286D69C4F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CA287D2235C195CEFDD9E04552A69626B40D047(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_Reset_mD78597C884C1CF484621AD6E3C4C804606516418(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_get_Current_m77788F0D9F379C55D07A5A826372D68CDE3BE01E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator_ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_0_0_0_var), NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9__ctor_m215F7E1C7A95B775D4A6C3685A88C3828952C953(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_IDisposable_Dispose_m2355711058AABE72EE0B95B964549A077133EB08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D276B8AF22A1B90B8BDA3BF2B2433684547E0F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_Reset_m2583B91C2D4528E3F0229447698504EB78845413(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_get_Current_mBD090FEC64CA9B04035490B794A98559F185CE3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3_CustomAttributesCacheGenerator_ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var), NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsSelected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsLocked(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_canvasComponent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayDisplayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_placedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_welcomePanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_dismissButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_arCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_redButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_greenButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_blueButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_0_0_0_var), NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3__ctor_mE02ACBFA8B9BDE9C1100F2FE2C9D9AECB2FC7E49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_IDisposable_Dispose_mA582B2818076E9F47A31D65528C783BFA85AF45D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8DE4374A2E804BCC916100B758490B3E22166E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_Reset_mED0C0255F595058B67D42CC3F1DDA280C401940E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_get_Current_mD260A9607232B73066C5B0C394F7D003122B1426(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4__ctor_m627C75E16CFFC21E43C85FD808F084C23F879467(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_IDisposable_Dispose_mB6E1F5FE34190BD568795E698854F919F42D2517(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED3D6C9EFA62A88C3A5F99225082645FDB40FDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_Reset_mA05D4C7FEDAFADD6FB5F7E05B39C639C9BE1DE91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_get_Current_m78ABB904C66128E52D3E0864049B385782F97A1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5__ctor_m8724EB67A9122B2F4302CE1F9BEF508FB312A75D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_IDisposable_Dispose_mFA88082E025A34CC58193233DE3A7CBB0A7C7447(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648F99BDB79263F8C23AE2C426D0CA80404214F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m198D96D61D02806686153B9C781E94538B092175(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_mBF0E7BDDDD893BF314F4258A88B8286D6757E266(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var), NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForButtonOff_m784D4105069E2BDAB7F6DDADD1D6B282B6B24E19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_0_0_0_var), NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_0_0_0_var), NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var), NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12__ctor_m0B82BD042726E7B3797C9358FD5605F45A1A7508(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_IDisposable_Dispose_mC3D095F7B5A65ECD38652C017497B6AF83948D53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEC800CCAFC224E0F90179E76696E0E1079DB901(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_Reset_m2003529E6D7AE8870E962520167593F92E76B873(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_get_Current_mC5AD80BA36B82FEB132B2EE119D3A9324EE9E28F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22__ctor_m3BA7AAE197C8D4624C14D097777110B6391D394A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_IDisposable_Dispose_mDE911E9E6F03D450DB69669BA000CCA33245D6CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F13D8099C675D59A1A966576A14473AB3FD033(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_Reset_m299B6E03721A150377174B736F6E0CE7CBDA6448(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_get_Current_mC825DCF994513F03F8C8B893C90468134788362F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7_CustomAttributesCacheGenerator_Dampening(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x6D\x6F\x76\x65\x73\x20\x74\x6F\x20\x69\x74\x73\x20\x74\x61\x72\x67\x65\x74\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7_CustomAttributesCacheGenerator_RemainingDelta(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[99] = 
{
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator,
	ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator,
	ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_CustomAttributesCacheGenerator,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator,
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24_CustomAttributesCacheGenerator,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsSelected,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsLocked,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayText,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_canvasComponent,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayDisplayText,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_placedPrefab,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_welcomePanel,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_dismissButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_arCamera,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_redButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_greenButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_blueButton,
	LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7_CustomAttributesCacheGenerator_Dampening,
	LeanTranslateSmooth_t8D012F14D05F255F9091B66A504E91D4FEA377B7_CustomAttributesCacheGenerator_RemainingDelta,
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3,
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B,
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8__ctor_mF6B6D61ED9803BAA7972088214215CD50BA87DA3,
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_IDisposable_Dispose_m878EE75B726025F5D9D841AD3C4FF97E0AD93A31,
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47D83E3B2E1D848C93B56942BA0221FF00148CA0,
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_Reset_mDEAFCA4F8352195F9810D55DD864B6B429C3F31D,
	U3CWaitForPanelOffU3Ed__8_t512C62D6942FB84948265B45FAA40BF5C7546EC9_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_get_Current_m438E91004CD053B214798E0365536CEF2A4D0B81,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12__ctor_mDF11A65818476D125AAD342CD06AC559289159F6,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_IDisposable_Dispose_m989B47B6D360334473E353EDB842D3286D69C4F6,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CA287D2235C195CEFDD9E04552A69626B40D047,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_Reset_mD78597C884C1CF484621AD6E3C4C804606516418,
	U3CWaitForDestructionU3Ed__12_t7A8CC9509B91E9BB88052356DA91A08C8424D98F_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_get_Current_m77788F0D9F379C55D07A5A826372D68CDE3BE01E,
	ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator_ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9__ctor_m215F7E1C7A95B775D4A6C3685A88C3828952C953,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_IDisposable_Dispose_m2355711058AABE72EE0B95B964549A077133EB08,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D276B8AF22A1B90B8BDA3BF2B2433684547E0F8,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_Reset_m2583B91C2D4528E3F0229447698504EB78845413,
	U3CWaitForBaseAnimationU3Ed__9_tE6DF8C65D14783BD11DC98ED77F912A70BF05C8A_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_get_Current_mBD090FEC64CA9B04035490B794A98559F185CE3B,
	ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3_CustomAttributesCacheGenerator_ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3__ctor_mE02ACBFA8B9BDE9C1100F2FE2C9D9AECB2FC7E49,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_IDisposable_Dispose_mA582B2818076E9F47A31D65528C783BFA85AF45D,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8DE4374A2E804BCC916100B758490B3E22166E8,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_Reset_mED0C0255F595058B67D42CC3F1DDA280C401940E,
	U3CWaitForNetworkU3Ed__3_tF965EC8FED2D7851B7A8E8FB3AE3B4A5F42B0FC0_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_get_Current_mD260A9607232B73066C5B0C394F7D003122B1426,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4__ctor_m627C75E16CFFC21E43C85FD808F084C23F879467,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_IDisposable_Dispose_mB6E1F5FE34190BD568795E698854F919F42D2517,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED3D6C9EFA62A88C3A5F99225082645FDB40FDC,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_Reset_mA05D4C7FEDAFADD6FB5F7E05B39C639C9BE1DE91,
	U3CWaitForValidAssetU3Ed__4_t63ACF79A6AC72270FACB058BBBA2977AC7D4A3D4_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_get_Current_m78ABB904C66128E52D3E0864049B385782F97A1F,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5__ctor_m8724EB67A9122B2F4302CE1F9BEF508FB312A75D,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_IDisposable_Dispose_mFA88082E025A34CC58193233DE3A7CBB0A7C7447,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648F99BDB79263F8C23AE2C426D0CA80404214F7,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m198D96D61D02806686153B9C781E94538B092175,
	U3CGetDisplayBundleRoutineU3Ed__5_t918B54EF21D5BF9396289B19B8B2004DB1B1B3A8_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_mBF0E7BDDDD893BF314F4258A88B8286D6757E266,
	Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForButtonOff_m784D4105069E2BDAB7F6DDADD1D6B282B6B24E19,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12__ctor_m0B82BD042726E7B3797C9358FD5605F45A1A7508,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_IDisposable_Dispose_mC3D095F7B5A65ECD38652C017497B6AF83948D53,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEC800CCAFC224E0F90179E76696E0E1079DB901,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_Reset_m2003529E6D7AE8870E962520167593F92E76B873,
	U3CWaitForButtonOffU3Ed__12_tA1516F0EA3857B8D8758F11AD1F452185D9397EB_CustomAttributesCacheGenerator_U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_get_Current_mC5AD80BA36B82FEB132B2EE119D3A9324EE9E28F,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22__ctor_m3BA7AAE197C8D4624C14D097777110B6391D394A,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_IDisposable_Dispose_mDE911E9E6F03D450DB69669BA000CCA33245D6CB,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F13D8099C675D59A1A966576A14473AB3FD033,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_Reset_m299B6E03721A150377174B736F6E0CE7CBDA6448,
	U3CWaitForDownloadU3Ed__22_t76F416B71C19CE998D9AF809F57D9FDF07847A4B_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_get_Current_mC825DCF994513F03F8C8B893C90468134788362F,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
