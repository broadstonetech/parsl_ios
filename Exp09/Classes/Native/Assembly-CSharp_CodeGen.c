﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AddToCartScript::AddToCartItem()
extern void AddToCartScript_AddToCartItem_m69D02A77BE080494C9A091DB293A2E725AA3E65E (void);
// 0x00000002 System.Void AddToCartScript::.ctor()
extern void AddToCartScript__ctor_mDA5D0F9A8DAFE087715F080CB77A9C4CA8FF9D7B (void);
// 0x00000003 System.Void AnimateModel::Start()
extern void AnimateModel_Start_m120DD4C0901DC917E52C9F1215768D3E5E6FD3BC (void);
// 0x00000004 System.Void AnimateModel::Update()
extern void AnimateModel_Update_m6E878217EC241361AFD3591C94B49632761387F5 (void);
// 0x00000005 System.Void AnimateModel::.ctor()
extern void AnimateModel__ctor_m828FACE086F16333DCEBEC48AD64F71755330922 (void);
// 0x00000006 System.Void ArCanvas::ShowCanvas()
extern void ArCanvas_ShowCanvas_mD33E15395281214ECFDF20787F7211EB6FB1AFD6 (void);
// 0x00000007 System.Collections.IEnumerator ArCanvas::WaitForPanelOff()
extern void ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3 (void);
// 0x00000008 System.Void ArCanvas::Start()
extern void ArCanvas_Start_mEFBBCCF7FF62B5A813B12BC305686106788BB645 (void);
// 0x00000009 System.Void ArCanvas::RemoveFromScene()
extern void ArCanvas_RemoveFromScene_mEA7ECC175F7F9E44258C2100E18FDAF94AC13354 (void);
// 0x0000000A System.Collections.IEnumerator ArCanvas::WaitForDestruction()
extern void ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B (void);
// 0x0000000B System.Void ArCanvas::AddToCartPanelPop()
extern void ArCanvas_AddToCartPanelPop_m41789DA41554D1AD4F1E26D4E925A5E3275E4CC2 (void);
// 0x0000000C System.Void ArCanvas::.ctor()
extern void ArCanvas__ctor_mB28079D2DF6AA059F08E3182D22070D36670D212 (void);
// 0x0000000D System.Void ArCanvas/NativeAPI::sendMessageToMobileApp(System.String)
extern void NativeAPI_sendMessageToMobileApp_m1E9509CED0236FABB9BCB35627FFBDD2707C662C (void);
// 0x0000000E System.Void ArCanvas/NativeAPI::destroyUnity(System.String)
extern void NativeAPI_destroyUnity_m447226448B75964021C0576B101B825FB832F999 (void);
// 0x0000000F System.Void ArCanvas/NativeAPI::.ctor()
extern void NativeAPI__ctor_m590217DC4297E301A3A81840213D71E04843D643 (void);
// 0x00000010 System.Void ArCanvas/<WaitForPanelOff>d__8::.ctor(System.Int32)
extern void U3CWaitForPanelOffU3Ed__8__ctor_mF6B6D61ED9803BAA7972088214215CD50BA87DA3 (void);
// 0x00000011 System.Void ArCanvas/<WaitForPanelOff>d__8::System.IDisposable.Dispose()
extern void U3CWaitForPanelOffU3Ed__8_System_IDisposable_Dispose_m878EE75B726025F5D9D841AD3C4FF97E0AD93A31 (void);
// 0x00000012 System.Boolean ArCanvas/<WaitForPanelOff>d__8::MoveNext()
extern void U3CWaitForPanelOffU3Ed__8_MoveNext_m7DDF13041D5008E313E1A3559CCDE302ACEC8D6B (void);
// 0x00000013 System.Object ArCanvas/<WaitForPanelOff>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPanelOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47D83E3B2E1D848C93B56942BA0221FF00148CA0 (void);
// 0x00000014 System.Void ArCanvas/<WaitForPanelOff>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_Reset_mDEAFCA4F8352195F9810D55DD864B6B429C3F31D (void);
// 0x00000015 System.Object ArCanvas/<WaitForPanelOff>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_get_Current_m438E91004CD053B214798E0365536CEF2A4D0B81 (void);
// 0x00000016 System.Void ArCanvas/<WaitForDestruction>d__12::.ctor(System.Int32)
extern void U3CWaitForDestructionU3Ed__12__ctor_mDF11A65818476D125AAD342CD06AC559289159F6 (void);
// 0x00000017 System.Void ArCanvas/<WaitForDestruction>d__12::System.IDisposable.Dispose()
extern void U3CWaitForDestructionU3Ed__12_System_IDisposable_Dispose_m989B47B6D360334473E353EDB842D3286D69C4F6 (void);
// 0x00000018 System.Boolean ArCanvas/<WaitForDestruction>d__12::MoveNext()
extern void U3CWaitForDestructionU3Ed__12_MoveNext_m150A621D43C3EFF8C1E20DD278F04CE4B5623E17 (void);
// 0x00000019 System.Object ArCanvas/<WaitForDestruction>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForDestructionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CA287D2235C195CEFDD9E04552A69626B40D047 (void);
// 0x0000001A System.Void ArCanvas/<WaitForDestruction>d__12::System.Collections.IEnumerator.Reset()
extern void U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_Reset_mD78597C884C1CF484621AD6E3C4C804606516418 (void);
// 0x0000001B System.Object ArCanvas/<WaitForDestruction>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_get_Current_m77788F0D9F379C55D07A5A826372D68CDE3BE01E (void);
// 0x0000001C System.Void ARTapToPlace::Awake()
extern void ARTapToPlace_Awake_m749715AA31BC8E5EE2CFAF15537E12D247CA734C (void);
// 0x0000001D System.Boolean ARTapToPlace::TryGetTouchPosition(UnityEngine.Vector2&)
extern void ARTapToPlace_TryGetTouchPosition_m852168C24C6C48EFD8EBE81BF129AADE5B0C4CCC (void);
// 0x0000001E System.Void ARTapToPlace::Update()
extern void ARTapToPlace_Update_m556D3AD3B840B808FFFCB39191E4830192B5485F (void);
// 0x0000001F System.Collections.IEnumerator ARTapToPlace::WaitForBaseAnimation()
extern void ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C (void);
// 0x00000020 System.Void ARTapToPlace::.ctor()
extern void ARTapToPlace__ctor_mACE9DBBF058C946B4468A98EC63AF1DF7E7C43FF (void);
// 0x00000021 System.Void ARTapToPlace::.cctor()
extern void ARTapToPlace__cctor_m21790C08373A84D2B9B2453AB8162449818B8955 (void);
// 0x00000022 System.Void ARTapToPlace/<WaitForBaseAnimation>d__9::.ctor(System.Int32)
extern void U3CWaitForBaseAnimationU3Ed__9__ctor_m215F7E1C7A95B775D4A6C3685A88C3828952C953 (void);
// 0x00000023 System.Void ARTapToPlace/<WaitForBaseAnimation>d__9::System.IDisposable.Dispose()
extern void U3CWaitForBaseAnimationU3Ed__9_System_IDisposable_Dispose_m2355711058AABE72EE0B95B964549A077133EB08 (void);
// 0x00000024 System.Boolean ARTapToPlace/<WaitForBaseAnimation>d__9::MoveNext()
extern void U3CWaitForBaseAnimationU3Ed__9_MoveNext_m3C9E46D6C6E5049E28F6CEF15D3D6B63101D9830 (void);
// 0x00000025 System.Object ARTapToPlace/<WaitForBaseAnimation>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D276B8AF22A1B90B8BDA3BF2B2433684547E0F8 (void);
// 0x00000026 System.Void ARTapToPlace/<WaitForBaseAnimation>d__9::System.Collections.IEnumerator.Reset()
extern void U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_Reset_m2583B91C2D4528E3F0229447698504EB78845413 (void);
// 0x00000027 System.Object ARTapToPlace/<WaitForBaseAnimation>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_get_Current_mBD090FEC64CA9B04035490B794A98559F185CE3B (void);
// 0x00000028 System.Void ARTapToPlaceAvatar::Awake()
extern void ARTapToPlaceAvatar_Awake_m87AF3A614FB8CA4A5A42B4E39158BBB14B0C5493 (void);
// 0x00000029 System.Boolean ARTapToPlaceAvatar::TryGetTouchPosition(UnityEngine.Vector2&)
extern void ARTapToPlaceAvatar_TryGetTouchPosition_m7989AD5A9E5D9E7F0B78E11A45CBF91812C149B0 (void);
// 0x0000002A System.Void ARTapToPlaceAvatar::Update()
extern void ARTapToPlaceAvatar_Update_m51408E6611CE2E7331243B57C12F6E3EA3C32850 (void);
// 0x0000002B System.Void ARTapToPlaceAvatar::.ctor()
extern void ARTapToPlaceAvatar__ctor_m3E5DD5CB3D1730710E0918224BB165454CE34947 (void);
// 0x0000002C System.Void ARTapToPlaceAvatar::.cctor()
extern void ARTapToPlaceAvatar__cctor_m87D258890B7CA9FECDFF2EB8D3A4F95B97758FC6 (void);
// 0x0000002D System.Void DragObject1::OnMouseDown()
extern void DragObject1_OnMouseDown_m4EDE2CBD6710F1D5A92CF284BC445D9CA169783B (void);
// 0x0000002E System.Void DragObject1::OnMouseDrag()
extern void DragObject1_OnMouseDrag_m0B316483CDEBF2D42FCBCAEF9FCF0F0C1E975C83 (void);
// 0x0000002F System.Void DragObject1::.ctor()
extern void DragObject1__ctor_m8030BF795D44667E18A6D43A020CE65078729ADD (void);
// 0x00000030 System.Void LookAtCamera::Start()
extern void LookAtCamera_Start_mCFE758FBB48D39BDD130AC7A6FAF30711DBFE8A6 (void);
// 0x00000031 System.Void LookAtCamera::Update()
extern void LookAtCamera_Update_m6D9202748A260CEBA5420C43FA50844A642FACFB (void);
// 0x00000032 System.Void LookAtCamera::.ctor()
extern void LookAtCamera__ctor_mCA03F526C152A3EE7381FC8F3E232BB791884238 (void);
// 0x00000033 System.Void MyDrag::Start()
extern void MyDrag_Start_m4A84FE502B25439D102526CE0A287002A0D6BE61 (void);
// 0x00000034 System.Void MyDrag::Update()
extern void MyDrag_Update_m83F87C06848FC0324F2F711559DC62C482FEB2C8 (void);
// 0x00000035 System.Void MyDrag::Move()
extern void MyDrag_Move_m990743428A7BA99499B38DEDC4FABBC548C876B5 (void);
// 0x00000036 System.Void MyDrag::.ctor()
extern void MyDrag__ctor_mDD8B0C6C518ADF555A75B5F63798C3F8ADEE7E6C (void);
// 0x00000037 System.Void ParslAvatarController::Start()
extern void ParslAvatarController_Start_m041B82DA2972CE14E6D62FDE7C77CF64C7A6742C (void);
// 0x00000038 System.Void ParslAvatarController::ModelLoad(System.Int32)
extern void ParslAvatarController_ModelLoad_m64A42862A4C706201748ACFB7C5252BA48B34492 (void);
// 0x00000039 System.Void ParslAvatarController::Emote(System.Int32)
extern void ParslAvatarController_Emote_mBCFA7715DD793D0FD92B38D934F5F3434B39C449 (void);
// 0x0000003A System.Collections.IEnumerator ParslAvatarController::WaitForBaseAnimation()
extern void ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5 (void);
// 0x0000003B System.Void ParslAvatarController::ResetScene()
extern void ParslAvatarController_ResetScene_mBAAC05A75FCB803B8376B8B309400F489160132C (void);
// 0x0000003C System.Void ParslAvatarController::Back()
extern void ParslAvatarController_Back_m6EE03CC0CC8E42395AD6699CA1D9D97B0175244E (void);
// 0x0000003D System.Void ParslAvatarController::_cancleConfirmation()
extern void ParslAvatarController__cancleConfirmation_m697C1E42E74CB344782036B4ADF387E2BD0CF6B1 (void);
// 0x0000003E System.Void ParslAvatarController::_confirmConfirmation()
extern void ParslAvatarController__confirmConfirmation_m873AFAFA72BB21917917079FD0AAB17EEB43C097 (void);
// 0x0000003F System.Void ParslAvatarController::UploadCharacterValues()
extern void ParslAvatarController_UploadCharacterValues_m3E8E5C574E7BBD26922F30ED6E92338E2857D3F0 (void);
// 0x00000040 System.Void ParslAvatarController::MaleCharacterButton()
extern void ParslAvatarController_MaleCharacterButton_m79839D62D6B8A94760178FCF4455FD9782F7E523 (void);
// 0x00000041 System.Void ParslAvatarController::FemaleCharacterButton()
extern void ParslAvatarController_FemaleCharacterButton_m94C1C272FB4A6423751C4EB6DEAEA7CD1C7D24A4 (void);
// 0x00000042 System.Void ParslAvatarController::ChildCharacterButton()
extern void ParslAvatarController_ChildCharacterButton_mE994338D8C51860F2BAB23F5F79E3A90A5759758 (void);
// 0x00000043 System.Void ParslAvatarController::.ctor()
extern void ParslAvatarController__ctor_mC0DBE4814F323F6F9B28828E1DA67849F2181508 (void);
// 0x00000044 System.Void ParslAvatarController/NativeAPI::avatarModule(System.String,System.String)
extern void NativeAPI_avatarModule_m111BF0F2A32BB0C44F38FD36BC7A3B35742A5D4F (void);
// 0x00000045 System.Void ParslAvatarController/NativeAPI::.ctor()
extern void NativeAPI__ctor_m0D84FDA583D2D7DDD542EC04BDCA079EC0AC2AD0 (void);
// 0x00000046 System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::.ctor(System.Int32)
extern void U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B (void);
// 0x00000047 System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::System.IDisposable.Dispose()
extern void U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76 (void);
// 0x00000048 System.Boolean ParslAvatarController/<WaitForBaseAnimation>d__13::MoveNext()
extern void U3CWaitForBaseAnimationU3Ed__13_MoveNext_m6F2C1477856D8F875A4478284439B8BC8AF8E116 (void);
// 0x00000049 System.Object ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2 (void);
// 0x0000004A System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3 (void);
// 0x0000004B System.Object ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648 (void);
// 0x0000004C System.Boolean PlacementObject::get_Selected()
extern void PlacementObject_get_Selected_m016672B022C1F4823D54D5D6BD75BF99BFE5870A (void);
// 0x0000004D System.Void PlacementObject::set_Selected(System.Boolean)
extern void PlacementObject_set_Selected_mB630BAA0B59D7CE262F123ABA59C77BA8A3CD47E (void);
// 0x0000004E System.Boolean PlacementObject::get_Locked()
extern void PlacementObject_get_Locked_mC14FA7656C55BAB831982E7E1F076CA9F22B0431 (void);
// 0x0000004F System.Void PlacementObject::set_Locked(System.Boolean)
extern void PlacementObject_set_Locked_m60DAD119123DDD5179F6A1C9B6771EC1D2F3845E (void);
// 0x00000050 System.Void PlacementObject::SetOverlayText(System.String)
extern void PlacementObject_SetOverlayText_mF8D94744BC43A38F63855EC7A4E2229CAB983608 (void);
// 0x00000051 System.Void PlacementObject::Awake()
extern void PlacementObject_Awake_mED0B25739C123D2B3EB209A59FE66CAA19F26E2B (void);
// 0x00000052 System.Void PlacementObject::ToggleOverlay()
extern void PlacementObject_ToggleOverlay_m0D61DEBC8FD95937B8FE1C7EE641F301728E5D06 (void);
// 0x00000053 System.Void PlacementObject::ToggleCanvas()
extern void PlacementObject_ToggleCanvas_mD425EA9347677BE51246528006BBE50FE96D1755 (void);
// 0x00000054 System.Void PlacementObject::.ctor()
extern void PlacementObject__ctor_m424EC3A34F908843DA88EF32023510705ADB7534 (void);
// 0x00000055 UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::get_PlacedPrefab()
extern void PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m47A5436759E4FFA877926D91D10C3EA3E0443CA4 (void);
// 0x00000056 System.Void PlacementWithMultipleDraggingDroppingController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m5A1DF8B13B979828211555EF49FE24E0BEC306C4 (void);
// 0x00000057 System.Void PlacementWithMultipleDraggingDroppingController::Awake()
extern void PlacementWithMultipleDraggingDroppingController_Awake_mEA748B763BFF99C724A27BECA9507A967A79D38C (void);
// 0x00000058 System.Void PlacementWithMultipleDraggingDroppingController::ChangePrefabSelection(System.String)
extern void PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mCA3844D4E77D8203E8C62556847709F2B4640B64 (void);
// 0x00000059 System.Void PlacementWithMultipleDraggingDroppingController::Dismiss()
extern void PlacementWithMultipleDraggingDroppingController_Dismiss_m5AB0F7E5623981820F41C12A7961B2248676FE2B (void);
// 0x0000005A System.Void PlacementWithMultipleDraggingDroppingController::Update()
extern void PlacementWithMultipleDraggingDroppingController_Update_m84750C36697703609A9686184BC3369C275C4257 (void);
// 0x0000005B System.Void PlacementWithMultipleDraggingDroppingController::.ctor()
extern void PlacementWithMultipleDraggingDroppingController__ctor_mDB3EA5A58979804F0E380438760B4250B16125F2 (void);
// 0x0000005C System.Void PlacementWithMultipleDraggingDroppingController::.cctor()
extern void PlacementWithMultipleDraggingDroppingController__cctor_mAF496DA2B4A28200039E39F19FF8D22344651598 (void);
// 0x0000005D System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_0()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144 (void);
// 0x0000005E System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_1()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59 (void);
// 0x0000005F System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_2()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E (void);
// 0x00000060 System.Void ReturnCartList::.ctor()
extern void ReturnCartList__ctor_mFBCE2C6653EFFEACC54DC0C6D65E0ED43609E58A (void);
// 0x00000061 System.Void CSharpscaling::Update()
extern void CSharpscaling_Update_m522915C0C9FD444E0BFEAD25993C4C208260247C (void);
// 0x00000062 System.Void CSharpscaling::.ctor()
extern void CSharpscaling__ctor_mED682225400E498E39AE9C4EA60E4C4B7C770197 (void);
// 0x00000063 System.Void onClickForScaling::OnMouseDown()
extern void onClickForScaling_OnMouseDown_mA0759701B4EF289EB795C08A59E104DF6F436B6B (void);
// 0x00000064 System.Void onClickForScaling::.ctor()
extern void onClickForScaling__ctor_m5190794CA361BAA49FC175F6ACC0C60A0A29AB60 (void);
// 0x00000065 System.Void rotateController::Update()
extern void rotateController_Update_mB71E56B5BD55A3F24C437AECFD9242ED257BA1BF (void);
// 0x00000066 System.Void rotateController::OnMouseDown()
extern void rotateController_OnMouseDown_m284DC0D713B8D7AD1742183B753D6EC45B72A26D (void);
// 0x00000067 System.Void rotateController::OnMouseUp()
extern void rotateController_OnMouseUp_m6EF7A213E4D61D029634814A82DE1727BB0D0564 (void);
// 0x00000068 System.Void rotateController::.ctor()
extern void rotateController__ctor_mABBEC5D25C68F797F37D0C8F6C847FE9D1714078 (void);
// 0x00000069 System.Void API::GetBundleObject(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void API_GetBundleObject_m1F40F48A9C8EC9F9A629DE6EF5DA168D632116A7 (void);
// 0x0000006A System.Collections.IEnumerator API::WaitForNetwork()
extern void API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5 (void);
// 0x0000006B System.Collections.IEnumerator API::WaitForValidAsset()
extern void API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36 (void);
// 0x0000006C System.Collections.IEnumerator API::GetDisplayBundleRoutine(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546 (void);
// 0x0000006D System.Void API::.ctor()
extern void API__ctor_m9FD4A0A4E81EA585D18E1B9B87805059B0A0C576 (void);
// 0x0000006E System.Void API/<WaitForNetwork>d__3::.ctor(System.Int32)
extern void U3CWaitForNetworkU3Ed__3__ctor_mE02ACBFA8B9BDE9C1100F2FE2C9D9AECB2FC7E49 (void);
// 0x0000006F System.Void API/<WaitForNetwork>d__3::System.IDisposable.Dispose()
extern void U3CWaitForNetworkU3Ed__3_System_IDisposable_Dispose_mA582B2818076E9F47A31D65528C783BFA85AF45D (void);
// 0x00000070 System.Boolean API/<WaitForNetwork>d__3::MoveNext()
extern void U3CWaitForNetworkU3Ed__3_MoveNext_mE361F4881D0348EECB006B04F2167DF71710CC62 (void);
// 0x00000071 System.Object API/<WaitForNetwork>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForNetworkU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8DE4374A2E804BCC916100B758490B3E22166E8 (void);
// 0x00000072 System.Void API/<WaitForNetwork>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_Reset_mED0C0255F595058B67D42CC3F1DDA280C401940E (void);
// 0x00000073 System.Object API/<WaitForNetwork>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_get_Current_mD260A9607232B73066C5B0C394F7D003122B1426 (void);
// 0x00000074 System.Void API/<WaitForValidAsset>d__4::.ctor(System.Int32)
extern void U3CWaitForValidAssetU3Ed__4__ctor_m627C75E16CFFC21E43C85FD808F084C23F879467 (void);
// 0x00000075 System.Void API/<WaitForValidAsset>d__4::System.IDisposable.Dispose()
extern void U3CWaitForValidAssetU3Ed__4_System_IDisposable_Dispose_mB6E1F5FE34190BD568795E698854F919F42D2517 (void);
// 0x00000076 System.Boolean API/<WaitForValidAsset>d__4::MoveNext()
extern void U3CWaitForValidAssetU3Ed__4_MoveNext_m33180D67981B0A14AE999AF6DAAA89C1144835BA (void);
// 0x00000077 System.Object API/<WaitForValidAsset>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForValidAssetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED3D6C9EFA62A88C3A5F99225082645FDB40FDC (void);
// 0x00000078 System.Void API/<WaitForValidAsset>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_Reset_mA05D4C7FEDAFADD6FB5F7E05B39C639C9BE1DE91 (void);
// 0x00000079 System.Object API/<WaitForValidAsset>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_get_Current_m78ABB904C66128E52D3E0864049B385782F97A1F (void);
// 0x0000007A System.Void API/<GetDisplayBundleRoutine>d__5::.ctor(System.Int32)
extern void U3CGetDisplayBundleRoutineU3Ed__5__ctor_m8724EB67A9122B2F4302CE1F9BEF508FB312A75D (void);
// 0x0000007B System.Void API/<GetDisplayBundleRoutine>d__5::System.IDisposable.Dispose()
extern void U3CGetDisplayBundleRoutineU3Ed__5_System_IDisposable_Dispose_mFA88082E025A34CC58193233DE3A7CBB0A7C7447 (void);
// 0x0000007C System.Boolean API/<GetDisplayBundleRoutine>d__5::MoveNext()
extern void U3CGetDisplayBundleRoutineU3Ed__5_MoveNext_m406B7396E9CF33CBFF190C8F9192F01AB01E6C09 (void);
// 0x0000007D System.Object API/<GetDisplayBundleRoutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648F99BDB79263F8C23AE2C426D0CA80404214F7 (void);
// 0x0000007E System.Void API/<GetDisplayBundleRoutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m198D96D61D02806686153B9C781E94538B092175 (void);
// 0x0000007F System.Object API/<GetDisplayBundleRoutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_mBF0E7BDDDD893BF314F4258A88B8286D6757E266 (void);
// 0x00000080 System.Void ContentController::LoadContent(System.String)
extern void ContentController_LoadContent_m89D12A8DC1117E9DA3713D745572A20AD3AF0A1A (void);
// 0x00000081 System.Void ContentController::OnContentLoaded(UnityEngine.GameObject)
extern void ContentController_OnContentLoaded_m5BF69A7D1CE991B153CE5FD099EEA464DA70866F (void);
// 0x00000082 System.Void ContentController::DestroyAllChildren()
extern void ContentController_DestroyAllChildren_m1FFD90643F2B1719A51F88A717E300F588AC9BF9 (void);
// 0x00000083 System.Void ContentController::.ctor()
extern void ContentController__ctor_m00468CB1576EC1D2FB13CAF80FA88D8AE47E2FFF (void);
// 0x00000084 System.Void PlaceContent::Update()
extern void PlaceContent_Update_mF3967E7E13C6EEABF0D7EB88616C57AE4E932781 (void);
// 0x00000085 System.Boolean PlaceContent::IsClickOverUI()
extern void PlaceContent_IsClickOverUI_m908B17F5232D4EEA755D4CD9506789DDDA480B5E (void);
// 0x00000086 System.Void PlaceContent::.ctor()
extern void PlaceContent__ctor_mF4A2CFE6250D7C4D33FF40C14C7AA78CB76B967E (void);
// 0x00000087 System.Void ToggleAR::OnValueChanged(System.Boolean)
extern void ToggleAR_OnValueChanged_m32E23F57B684D6AA4FF40229CF99D47F2A5EAE43 (void);
// 0x00000088 System.Void ToggleAR::VisualizePlanes(System.Boolean)
extern void ToggleAR_VisualizePlanes_m64F47DB36EF2D3AA7A8E81F35BF1F5FF584F7073 (void);
// 0x00000089 System.Void ToggleAR::VisualizePoints(System.Boolean)
extern void ToggleAR_VisualizePoints_m9362E15B317E73997AF22D5575F32F799D489E97 (void);
// 0x0000008A System.Void ToggleAR::.ctor()
extern void ToggleAR__ctor_mA1CC296DD72AA9242A6D6153B5DD8336C03C3EF1 (void);
// 0x0000008B System.Void Activate::Start()
extern void Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A (void);
// 0x0000008C System.Void Activate::Update()
extern void Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4 (void);
// 0x0000008D System.Void Activate::assss()
extern void Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD (void);
// 0x0000008E System.Void Activate::.ctor()
extern void Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C (void);
// 0x0000008F System.Void Anim::Update()
extern void Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46 (void);
// 0x00000090 System.Void Anim::playanimation()
extern void Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B (void);
// 0x00000091 System.Void Anim::stand()
extern void Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21 (void);
// 0x00000092 System.Collections.IEnumerator Anim::delay()
extern void Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3 (void);
// 0x00000093 System.Void Anim::.ctor()
extern void Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8 (void);
// 0x00000094 System.Void Anim/<delay>d__3::.ctor(System.Int32)
extern void U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD (void);
// 0x00000095 System.Void Anim/<delay>d__3::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A (void);
// 0x00000096 System.Boolean Anim/<delay>d__3::MoveNext()
extern void U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF (void);
// 0x00000097 System.Object Anim/<delay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26 (void);
// 0x00000098 System.Void Anim/<delay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521 (void);
// 0x00000099 System.Object Anim/<delay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D (void);
// 0x0000009A System.Void ObjectSpawner::Start()
extern void ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE (void);
// 0x0000009B System.Void ObjectSpawner::PassControlToNative()
extern void ObjectSpawner_PassControlToNative_mEC1B416BBF088332876823DF721F0BF66704D1AF (void);
// 0x0000009C System.Collections.IEnumerator ObjectSpawner::WaitForButtonOff()
extern void ObjectSpawner_WaitForButtonOff_m784D4105069E2BDAB7F6DDADD1D6B282B6B24E19 (void);
// 0x0000009D System.Void ObjectSpawner::ControlTransferButtonActive()
extern void ObjectSpawner_ControlTransferButtonActive_m205E2F249A1DD4CD40F2004F116EEB6481BA0148 (void);
// 0x0000009E System.Void ObjectSpawner::AlertForReloading()
extern void ObjectSpawner_AlertForReloading_m955F5E161A4CEB04B549A8B55417167B7DEFCE99 (void);
// 0x0000009F System.Void ObjectSpawner::AlertYes()
extern void ObjectSpawner_AlertYes_m820A1B8E0CFE132FC51E7B18443D31D090EA9CD7 (void);
// 0x000000A0 System.Void ObjectSpawner::AlertNo()
extern void ObjectSpawner_AlertNo_mB59A9902EF7BC7D928A748FD01A71B74B6FB2424 (void);
// 0x000000A1 System.Void ObjectSpawner::ReLoadUnity()
extern void ObjectSpawner_ReLoadUnity_m6C86083ACC3A8BA0C4ED0AAB731B1B5A6021FD89 (void);
// 0x000000A2 System.Void ObjectSpawner::Activate(System.String)
extern void ObjectSpawner_Activate_m34247E7E02036EBF855FE054FE1FFC3EC525CCA8 (void);
// 0x000000A3 System.Collections.IEnumerator ObjectSpawner::WaitForDownload()
extern void ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E (void);
// 0x000000A4 System.Void ObjectSpawner::Redeem(System.String)
extern void ObjectSpawner_Redeem_m5BED17761A501AE11EAD3E0C68D452B096FE9E70 (void);
// 0x000000A5 System.Collections.IEnumerator ObjectSpawner::WaitForRedeemDownload()
extern void ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524 (void);
// 0x000000A6 System.Void ObjectSpawner::RedeemCharacter(System.String)
extern void ObjectSpawner_RedeemCharacter_m85A125750ED5B2CC129A4BD56C3298D299AE7196 (void);
// 0x000000A7 System.Void ObjectSpawner::CreateAvatarScreen()
extern void ObjectSpawner_CreateAvatarScreen_mB2A411FBDB5848724F8C6B2E8B080776063DBDA9 (void);
// 0x000000A8 System.Void ObjectSpawner::.ctor()
extern void ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3 (void);
// 0x000000A9 System.Void ObjectSpawner/NativeAPI::showContainerView(System.String)
extern void NativeAPI_showContainerView_m48458AEB529CFEEBFA21C2C8F61A81573B7FEE78 (void);
// 0x000000AA System.Void ObjectSpawner/NativeAPI::sendMessageOnReload(System.String)
extern void NativeAPI_sendMessageOnReload_mE3443F746520DB3FD3909EBC62598F438DE45771 (void);
// 0x000000AB System.Void ObjectSpawner/NativeAPI::modelDownloaded(System.Boolean)
extern void NativeAPI_modelDownloaded_m2CB305BC0327B63C44CFB00C1905881618AE07F6 (void);
// 0x000000AC System.Void ObjectSpawner/NativeAPI::modelDownloadingDone(System.String)
extern void NativeAPI_modelDownloadingDone_mA22BB2476C75156FAF3A540FC40E485B7A6C1036 (void);
// 0x000000AD System.Void ObjectSpawner/NativeAPI::.ctor()
extern void NativeAPI__ctor_m83D6870123E118D4FD44168A1A5F4D593D1D212C (void);
// 0x000000AE System.Void ObjectSpawner/<WaitForButtonOff>d__12::.ctor(System.Int32)
extern void U3CWaitForButtonOffU3Ed__12__ctor_m0B82BD042726E7B3797C9358FD5605F45A1A7508 (void);
// 0x000000AF System.Void ObjectSpawner/<WaitForButtonOff>d__12::System.IDisposable.Dispose()
extern void U3CWaitForButtonOffU3Ed__12_System_IDisposable_Dispose_mC3D095F7B5A65ECD38652C017497B6AF83948D53 (void);
// 0x000000B0 System.Boolean ObjectSpawner/<WaitForButtonOff>d__12::MoveNext()
extern void U3CWaitForButtonOffU3Ed__12_MoveNext_mC4AF31B247869CB92E06D9FA9FA7703519CC1645 (void);
// 0x000000B1 System.Object ObjectSpawner/<WaitForButtonOff>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForButtonOffU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEC800CCAFC224E0F90179E76696E0E1079DB901 (void);
// 0x000000B2 System.Void ObjectSpawner/<WaitForButtonOff>d__12::System.Collections.IEnumerator.Reset()
extern void U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_Reset_m2003529E6D7AE8870E962520167593F92E76B873 (void);
// 0x000000B3 System.Object ObjectSpawner/<WaitForButtonOff>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_get_Current_mC5AD80BA36B82FEB132B2EE119D3A9324EE9E28F (void);
// 0x000000B4 System.Void ObjectSpawner/<WaitForDownload>d__22::.ctor(System.Int32)
extern void U3CWaitForDownloadU3Ed__22__ctor_m3BA7AAE197C8D4624C14D097777110B6391D394A (void);
// 0x000000B5 System.Void ObjectSpawner/<WaitForDownload>d__22::System.IDisposable.Dispose()
extern void U3CWaitForDownloadU3Ed__22_System_IDisposable_Dispose_mDE911E9E6F03D450DB69669BA000CCA33245D6CB (void);
// 0x000000B6 System.Boolean ObjectSpawner/<WaitForDownload>d__22::MoveNext()
extern void U3CWaitForDownloadU3Ed__22_MoveNext_m6D8CC7E7503176C6543A45AE087E393A00E76169 (void);
// 0x000000B7 System.Object ObjectSpawner/<WaitForDownload>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForDownloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F13D8099C675D59A1A966576A14473AB3FD033 (void);
// 0x000000B8 System.Void ObjectSpawner/<WaitForDownload>d__22::System.Collections.IEnumerator.Reset()
extern void U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_Reset_m299B6E03721A150377174B736F6E0CE7CBDA6448 (void);
// 0x000000B9 System.Object ObjectSpawner/<WaitForDownload>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_get_Current_mC825DCF994513F03F8C8B893C90468134788362F (void);
// 0x000000BA System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::.ctor(System.Int32)
extern void U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1 (void);
// 0x000000BB System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::System.IDisposable.Dispose()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022 (void);
// 0x000000BC System.Boolean ObjectSpawner/<WaitForRedeemDownload>d__27::MoveNext()
extern void U3CWaitForRedeemDownloadU3Ed__27_MoveNext_m0D750FD47DAC5AB2A6AFFBD69B56EED050C54F30 (void);
// 0x000000BD System.Object ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0 (void);
// 0x000000BE System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015 (void);
// 0x000000BF System.Object ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91 (void);
// 0x000000C0 System.Void PlacementIndicator::Start()
extern void PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200 (void);
// 0x000000C1 System.Void PlacementIndicator::Update()
extern void PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4 (void);
// 0x000000C2 System.Void PlacementIndicator::.ctor()
extern void PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE (void);
// 0x000000C3 System.Void Lean.Touch.LeanTranslateSmooth::.ctor()
extern void LeanTranslateSmooth__ctor_mD70387CD36585DFA9BE86364AD114FF23647BB14 (void);
static Il2CppMethodPointer s_methodPointers[195] = 
{
	AddToCartScript_AddToCartItem_m69D02A77BE080494C9A091DB293A2E725AA3E65E,
	AddToCartScript__ctor_mDA5D0F9A8DAFE087715F080CB77A9C4CA8FF9D7B,
	AnimateModel_Start_m120DD4C0901DC917E52C9F1215768D3E5E6FD3BC,
	AnimateModel_Update_m6E878217EC241361AFD3591C94B49632761387F5,
	AnimateModel__ctor_m828FACE086F16333DCEBEC48AD64F71755330922,
	ArCanvas_ShowCanvas_mD33E15395281214ECFDF20787F7211EB6FB1AFD6,
	ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3,
	ArCanvas_Start_mEFBBCCF7FF62B5A813B12BC305686106788BB645,
	ArCanvas_RemoveFromScene_mEA7ECC175F7F9E44258C2100E18FDAF94AC13354,
	ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B,
	ArCanvas_AddToCartPanelPop_m41789DA41554D1AD4F1E26D4E925A5E3275E4CC2,
	ArCanvas__ctor_mB28079D2DF6AA059F08E3182D22070D36670D212,
	NativeAPI_sendMessageToMobileApp_m1E9509CED0236FABB9BCB35627FFBDD2707C662C,
	NativeAPI_destroyUnity_m447226448B75964021C0576B101B825FB832F999,
	NativeAPI__ctor_m590217DC4297E301A3A81840213D71E04843D643,
	U3CWaitForPanelOffU3Ed__8__ctor_mF6B6D61ED9803BAA7972088214215CD50BA87DA3,
	U3CWaitForPanelOffU3Ed__8_System_IDisposable_Dispose_m878EE75B726025F5D9D841AD3C4FF97E0AD93A31,
	U3CWaitForPanelOffU3Ed__8_MoveNext_m7DDF13041D5008E313E1A3559CCDE302ACEC8D6B,
	U3CWaitForPanelOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47D83E3B2E1D848C93B56942BA0221FF00148CA0,
	U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_Reset_mDEAFCA4F8352195F9810D55DD864B6B429C3F31D,
	U3CWaitForPanelOffU3Ed__8_System_Collections_IEnumerator_get_Current_m438E91004CD053B214798E0365536CEF2A4D0B81,
	U3CWaitForDestructionU3Ed__12__ctor_mDF11A65818476D125AAD342CD06AC559289159F6,
	U3CWaitForDestructionU3Ed__12_System_IDisposable_Dispose_m989B47B6D360334473E353EDB842D3286D69C4F6,
	U3CWaitForDestructionU3Ed__12_MoveNext_m150A621D43C3EFF8C1E20DD278F04CE4B5623E17,
	U3CWaitForDestructionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5CA287D2235C195CEFDD9E04552A69626B40D047,
	U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_Reset_mD78597C884C1CF484621AD6E3C4C804606516418,
	U3CWaitForDestructionU3Ed__12_System_Collections_IEnumerator_get_Current_m77788F0D9F379C55D07A5A826372D68CDE3BE01E,
	ARTapToPlace_Awake_m749715AA31BC8E5EE2CFAF15537E12D247CA734C,
	ARTapToPlace_TryGetTouchPosition_m852168C24C6C48EFD8EBE81BF129AADE5B0C4CCC,
	ARTapToPlace_Update_m556D3AD3B840B808FFFCB39191E4830192B5485F,
	ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C,
	ARTapToPlace__ctor_mACE9DBBF058C946B4468A98EC63AF1DF7E7C43FF,
	ARTapToPlace__cctor_m21790C08373A84D2B9B2453AB8162449818B8955,
	U3CWaitForBaseAnimationU3Ed__9__ctor_m215F7E1C7A95B775D4A6C3685A88C3828952C953,
	U3CWaitForBaseAnimationU3Ed__9_System_IDisposable_Dispose_m2355711058AABE72EE0B95B964549A077133EB08,
	U3CWaitForBaseAnimationU3Ed__9_MoveNext_m3C9E46D6C6E5049E28F6CEF15D3D6B63101D9830,
	U3CWaitForBaseAnimationU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D276B8AF22A1B90B8BDA3BF2B2433684547E0F8,
	U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_Reset_m2583B91C2D4528E3F0229447698504EB78845413,
	U3CWaitForBaseAnimationU3Ed__9_System_Collections_IEnumerator_get_Current_mBD090FEC64CA9B04035490B794A98559F185CE3B,
	ARTapToPlaceAvatar_Awake_m87AF3A614FB8CA4A5A42B4E39158BBB14B0C5493,
	ARTapToPlaceAvatar_TryGetTouchPosition_m7989AD5A9E5D9E7F0B78E11A45CBF91812C149B0,
	ARTapToPlaceAvatar_Update_m51408E6611CE2E7331243B57C12F6E3EA3C32850,
	ARTapToPlaceAvatar__ctor_m3E5DD5CB3D1730710E0918224BB165454CE34947,
	ARTapToPlaceAvatar__cctor_m87D258890B7CA9FECDFF2EB8D3A4F95B97758FC6,
	DragObject1_OnMouseDown_m4EDE2CBD6710F1D5A92CF284BC445D9CA169783B,
	DragObject1_OnMouseDrag_m0B316483CDEBF2D42FCBCAEF9FCF0F0C1E975C83,
	DragObject1__ctor_m8030BF795D44667E18A6D43A020CE65078729ADD,
	LookAtCamera_Start_mCFE758FBB48D39BDD130AC7A6FAF30711DBFE8A6,
	LookAtCamera_Update_m6D9202748A260CEBA5420C43FA50844A642FACFB,
	LookAtCamera__ctor_mCA03F526C152A3EE7381FC8F3E232BB791884238,
	MyDrag_Start_m4A84FE502B25439D102526CE0A287002A0D6BE61,
	MyDrag_Update_m83F87C06848FC0324F2F711559DC62C482FEB2C8,
	MyDrag_Move_m990743428A7BA99499B38DEDC4FABBC548C876B5,
	MyDrag__ctor_mDD8B0C6C518ADF555A75B5F63798C3F8ADEE7E6C,
	ParslAvatarController_Start_m041B82DA2972CE14E6D62FDE7C77CF64C7A6742C,
	ParslAvatarController_ModelLoad_m64A42862A4C706201748ACFB7C5252BA48B34492,
	ParslAvatarController_Emote_mBCFA7715DD793D0FD92B38D934F5F3434B39C449,
	ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5,
	ParslAvatarController_ResetScene_mBAAC05A75FCB803B8376B8B309400F489160132C,
	ParslAvatarController_Back_m6EE03CC0CC8E42395AD6699CA1D9D97B0175244E,
	ParslAvatarController__cancleConfirmation_m697C1E42E74CB344782036B4ADF387E2BD0CF6B1,
	ParslAvatarController__confirmConfirmation_m873AFAFA72BB21917917079FD0AAB17EEB43C097,
	ParslAvatarController_UploadCharacterValues_m3E8E5C574E7BBD26922F30ED6E92338E2857D3F0,
	ParslAvatarController_MaleCharacterButton_m79839D62D6B8A94760178FCF4455FD9782F7E523,
	ParslAvatarController_FemaleCharacterButton_m94C1C272FB4A6423751C4EB6DEAEA7CD1C7D24A4,
	ParslAvatarController_ChildCharacterButton_mE994338D8C51860F2BAB23F5F79E3A90A5759758,
	ParslAvatarController__ctor_mC0DBE4814F323F6F9B28828E1DA67849F2181508,
	NativeAPI_avatarModule_m111BF0F2A32BB0C44F38FD36BC7A3B35742A5D4F,
	NativeAPI__ctor_m0D84FDA583D2D7DDD542EC04BDCA079EC0AC2AD0,
	U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B,
	U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76,
	U3CWaitForBaseAnimationU3Ed__13_MoveNext_m6F2C1477856D8F875A4478284439B8BC8AF8E116,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648,
	PlacementObject_get_Selected_m016672B022C1F4823D54D5D6BD75BF99BFE5870A,
	PlacementObject_set_Selected_mB630BAA0B59D7CE262F123ABA59C77BA8A3CD47E,
	PlacementObject_get_Locked_mC14FA7656C55BAB831982E7E1F076CA9F22B0431,
	PlacementObject_set_Locked_m60DAD119123DDD5179F6A1C9B6771EC1D2F3845E,
	PlacementObject_SetOverlayText_mF8D94744BC43A38F63855EC7A4E2229CAB983608,
	PlacementObject_Awake_mED0B25739C123D2B3EB209A59FE66CAA19F26E2B,
	PlacementObject_ToggleOverlay_m0D61DEBC8FD95937B8FE1C7EE641F301728E5D06,
	PlacementObject_ToggleCanvas_mD425EA9347677BE51246528006BBE50FE96D1755,
	PlacementObject__ctor_m424EC3A34F908843DA88EF32023510705ADB7534,
	PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m47A5436759E4FFA877926D91D10C3EA3E0443CA4,
	PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m5A1DF8B13B979828211555EF49FE24E0BEC306C4,
	PlacementWithMultipleDraggingDroppingController_Awake_mEA748B763BFF99C724A27BECA9507A967A79D38C,
	PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mCA3844D4E77D8203E8C62556847709F2B4640B64,
	PlacementWithMultipleDraggingDroppingController_Dismiss_m5AB0F7E5623981820F41C12A7961B2248676FE2B,
	PlacementWithMultipleDraggingDroppingController_Update_m84750C36697703609A9686184BC3369C275C4257,
	PlacementWithMultipleDraggingDroppingController__ctor_mDB3EA5A58979804F0E380438760B4250B16125F2,
	PlacementWithMultipleDraggingDroppingController__cctor_mAF496DA2B4A28200039E39F19FF8D22344651598,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E,
	ReturnCartList__ctor_mFBCE2C6653EFFEACC54DC0C6D65E0ED43609E58A,
	CSharpscaling_Update_m522915C0C9FD444E0BFEAD25993C4C208260247C,
	CSharpscaling__ctor_mED682225400E498E39AE9C4EA60E4C4B7C770197,
	onClickForScaling_OnMouseDown_mA0759701B4EF289EB795C08A59E104DF6F436B6B,
	onClickForScaling__ctor_m5190794CA361BAA49FC175F6ACC0C60A0A29AB60,
	rotateController_Update_mB71E56B5BD55A3F24C437AECFD9242ED257BA1BF,
	rotateController_OnMouseDown_m284DC0D713B8D7AD1742183B753D6EC45B72A26D,
	rotateController_OnMouseUp_m6EF7A213E4D61D029634814A82DE1727BB0D0564,
	rotateController__ctor_mABBEC5D25C68F797F37D0C8F6C847FE9D1714078,
	API_GetBundleObject_m1F40F48A9C8EC9F9A629DE6EF5DA168D632116A7,
	API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5,
	API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36,
	API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546,
	API__ctor_m9FD4A0A4E81EA585D18E1B9B87805059B0A0C576,
	U3CWaitForNetworkU3Ed__3__ctor_mE02ACBFA8B9BDE9C1100F2FE2C9D9AECB2FC7E49,
	U3CWaitForNetworkU3Ed__3_System_IDisposable_Dispose_mA582B2818076E9F47A31D65528C783BFA85AF45D,
	U3CWaitForNetworkU3Ed__3_MoveNext_mE361F4881D0348EECB006B04F2167DF71710CC62,
	U3CWaitForNetworkU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8DE4374A2E804BCC916100B758490B3E22166E8,
	U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_Reset_mED0C0255F595058B67D42CC3F1DDA280C401940E,
	U3CWaitForNetworkU3Ed__3_System_Collections_IEnumerator_get_Current_mD260A9607232B73066C5B0C394F7D003122B1426,
	U3CWaitForValidAssetU3Ed__4__ctor_m627C75E16CFFC21E43C85FD808F084C23F879467,
	U3CWaitForValidAssetU3Ed__4_System_IDisposable_Dispose_mB6E1F5FE34190BD568795E698854F919F42D2517,
	U3CWaitForValidAssetU3Ed__4_MoveNext_m33180D67981B0A14AE999AF6DAAA89C1144835BA,
	U3CWaitForValidAssetU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED3D6C9EFA62A88C3A5F99225082645FDB40FDC,
	U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_Reset_mA05D4C7FEDAFADD6FB5F7E05B39C639C9BE1DE91,
	U3CWaitForValidAssetU3Ed__4_System_Collections_IEnumerator_get_Current_m78ABB904C66128E52D3E0864049B385782F97A1F,
	U3CGetDisplayBundleRoutineU3Ed__5__ctor_m8724EB67A9122B2F4302CE1F9BEF508FB312A75D,
	U3CGetDisplayBundleRoutineU3Ed__5_System_IDisposable_Dispose_mFA88082E025A34CC58193233DE3A7CBB0A7C7447,
	U3CGetDisplayBundleRoutineU3Ed__5_MoveNext_m406B7396E9CF33CBFF190C8F9192F01AB01E6C09,
	U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m648F99BDB79263F8C23AE2C426D0CA80404214F7,
	U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_Reset_m198D96D61D02806686153B9C781E94538B092175,
	U3CGetDisplayBundleRoutineU3Ed__5_System_Collections_IEnumerator_get_Current_mBF0E7BDDDD893BF314F4258A88B8286D6757E266,
	ContentController_LoadContent_m89D12A8DC1117E9DA3713D745572A20AD3AF0A1A,
	ContentController_OnContentLoaded_m5BF69A7D1CE991B153CE5FD099EEA464DA70866F,
	ContentController_DestroyAllChildren_m1FFD90643F2B1719A51F88A717E300F588AC9BF9,
	ContentController__ctor_m00468CB1576EC1D2FB13CAF80FA88D8AE47E2FFF,
	PlaceContent_Update_mF3967E7E13C6EEABF0D7EB88616C57AE4E932781,
	PlaceContent_IsClickOverUI_m908B17F5232D4EEA755D4CD9506789DDDA480B5E,
	PlaceContent__ctor_mF4A2CFE6250D7C4D33FF40C14C7AA78CB76B967E,
	ToggleAR_OnValueChanged_m32E23F57B684D6AA4FF40229CF99D47F2A5EAE43,
	ToggleAR_VisualizePlanes_m64F47DB36EF2D3AA7A8E81F35BF1F5FF584F7073,
	ToggleAR_VisualizePoints_m9362E15B317E73997AF22D5575F32F799D489E97,
	ToggleAR__ctor_mA1CC296DD72AA9242A6D6153B5DD8336C03C3EF1,
	Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A,
	Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4,
	Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD,
	Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C,
	Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46,
	Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B,
	Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21,
	Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8,
	U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF,
	U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE,
	ObjectSpawner_PassControlToNative_mEC1B416BBF088332876823DF721F0BF66704D1AF,
	ObjectSpawner_WaitForButtonOff_m784D4105069E2BDAB7F6DDADD1D6B282B6B24E19,
	ObjectSpawner_ControlTransferButtonActive_m205E2F249A1DD4CD40F2004F116EEB6481BA0148,
	ObjectSpawner_AlertForReloading_m955F5E161A4CEB04B549A8B55417167B7DEFCE99,
	ObjectSpawner_AlertYes_m820A1B8E0CFE132FC51E7B18443D31D090EA9CD7,
	ObjectSpawner_AlertNo_mB59A9902EF7BC7D928A748FD01A71B74B6FB2424,
	ObjectSpawner_ReLoadUnity_m6C86083ACC3A8BA0C4ED0AAB731B1B5A6021FD89,
	ObjectSpawner_Activate_m34247E7E02036EBF855FE054FE1FFC3EC525CCA8,
	ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E,
	ObjectSpawner_Redeem_m5BED17761A501AE11EAD3E0C68D452B096FE9E70,
	ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524,
	ObjectSpawner_RedeemCharacter_m85A125750ED5B2CC129A4BD56C3298D299AE7196,
	ObjectSpawner_CreateAvatarScreen_mB2A411FBDB5848724F8C6B2E8B080776063DBDA9,
	ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3,
	NativeAPI_showContainerView_m48458AEB529CFEEBFA21C2C8F61A81573B7FEE78,
	NativeAPI_sendMessageOnReload_mE3443F746520DB3FD3909EBC62598F438DE45771,
	NativeAPI_modelDownloaded_m2CB305BC0327B63C44CFB00C1905881618AE07F6,
	NativeAPI_modelDownloadingDone_mA22BB2476C75156FAF3A540FC40E485B7A6C1036,
	NativeAPI__ctor_m83D6870123E118D4FD44168A1A5F4D593D1D212C,
	U3CWaitForButtonOffU3Ed__12__ctor_m0B82BD042726E7B3797C9358FD5605F45A1A7508,
	U3CWaitForButtonOffU3Ed__12_System_IDisposable_Dispose_mC3D095F7B5A65ECD38652C017497B6AF83948D53,
	U3CWaitForButtonOffU3Ed__12_MoveNext_mC4AF31B247869CB92E06D9FA9FA7703519CC1645,
	U3CWaitForButtonOffU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEC800CCAFC224E0F90179E76696E0E1079DB901,
	U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_Reset_m2003529E6D7AE8870E962520167593F92E76B873,
	U3CWaitForButtonOffU3Ed__12_System_Collections_IEnumerator_get_Current_mC5AD80BA36B82FEB132B2EE119D3A9324EE9E28F,
	U3CWaitForDownloadU3Ed__22__ctor_m3BA7AAE197C8D4624C14D097777110B6391D394A,
	U3CWaitForDownloadU3Ed__22_System_IDisposable_Dispose_mDE911E9E6F03D450DB69669BA000CCA33245D6CB,
	U3CWaitForDownloadU3Ed__22_MoveNext_m6D8CC7E7503176C6543A45AE087E393A00E76169,
	U3CWaitForDownloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F13D8099C675D59A1A966576A14473AB3FD033,
	U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_Reset_m299B6E03721A150377174B736F6E0CE7CBDA6448,
	U3CWaitForDownloadU3Ed__22_System_Collections_IEnumerator_get_Current_mC825DCF994513F03F8C8B893C90468134788362F,
	U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1,
	U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022,
	U3CWaitForRedeemDownloadU3Ed__27_MoveNext_m0D750FD47DAC5AB2A6AFFBD69B56EED050C54F30,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91,
	PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200,
	PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4,
	PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE,
	LeanTranslateSmooth__ctor_mD70387CD36585DFA9BE86364AD114FF23647BB14,
};
static const int32_t s_InvokerIndices[195] = 
{
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3754,
	3833,
	3833,
	3754,
	3833,
	3833,
	5923,
	5923,
	3833,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3833,
	2485,
	3833,
	3754,
	3833,
	5988,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3833,
	2485,
	3833,
	3833,
	5988,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3080,
	3080,
	3754,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	5468,
	3833,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3782,
	3123,
	3782,
	3123,
	3099,
	3833,
	3833,
	3833,
	3833,
	3754,
	3099,
	3833,
	3099,
	3833,
	3833,
	3833,
	5988,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	936,
	3754,
	3754,
	773,
	3833,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3099,
	3099,
	3833,
	3833,
	3833,
	3782,
	3833,
	3123,
	3123,
	3123,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3833,
	3754,
	3833,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3833,
	3833,
	3754,
	3833,
	3833,
	3833,
	3833,
	3833,
	3099,
	3754,
	3099,
	3754,
	3099,
	3833,
	3833,
	5923,
	5923,
	5925,
	5923,
	3833,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3080,
	3833,
	3782,
	3754,
	3833,
	3754,
	3833,
	3833,
	3833,
	3833,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	195,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
