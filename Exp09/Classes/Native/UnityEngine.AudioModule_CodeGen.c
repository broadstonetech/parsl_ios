﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern void AudioSettings_InvokeOnAudioConfigurationChanged_m2CBD1FC39E7AE46E07E777990310D1DC40FB980E (void);
// 0x00000002 System.Boolean UnityEngine.AudioSettings::StartAudioOutput()
extern void AudioSettings_StartAudioOutput_m372FC1B86734F04E2AAE161E2A70F3E5E8FCE583 (void);
// 0x00000003 System.Boolean UnityEngine.AudioSettings::StopAudioOutput()
extern void AudioSettings_StopAudioOutput_m5012E0AC92B63A7E23C7EBFE9A0F0EA3FA03C5A6 (void);
// 0x00000004 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern void AudioConfigurationChangeHandler__ctor_mB63AFBABA4712DF64F06A65CC7CE3C9E8C58080B (void);
// 0x00000005 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern void AudioConfigurationChangeHandler_Invoke_mDC001A19067B6A02B0DE21A4D66FC8D82529F911 (void);
// 0x00000006 System.Boolean UnityEngine.AudioSettings/Mobile::get_muteState()
extern void Mobile_get_muteState_m692B429F6C1592C72DCCDCD2A00BF126BC51F6EF (void);
// 0x00000007 System.Void UnityEngine.AudioSettings/Mobile::set_muteState(System.Boolean)
extern void Mobile_set_muteState_mA43DEFC30F87157C7E0030B009DD8C8D0CC422F6 (void);
// 0x00000008 System.Boolean UnityEngine.AudioSettings/Mobile::get_stopAudioOutputOnMute()
extern void Mobile_get_stopAudioOutputOnMute_m0FCE9C6EF6597B73D4C36AB361403093A4A2CC23 (void);
// 0x00000009 System.Void UnityEngine.AudioSettings/Mobile::InvokeOnMuteStateChanged(System.Boolean)
extern void Mobile_InvokeOnMuteStateChanged_m89C9E4AE29FB7A0140E6F73F1A00B54351548664 (void);
// 0x0000000A System.Void UnityEngine.AudioSettings/Mobile::StartAudioOutput()
extern void Mobile_StartAudioOutput_m3F2C349725ED8D6AA8C8223651658814D312FE09 (void);
// 0x0000000B System.Void UnityEngine.AudioSettings/Mobile::StopAudioOutput()
extern void Mobile_StopAudioOutput_m724EB5F22195200EA75828AAA436CE13A1B9989E (void);
// 0x0000000C System.Void UnityEngine.AudioSettings/Mobile::.cctor()
extern void Mobile__cctor_m1431158D3DD6F1D3453E5A545C25C8218315C315 (void);
// 0x0000000D System.Void UnityEngine.AudioClip::.ctor()
extern void AudioClip__ctor_m25D83075F6C4735DF4E997F08E13E7EF6114C9B3 (void);
// 0x0000000E System.Boolean UnityEngine.AudioClip::GetData(UnityEngine.AudioClip,System.Single[],System.Int32,System.Int32)
extern void AudioClip_GetData_m4BBF0E17E9E229EAE137E3275B5FBE6961D235E0 (void);
// 0x0000000F System.String UnityEngine.AudioClip::GetName()
extern void AudioClip_GetName_m7EC70BE0E037DDF4281B20EDDFEB9DD6D0CD541E (void);
// 0x00000010 System.Int32 UnityEngine.AudioClip::get_channels()
extern void AudioClip_get_channels_m7592B378317BFA41DF2228636124E4DD5B86D3B8 (void);
// 0x00000011 System.Boolean UnityEngine.AudioClip::GetData(System.Single[],System.Int32)
extern void AudioClip_GetData_m2D7410645789EBED93CAA8146D271C79156E2CB0 (void);
// 0x00000012 System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern void AudioClip_InvokePCMReaderCallback_Internal_m9CB2976CDC2C73A92479F8C11C30B17FAA05751F (void);
// 0x00000013 System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern void AudioClip_InvokePCMSetPositionCallback_Internal_m9F3ACF3A244349568C0D0D1D40EE72EF013FB45D (void);
// 0x00000014 System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern void PCMReaderCallback__ctor_mCA9CC5271DE0E4083B85759CA74EED1C1CD219F7 (void);
// 0x00000015 System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern void PCMReaderCallback_Invoke_mE5E7A777A52B9627F9A6A57A140E5C4AAB5A1387 (void);
// 0x00000016 System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern void PCMSetPositionCallback__ctor_m0204C8557D7FB9E95F33168EDFD64182D9342002 (void);
// 0x00000017 System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern void PCMSetPositionCallback_Invoke_m1FBFFA5FC15B57601D6D13F4A574F7CAD2A93B7E (void);
// 0x00000018 System.Void UnityEngine.AudioSource::PlayHelper(UnityEngine.AudioSource,System.UInt64)
extern void AudioSource_PlayHelper_mAE7BC6AD4F09442D5EB0FF7711ECAB7580195814 (void);
// 0x00000019 System.Void UnityEngine.AudioSource::Stop(System.Boolean)
extern void AudioSource_Stop_mD2260A580FFE563C64EFBD98DAC6376021ABF826 (void);
// 0x0000001A System.Int32 UnityEngine.AudioSource::get_timeSamples()
extern void AudioSource_get_timeSamples_m2D16DA78011B58C24BCA679B44D2BF832DFCA0BE (void);
// 0x0000001B UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern void AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC (void);
// 0x0000001C System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern void AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B (void);
// 0x0000001D System.Void UnityEngine.AudioSource::Play()
extern void AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1 (void);
// 0x0000001E System.Void UnityEngine.AudioSource::Stop()
extern void AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F (void);
// 0x0000001F System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern void AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8 (void);
// 0x00000020 System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern void AudioSource_set_loop_mDD9FB746D8A7392472E5484EEF8D0A667993E3E0 (void);
// 0x00000021 System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern void AudioSource_set_mute_m69E2DFCF261D2D187ED756096B7E0DE622292C71 (void);
// 0x00000022 System.Int32 UnityEngine.Microphone::GetMicrophoneDeviceIDFromName(System.String)
extern void Microphone_GetMicrophoneDeviceIDFromName_m95AE47EB540C5FF0F7E69B1ED99C39F5D11E0CF7 (void);
// 0x00000023 UnityEngine.AudioClip UnityEngine.Microphone::StartRecord(System.Int32,System.Boolean,System.Single,System.Int32)
extern void Microphone_StartRecord_m8A44420F4A5402287A6A44B829E8CF21E9A59489 (void);
// 0x00000024 UnityEngine.AudioClip UnityEngine.Microphone::Start(System.String,System.Boolean,System.Int32,System.Int32)
extern void Microphone_Start_m7F08B42DC2B97BE649F7329C0FAC54125FE0AC5D (void);
// 0x00000025 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern void AudioClipPlayable_GetHandle_mBEB846B088961170B6DB961951B511C11B98E0B8 (void);
// 0x00000026 System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern void AudioClipPlayable_Equals_m52ECDD49AE6BD8AB4C0AC83C417A0C1B23E3E55E (void);
// 0x00000027 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern void AudioMixerPlayable_GetHandle_m76EFC486A7639C4842F590F544B60988CF27BB17 (void);
// 0x00000028 System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern void AudioMixerPlayable_Equals_mB55D2602ACCD196F61AF3D1AE90B81930A9AB7E8 (void);
// 0x00000029 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesAvailable_mE6689CFA13C0621F305F389FEEE4D543B71BF236 (void);
// 0x0000002A System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesOverflow_m998BEADD2A2B4BEF0906A31108B6DC486411CC78 (void);
// 0x0000002B System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::.ctor(System.Object,System.IntPtr)
extern void SampleFramesHandler__ctor_m389B32B949592BFD1BA53D0C0983CA6B5BA6AAC7 (void);
// 0x0000002C System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32)
extern void SampleFramesHandler_Invoke_mCB6172CE3EF20C5E12A697A5CE5EEDED9A3B5779 (void);
static Il2CppMethodPointer s_methodPointers[44] = 
{
	AudioSettings_InvokeOnAudioConfigurationChanged_m2CBD1FC39E7AE46E07E777990310D1DC40FB980E,
	AudioSettings_StartAudioOutput_m372FC1B86734F04E2AAE161E2A70F3E5E8FCE583,
	AudioSettings_StopAudioOutput_m5012E0AC92B63A7E23C7EBFE9A0F0EA3FA03C5A6,
	AudioConfigurationChangeHandler__ctor_mB63AFBABA4712DF64F06A65CC7CE3C9E8C58080B,
	AudioConfigurationChangeHandler_Invoke_mDC001A19067B6A02B0DE21A4D66FC8D82529F911,
	Mobile_get_muteState_m692B429F6C1592C72DCCDCD2A00BF126BC51F6EF,
	Mobile_set_muteState_mA43DEFC30F87157C7E0030B009DD8C8D0CC422F6,
	Mobile_get_stopAudioOutputOnMute_m0FCE9C6EF6597B73D4C36AB361403093A4A2CC23,
	Mobile_InvokeOnMuteStateChanged_m89C9E4AE29FB7A0140E6F73F1A00B54351548664,
	Mobile_StartAudioOutput_m3F2C349725ED8D6AA8C8223651658814D312FE09,
	Mobile_StopAudioOutput_m724EB5F22195200EA75828AAA436CE13A1B9989E,
	Mobile__cctor_m1431158D3DD6F1D3453E5A545C25C8218315C315,
	AudioClip__ctor_m25D83075F6C4735DF4E997F08E13E7EF6114C9B3,
	AudioClip_GetData_m4BBF0E17E9E229EAE137E3275B5FBE6961D235E0,
	AudioClip_GetName_m7EC70BE0E037DDF4281B20EDDFEB9DD6D0CD541E,
	AudioClip_get_channels_m7592B378317BFA41DF2228636124E4DD5B86D3B8,
	AudioClip_GetData_m2D7410645789EBED93CAA8146D271C79156E2CB0,
	AudioClip_InvokePCMReaderCallback_Internal_m9CB2976CDC2C73A92479F8C11C30B17FAA05751F,
	AudioClip_InvokePCMSetPositionCallback_Internal_m9F3ACF3A244349568C0D0D1D40EE72EF013FB45D,
	PCMReaderCallback__ctor_mCA9CC5271DE0E4083B85759CA74EED1C1CD219F7,
	PCMReaderCallback_Invoke_mE5E7A777A52B9627F9A6A57A140E5C4AAB5A1387,
	PCMSetPositionCallback__ctor_m0204C8557D7FB9E95F33168EDFD64182D9342002,
	PCMSetPositionCallback_Invoke_m1FBFFA5FC15B57601D6D13F4A574F7CAD2A93B7E,
	AudioSource_PlayHelper_mAE7BC6AD4F09442D5EB0FF7711ECAB7580195814,
	AudioSource_Stop_mD2260A580FFE563C64EFBD98DAC6376021ABF826,
	AudioSource_get_timeSamples_m2D16DA78011B58C24BCA679B44D2BF832DFCA0BE,
	AudioSource_get_clip_mE4454E38D2C0A4C8CC780A435FC1DBD4D47D16DC,
	AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B,
	AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1,
	AudioSource_Stop_mADA564D223832A64F8CF3EFBDEB534C0D658810F,
	AudioSource_get_isPlaying_mEA69477C77D542971F7B454946EF25DFBE0AF6A8,
	AudioSource_set_loop_mDD9FB746D8A7392472E5484EEF8D0A667993E3E0,
	AudioSource_set_mute_m69E2DFCF261D2D187ED756096B7E0DE622292C71,
	Microphone_GetMicrophoneDeviceIDFromName_m95AE47EB540C5FF0F7E69B1ED99C39F5D11E0CF7,
	Microphone_StartRecord_m8A44420F4A5402287A6A44B829E8CF21E9A59489,
	Microphone_Start_m7F08B42DC2B97BE649F7329C0FAC54125FE0AC5D,
	AudioClipPlayable_GetHandle_mBEB846B088961170B6DB961951B511C11B98E0B8,
	AudioClipPlayable_Equals_m52ECDD49AE6BD8AB4C0AC83C417A0C1B23E3E55E,
	AudioMixerPlayable_GetHandle_m76EFC486A7639C4842F590F544B60988CF27BB17,
	AudioMixerPlayable_Equals_mB55D2602ACCD196F61AF3D1AE90B81930A9AB7E8,
	AudioSampleProvider_InvokeSampleFramesAvailable_mE6689CFA13C0621F305F389FEEE4D543B71BF236,
	AudioSampleProvider_InvokeSampleFramesOverflow_m998BEADD2A2B4BEF0906A31108B6DC486411CC78,
	SampleFramesHandler__ctor_m389B32B949592BFD1BA53D0C0983CA6B5BA6AAC7,
	SampleFramesHandler_Invoke_mCB6172CE3EF20C5E12A697A5CE5EEDED9A3B5779,
};
extern void AudioClipPlayable_GetHandle_mBEB846B088961170B6DB961951B511C11B98E0B8_AdjustorThunk (void);
extern void AudioClipPlayable_Equals_m52ECDD49AE6BD8AB4C0AC83C417A0C1B23E3E55E_AdjustorThunk (void);
extern void AudioMixerPlayable_GetHandle_m76EFC486A7639C4842F590F544B60988CF27BB17_AdjustorThunk (void);
extern void AudioMixerPlayable_Equals_mB55D2602ACCD196F61AF3D1AE90B81930A9AB7E8_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x06000025, AudioClipPlayable_GetHandle_mBEB846B088961170B6DB961951B511C11B98E0B8_AdjustorThunk },
	{ 0x06000026, AudioClipPlayable_Equals_m52ECDD49AE6BD8AB4C0AC83C417A0C1B23E3E55E_AdjustorThunk },
	{ 0x06000027, AudioMixerPlayable_GetHandle_m76EFC486A7639C4842F590F544B60988CF27BB17_AdjustorThunk },
	{ 0x06000028, AudioMixerPlayable_Equals_mB55D2602ACCD196F61AF3D1AE90B81930A9AB7E8_AdjustorThunk },
};
static const int32_t s_InvokerIndices[44] = 
{
	5925,
	5978,
	5978,
	1659,
	3123,
	5978,
	5925,
	5978,
	5925,
	5988,
	5988,
	5988,
	3833,
	4533,
	3754,
	3733,
	1242,
	3099,
	3080,
	1659,
	3099,
	1659,
	3080,
	5465,
	3123,
	3733,
	3754,
	3099,
	3833,
	3833,
	3782,
	3123,
	3123,
	5665,
	4457,
	4486,
	3758,
	2522,
	3758,
	2523,
	3080,
	3080,
	1659,
	1657,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_AudioModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_AudioModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AudioModule_CodeGenModule = 
{
	"UnityEngine.AudioModule.dll",
	44,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_AudioModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
