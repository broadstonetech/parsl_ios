﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Int64 BufferedBinaryReader::get_Position()
extern void BufferedBinaryReader_get_Position_m634DBC609CAB28CDEC1104360DCBCD8CA2F246D0 (void);
// 0x00000002 System.Void BufferedBinaryReader::set_Position(System.Int64)
extern void BufferedBinaryReader_set_Position_m4B83D7F75FE5E2A812D92B42CC88519F297CD813 (void);
// 0x00000003 System.Void BufferedBinaryReader::.ctor(System.IO.Stream,System.Int32)
extern void BufferedBinaryReader__ctor_mBC3B4C93D65BC052BB3AA10FFFD64B672994BCCF (void);
// 0x00000004 System.Void BufferedBinaryReader::FillBuffer(System.Int32)
extern void BufferedBinaryReader_FillBuffer_mD3BC7A6C0813D2C5F5FCEB5CD367952C0CF9E291 (void);
// 0x00000005 System.Byte BufferedBinaryReader::ReadByte()
extern void BufferedBinaryReader_ReadByte_mEBFE4BBD75D20F2B2748FCB577E64F5D9299AB87 (void);
// 0x00000006 System.SByte BufferedBinaryReader::ReadSByte()
extern void BufferedBinaryReader_ReadSByte_m2E2A8A4735F89856AB2B4318702B4B65300D3916 (void);
// 0x00000007 System.UInt16 BufferedBinaryReader::ReadUInt16()
extern void BufferedBinaryReader_ReadUInt16_m5AEF216915781971C456484FAD34D2C133BDBFFD (void);
// 0x00000008 System.Int16 BufferedBinaryReader::ReadInt16()
extern void BufferedBinaryReader_ReadInt16_mFE2F9EC3FC30E759A11E56E597CA3D41F11ECDCB (void);
// 0x00000009 System.UInt32 BufferedBinaryReader::ReadUInt32()
extern void BufferedBinaryReader_ReadUInt32_m8DAE5258919A6A802F16F3DF54A59335E59E8FB3 (void);
// 0x0000000A System.Int32 BufferedBinaryReader::ReadInt32()
extern void BufferedBinaryReader_ReadInt32_m131C895FE68A737B3DBB7E522EC722C9A47CB403 (void);
// 0x0000000B System.Single BufferedBinaryReader::ReadSingle()
extern void BufferedBinaryReader_ReadSingle_m8F635100C180A0EB837A20389A6B5AE7F5E1C8D2 (void);
// 0x0000000C System.Void BufferedBinaryReader::Skip(System.Int32)
extern void BufferedBinaryReader_Skip_m1246BA04F480EB9FD0BCB75B801479E4F4E28BFE (void);
// 0x0000000D System.Void BufferedBinaryReader::Dispose()
extern void BufferedBinaryReader_Dispose_m19C080EFAFADF78061692EC4DDC076332D9255E4 (void);
// 0x0000000E BufferedBinaryReader/Bit2Converter BufferedBinaryReader/Bit2Converter::Read(System.Byte[],System.Int32&)
extern void Bit2Converter_Read_m6A9F724C42A23D7794F2B98393B19193CACDFF50 (void);
// 0x0000000F BufferedBinaryReader/Bit4Converter BufferedBinaryReader/Bit4Converter::Read(System.Byte[],System.Int32&)
extern void Bit4Converter_Read_m2E8694B24CE49956E94B493FF1544870F0478DC2 (void);
// 0x00000010 System.Void RuntimeTest::Start()
extern void RuntimeTest_Start_m10C161C683779646859EBEC263332F46A71EC6D3 (void);
// 0x00000011 System.Void RuntimeTest::OnAvatarLoaded(UnityEngine.GameObject,Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData)
extern void RuntimeTest_OnAvatarLoaded_mCD4A8D837AE479E6E7923863624A410D793B904D (void);
// 0x00000012 System.Void RuntimeTest::.ctor()
extern void RuntimeTest__ctor_m337FB724C20A0F9E603DEBA02C8E50BFD57A9361 (void);
// 0x00000013 System.Void DragRotate::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragRotate_OnDrag_m241F16E3337FB0DF2D81A24CC7ECDE8829D4DDE6 (void);
// 0x00000014 System.Void DragRotate::.ctor()
extern void DragRotate__ctor_mB807CA7B51B3BE23E1718827DC15B9188C87A9F5 (void);
// 0x00000015 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.WebviewTest::Start()
extern void WebviewTest_Start_m964A2663ACE6EEDD8309B66FADE3895C658BC5B4 (void);
// 0x00000016 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.WebviewTest::DisplayWebView()
extern void WebviewTest_DisplayWebView_mB759A88308464996744A2261D233C379AB381E56 (void);
// 0x00000017 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.WebviewTest::OnAvatarCreated(System.String)
extern void WebviewTest_OnAvatarCreated_mA2F66DE56804FDB1537772E558C0794A1169D676 (void);
// 0x00000018 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.WebviewTest::OnAvatarLoaded(UnityEngine.GameObject,Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData)
extern void WebviewTest_OnAvatarLoaded_m7B5D0E39BC3FDB70BEDAD7068434F6C225501967 (void);
// 0x00000019 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.WebviewTest::.ctor()
extern void WebviewTest__ctor_m956F0DD42A57289950DCA18FAAD7C8572660AE53 (void);
// 0x0000001A System.Int32 Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader::get_Timeout()
extern void AvatarLoader_get_Timeout_mE8CB3BCEC9F34E86871DE081D1A330578C69EF3C (void);
// 0x0000001B System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader::set_Timeout(System.Int32)
extern void AvatarLoader_set_Timeout_mD13FEA8E0424644DB952C51D62791745CDDB68CA (void);
// 0x0000001C System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader::LoadAvatar(System.String,System.Action`2<UnityEngine.GameObject,Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData>)
extern void AvatarLoader_LoadAvatar_m7E97807F5051768CD371052088FF1C25A0234B85 (void);
// 0x0000001D System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader::.ctor()
extern void AvatarLoader__ctor_mB620008D910565DF3B8DEE48579CC60E3A44DEE1 (void);
// 0x0000001E System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation::LoadAvatarAsync(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
extern void LoadOperation_LoadAvatarAsync_m1ECC417961699E00B7288CA0D359E51641B0177F (void);
// 0x0000001F System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation::DownloadAvatar(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
extern void LoadOperation_DownloadAvatar_m8C41A534B940C0DB563C4C38D868D9E16FE14C4D (void);
// 0x00000020 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation::OnImportFinished(UnityEngine.GameObject)
extern void LoadOperation_OnImportFinished_mB0C9D1C7EFBD9399D4FE0FA06FB1DCC7A69C80AF (void);
// 0x00000021 System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation::PrepareAvatarAsync(UnityEngine.GameObject)
extern void LoadOperation_PrepareAvatarAsync_mAFE7F9F3A13710DE1D43AF1133462C51F1518A76 (void);
// 0x00000022 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation::.ctor()
extern void LoadOperation__ctor_m68C5789147B330F82A5B16F4851DE558A0B1298F (void);
// 0x00000023 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::.ctor(System.Int32)
extern void U3CLoadAvatarAsyncU3Ed__2__ctor_mBD4F78947512C1C4071220D62833955E48D55EE7 (void);
// 0x00000024 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::System.IDisposable.Dispose()
extern void U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_m0CBBA5987404C4EC2B71743C4BA0F0831EE98D9A (void);
// 0x00000025 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::MoveNext()
extern void U3CLoadAvatarAsyncU3Ed__2_MoveNext_m5A8F2DA18C09C685B16B0F32B2F4E281CB05461D (void);
// 0x00000026 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD133114924F55ADBDF3A6CAAE4061E50CAC7A9E4 (void);
// 0x00000027 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mB19A8FCBACFD78E2F5825DC6995D0723E30CBE85 (void);
// 0x00000028 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<LoadAvatarAsync>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_mD85B71B8CD797CA29F0378FEED1261B7E8321080 (void);
// 0x00000029 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::.ctor(System.Int32)
extern void U3CDownloadAvatarU3Ed__3__ctor_m68352DFBC6229C147598D5857B04E6ADEF630B85 (void);
// 0x0000002A System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::System.IDisposable.Dispose()
extern void U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mC275D51C7FF17A0C516114FF31C4E2D6CB73F551 (void);
// 0x0000002B System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::MoveNext()
extern void U3CDownloadAvatarU3Ed__3_MoveNext_m0FBD65D03B0D3DC779B783869FBA672B9F20C50B (void);
// 0x0000002C System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::<>m__Finally1()
extern void U3CDownloadAvatarU3Ed__3_U3CU3Em__Finally1_m7063558E183950E9ACFB5C5D5C2DE21853C42308 (void);
// 0x0000002D System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD843D5776566598C09ACDB97436644089FECE376 (void);
// 0x0000002E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m8334C59C0F1A6BC4B759C04AD6760BC32EF6301A (void);
// 0x0000002F System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<DownloadAvatar>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_mFB2775F3A311FBB224A9179EF21700527E1B33D9 (void);
// 0x00000030 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::.ctor(System.Int32)
extern void U3CPrepareAvatarAsyncU3Ed__5__ctor_mC4B4D228743E776D60E4E03F426C756904843118 (void);
// 0x00000031 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::System.IDisposable.Dispose()
extern void U3CPrepareAvatarAsyncU3Ed__5_System_IDisposable_Dispose_mF77F6CDFC1C997744FB30300C5CE96795AE19ECC (void);
// 0x00000032 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::MoveNext()
extern void U3CPrepareAvatarAsyncU3Ed__5_MoveNext_m30F07D5A0251A13329E5F555B2E3289B7A565152 (void);
// 0x00000033 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPrepareAvatarAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF636842F021B15398C2F08AF5EB996DC63C0425 (void);
// 0x00000034 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::System.Collections.IEnumerator.Reset()
extern void U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m1FEAF518F8173872C6C8E22047A733575E8E9CC7 (void);
// 0x00000035 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoader/LoadOperation/<PrepareAvatarAsync>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_m9BBD626EA4994CB9449CF0E46F2273F2E19E642B (void);
// 0x00000036 System.Int32 Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::get_Timeout()
extern void AvatarLoaderBase_get_Timeout_m751F8141D184C514A2921289CA5C2D158925F2B1 (void);
// 0x00000037 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::set_Timeout(System.Int32)
extern void AvatarLoaderBase_set_Timeout_m8037ABC73D784A1701B268248C1653CC73FD09AD (void);
// 0x00000038 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::LoadAvatar(System.String,System.Action`2<UnityEngine.GameObject,Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData>)
extern void AvatarLoaderBase_LoadAvatar_mB3A3FB205BF0A074FAFFFF5BAA8B20526B17A917 (void);
// 0x00000039 System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::LoadAvatarAsync(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
// 0x0000003A System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::DownloadAvatar(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
// 0x0000003B System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::DownloadMetaData(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri,UnityEngine.GameObject)
extern void AvatarLoaderBase_DownloadMetaData_m200441C7F7CE70C78D87747769B1CAEC7C60D74F (void);
// 0x0000003C System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::get_AnimatorControllerName()
extern void AvatarLoaderBase_get_AnimatorControllerName_mF9BB1E03049FC432DF909537BC5A5034F30D07FA (void);
// 0x0000003D System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::get_AnimationAvatarSource()
extern void AvatarLoaderBase_get_AnimationAvatarSource_m8D255A218733FF89B9C16C952C6C5D3899676C2C (void);
// 0x0000003E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::RestructureAndSetAnimator(UnityEngine.GameObject)
extern void AvatarLoaderBase_RestructureAndSetAnimator_m83E0BC0403952A729293353A15473ABCBCC7E4D8 (void);
// 0x0000003F System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::OutfitVersion(System.Int32)
extern void AvatarLoaderBase_OutfitVersion_m550FC6EB901AD6DF4D5D00D8FE1448C0A95B59A2 (void);
// 0x00000040 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::SetMetaData(System.String,UnityEngine.GameObject)
extern void AvatarLoaderBase_SetMetaData_mFF38163711C06B971CC0F620B0C72BADF8F99E56 (void);
// 0x00000041 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::SetAvatarAssetNames(UnityEngine.GameObject)
extern void AvatarLoaderBase_SetAvatarAssetNames_mBE306862F0A883CF9BD844431BE95EC0FEE1664A (void);
// 0x00000042 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::SetTextureNames(UnityEngine.Renderer,System.String)
extern void AvatarLoaderBase_SetTextureNames_m7ED78FE09ABC14262BC20A469D3AE03797CCE43E (void);
// 0x00000043 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::SetMeshName(UnityEngine.Renderer,System.String)
extern void AvatarLoaderBase_SetMeshName_m20388314C8E75CFCFA305893F1A5B9855483F678 (void);
// 0x00000044 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::.ctor()
extern void AvatarLoaderBase__ctor_mF676DA3A64576376F8858711AB6B07754BE1C6C8 (void);
// 0x00000045 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase::.cctor()
extern void AvatarLoaderBase__cctor_m583C1C214966437DABC8A2378E3B8DFE6FD86D25 (void);
// 0x00000046 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<LoadAvatar>d__19::MoveNext()
extern void U3CLoadAvatarU3Ed__19_MoveNext_m9BD287FCF1FD3F252007B2DE8FDAE58D51FB2CF6 (void);
// 0x00000047 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<LoadAvatar>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E (void);
// 0x00000048 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::.ctor(System.Int32)
extern void U3CDownloadMetaDataU3Ed__22__ctor_m28B2083136EB545C725ED009EBBFE7A5BB8BD813 (void);
// 0x00000049 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::System.IDisposable.Dispose()
extern void U3CDownloadMetaDataU3Ed__22_System_IDisposable_Dispose_mB1728B9B8A43C8600AAD903B7BD21363587EDF66 (void);
// 0x0000004A System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::MoveNext()
extern void U3CDownloadMetaDataU3Ed__22_MoveNext_m2842A64D0FEBA4FDF20FAA8FAF2D70EC77FE6DC7 (void);
// 0x0000004B System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::<>m__Finally1()
extern void U3CDownloadMetaDataU3Ed__22_U3CU3Em__Finally1_m0111D7C9C9407BE69C2410A3B24F2EB20D90D54A (void);
// 0x0000004C System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadMetaDataU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A9228438F9EC5F8959B62258365983010A2483B (void);
// 0x0000004D System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::System.Collections.IEnumerator.Reset()
extern void U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_Reset_mBC340BC3995F6746CAEC44E1096EF92C709DC67B (void);
// 0x0000004E System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarLoaderBase/<DownloadMetaData>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_get_Current_mCE4A69299659B9C8D5DCE6C126C1D33E7A6938C8 (void);
// 0x0000004F System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader::LoadAvatarAsync(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
extern void EditorAvatarLoader_LoadAvatarAsync_mAFCF3C276AA7B1B716C234BEF2CED8D28DACA28F (void);
// 0x00000050 System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader::DownloadAvatar(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
extern void EditorAvatarLoader_DownloadAvatar_mD546367EBD7548B38CDE90EFB47032A424403882 (void);
// 0x00000051 UnityEngine.GameObject Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader::InstantiateAvatar(Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri)
extern void EditorAvatarLoader_InstantiateAvatar_m61DD09B8820E0D696712B2F6384BC2A7D22C06C4 (void);
// 0x00000052 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader::.ctor()
extern void EditorAvatarLoader__ctor_mA35B40EA36ED34D3369BD349632A55F9612E2207 (void);
// 0x00000053 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::.ctor(System.Int32)
extern void U3CLoadAvatarAsyncU3Ed__2__ctor_m33FA85ABA06CE6BB59613671CDD337BE19933C95 (void);
// 0x00000054 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::System.IDisposable.Dispose()
extern void U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_mA359D6B2EDEC85F23AFF2C7A98B21AF9DE7F9EFE (void);
// 0x00000055 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::MoveNext()
extern void U3CLoadAvatarAsyncU3Ed__2_MoveNext_mCCAC4AB969396E419F5A6F03D7BF7BE317BB8B2B (void);
// 0x00000056 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AFFF62195D799E9059CEC04FD7ACF19988D46AC (void);
// 0x00000057 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mF50263B27F63A1E65E26495BF41E351FD6011120 (void);
// 0x00000058 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<LoadAvatarAsync>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_m46EABEC84BC757D097187BBB5F759C594BFB78ED (void);
// 0x00000059 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDC5E05C8A139AAA010129DFC7FC63D1FBBE1470C (void);
// 0x0000005A System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<>c__DisplayClass3_0::<DownloadAvatar>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDownloadAvatarU3Eb__0_mD236682FDD6AB8CEB5AA9E4AF8EF4FA33F56B5F8 (void);
// 0x0000005B System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::.ctor(System.Int32)
extern void U3CDownloadAvatarU3Ed__3__ctor_mFDBEDB706E0A529EA40637D297209930BAC2ECE6 (void);
// 0x0000005C System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::System.IDisposable.Dispose()
extern void U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mB2353DD0FEC9E18048409BDB9C2D4CB6725B6D6D (void);
// 0x0000005D System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::MoveNext()
extern void U3CDownloadAvatarU3Ed__3_MoveNext_mF191D90C2A88125C7F71437DAAC13EB764ADD4C6 (void);
// 0x0000005E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::<>m__Finally1()
extern void U3CDownloadAvatarU3Ed__3_U3CU3Em__Finally1_m49F2D0585294D8134BBC0D9D633B322B4FB7B807 (void);
// 0x0000005F System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43F2C2D1AA214B91EA1926994D2052EE57B8B5E2 (void);
// 0x00000060 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m1B694D6F834ED62F52DB8CEA18448795201387CC (void);
// 0x00000061 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EditorAvatarLoader/<DownloadAvatar>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_m630C20BDF06581F5C703266612A613384F544080 (void);
// 0x00000062 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler::Start()
extern void EyeAnimationHandler_Start_m50592966FA0081A6D83DD0C66E2981193AAA6F7E (void);
// 0x00000063 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler::AnimateEyes()
extern void EyeAnimationHandler_AnimateEyes_mE8EEA814D4114198A201B9EAE5259C6329493701 (void);
// 0x00000064 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler::RotateEyes()
extern void EyeAnimationHandler_RotateEyes_mF100A0DCCACAE6D01BDCD130C2CC8084C513A7B2 (void);
// 0x00000065 System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler::BlinkEyes()
extern void EyeAnimationHandler_BlinkEyes_m4A936B47896A1360C3F64FCF080281F38E614556 (void);
// 0x00000066 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler::.ctor()
extern void EyeAnimationHandler__ctor_m82C4E997E334CF10787DA7CACDA98F28EF19F44E (void);
// 0x00000067 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::.ctor(System.Int32)
extern void U3CBlinkEyesU3Ed__22__ctor_m0E1712F32F0BF56CD99EDB68EAB6293E79A892EC (void);
// 0x00000068 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::System.IDisposable.Dispose()
extern void U3CBlinkEyesU3Ed__22_System_IDisposable_Dispose_mB7812B1354202F0649E708CB552E0913EDC7C169 (void);
// 0x00000069 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::MoveNext()
extern void U3CBlinkEyesU3Ed__22_MoveNext_m66A7FE2A9503EDCC65C8F2017259749812679428 (void);
// 0x0000006A System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkEyesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21A7148FA71D14491CF4D751C5253D5C7378B5E3 (void);
// 0x0000006B System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::System.Collections.IEnumerator.Reset()
extern void U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_Reset_m9190DCEEDA0E422FE8E5A55599E8BF390C1270A9 (void);
// 0x0000006C System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.EyeAnimationHandler/<BlinkEyes>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_get_Current_mD837E0FCA30EF9CF3DB882D895B6581C550E9BCE (void);
// 0x0000006D System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::Start()
extern void VoiceHandler_Start_m0BF3C770C11C4639E70693E0509FB5F57B434397 (void);
// 0x0000006E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::Update()
extern void VoiceHandler_Update_mA7995F722E7909275461DE17E13EA28E5AB1044B (void);
// 0x0000006F System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::InitializeAudio()
extern void VoiceHandler_InitializeAudio_m6E2B25C2165D0863D53B89C00DA38EF509E02759 (void);
// 0x00000070 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::SetMicrophoneSource()
extern void VoiceHandler_SetMicrophoneSource_mCB61D3AA9104E80FDF57B3F0E28FD866FFCB39D8 (void);
// 0x00000071 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::SetAudioClipSource()
extern void VoiceHandler_SetAudioClipSource_m784611F6A7964B9BE8A058EA7D785D71C98052BD (void);
// 0x00000072 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::PlayCurrentAudioClip()
extern void VoiceHandler_PlayCurrentAudioClip_mE3652737AC6EA5F8A130DF88B92062A8365382E9 (void);
// 0x00000073 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::PlayAudioClip(UnityEngine.AudioClip)
extern void VoiceHandler_PlayAudioClip_mBDBE5530723FBEAFFF8EEB83FB91D8CE0A105B69 (void);
// 0x00000074 System.Single Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::GetAmplitude()
extern void VoiceHandler_GetAmplitude_m4CA4F6618BD43577C7BA912E0F7B2783057FD43B (void);
// 0x00000075 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::GetMeshAndSetIndex(Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/MeshType,UnityEngine.SkinnedMeshRenderer&,System.Int32&)
extern void VoiceHandler_GetMeshAndSetIndex_m0D6335810257E0658E6F3E7D4740F7051C6CDDD2 (void);
// 0x00000076 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::SetBlendshapeWeights(System.Single)
extern void VoiceHandler_SetBlendshapeWeights_mE7D5FF299BED1202F7B88D56DAE3A7E7A4D633A4 (void);
// 0x00000077 System.Collections.IEnumerator Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::CheckIOSMicrophonePermission()
extern void VoiceHandler_CheckIOSMicrophonePermission_m616B6A6A24D700321262DC6C3E9BFA6123BF79B2 (void);
// 0x00000078 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::OnDestroy()
extern void VoiceHandler_OnDestroy_mD9BDB8CE7D4CFD13C3732F597CF4BEA32084C431 (void);
// 0x00000079 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::.ctor()
extern void VoiceHandler__ctor_mA36B62D1EF5145F985AB61B2272CB794588D86FB (void);
// 0x0000007A System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler::<SetBlendshapeWeights>g__SetBlendShapeWeight|22_0(UnityEngine.SkinnedMeshRenderer,System.Int32,Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<>c__DisplayClass22_0&)
extern void VoiceHandler_U3CSetBlendshapeWeightsU3Eg__SetBlendShapeWeightU7C22_0_m41B18EE5CDEBA498B926EBA87611CBACAB08439A (void);
// 0x0000007B System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::.ctor(System.Int32)
extern void U3CCheckIOSMicrophonePermissionU3Ed__23__ctor_m264C205C0BE8F3073576CE32182AA5F0355C38A9 (void);
// 0x0000007C System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::System.IDisposable.Dispose()
extern void U3CCheckIOSMicrophonePermissionU3Ed__23_System_IDisposable_Dispose_m0B021614E8EEB33ED6F76C90E73991D7010EE85D (void);
// 0x0000007D System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::MoveNext()
extern void U3CCheckIOSMicrophonePermissionU3Ed__23_MoveNext_m87B979C5E3ACE387E54E7D7B9C8AADE9E619B07B (void);
// 0x0000007E System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m932D7A3FFA7C4E9FBD405B0F5E6139FF797C0442 (void);
// 0x0000007F System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::System.Collections.IEnumerator.Reset()
extern void U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_Reset_mE4D19B2D8261B6BC433381F815813EA2A43CC4D7 (void);
// 0x00000080 System.Object Wolf3D.ReadyPlayerMe.AvatarSDK.VoiceHandler/<CheckIOSMicrophonePermission>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_get_Current_m32974A9C230173BA61BD2D8F28060675019EE461 (void);
// 0x00000081 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData::IsOutfitMasculine()
extern void AvatarMetaData_IsOutfitMasculine_mA611D4EC8A43B4E1F23034EE091BF8D0CE46B8C8 (void);
// 0x00000082 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData::IsFullbody()
extern void AvatarMetaData_IsFullbody_m5CC81E8F9F1C73E9F01E50F5695D189B5EF19632 (void);
// 0x00000083 System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData::ToString()
extern void AvatarMetaData_ToString_m6EDCA59173AB0AB71DCAD7A611DB8FEF2FADF53A (void);
// 0x00000084 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarMetaData::.ctor()
extern void AvatarMetaData__ctor_m6A93DA1FF95E250A8AC74236B5F4AAC0017AA906 (void);
// 0x00000085 System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_Extension()
extern void AvatarUri_get_Extension_m7CB982F51D2BC6808595132919BDDAA41553C5C5 (void);
// 0x00000086 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_Extension(System.String)
extern void AvatarUri_set_Extension_mF323C6D000A26393118A08C8AB59B2D14DEA5898 (void);
// 0x00000087 System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_ModelName()
extern void AvatarUri_get_ModelName_m7C276606C508001D9F08A945A557E6AD32DD334F (void);
// 0x00000088 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_ModelName(System.String)
extern void AvatarUri_set_ModelName_m0B24DB53617858C7B3BFBA4CBD027B1C3B4F1284 (void);
// 0x00000089 System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_ModelPath()
extern void AvatarUri_get_ModelPath_m6D3C07770079C06DB146CFE587697662049CD4B6 (void);
// 0x0000008A System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_ModelPath(System.String)
extern void AvatarUri_set_ModelPath_m10161E6936F6A9B3BC4E7FADF82437E98167A36B (void);
// 0x0000008B System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_AbsoluteUrl()
extern void AvatarUri_get_AbsoluteUrl_mC941D4F13653D6E106E747785E35D8E37BFB854A (void);
// 0x0000008C System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_AbsoluteUrl(System.String)
extern void AvatarUri_set_AbsoluteUrl_m0F2DAA5A296AC51CCA4E93C61F08AEB09D76B732 (void);
// 0x0000008D System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_AbsolutePath()
extern void AvatarUri_get_AbsolutePath_m9C4FAE3747B4467F9E049337A6A35A663E717CA0 (void);
// 0x0000008E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_AbsolutePath(System.String)
extern void AvatarUri_set_AbsolutePath_m0A0348B71E1A7438CB60BD36F4DB3FBB3616A828 (void);
// 0x0000008F System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_AbsoluteName()
extern void AvatarUri_get_AbsoluteName_mB90E1916C3D359B671166D2104EF207EC8EEB487 (void);
// 0x00000090 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_AbsoluteName(System.String)
extern void AvatarUri_set_AbsoluteName_mB5FFDFE732D8E1F48AFA2A904AF29EAFEB7648F7 (void);
// 0x00000091 System.String Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::get_MetaDataUrl()
extern void AvatarUri_get_MetaDataUrl_m409F1074743073E4118DD865A7232A46B3AF1702 (void);
// 0x00000092 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::set_MetaDataUrl(System.String)
extern void AvatarUri_set_MetaDataUrl_m2BFE9FFD795BB7413C010C4287A633B198E3CF1F (void);
// 0x00000093 System.Threading.Tasks.Task`1<Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri> Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::Create(System.String)
extern void AvatarUri_Create_m248A0429CF7D4306570B2808B051E1683814C184 (void);
// 0x00000094 Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::CreateFromURL(System.String)
extern void AvatarUri_CreateFromURL_m13C5A5072DC706CE123440F0E6BB67A7392011CD (void);
// 0x00000095 System.Threading.Tasks.Task`1<System.String> Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::GetUrlFromShortCode(System.String)
extern void AvatarUri_GetUrlFromShortCode_m270A0D285ED4C8DA39692B922DFF3C20591BA134 (void);
// 0x00000096 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri::.ctor()
extern void AvatarUri__ctor_m86FB07D1ACAA0E7B998501B9EE8280B44C879AF0 (void);
// 0x00000097 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri/<Create>d__30::MoveNext()
extern void U3CCreateU3Ed__30_MoveNext_m03A90E20C246E269C23775C030305659959B9521 (void);
// 0x00000098 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri/<Create>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E (void);
// 0x00000099 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri/<GetUrlFromShortCode>d__32::MoveNext()
extern void U3CGetUrlFromShortCodeU3Ed__32_MoveNext_m9B9713B7096F62F82E7EA00CDD8D2F5C21C1BE89 (void);
// 0x0000009A System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.AvatarUri/<GetUrlFromShortCode>d__32::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C (void);
// 0x0000009B UnityEngine.Coroutine Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods::Run(System.Collections.IEnumerator)
extern void ExtensionMethods_Run_mA83E080C59D109AD7966D664986900522591A182 (void);
// 0x0000009C UnityEngine.SkinnedMeshRenderer Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods::GetMeshRenderer(UnityEngine.GameObject,Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/MeshType)
extern void ExtensionMethods_GetMeshRenderer_m7E058B376E70F57DF1238B386580D6E3BEC1F0E7 (void);
// 0x0000009D System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods::.cctor()
extern void ExtensionMethods__cctor_mD221B82969735C9B14E9753F621630506417D471 (void);
// 0x0000009E System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods::<GetMeshRenderer>g__GetMesh|7_3(System.Func`2<UnityEngine.SkinnedMeshRenderer,System.Boolean>,Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c__DisplayClass7_0&)
extern void ExtensionMethods_U3CGetMeshRendererU3Eg__GetMeshU7C7_3_m5C71F949054653625ECF6227E9D0B5F6F847200B (void);
// 0x0000009F System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/CoroutineRunner::Finalize()
extern void CoroutineRunner_Finalize_m164896F3BEEC1C0F678FAB766736B4EEACC72C06 (void);
// 0x000000A0 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/CoroutineRunner::.ctor()
extern void CoroutineRunner__ctor_mF33E200941BFAB35CBA728FC5DD26605C8F7B039 (void);
// 0x000000A1 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c::.cctor()
extern void U3CU3Ec__cctor_m4EE3EF58C42C9BB154C2900CB60D0BF2C53B2A32 (void);
// 0x000000A2 System.Void Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c::.ctor()
extern void U3CU3Ec__ctor_m3E5A3B189EA872FF4D056ED2E954F09A48F8141B (void);
// 0x000000A3 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c::<GetMeshRenderer>b__7_0(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CGetMeshRendererU3Eb__7_0_m2A4057BED3A5DFA2CE74E5DC4D0B9EDD998BAC28 (void);
// 0x000000A4 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c::<GetMeshRenderer>b__7_1(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CGetMeshRendererU3Eb__7_1_mD9D11B7B4CA9E14239FD52B1DB92D1BC396C7C31 (void);
// 0x000000A5 System.Boolean Wolf3D.ReadyPlayerMe.AvatarSDK.ExtensionMethods/<>c::<GetMeshRenderer>b__7_2(UnityEngine.SkinnedMeshRenderer)
extern void U3CU3Ec_U3CGetMeshRendererU3Eb__7_2_m7AE1735E1E30601EA8D312E98D52C693E6AC802F (void);
// 0x000000A6 System.Int32 Siccity.GLTFUtility.EnumExtensions::ByteSize(Siccity.GLTFUtility.GLType)
extern void EnumExtensions_ByteSize_m35DC2853AF8B37B64D323F8334391B278BDF9A5E (void);
// 0x000000A7 System.Int32 Siccity.GLTFUtility.EnumExtensions::ComponentCount(Siccity.GLTFUtility.AccessorType)
extern void EnumExtensions_ComponentCount_m49267065A12FAA7BC7594663FDEDC27EF2B2A1DC (void);
// 0x000000A8 System.Void Siccity.GLTFUtility.Exporter::ExportGLB(UnityEngine.GameObject)
extern void Exporter_ExportGLB_m43ECBEFF6CC882154E10A04AF8575C1F80C8FB33 (void);
// 0x000000A9 System.Void Siccity.GLTFUtility.Exporter::ExportGLTF(UnityEngine.GameObject)
extern void Exporter_ExportGLTF_mFE586D17500DB7AFBB49B066570E9604C6AB54F1 (void);
// 0x000000AA Siccity.GLTFUtility.GLTFObject Siccity.GLTFUtility.Exporter::CreateGLTFObject(UnityEngine.Transform)
extern void Exporter_CreateGLTFObject_mAE3BE1E824FD2CE22BAB7BA978C87DA8F45ED8FB (void);
// 0x000000AB UnityEngine.Coroutine Siccity.GLTFUtility.Extensions::RunCoroutine(System.Collections.IEnumerator)
extern void Extensions_RunCoroutine_m0CA03A7C0B5CB0A6CB46D0F21EFED6DE5E3011B2 (void);
// 0x000000AC T[] Siccity.GLTFUtility.Extensions::SubArray(T[],System.Int32,System.Int32)
// 0x000000AD System.Void Siccity.GLTFUtility.Extensions::UnpackTRS(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void Extensions_UnpackTRS_mF30329BE0D6854C0EDEB2CFA0F03506703B25339 (void);
// 0x000000AE System.Void Siccity.GLTFUtility.Extensions/CoroutineRunner::.ctor()
extern void CoroutineRunner__ctor_m3D99F94CCC341D48667E8B778BD90517A4DD4F1E (void);
// 0x000000AF System.Void Siccity.GLTFUtility.KHR_texture_transform::Apply(Siccity.GLTFUtility.GLTFMaterial/TextureInfo,UnityEngine.Material,System.String)
extern void KHR_texture_transform_Apply_m87448FE0C6C66F7A97191645B0204739A7AD90C5 (void);
// 0x000000B0 System.Void Siccity.GLTFUtility.KHR_texture_transform::.ctor()
extern void KHR_texture_transform__ctor_m54EDD81C9E18B042A5C7D28259C0DA2BF2B0880D (void);
// 0x000000B1 UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadFromFile(System.String,Siccity.GLTFUtility.Format)
extern void Importer_LoadFromFile_m058C890E55F38451D516608EA6010B7A735DD666 (void);
// 0x000000B2 UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadFromFile(System.String,Siccity.GLTFUtility.ImportSettings,Siccity.GLTFUtility.Format)
extern void Importer_LoadFromFile_m5277D0168E62F8752F6BDD8C8C2AEAA765B58276 (void);
// 0x000000B3 UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadFromFile(System.String,Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&,Siccity.GLTFUtility.Format)
extern void Importer_LoadFromFile_m220179CD75F5AF7692E3C57F31343459DEC43473 (void);
// 0x000000B4 UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadFromBytes(System.Byte[],Siccity.GLTFUtility.ImportSettings)
extern void Importer_LoadFromBytes_mE024166143950D6723FCC8955CB55410B0159AB7 (void);
// 0x000000B5 UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadFromBytes(System.Byte[],Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&)
extern void Importer_LoadFromBytes_m9CFA768A7A9A6570B11E7BA8DB158E47E4BEFFF1 (void);
// 0x000000B6 System.Void Siccity.GLTFUtility.Importer::LoadFromFileAsync(System.String,Siccity.GLTFUtility.ImportSettings,System.Action`1<UnityEngine.GameObject>,System.Action`1<System.Single>)
extern void Importer_LoadFromFileAsync_mCDABFA0973FBE7F1AEF4065C891651801C6365F2 (void);
// 0x000000B7 UnityEngine.GameObject Siccity.GLTFUtility.Importer::ImportGLB(System.String,Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&)
extern void Importer_ImportGLB_m29FD3339910FF6610E6999AAAA49BD894BA09107 (void);
// 0x000000B8 UnityEngine.GameObject Siccity.GLTFUtility.Importer::ImportGLB(System.Byte[],Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&)
extern void Importer_ImportGLB_mF5CE566623017A29C79E2DDC42D0196618F339EC (void);
// 0x000000B9 System.Void Siccity.GLTFUtility.Importer::ImportGLBAsync(System.String,Siccity.GLTFUtility.ImportSettings,System.Action`1<UnityEngine.GameObject>,System.Action`1<System.Single>)
extern void Importer_ImportGLBAsync_mF77E7F7950E28BC77CAC94EDE6BFB2FD24DF3B4E (void);
// 0x000000BA System.Void Siccity.GLTFUtility.Importer::ImportGLBAsync(System.Byte[],Siccity.GLTFUtility.ImportSettings,System.Action`1<UnityEngine.GameObject>,System.Action`1<System.Single>)
extern void Importer_ImportGLBAsync_m4F9C38158E5E900770F85E78FD5A5F761D89A19C (void);
// 0x000000BB System.String Siccity.GLTFUtility.Importer::GetGLBJson(System.IO.Stream,System.Int64&)
extern void Importer_GetGLBJson_m44C120D96396FAFEAC9220B45A4B75C912604917 (void);
// 0x000000BC UnityEngine.GameObject Siccity.GLTFUtility.Importer::ImportGLTF(System.String,Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&)
extern void Importer_ImportGLTF_m76F7D953B4471423ED71158143608036E5C67BAB (void);
// 0x000000BD System.Void Siccity.GLTFUtility.Importer::ImportGLTFAsync(System.String,Siccity.GLTFUtility.ImportSettings,System.Action`1<UnityEngine.GameObject>,System.Action`1<System.Single>)
extern void Importer_ImportGLTFAsync_mBF7C7DEF9DDCE12991AFE74652902453F7994C90 (void);
// 0x000000BE UnityEngine.GameObject Siccity.GLTFUtility.Importer::LoadInternal(Siccity.GLTFUtility.GLTFObject,System.String,System.Byte[],System.Int64,Siccity.GLTFUtility.ImportSettings,UnityEngine.AnimationClip[]&)
extern void Importer_LoadInternal_m5B17FBF2634BA38FBA7BAF2FE9B4479660E0A4DC (void);
// 0x000000BF System.Collections.IEnumerator Siccity.GLTFUtility.Importer::LoadAsync(System.String,System.String,System.Byte[],System.Int64,Siccity.GLTFUtility.ImportSettings,System.Action`1<UnityEngine.GameObject>,System.Action`1<System.Single>)
extern void Importer_LoadAsync_m4FC08F596DD7774E818976E385460DD131E93398 (void);
// 0x000000C0 System.Collections.IEnumerator Siccity.GLTFUtility.Importer::TaskSupervisor(Siccity.GLTFUtility.Importer/ImportTask,System.Action`1<System.Single>)
extern void Importer_TaskSupervisor_m7DD1809D6E78E40446C69FBDD60D87926EB76DF1 (void);
// 0x000000C1 System.Void Siccity.GLTFUtility.Importer::CheckExtensions(Siccity.GLTFUtility.GLTFObject)
extern void Importer_CheckExtensions_m650DA6F0BBE3B7318C947AD5F6F679DDB183A88D (void);
// 0x000000C2 System.Void Siccity.GLTFUtility.Importer/ImportTask`1::.ctor(Siccity.GLTFUtility.Importer/ImportTask[])
// 0x000000C3 TReturn Siccity.GLTFUtility.Importer/ImportTask`1::RunSynchronously()
// 0x000000C4 System.Boolean Siccity.GLTFUtility.Importer/ImportTask::get_IsReady()
extern void ImportTask_get_IsReady_m33C7CC33C16D6A1D6899FCDAC0CA22340ED44D7C (void);
// 0x000000C5 System.Boolean Siccity.GLTFUtility.Importer/ImportTask::get_IsCompleted()
extern void ImportTask_get_IsCompleted_mF1257CE54E903AA412CB894E7F1BA17227353B4F (void);
// 0x000000C6 System.Void Siccity.GLTFUtility.Importer/ImportTask::set_IsCompleted(System.Boolean)
extern void ImportTask_set_IsCompleted_m505F23568DDF7C554EE27016272CEE74A71F214B (void);
// 0x000000C7 System.Void Siccity.GLTFUtility.Importer/ImportTask::.ctor(Siccity.GLTFUtility.Importer/ImportTask[])
extern void ImportTask__ctor_mA8C2D32FF44B54CC145E63E941CDE524EF7E4A93 (void);
// 0x000000C8 System.Collections.IEnumerator Siccity.GLTFUtility.Importer/ImportTask::OnCoroutine(System.Action`1<System.Single>)
extern void ImportTask_OnCoroutine_m728AA9D6F6B6E6C99C57A8B3FFE0BCFDDD317EED (void);
// 0x000000C9 System.Void Siccity.GLTFUtility.Importer/ImportTask/<>c::.cctor()
extern void U3CU3Ec__cctor_m401EEF16A480CBC39E8D3797084AEE544A31EB78 (void);
// 0x000000CA System.Void Siccity.GLTFUtility.Importer/ImportTask/<>c::.ctor()
extern void U3CU3Ec__ctor_m511CAF964CA511B97BBDF6A52F68D37240D5BB43 (void);
// 0x000000CB System.Boolean Siccity.GLTFUtility.Importer/ImportTask/<>c::<get_IsReady>b__3_0(Siccity.GLTFUtility.Importer/ImportTask)
extern void U3CU3Ec_U3Cget_IsReadyU3Eb__3_0_mC567962DCD9C4EDFE4C9B20C87D9AE827BE31728 (void);
// 0x000000CC System.Void Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::.ctor(System.Int32)
extern void U3COnCoroutineU3Ed__9__ctor_m48041C4FB14FB5782E074B0C495000063148026C (void);
// 0x000000CD System.Void Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::System.IDisposable.Dispose()
extern void U3COnCoroutineU3Ed__9_System_IDisposable_Dispose_m67ABBF5C56A17B98AF1420671F6CDA4D7C9E5A1C (void);
// 0x000000CE System.Boolean Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::MoveNext()
extern void U3COnCoroutineU3Ed__9_MoveNext_m6A35C46A11D40191E98C1D30B52046EE16F9DDB1 (void);
// 0x000000CF System.Object Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9AB5731BBA2862D0FB21F18BC99898273B0BC0A5 (void);
// 0x000000D0 System.Void Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::System.Collections.IEnumerator.Reset()
extern void U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m293618DFBDD8ECC5807A2634D4D9485B11229E8C (void);
// 0x000000D1 System.Object Siccity.GLTFUtility.Importer/ImportTask/<OnCoroutine>d__9::System.Collections.IEnumerator.get_Current()
extern void U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mCC55F658F9053F6137398A7A715B3B0F7BB823D5 (void);
// 0x000000D2 System.Void Siccity.GLTFUtility.Importer/<>c::.cctor()
extern void U3CU3Ec__cctor_mDDC22C929EA7C127F8702D02401D14AF4063546A (void);
// 0x000000D3 System.Void Siccity.GLTFUtility.Importer/<>c::.ctor()
extern void U3CU3Ec__ctor_mB3B996537C243D877FEE21D4571312DE74CEFC0D (void);
// 0x000000D4 UnityEngine.AnimationClip Siccity.GLTFUtility.Importer/<>c::<LoadInternal>b__15_0(Siccity.GLTFUtility.GLTFAnimation/ImportResult)
extern void U3CU3Ec_U3CLoadInternalU3Eb__15_0_mF784F361A79029E4AB7EA9237F090E3EAFC017C8 (void);
// 0x000000D5 System.Boolean Siccity.GLTFUtility.Importer/<>c::<LoadAsync>b__16_1(Siccity.GLTFUtility.Importer/ImportTask)
extern void U3CU3Ec_U3CLoadAsyncU3Eb__16_1_m7A054BED01419416666F59134E62C1BC592D70C7 (void);
// 0x000000D6 System.Void Siccity.GLTFUtility.Importer/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m1CFC7891B19B5B9F7F38DB0C0BADD3134C7F600A (void);
// 0x000000D7 Siccity.GLTFUtility.GLTFObject Siccity.GLTFUtility.Importer/<>c__DisplayClass16_0::<LoadAsync>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CLoadAsyncU3Eb__0_m13F496152C3596DE76320B7E4B9211781721BDE8 (void);
// 0x000000D8 System.Void Siccity.GLTFUtility.Importer/<LoadAsync>d__16::.ctor(System.Int32)
extern void U3CLoadAsyncU3Ed__16__ctor_m76847F3E08DDF071B407B405D7ADABBC307726AF (void);
// 0x000000D9 System.Void Siccity.GLTFUtility.Importer/<LoadAsync>d__16::System.IDisposable.Dispose()
extern void U3CLoadAsyncU3Ed__16_System_IDisposable_Dispose_mD51CF56256EB3889DCA5538E6F7568F1EE24DD68 (void);
// 0x000000DA System.Boolean Siccity.GLTFUtility.Importer/<LoadAsync>d__16::MoveNext()
extern void U3CLoadAsyncU3Ed__16_MoveNext_m5DC7799B30A27DABA23BD780523206929D43A919 (void);
// 0x000000DB System.Object Siccity.GLTFUtility.Importer/<LoadAsync>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAsyncU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C24BE0512ABFB360B2F421DEA952FD566645D14 (void);
// 0x000000DC System.Void Siccity.GLTFUtility.Importer/<LoadAsync>d__16::System.Collections.IEnumerator.Reset()
extern void U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_Reset_m0D9F51120F9B7E43A23C9DE624925382DA3160A5 (void);
// 0x000000DD System.Object Siccity.GLTFUtility.Importer/<LoadAsync>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_get_Current_m365D7F07B28C4C5807A32EB050BF876E6D85D55C (void);
// 0x000000DE System.Void Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::.ctor(System.Int32)
extern void U3CTaskSupervisorU3Ed__17__ctor_mF99DE5C7C4C73E2783E7776108CA8B3ABE42645E (void);
// 0x000000DF System.Void Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::System.IDisposable.Dispose()
extern void U3CTaskSupervisorU3Ed__17_System_IDisposable_Dispose_m07973E18B963E1230482ECB0589595E0DB9DF5AD (void);
// 0x000000E0 System.Boolean Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::MoveNext()
extern void U3CTaskSupervisorU3Ed__17_MoveNext_mCCB43631E1D8059BFE73AE147FF2479EDA3D2424 (void);
// 0x000000E1 System.Object Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTaskSupervisorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49D10D59FCF34D9FDA6D7EAAAE8AE1B10FF12E29 (void);
// 0x000000E2 System.Void Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::System.Collections.IEnumerator.Reset()
extern void U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_Reset_mF7742948AC166E4AABD25D9C2105F6B5519635E4 (void);
// 0x000000E3 System.Object Siccity.GLTFUtility.Importer/<TaskSupervisor>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_get_Current_m219B092F317FA9E9F45B3319589854F628F5F065 (void);
// 0x000000E4 System.Void Siccity.GLTFUtility.ImportSettings::.ctor()
extern void ImportSettings__ctor_m34F786086FC674EC634567F897C8D5509276C2EA (void);
// 0x000000E5 UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::get_Metallic()
extern void ShaderSettings_get_Metallic_mD59F07C8A7DBD36167DAC0C875EDFB8AE43C0291 (void);
// 0x000000E6 UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::get_MetallicBlend()
extern void ShaderSettings_get_MetallicBlend_m0906331D0C9D57DE332AF2E44BC031CC3A786392 (void);
// 0x000000E7 UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::get_Specular()
extern void ShaderSettings_get_Specular_m71B0440D77958A7D0746F8534F058FAE65C3FF42 (void);
// 0x000000E8 UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::get_SpecularBlend()
extern void ShaderSettings_get_SpecularBlend_m49E435F053AF689777E7E0AD6D1893F31C9087DB (void);
// 0x000000E9 System.Void Siccity.GLTFUtility.ShaderSettings::CacheDefaultShaders()
extern void ShaderSettings_CacheDefaultShaders_m202173C22E952B5AFF80F043E35AA4D14611E39B (void);
// 0x000000EA UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::GetDefaultMetallic()
extern void ShaderSettings_GetDefaultMetallic_m7DE9E691A400F9ECB1AB1C0951CBE17923CBA233 (void);
// 0x000000EB UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::GetDefaultMetallicBlend()
extern void ShaderSettings_GetDefaultMetallicBlend_mB5A7E7BD2A65CE3F2C8FE2F05B9775BD66C84902 (void);
// 0x000000EC UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::GetDefaultSpecular()
extern void ShaderSettings_GetDefaultSpecular_mDB795CCB21E4B5F540500EE7B0FC2EB53BA752BA (void);
// 0x000000ED UnityEngine.Shader Siccity.GLTFUtility.ShaderSettings::GetDefaultSpecularBlend()
extern void ShaderSettings_GetDefaultSpecularBlend_m275BBE072EE5470DAB34B1D22366F39B1DD71690 (void);
// 0x000000EE System.Void Siccity.GLTFUtility.ShaderSettings::.ctor()
extern void ShaderSettings__ctor_m42A29BCD5922383ED0ADDD35455A80DC1CAA754B (void);
// 0x000000EF Siccity.GLTFUtility.GLTFAccessor/ImportResult Siccity.GLTFUtility.GLTFAccessor::Import(Siccity.GLTFUtility.GLTFBufferView/ImportResult[])
extern void GLTFAccessor_Import_m03BE7327C23901FA49A6C706BD6DF9BDF5D0E3FB (void);
// 0x000000F0 System.Void Siccity.GLTFUtility.GLTFAccessor::.ctor()
extern void GLTFAccessor__ctor_m49FDFCF1EC231D1268A3AA35BA28A6E5EC2F1EF1 (void);
// 0x000000F1 System.Void Siccity.GLTFUtility.GLTFAccessor/Sparse::.ctor()
extern void Sparse__ctor_m6197ABAFE9C8CAE6A777F72C299941CA223D9ADA (void);
// 0x000000F2 System.Void Siccity.GLTFUtility.GLTFAccessor/Sparse/Values::.ctor()
extern void Values__ctor_mB4B3EABF34AB1F0E95E6B31DC5B3D7E8CA3B6766 (void);
// 0x000000F3 System.Void Siccity.GLTFUtility.GLTFAccessor/Sparse/Indices::.ctor()
extern void Indices__ctor_m029072542F14F9914C1C9AD62239BB962F97B468 (void);
// 0x000000F4 UnityEngine.Matrix4x4[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadMatrix4x4()
extern void ImportResult_ReadMatrix4x4_m5C65684AEDCD8D87FD10A70D11E5C6C0F15D345C (void);
// 0x000000F5 UnityEngine.Vector4[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadVec4()
extern void ImportResult_ReadVec4_mFD19F92DC38075E2F815EF5A722831B8633B0831 (void);
// 0x000000F6 UnityEngine.Color[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadColor()
extern void ImportResult_ReadColor_m190921C7E32BD6A65A9E1FF5D9A1F7648D43E487 (void);
// 0x000000F7 UnityEngine.Vector3[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadVec3()
extern void ImportResult_ReadVec3_m5B4DA9644EF368B9D828F59531CCAA81DE310D9D (void);
// 0x000000F8 UnityEngine.Vector2[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadVec2()
extern void ImportResult_ReadVec2_m5EB1C60CEE9143AE18571D4E009C6A81FC7747B6 (void);
// 0x000000F9 System.Single[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadFloat()
extern void ImportResult_ReadFloat_m99D81FC447786E9B9C7A93C20D2E3277ED335E63 (void);
// 0x000000FA System.Int32[] Siccity.GLTFUtility.GLTFAccessor/ImportResult::ReadInt()
extern void ImportResult_ReadInt_mA9821C609234FBDCF4954FA6E8FBB52F7F7C432A (void);
// 0x000000FB System.Func`2<BufferedBinaryReader,System.Int32> Siccity.GLTFUtility.GLTFAccessor/ImportResult::GetIntReader(Siccity.GLTFUtility.GLType)
extern void ImportResult_GetIntReader_m60E69F5F42DA7F9EE5C8695B1FC531998312F059 (void);
// 0x000000FC System.Func`2<BufferedBinaryReader,System.Single> Siccity.GLTFUtility.GLTFAccessor/ImportResult::GetFloatReader(Siccity.GLTFUtility.GLType)
extern void ImportResult_GetFloatReader_m78ED6CB1AEBC09F46651988560CC4E97ADC20B3E (void);
// 0x000000FD System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult::GetComponentSize()
extern void ImportResult_GetComponentSize_mDECD61E46D694ECAADA96F0E9A850D58BE3F8D75 (void);
// 0x000000FE System.Boolean Siccity.GLTFUtility.GLTFAccessor/ImportResult::ValidateByteStride(System.Int32)
extern void ImportResult_ValidateByteStride_m01087D76571B6AA5E55A1E733B58C2EE227AF532 (void);
// 0x000000FF System.Boolean Siccity.GLTFUtility.GLTFAccessor/ImportResult::ValidateAccessorType(Siccity.GLTFUtility.AccessorType,Siccity.GLTFUtility.AccessorType)
extern void ImportResult_ValidateAccessorType_m061737BC0198ECD54CB321700DF96424E58CDA31 (void);
// 0x00000100 System.Boolean Siccity.GLTFUtility.GLTFAccessor/ImportResult::ValidateAccessorTypeAny(Siccity.GLTFUtility.AccessorType,Siccity.GLTFUtility.AccessorType[])
extern void ImportResult_ValidateAccessorTypeAny_m5CE190A5035881071F70FF155FB7FE9CF408BD72 (void);
// 0x00000101 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult::.ctor()
extern void ImportResult__ctor_m4F8C70D14809EC9C1DD66AE9EE6C0067CB84F669 (void);
// 0x00000102 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult/Sparse::.ctor()
extern void Sparse__ctor_m4B42BCB08F4D361F87350BF71D056BE5E36C0B8A (void);
// 0x00000103 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult/Sparse/Values::.ctor()
extern void Values__ctor_m797FE3FD346A7C05897F4ACD2006F8EFE2C8E45B (void);
// 0x00000104 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult/Sparse/Indices::.ctor()
extern void Indices__ctor_m45FC7D34D1DC715FD0D6F6BCE2ACE1B858CCC367 (void);
// 0x00000105 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::.cctor()
extern void U3CU3Ec__cctor_mA58C424BCA709CFF8A8B5F3E5AB4F344FADDEA78 (void);
// 0x00000106 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::.ctor()
extern void U3CU3Ec__ctor_mD9E00632150502572A482C375B346D4D5AB1630F (void);
// 0x00000107 System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_0(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_0_m5BA2B435FF5C01AFD0DDB48254D311E8E75CE525 (void);
// 0x00000108 System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_1(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_1_m05336D4EF2293AFB4A21D116156F52B8605AD123 (void);
// 0x00000109 System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_2(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_2_m368122150278BA7380B428DFB66E2D3C07000EB3 (void);
// 0x0000010A System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_3(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_3_m82F59D82B4E9AA76C76550DCF0FC98D69D0CF4FD (void);
// 0x0000010B System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_4(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_4_mEB1C67A4FBF84D3550333A244D50791BED270031 (void);
// 0x0000010C System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_5(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_5_m148BD797DCD0B3FCA6E2FF7C48A3A2896FF6E701 (void);
// 0x0000010D System.Int32 Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetIntReader>b__15_6(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetIntReaderU3Eb__15_6_m575F43E651AA139D2C48A91811083291AA0123D4 (void);
// 0x0000010E System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_0(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_0_m75C31BDCB7059D735D6E65647C46EB4624FB53D3 (void);
// 0x0000010F System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_1(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_1_mE7092443697BA7C1EDD28D0031C6C1BC2640A5E5 (void);
// 0x00000110 System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_2(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_2_mE200DC4CFCE207BC55EA61EF5E2D936DB14EED93 (void);
// 0x00000111 System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_3(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_3_m24BBD284075E0154C7407199D4955411597CF991 (void);
// 0x00000112 System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_4(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_4_m912FD88EDD67D1EDDB0B4E760037F469D5D31814 (void);
// 0x00000113 System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_5(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_5_m339242F45669729631D387F4A6137CB7321F1210 (void);
// 0x00000114 System.Single Siccity.GLTFUtility.GLTFAccessor/ImportResult/<>c::<GetFloatReader>b__16_6(BufferedBinaryReader)
extern void U3CU3Ec_U3CGetFloatReaderU3Eb__16_6_m8560F3A7B66AC0C7976DB2A3872BA11DB6F56408 (void);
// 0x00000115 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFAccessor>,Siccity.GLTFUtility.GLTFBufferView/ImportTask)
extern void ImportTask__ctor_m1686989991B0C4688310310A8B2A522798B4ABAC (void);
// 0x00000116 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m6FB7AD07D3A16E030A3784BE079991E693A42060 (void);
// 0x00000117 System.Void Siccity.GLTFUtility.GLTFAccessor/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m635D56C7D5EC9685DA1204E8BA6BCC60DCF845A9 (void);
// 0x00000118 Siccity.GLTFUtility.GLTFAnimation/ImportResult Siccity.GLTFUtility.GLTFAnimation::Import(Siccity.GLTFUtility.GLTFAccessor/ImportResult[],Siccity.GLTFUtility.GLTFNode/ImportResult[],Siccity.GLTFUtility.ImportSettings)
extern void GLTFAnimation_Import_m8AED9E41BD1AF833636AFD853403B8C2B54B82B9 (void);
// 0x00000119 UnityEngine.Keyframe Siccity.GLTFUtility.GLTFAnimation::CreateKeyframe(System.Int32,System.Single[],T[],System.Func`2<T,System.Single>,Siccity.GLTFUtility.InterpolationMode)
// 0x0000011A System.Void Siccity.GLTFUtility.GLTFAnimation::.ctor()
extern void GLTFAnimation__ctor_m88B6A57FB663F69A39AA55E3A11575B984FD57F4 (void);
// 0x0000011B System.Void Siccity.GLTFUtility.GLTFAnimation/Sampler::.ctor()
extern void Sampler__ctor_mCD7EE8A365A6859DF230D28C357D194C3F51FC26 (void);
// 0x0000011C System.Void Siccity.GLTFUtility.GLTFAnimation/Channel::.ctor()
extern void Channel__ctor_mEDEFF0FBA488E54C46624D52B21C538DD306BB2C (void);
// 0x0000011D System.Void Siccity.GLTFUtility.GLTFAnimation/Target::.ctor()
extern void Target__ctor_mBD5255A5403EAC750557FAA4F71B4D286FD8378B (void);
// 0x0000011E System.Void Siccity.GLTFUtility.GLTFAnimation/ImportResult::.ctor()
extern void ImportResult__ctor_m5251D20424615BC9E43D79D8008D7FECA099411A (void);
// 0x0000011F System.Void Siccity.GLTFUtility.GLTFAnimation/<>c::.cctor()
extern void U3CU3Ec__cctor_m5A668E586775BAFD14D9F9393491EC2F57688C3C (void);
// 0x00000120 System.Void Siccity.GLTFUtility.GLTFAnimation/<>c::.ctor()
extern void U3CU3Ec__ctor_mBE71CDA27C7E14749A7BB1FD0E132D4D3109607F (void);
// 0x00000121 System.Boolean Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_0(Siccity.GLTFUtility.GLTFNode/ImportResult)
extern void U3CU3Ec_U3CImportU3Eb__7_0_m6CDC747F13A20E7F6226A62633A616561DFE0793 (void);
// 0x00000122 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_1(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_1_mE1054BB35DC586A532AACDE0A5DF54AC10D6C511 (void);
// 0x00000123 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_2(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_2_m75CEB549938D5A852DA01D7793475427BCB5CE93 (void);
// 0x00000124 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_3(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_3_m8D237D17638AAB3AE71BCE21B46FC60F476A65F5 (void);
// 0x00000125 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_4(UnityEngine.Vector4)
extern void U3CU3Ec_U3CImportU3Eb__7_4_mFE6FCDAAC2E650CDE2C78D53061C39552C5B9AA7 (void);
// 0x00000126 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_5(UnityEngine.Vector4)
extern void U3CU3Ec_U3CImportU3Eb__7_5_mC19AE5D84FAAEA35046A5282241B7ED8000A52DF (void);
// 0x00000127 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_6(UnityEngine.Vector4)
extern void U3CU3Ec_U3CImportU3Eb__7_6_mD9F2E3AEA69B6B7D493E8D2390ACA2200647849A (void);
// 0x00000128 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_7(UnityEngine.Vector4)
extern void U3CU3Ec_U3CImportU3Eb__7_7_m536020F1CCF02CF40DE3B20D7FA0CDA85E054889 (void);
// 0x00000129 System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_8(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_8_m37786D7C3F398F4AA0894B3AFBCCF9220036EFFA (void);
// 0x0000012A System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_9(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_9_m690988444A65819269026F242B9A9BAC5D48B7F5 (void);
// 0x0000012B System.Single Siccity.GLTFUtility.GLTFAnimation/<>c::<Import>b__7_10(UnityEngine.Vector3)
extern void U3CU3Ec_U3CImportU3Eb__7_10_m68B1356BD8CA00B0883912527157E9D0025818E8 (void);
// 0x0000012C Siccity.GLTFUtility.GLTFAnimation/ImportResult[] Siccity.GLTFUtility.GLTFAnimationExtensions::Import(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFAnimation>,Siccity.GLTFUtility.GLTFAccessor/ImportResult[],Siccity.GLTFUtility.GLTFNode/ImportResult[],Siccity.GLTFUtility.ImportSettings)
extern void GLTFAnimationExtensions_Import_m3F291F7CC01854BE61DF4FE8617630A80E8D55F1 (void);
// 0x0000012D System.Void Siccity.GLTFUtility.GLTFAsset::.ctor()
extern void GLTFAsset__ctor_m2E5435A11B0564E6B54B0D1BF75C92703C3C7EE3 (void);
// 0x0000012E Siccity.GLTFUtility.GLTFBuffer/ImportResult Siccity.GLTFUtility.GLTFBuffer::Import(System.String,System.Byte[],System.Int64)
extern void GLTFBuffer_Import_m2321E35D9E9735F3E5A608ECD2D4A2D1C80E5B62 (void);
// 0x0000012F System.Void Siccity.GLTFUtility.GLTFBuffer::.ctor()
extern void GLTFBuffer__ctor_m904E8D9212E35FE97FE3AD4872F34CDF656168BC (void);
// 0x00000130 System.Void Siccity.GLTFUtility.GLTFBuffer/ImportResult::Dispose()
extern void ImportResult_Dispose_mAC75FAF48D450506451DB92E081AF037D9D465DF (void);
// 0x00000131 System.Void Siccity.GLTFUtility.GLTFBuffer/ImportResult::.ctor()
extern void ImportResult__ctor_mB56AA6B17C486D96553F1EC6952316155A480AD8 (void);
// 0x00000132 System.Void Siccity.GLTFUtility.GLTFBuffer/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFBuffer>,System.String,System.Byte[],System.Int64)
extern void ImportTask__ctor_m290BAB1B23DD1936DF98B8419A9A47B61261BB7A (void);
// 0x00000133 System.Void Siccity.GLTFUtility.GLTFBuffer/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m6C0EED70726AA554DC8CBD9200296DED0C4031C0 (void);
// 0x00000134 System.Void Siccity.GLTFUtility.GLTFBuffer/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mF1CCF067259D764F3D779F484B380E0B1048A154 (void);
// 0x00000135 System.Void Siccity.GLTFUtility.GLTFBufferView::.ctor()
extern void GLTFBufferView__ctor_m813A1D91373C5FF8498692FAF84ECF9E891EBF20 (void);
// 0x00000136 System.Void Siccity.GLTFUtility.GLTFBufferView/ImportResult::.ctor()
extern void ImportResult__ctor_m65F7CD3FCADED77304E1F469B6701A5D545235CF (void);
// 0x00000137 System.Void Siccity.GLTFUtility.GLTFBufferView/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFBufferView>,Siccity.GLTFUtility.GLTFBuffer/ImportTask)
extern void ImportTask__ctor_m71EA93DB8C4CC26AA4FBBFEF47F1C7658AB06514 (void);
// 0x00000138 System.Void Siccity.GLTFUtility.GLTFBufferView/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m99A3EB6C97D1E36E3ADBBADABE44BE756B21F6F5 (void);
// 0x00000139 System.Void Siccity.GLTFUtility.GLTFBufferView/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m8C57D34460367647824ED366C9153B9AD8A35AFB (void);
// 0x0000013A System.Void Siccity.GLTFUtility.GLTFCamera::.ctor()
extern void GLTFCamera__ctor_mDBE6527F9018BE1FD8FE2BB4F2115C5EA4E5F79E (void);
// 0x0000013B System.Void Siccity.GLTFUtility.GLTFCamera/Orthographic::.ctor()
extern void Orthographic__ctor_mB6EA6BA991C32A3989337109579FC71471238880 (void);
// 0x0000013C System.Void Siccity.GLTFUtility.GLTFCamera/Perspective::.ctor()
extern void Perspective__ctor_m353DEEACF172412C49693286FFD636C104758D14 (void);
// 0x0000013D System.Void Siccity.GLTFUtility.GLTFImage::.ctor()
extern void GLTFImage__ctor_m6EB9AF82F8ED524A9FC6846FFC6E491C47EB125B (void);
// 0x0000013E System.Void Siccity.GLTFUtility.GLTFImage/ImportResult::.ctor(System.Byte[],System.String)
extern void ImportResult__ctor_m4E1419C0874C38FF52CFF15A314CE79988DC9A5E (void);
// 0x0000013F System.Collections.IEnumerator Siccity.GLTFUtility.GLTFImage/ImportResult::CreateTextureAsync(System.Boolean,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.Single>)
extern void ImportResult_CreateTextureAsync_mE370E435B53F299305E45F88E0435DFF6DF0EE93 (void);
// 0x00000140 System.Void Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::.ctor(System.Int32)
extern void U3CCreateTextureAsyncU3Ed__3__ctor_m41C4795268B5A5FDBD09AAC759CB825B4E7ADDEC (void);
// 0x00000141 System.Void Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::System.IDisposable.Dispose()
extern void U3CCreateTextureAsyncU3Ed__3_System_IDisposable_Dispose_m309F94D77BA47F75B8D25738544C71739FF6FE9E (void);
// 0x00000142 System.Boolean Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::MoveNext()
extern void U3CCreateTextureAsyncU3Ed__3_MoveNext_mE1D1DEA5A0EDAA8C199965EC1269EE7BDF35B144 (void);
// 0x00000143 System.Void Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::<>m__Finally1()
extern void U3CCreateTextureAsyncU3Ed__3_U3CU3Em__Finally1_mCFAFCB3B96460B1412E4849A5C249C74E520B136 (void);
// 0x00000144 System.Object Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateTextureAsyncU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BA6DA399F310B5E0D0BE1275626D33722409938 (void);
// 0x00000145 System.Void Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_Reset_mC1E6231288207824BC54FEB4B4706F8AD9A2F8EE (void);
// 0x00000146 System.Object Siccity.GLTFUtility.GLTFImage/ImportResult/<CreateTextureAsync>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_get_Current_m0581A5FAAE854B83BCF99A47E55DAF51A4F0387B (void);
// 0x00000147 System.Void Siccity.GLTFUtility.GLTFImage/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFImage>,System.String,Siccity.GLTFUtility.GLTFBufferView/ImportTask)
extern void ImportTask__ctor_m1A726621D81C1A0BBA62D87565A43636502FCA60 (void);
// 0x00000148 System.Void Siccity.GLTFUtility.GLTFImage/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m015CC88C2A9EE8B523968039384B7F5CDA8CFF3E (void);
// 0x00000149 System.Void Siccity.GLTFUtility.GLTFImage/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m4E5339ADD7C9202FC24DC01D2B94F0ED3EB3E28E (void);
// 0x0000014A UnityEngine.Material Siccity.GLTFUtility.GLTFMaterial::get_defaultMaterial()
extern void GLTFMaterial_get_defaultMaterial_mEBF9532DF99E19839D892A8B64E00219EA681C3B (void);
// 0x0000014B System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMaterial::CreateMaterial(Siccity.GLTFUtility.GLTFTexture/ImportResult[],Siccity.GLTFUtility.ShaderSettings,System.Action`1<UnityEngine.Material>)
extern void GLTFMaterial_CreateMaterial_m1BFA5DB15E037943CB2915DFB9B2001020343636 (void);
// 0x0000014C System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMaterial::TryGetTexture(Siccity.GLTFUtility.GLTFTexture/ImportResult[],Siccity.GLTFUtility.GLTFMaterial/TextureInfo,System.Boolean,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.Single>)
extern void GLTFMaterial_TryGetTexture_m8BF1941ECA59813B13C2CD0DC135FC798F05043A (void);
// 0x0000014D System.Void Siccity.GLTFUtility.GLTFMaterial::.ctor()
extern void GLTFMaterial__ctor_m539C5E900EC8607C281A2FB3ABAC2C0EE07A04EB (void);
// 0x0000014E System.Void Siccity.GLTFUtility.GLTFMaterial/ImportResult::.ctor()
extern void ImportResult__ctor_mBA4D29E0CB83313D95F926BAEF5197D2A5A4B293 (void);
// 0x0000014F System.Void Siccity.GLTFUtility.GLTFMaterial/Extensions::.ctor()
extern void Extensions__ctor_m99E9A7AC1A4C37725E36EC5D5BA14BBB5C9CFFDC (void);
// 0x00000150 System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness::CreateMaterial(Siccity.GLTFUtility.GLTFTexture/ImportResult[],Siccity.GLTFUtility.AlphaMode,Siccity.GLTFUtility.ShaderSettings,System.Action`1<UnityEngine.Material>)
extern void PbrMetalRoughness_CreateMaterial_m510DD5D199D7102C9C43BBC3EB580743EBE52DAE (void);
// 0x00000151 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness::.ctor()
extern void PbrMetalRoughness__ctor_mDBD457660EBAED214B9EE118AFBEB138C02842DA (void);
// 0x00000152 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m2A8AE30EFB428AD0334E0259F5F36B7D91BC0C00 (void);
// 0x00000153 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<>c__DisplayClass5_0::<CreateMaterial>b__0(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__0_m319D000D5BEA4D5641492C65200B577238C5C10D (void);
// 0x00000154 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<>c__DisplayClass5_0::<CreateMaterial>b__1(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__1_mBC7E8EAC107163F40648295A174D5A39A6D98F74 (void);
// 0x00000155 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::.ctor(System.Int32)
extern void U3CCreateMaterialU3Ed__5__ctor_m0A51ABDB657E7515358686A7C789F8BBC753C944 (void);
// 0x00000156 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::System.IDisposable.Dispose()
extern void U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m13CED19482847457E82EBDB9245B7CD31CB07240 (void);
// 0x00000157 System.Boolean Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::MoveNext()
extern void U3CCreateMaterialU3Ed__5_MoveNext_mB20A36BCC15B15DED7B4F9291E01A376019AF20C (void);
// 0x00000158 System.Object Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDDD4A652A44B7176224930CF73F79BD54250E8C (void);
// 0x00000159 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_mBC061664859BF0BCFDE8D797FFFC0B168990E006 (void);
// 0x0000015A System.Object Siccity.GLTFUtility.GLTFMaterial/PbrMetalRoughness/<CreateMaterial>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_mFE01300D7830C2D0E963D4EB1C7BB3CF05891049 (void);
// 0x0000015B System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness::CreateMaterial(Siccity.GLTFUtility.GLTFTexture/ImportResult[],Siccity.GLTFUtility.AlphaMode,Siccity.GLTFUtility.ShaderSettings,System.Action`1<UnityEngine.Material>)
extern void PbrSpecularGlossiness_CreateMaterial_m87F25CC94EAABAFC3DEAE2FC18C02FA6B3C3F53D (void);
// 0x0000015C System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness::.ctor()
extern void PbrSpecularGlossiness__ctor_m7978A0D2413E41A2CFFA3D27D90E05AE2C3E2C4B (void);
// 0x0000015D System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m8D558DCB6560F1D99E4F19A2CAB3EE1A03603FAF (void);
// 0x0000015E System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<>c__DisplayClass5_0::<CreateMaterial>b__0(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__0_mFC38B15C8C304E70AC225BB0A52EAE2CF5286DBC (void);
// 0x0000015F System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<>c__DisplayClass5_0::<CreateMaterial>b__1(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__1_mA49C86103AD35FA41520FFE97D603454550A64AA (void);
// 0x00000160 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::.ctor(System.Int32)
extern void U3CCreateMaterialU3Ed__5__ctor_m44A29489BC2A499F5B2F902E55565A4FBA517217 (void);
// 0x00000161 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::System.IDisposable.Dispose()
extern void U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m12CE43689F6D8E80891D8B58C9734A3D34E2B981 (void);
// 0x00000162 System.Boolean Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::MoveNext()
extern void U3CCreateMaterialU3Ed__5_MoveNext_mA6AFF0E5FCD6D8C4AC92CD06AEC7A1B76655763A (void);
// 0x00000163 System.Object Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D6317ECF4FAC9564EDB508BF2C605119B8AC149 (void);
// 0x00000164 System.Void Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_m02FC2F8E28E73A773C1F110BF34CEAB125EAD61D (void);
// 0x00000165 System.Object Siccity.GLTFUtility.GLTFMaterial/PbrSpecularGlossiness/<CreateMaterial>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_m50CBFD918D48E2F4F72CBC1D965FFC4848C89846 (void);
// 0x00000166 System.Void Siccity.GLTFUtility.GLTFMaterial/TextureInfo::.ctor()
extern void TextureInfo__ctor_m9076A8B7BAC61017E1658F98C7D2A9B5F869CAFA (void);
// 0x00000167 System.Void Siccity.GLTFUtility.GLTFMaterial/TextureInfo/Extensions::Apply(Siccity.GLTFUtility.GLTFMaterial/TextureInfo,UnityEngine.Material,System.String)
extern void Extensions_Apply_mBC4E7987462D68D0704439134696870D65ECAF2B (void);
// 0x00000168 System.Void Siccity.GLTFUtility.GLTFMaterial/TextureInfo/Extensions::.ctor()
extern void Extensions__ctor_m26F2170661C93724EA5008D62C6270747163B295 (void);
// 0x00000169 System.Void Siccity.GLTFUtility.GLTFMaterial/TextureInfo/IExtension::Apply(Siccity.GLTFUtility.GLTFMaterial/TextureInfo,UnityEngine.Material,System.String)
// 0x0000016A System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFMaterial>,Siccity.GLTFUtility.GLTFTexture/ImportTask,Siccity.GLTFUtility.ImportSettings)
extern void ImportTask__ctor_m7E45F12979003FCDAC3D8AF1E41B43FA92AF1AA7 (void);
// 0x0000016B System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMaterial/ImportTask::OnCoroutine(System.Action`1<System.Single>)
extern void ImportTask_OnCoroutine_m126F3069CBD3CB5D2B22552C9654A993A7E4AB81 (void);
// 0x0000016C System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m3CB9D5A4BD17576EBDBB230EA01EA1F51692265C (void);
// 0x0000016D System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<>c__DisplayClass3_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_m38BE1BE63B61CC56EDD73B870B2E173789D35CA7 (void);
// 0x0000016E System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mCAB600ABEBEEE55A01DE664954DDE1FD669FC86D (void);
// 0x0000016F System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<>c__DisplayClass4_0::<OnCoroutine>b__0(UnityEngine.Material)
extern void U3CU3Ec__DisplayClass4_0_U3COnCoroutineU3Eb__0_mADA04E150D9F97D231E6854DEC45B0F6B666B3DF (void);
// 0x00000170 System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::.ctor(System.Int32)
extern void U3COnCoroutineU3Ed__4__ctor_mC000ADF62EDF90D65CCEF5F37D3B0545DA3969AD (void);
// 0x00000171 System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::System.IDisposable.Dispose()
extern void U3COnCoroutineU3Ed__4_System_IDisposable_Dispose_m839B941EFE59407CAC6CFF970CE785DDCB4A525D (void);
// 0x00000172 System.Boolean Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::MoveNext()
extern void U3COnCoroutineU3Ed__4_MoveNext_m3BB863C14B661CD5BA9C6C9A23D9B337A931DB87 (void);
// 0x00000173 System.Object Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFE681C78510873E7CBC57648CD563EACCF5393 (void);
// 0x00000174 System.Void Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m63FF5A76FE5B7F03B362CECBF4B0CFDE61304E0A (void);
// 0x00000175 System.Object Siccity.GLTFUtility.GLTFMaterial/ImportTask/<OnCoroutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m9EAB85B83380E6DBCED03E066A9EE672642ED63B (void);
// 0x00000176 System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mE66B3CC45A159086D47D8E9CC284D5EAC6A626A4 (void);
// 0x00000177 System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::<CreateMaterial>b__0(UnityEngine.Material)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__0_mF275D9FE3D30DF29EDF1D0A43DD60C4EE5FF7894 (void);
// 0x00000178 System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::<CreateMaterial>b__1(UnityEngine.Material)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__1_mA9880FC550C2A52F174B07A1192F7188ECAEE19D (void);
// 0x00000179 System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::<CreateMaterial>b__2(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__2_m10C0B25C17F8374F2AA9F4C2771530157592DB7A (void);
// 0x0000017A System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::<CreateMaterial>b__3(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__3_m228D0AEBC560D3AAF5E5C04367F7A27D2CD4478E (void);
// 0x0000017B System.Void Siccity.GLTFUtility.GLTFMaterial/<>c__DisplayClass13_0::<CreateMaterial>b__4(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__4_m93DA3E7D3E95CD8332B2FFFA6C65D528C068D954 (void);
// 0x0000017C System.Void Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::.ctor(System.Int32)
extern void U3CCreateMaterialU3Ed__13__ctor_m906F8A69518299C3BE456AB85AD3D56E2CAB0E5E (void);
// 0x0000017D System.Void Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::System.IDisposable.Dispose()
extern void U3CCreateMaterialU3Ed__13_System_IDisposable_Dispose_mB221ED8C778CB3745D04CF99F434E2FED1FBFE09 (void);
// 0x0000017E System.Boolean Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::MoveNext()
extern void U3CCreateMaterialU3Ed__13_MoveNext_m2B43FB11A3B4657A587328A4157833A515A2863E (void);
// 0x0000017F System.Object Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateMaterialU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FE40E1FC57B8887B00B84B6167F1EA8C98A005D (void);
// 0x00000180 System.Void Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_Reset_m42BEBD986CDAECFAC5912B7188178DF75B39F309 (void);
// 0x00000181 System.Object Siccity.GLTFUtility.GLTFMaterial/<CreateMaterial>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_get_Current_m9A6DBBE0CD322FC365D0EC8669134D2CF62C4C35 (void);
// 0x00000182 System.Void Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::.ctor(System.Int32)
extern void U3CTryGetTextureU3Ed__14__ctor_m49A3D3B99F86821FB6B1D9088E1FB565431BA72F (void);
// 0x00000183 System.Void Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::System.IDisposable.Dispose()
extern void U3CTryGetTextureU3Ed__14_System_IDisposable_Dispose_m2C0609E384C9CA7CD0692B5FFDED45328ACE9E2F (void);
// 0x00000184 System.Boolean Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::MoveNext()
extern void U3CTryGetTextureU3Ed__14_MoveNext_m7FF2FE0A46723851BCEA5FB39E176EFFCAA4B6B8 (void);
// 0x00000185 System.Object Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTryGetTextureU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AA5956C39B5D0E45D48C93B3B5F49395438AD99 (void);
// 0x00000186 System.Void Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_Reset_m6F5354F3EBCA5488FBFE81D513788587166AA037 (void);
// 0x00000187 System.Object Siccity.GLTFUtility.GLTFMaterial/<TryGetTexture>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_get_Current_mE2011302FAE1B32CC0672009985C052CA2ED8FFF (void);
// 0x00000188 System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFMesh/ExportResult> Siccity.GLTFUtility.GLTFMesh::Export(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFNode/ExportResult>)
extern void GLTFMesh_Export_m2E90C0D95740BDF062D300792C29CEE10D0E682B (void);
// 0x00000189 Siccity.GLTFUtility.GLTFMesh/ExportResult Siccity.GLTFUtility.GLTFMesh::Export(UnityEngine.Mesh)
extern void GLTFMesh_Export_m653A9262512A09F2EF078495463B66B9FCE0E297 (void);
// 0x0000018A System.Void Siccity.GLTFUtility.GLTFMesh::.ctor()
extern void GLTFMesh__ctor_m201F15328639BEACC8C40804F68776175D1FBF6A (void);
// 0x0000018B System.Void Siccity.GLTFUtility.GLTFMesh/Extras::.ctor()
extern void Extras__ctor_mE4A4FF1622A4779A305E12FF311A46FAFAF36C11 (void);
// 0x0000018C System.Void Siccity.GLTFUtility.GLTFMesh/ImportResult::.ctor()
extern void ImportResult__ctor_mBCA9F284C327351A9E5C23D67BB03A9C3F406F1D (void);
// 0x0000018D System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFMesh>,Siccity.GLTFUtility.GLTFAccessor/ImportTask,Siccity.GLTFUtility.GLTFBufferView/ImportTask,Siccity.GLTFUtility.GLTFMaterial/ImportTask,Siccity.GLTFUtility.ImportSettings)
extern void ImportTask__ctor_mE3BF0B78FC7680E08663E80ABA3EA85DA265F615 (void);
// 0x0000018E System.Collections.IEnumerator Siccity.GLTFUtility.GLTFMesh/ImportTask::OnCoroutine(System.Action`1<System.Single>)
extern void ImportTask_OnCoroutine_mD6781BEBFC39FEA29FF35630A736F1D2148FC2F3 (void);
// 0x0000018F System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::.ctor(Siccity.GLTFUtility.GLTFMesh,Siccity.GLTFUtility.GLTFAccessor/ImportResult[],Siccity.GLTFUtility.GLTFBufferView/ImportResult[])
extern void MeshData__ctor_mE2068B5E8E71B40AD3FAC60121960604E237692C (void);
// 0x00000190 UnityEngine.Vector3[] Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::GetMorphWeights(System.Nullable`1<System.Int32>,System.Int32,System.Int32,Siccity.GLTFUtility.GLTFAccessor/ImportResult[])
extern void MeshData_GetMorphWeights_mB0510DD795C76C408DC0ED5C886FD49C88255F1F (void);
// 0x00000191 UnityEngine.Mesh Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::ToMesh()
extern void MeshData_ToMesh_mCC0EE262937B0FF61D0C2F73B49B9B3ADE48767F (void);
// 0x00000192 UnityEngine.Mesh Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::FixBlendShapeNormals(UnityEngine.Mesh)
extern void MeshData_FixBlendShapeNormals_mA348C32649DC4921D8A60D62AAFD01401E867194 (void);
// 0x00000193 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::NormalizeWeights(UnityEngine.Vector4&)
extern void MeshData_NormalizeWeights_m3DFCC671A03F738EA398948D5C31BDB87A865A94 (void);
// 0x00000194 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::ReadUVs(System.Collections.Generic.List`1<UnityEngine.Vector2>&,Siccity.GLTFUtility.GLTFAccessor/ImportResult[],System.Nullable`1<System.Int32>,System.Int32)
extern void MeshData_ReadUVs_mC877E0948DB9E701A8E86C724C87C88F71BA5C2D (void);
// 0x00000195 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData::FlipY(UnityEngine.Vector2[]&)
extern void MeshData_FlipY_m503D5D4F522BA76CF6E5B6F3F6A9F5C8CDD7DABD (void);
// 0x00000196 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/BlendShape::.ctor()
extern void BlendShape__ctor_mFA9F525133C3FFF92D57BAEEBDAA0DEB0D3AA75E (void);
// 0x00000197 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mA49A482206787357DB8495F58C42CC099460331F (void);
// 0x00000198 System.Boolean Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c__DisplayClass19_0::<.ctor>b__0(Siccity.GLTFUtility.GLTFPrimitive)
extern void U3CU3Ec__DisplayClass19_0_U3C_ctorU3Eb__0_m9EF40A59B69FF56030297B76F5932CB26C5C2B6D (void);
// 0x00000199 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c__DisplayClass19_1::.ctor()
extern void U3CU3Ec__DisplayClass19_1__ctor_m644CEF863082528137EAF7120F52F453BB4200F7 (void);
// 0x0000019A System.Int32 Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c__DisplayClass19_1::<.ctor>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass19_1_U3C_ctorU3Eb__1_mDCB5EDE1BF36B2A1F09C926B478B2123DC8F7007 (void);
// 0x0000019B System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::.cctor()
extern void U3CU3Ec__cctor_m30B6AF535111D074CA46764E0E4E52CE4548E6F1 (void);
// 0x0000019C System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::.ctor()
extern void U3CU3Ec__ctor_m8F81E088321395ED6FF9E84B310AA5BAA3B5DF77 (void);
// 0x0000019D UnityEngine.Vector3 Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::<.ctor>b__19_4(UnityEngine.Vector3)
extern void U3CU3Ec_U3C_ctorU3Eb__19_4_mE41E02C21D22A3D227FB2A4CC821315CE61D5E8C (void);
// 0x0000019E UnityEngine.Vector3 Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::<.ctor>b__19_2(UnityEngine.Vector3)
extern void U3CU3Ec_U3C_ctorU3Eb__19_2_mA6A7991CC93D9C82ED0D7DD1A4315D8C209FB1AC (void);
// 0x0000019F UnityEngine.Vector4 Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::<.ctor>b__19_3(UnityEngine.Vector4)
extern void U3CU3Ec_U3C_ctorU3Eb__19_3_m66A75DC4C4E5AF6D57C5E71F0AA805F99EC4F28B (void);
// 0x000001A0 UnityEngine.Vector3 Siccity.GLTFUtility.GLTFMesh/ImportTask/MeshData/<>c::<GetMorphWeights>b__20_0(UnityEngine.Vector3)
extern void U3CU3Ec_U3CGetMorphWeightsU3Eb__20_0_mCF098509AE1304701AAA2475E95A9E576D911DCB (void);
// 0x000001A1 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mEE7DA844CE4FF5A4AE67EB17E8BA1B4F18E703CA (void);
// 0x000001A2 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/<>c__DisplayClass4_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mAAFC5917A6A627F2403E5E3C855B08C78FAE22D5 (void);
// 0x000001A3 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::.ctor(System.Int32)
extern void U3COnCoroutineU3Ed__5__ctor_m10BD190BCCD4CA5D3DE6E9E67C56D841671D3738 (void);
// 0x000001A4 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::System.IDisposable.Dispose()
extern void U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m519BC153E6FD02D1D79F068287499513399CCBAF (void);
// 0x000001A5 System.Boolean Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::MoveNext()
extern void U3COnCoroutineU3Ed__5_MoveNext_m8A1FFCA3117503C3574ADC4B7B10401C9B544F90 (void);
// 0x000001A6 System.Object Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CDE424A2B66B71D81B41A452ED5272CC3254BBE (void);
// 0x000001A7 System.Void Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCD839561719DB78CC9B400ABC9044F9D8D65A945 (void);
// 0x000001A8 System.Object Siccity.GLTFUtility.GLTFMesh/ImportTask/<OnCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m27C3951830591BFBDD37468D99DFF91264FEAF9A (void);
// 0x000001A9 System.Void Siccity.GLTFUtility.GLTFMesh/ExportResult::.ctor()
extern void ExportResult__ctor_m0289C3735F27BF0157D4832DEB885EC1C97CCAD5 (void);
// 0x000001AA System.Void Siccity.GLTFUtility.GLTFNode::set_matrix(UnityEngine.Matrix4x4)
extern void GLTFNode_set_matrix_m5021D50B20D9FC495434B1285D7B83307F5A76EB (void);
// 0x000001AB System.Boolean Siccity.GLTFUtility.GLTFNode::ShouldSerializetranslation()
extern void GLTFNode_ShouldSerializetranslation_mA84C635C6EB142EBE31A52456728A866D0827386 (void);
// 0x000001AC System.Boolean Siccity.GLTFUtility.GLTFNode::ShouldSerializerotation()
extern void GLTFNode_ShouldSerializerotation_m255418FA94A8C57291AA27AEF88409FC7A192451 (void);
// 0x000001AD System.Boolean Siccity.GLTFUtility.GLTFNode::ShouldSerializescale()
extern void GLTFNode_ShouldSerializescale_m304207CE77A2D451D74A91288E12D0709B09BCE2 (void);
// 0x000001AE System.Void Siccity.GLTFUtility.GLTFNode::ApplyTRS(UnityEngine.Transform)
extern void GLTFNode_ApplyTRS_m2CA2377D8AD1ADD7E03EB4479ED8D32CFFC25639 (void);
// 0x000001AF System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFNode/ExportResult> Siccity.GLTFUtility.GLTFNode::Export(UnityEngine.Transform)
extern void GLTFNode_Export_mA3BE477CF76167DC1DD6D0208052FC94E065B57D (void);
// 0x000001B0 System.Void Siccity.GLTFUtility.GLTFNode::CreateNodeListRecursive(UnityEngine.Transform,System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFNode/ExportResult>)
extern void GLTFNode_CreateNodeListRecursive_mB89E4931C9485A37B1EA8C9FE047BCD7399A467E (void);
// 0x000001B1 System.Void Siccity.GLTFUtility.GLTFNode::.ctor()
extern void GLTFNode__ctor_mD2392D42A509C53A90A2D1C2BF50A2F419FCC358 (void);
// 0x000001B2 System.Boolean Siccity.GLTFUtility.GLTFNode/ImportResult::get_IsRoot()
extern void ImportResult_get_IsRoot_mE17F73630C3003C720D7C2CE2385F499CFB2CF40 (void);
// 0x000001B3 System.Void Siccity.GLTFUtility.GLTFNode/ImportResult::.ctor()
extern void ImportResult__ctor_mD715A3036A7B0BB5C348FD1C6A8829817412978A (void);
// 0x000001B4 System.Void Siccity.GLTFUtility.GLTFNode/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFNode>,Siccity.GLTFUtility.GLTFMesh/ImportTask,Siccity.GLTFUtility.GLTFSkin/ImportTask,System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFCamera>)
extern void ImportTask__ctor_m0F2061D9EFCCBD960AE1FD5DE4C429CF5CE8FFAA (void);
// 0x000001B5 System.Collections.IEnumerator Siccity.GLTFUtility.GLTFNode/ImportTask::OnCoroutine(System.Action`1<System.Single>)
extern void ImportTask_OnCoroutine_m9F5F609BFEA83B8E95A3EFA0C47E70CBC0B506A2 (void);
// 0x000001B6 System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<>c::.cctor()
extern void U3CU3Ec__cctor_m7ABA7DD3B9857858D334FD8AD7A4CBCF895F55D3 (void);
// 0x000001B7 System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<>c::.ctor()
extern void U3CU3Ec__ctor_m6D93EA8A1C5CFC4B2BA5DE76D69E63AA012C97C0 (void);
// 0x000001B8 System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<>c::<.ctor>b__4_0()
extern void U3CU3Ec_U3C_ctorU3Eb__4_0_m608491E7C34161ADFB68A93597FA4C86B80A06CF (void);
// 0x000001B9 System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::.ctor(System.Int32)
extern void U3COnCoroutineU3Ed__5__ctor_mA8A51B40E3A30D79F493009EAEFAA8487B2DB3FE (void);
// 0x000001BA System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::System.IDisposable.Dispose()
extern void U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m858F98D21F6F66A0CBA1350D4CE8684CBE2BD24E (void);
// 0x000001BB System.Boolean Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::MoveNext()
extern void U3COnCoroutineU3Ed__5_MoveNext_m9A3A4A26BB83082AA2E09FBD4AAEEC3A2B3FCA64 (void);
// 0x000001BC System.Object Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDAFFD7EE3601FC7392DCE575962FB08F6247FF8 (void);
// 0x000001BD System.Void Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m488AE3FB7BA142292F30FA08BAEBF3FA87CB8FD7 (void);
// 0x000001BE System.Object Siccity.GLTFUtility.GLTFNode/ImportTask/<OnCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF44DD69E2CE4C8639CC4D7D7A9011401805C3E1D (void);
// 0x000001BF System.Void Siccity.GLTFUtility.GLTFNode/ExportResult::.ctor()
extern void ExportResult__ctor_mE90844A3936BC12C95EFF1BAFE08AEE632E3AAD8 (void);
// 0x000001C0 UnityEngine.GameObject Siccity.GLTFUtility.GLTFNodeExtensions::GetRoot(Siccity.GLTFUtility.GLTFNode/ImportResult[])
extern void GLTFNodeExtensions_GetRoot_m16F36DAA9BC509FC186B083147EE9BBCEBEC1951 (void);
// 0x000001C1 System.Void Siccity.GLTFUtility.GLTFNodeExtensions/<>c::.cctor()
extern void U3CU3Ec__cctor_m44F5837C79631E04A4A7A7B80764CA322AA95BB4 (void);
// 0x000001C2 System.Void Siccity.GLTFUtility.GLTFNodeExtensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m6287CCF554071E2C20BAF91502FBC2B7AF5727E9 (void);
// 0x000001C3 System.Boolean Siccity.GLTFUtility.GLTFNodeExtensions/<>c::<GetRoot>b__0_0(Siccity.GLTFUtility.GLTFNode/ImportResult)
extern void U3CU3Ec_U3CGetRootU3Eb__0_0_m36A01DA28B5CB43244373B4611204DCA8B93B169 (void);
// 0x000001C4 System.Void Siccity.GLTFUtility.GLTFObject::.ctor()
extern void GLTFObject__ctor_mA0A38F7E8C513A8DE58549B1529B2D2E74626FB1 (void);
// 0x000001C5 System.Void Siccity.GLTFUtility.GLTFPrimitive::.ctor()
extern void GLTFPrimitive__ctor_m6B988D08D05C3E98E6971212E33BEB2627EC590A (void);
// 0x000001C6 System.Void Siccity.GLTFUtility.GLTFPrimitive/GLTFAttributes::.ctor()
extern void GLTFAttributes__ctor_mD4789A48EF7BAF7C8F1024125B244BDF57F4595C (void);
// 0x000001C7 System.Void Siccity.GLTFUtility.GLTFProperty::.ctor()
extern void GLTFProperty__ctor_mDDC6FB840932B7B0FBA52F720A7CDE13CC96DD3C (void);
// 0x000001C8 System.Void Siccity.GLTFUtility.GLTFScene::.ctor()
extern void GLTFScene__ctor_m2955AEFDC17FFACDBFB0FAC8E2A546C8F6A837EF (void);
// 0x000001C9 Siccity.GLTFUtility.GLTFSkin/ImportResult Siccity.GLTFUtility.GLTFSkin::Import(Siccity.GLTFUtility.GLTFAccessor/ImportResult[])
extern void GLTFSkin_Import_mE1A672644F9B89EDD670375B6679EE0A60700B86 (void);
// 0x000001CA System.Void Siccity.GLTFUtility.GLTFSkin::.ctor()
extern void GLTFSkin__ctor_m6DB48FE07EAF41283E9A3DAE8EFE4388D873FB1D (void);
// 0x000001CB UnityEngine.SkinnedMeshRenderer Siccity.GLTFUtility.GLTFSkin/ImportResult::SetupSkinnedRenderer(UnityEngine.GameObject,UnityEngine.Mesh,Siccity.GLTFUtility.GLTFNode/ImportResult[])
extern void ImportResult_SetupSkinnedRenderer_m4638DC0973BA965AC04BBB879D5A2F29B9B8B6EE (void);
// 0x000001CC System.Void Siccity.GLTFUtility.GLTFSkin/ImportResult::.ctor()
extern void ImportResult__ctor_m580EFB5BB14054DCDB6D22A8C8B5A3D1F79D3BA7 (void);
// 0x000001CD System.Void Siccity.GLTFUtility.GLTFSkin/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFSkin>,Siccity.GLTFUtility.GLTFAccessor/ImportTask)
extern void ImportTask__ctor_m8BB023A8C9DEBAAC8FC2222CE3C51C08EBA4AF36 (void);
// 0x000001CE System.Void Siccity.GLTFUtility.GLTFSkin/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m24DDE1B93CAA568343C8BAEC5F5E9F3E05996E16 (void);
// 0x000001CF System.Void Siccity.GLTFUtility.GLTFSkin/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mF0A61A668646DA96A0637A3BCBFB91F58E5B7F4C (void);
// 0x000001D0 Siccity.GLTFUtility.GLTFTexture/ImportResult Siccity.GLTFUtility.GLTFTexture::Import(Siccity.GLTFUtility.GLTFImage/ImportResult[])
extern void GLTFTexture_Import_mA898C6D1F23331F4A25540655B10BC1687F67AA8 (void);
// 0x000001D1 System.Void Siccity.GLTFUtility.GLTFTexture::.ctor()
extern void GLTFTexture__ctor_m1D35B0EC3C13E5DF89080CD90F88F9C388154D63 (void);
// 0x000001D2 System.Void Siccity.GLTFUtility.GLTFTexture/ImportResult::.ctor(Siccity.GLTFUtility.GLTFImage/ImportResult)
extern void ImportResult__ctor_m6BDB049B8F514434D1E2A3B58B8541855567F8CB (void);
// 0x000001D3 System.Collections.IEnumerator Siccity.GLTFUtility.GLTFTexture/ImportResult::GetTextureCached(System.Boolean,System.Action`1<UnityEngine.Texture2D>,System.Action`1<System.Single>)
extern void ImportResult_GetTextureCached_mB90AC91183452AAC0FEE818967721C5674EEB90C (void);
// 0x000001D4 System.Void Siccity.GLTFUtility.GLTFTexture/ImportResult::<GetTextureCached>b__3_0(UnityEngine.Texture2D)
extern void ImportResult_U3CGetTextureCachedU3Eb__3_0_mAC38EEC6CF9BF4CB79BADD9282751F7055D4B77E (void);
// 0x000001D5 System.Void Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::.ctor(System.Int32)
extern void U3CGetTextureCachedU3Ed__3__ctor_m08187E332D6D4CBF1F26C0854566D71C1B1481EC (void);
// 0x000001D6 System.Void Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::System.IDisposable.Dispose()
extern void U3CGetTextureCachedU3Ed__3_System_IDisposable_Dispose_mB8C460D10E2AC911E0BF6E36D837C16AB6A0BB39 (void);
// 0x000001D7 System.Boolean Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::MoveNext()
extern void U3CGetTextureCachedU3Ed__3_MoveNext_m78C5CE649F9526425B139A1046A4B1764F1DA534 (void);
// 0x000001D8 System.Object Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetTextureCachedU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DB4C8C659FA598EA2FAB80A36EE08E786B081D (void);
// 0x000001D9 System.Void Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::System.Collections.IEnumerator.Reset()
extern void U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_Reset_m11B2A8AB90940DBA21D26137251ACF2F86E21FD8 (void);
// 0x000001DA System.Object Siccity.GLTFUtility.GLTFTexture/ImportResult/<GetTextureCached>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_get_Current_m60485DC51BA051C07BC32A4FE37E6B3419AB0673 (void);
// 0x000001DB System.Void Siccity.GLTFUtility.GLTFTexture/ImportTask::.ctor(System.Collections.Generic.List`1<Siccity.GLTFUtility.GLTFTexture>,Siccity.GLTFUtility.GLTFImage/ImportTask)
extern void ImportTask__ctor_mF0750C69A56476A492446B29FF48197FA789A1DD (void);
// 0x000001DC System.Void Siccity.GLTFUtility.GLTFTexture/ImportTask/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mFEAF65681DCEFEA5FED8A93B52FD1D8116B6CE4B (void);
// 0x000001DD System.Void Siccity.GLTFUtility.GLTFTexture/ImportTask/<>c__DisplayClass0_0::<.ctor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mDC4FCD5BA4B22053C7B46EECE6460A817C698382 (void);
// 0x000001DE System.Void Siccity.GLTFUtility.Converters.ColorRGBConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ColorRGBConverter_WriteJson_m1C28F266A54B68FC8187E70C4D96FE3410D5E0AD (void);
// 0x000001DF System.Object Siccity.GLTFUtility.Converters.ColorRGBConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ColorRGBConverter_ReadJson_m86088870AEF5A52477E1C3051B332884C0E58600 (void);
// 0x000001E0 System.Boolean Siccity.GLTFUtility.Converters.ColorRGBConverter::CanConvert(System.Type)
extern void ColorRGBConverter_CanConvert_m6AF673F9E6658344F83B7C7A66C76611DBBB9C27 (void);
// 0x000001E1 System.Void Siccity.GLTFUtility.Converters.ColorRGBConverter::.ctor()
extern void ColorRGBConverter__ctor_m1F339F8FB512F62B6ADC38F9422D5672D339ADFB (void);
// 0x000001E2 System.Void Siccity.GLTFUtility.Converters.ColorRGBAConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ColorRGBAConverter_WriteJson_mC5C32C6A22C8B6CB75ABD82BD0CF0B5C6A35646F (void);
// 0x000001E3 System.Object Siccity.GLTFUtility.Converters.ColorRGBAConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ColorRGBAConverter_ReadJson_m42368D4B50A055D01261C501CB408BF9FE427B37 (void);
// 0x000001E4 System.Boolean Siccity.GLTFUtility.Converters.ColorRGBAConverter::CanConvert(System.Type)
extern void ColorRGBAConverter_CanConvert_m87B2B7CB0F90F43E121E6ED7E0EFB87DE2456046 (void);
// 0x000001E5 System.Void Siccity.GLTFUtility.Converters.ColorRGBAConverter::.ctor()
extern void ColorRGBAConverter__ctor_mD26F98C66A8EA2E171193CED3F38542A43B663E2 (void);
// 0x000001E6 System.Void Siccity.GLTFUtility.Converters.EnumConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void EnumConverter_WriteJson_mCE2B439B5DCF94A4E4906551B82B2B18D88CF4F5 (void);
// 0x000001E7 System.Object Siccity.GLTFUtility.Converters.EnumConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void EnumConverter_ReadJson_m578A41050853AC55790EF219550665CCA879D1C2 (void);
// 0x000001E8 System.Boolean Siccity.GLTFUtility.Converters.EnumConverter::CanConvert(System.Type)
extern void EnumConverter_CanConvert_m3A551B8A9B03DF4B7E62992DCBEE9C2E60876AC7 (void);
// 0x000001E9 System.Void Siccity.GLTFUtility.Converters.EnumConverter::.ctor()
extern void EnumConverter__ctor_m81BE23CA91C829E2182090E4BF68E4BC800E8EDF (void);
// 0x000001EA System.Void Siccity.GLTFUtility.Converters.Matrix4x4Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Matrix4x4Converter_WriteJson_mB78BAB9E1D5B79B15C9B6615D65753FCBDBC4A17 (void);
// 0x000001EB System.Object Siccity.GLTFUtility.Converters.Matrix4x4Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Matrix4x4Converter_ReadJson_mE2E93DCBD0B02F1A305758D36649172F85EE733C (void);
// 0x000001EC System.Boolean Siccity.GLTFUtility.Converters.Matrix4x4Converter::CanConvert(System.Type)
extern void Matrix4x4Converter_CanConvert_mB876817FF2135B0164DD06F1A3B703C29D5A42AB (void);
// 0x000001ED System.Void Siccity.GLTFUtility.Converters.Matrix4x4Converter::.ctor()
extern void Matrix4x4Converter__ctor_mD0BF9C90F2518295B43FA69E2E2014827A23D968 (void);
// 0x000001EE System.Void Siccity.GLTFUtility.Converters.QuaternionConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void QuaternionConverter_WriteJson_m0C104DBB426A66A55135C227059C1AA67004AF63 (void);
// 0x000001EF System.Object Siccity.GLTFUtility.Converters.QuaternionConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void QuaternionConverter_ReadJson_m2CCD8FD7E2D91F81F3DA8497B960B0FB74C2EE87 (void);
// 0x000001F0 System.Boolean Siccity.GLTFUtility.Converters.QuaternionConverter::CanConvert(System.Type)
extern void QuaternionConverter_CanConvert_m490A79FA7780312FF87AE9971946E8A5D045FF5F (void);
// 0x000001F1 System.Void Siccity.GLTFUtility.Converters.QuaternionConverter::.ctor()
extern void QuaternionConverter__ctor_mD98DE4AE9A496B15C8113D6EDD512ED277E9B20D (void);
// 0x000001F2 System.Void Siccity.GLTFUtility.Converters.TranslationConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void TranslationConverter_WriteJson_m82D7FF22C853A69DD32730A35B68F5FEAEE585D9 (void);
// 0x000001F3 System.Object Siccity.GLTFUtility.Converters.TranslationConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void TranslationConverter_ReadJson_mA106EA103EB9F66621B17FCBC7D960E61EA1231F (void);
// 0x000001F4 System.Boolean Siccity.GLTFUtility.Converters.TranslationConverter::CanConvert(System.Type)
extern void TranslationConverter_CanConvert_mBE936BB8122DDFF178B3759683C50AB454FF81C7 (void);
// 0x000001F5 System.Void Siccity.GLTFUtility.Converters.TranslationConverter::.ctor()
extern void TranslationConverter__ctor_m1C1C3FB54A3FB2F6AFD6247DFFD0AB58CF127E60 (void);
// 0x000001F6 System.Void Siccity.GLTFUtility.Converters.Vector2Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Vector2Converter_WriteJson_mA9A1F9DBD58F1CB676BB78F9D858760874A9250B (void);
// 0x000001F7 System.Object Siccity.GLTFUtility.Converters.Vector2Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Vector2Converter_ReadJson_m4FE619C78788AA70AF9476F996777150E039619B (void);
// 0x000001F8 System.Boolean Siccity.GLTFUtility.Converters.Vector2Converter::CanConvert(System.Type)
extern void Vector2Converter_CanConvert_m92F8EAC4287319D6EDF5C0AF1DE27504F605982D (void);
// 0x000001F9 System.Void Siccity.GLTFUtility.Converters.Vector2Converter::.ctor()
extern void Vector2Converter__ctor_m83A75A2959B7ECC04BD080321DC6E9F1BFA3D773 (void);
// 0x000001FA System.Void Siccity.GLTFUtility.Converters.Vector3Converter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Vector3Converter_WriteJson_m9202133E7D3FAE8971632B284C255F12E7260484 (void);
// 0x000001FB System.Object Siccity.GLTFUtility.Converters.Vector3Converter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void Vector3Converter_ReadJson_m55E8FEF512C204E7E16460D167A42B9F6464D2F9 (void);
// 0x000001FC System.Boolean Siccity.GLTFUtility.Converters.Vector3Converter::CanConvert(System.Type)
extern void Vector3Converter_CanConvert_m1415AFFD6226BBC0F2F83E21BE5A9B8B20CB06C2 (void);
// 0x000001FD System.Void Siccity.GLTFUtility.Converters.Vector3Converter::.ctor()
extern void Vector3Converter__ctor_m184435212232544BE6EB6975977D3BB5F4BEB479 (void);
static Il2CppMethodPointer s_methodPointers[509] = 
{
	BufferedBinaryReader_get_Position_m634DBC609CAB28CDEC1104360DCBCD8CA2F246D0,
	BufferedBinaryReader_set_Position_m4B83D7F75FE5E2A812D92B42CC88519F297CD813,
	BufferedBinaryReader__ctor_mBC3B4C93D65BC052BB3AA10FFFD64B672994BCCF,
	BufferedBinaryReader_FillBuffer_mD3BC7A6C0813D2C5F5FCEB5CD367952C0CF9E291,
	BufferedBinaryReader_ReadByte_mEBFE4BBD75D20F2B2748FCB577E64F5D9299AB87,
	BufferedBinaryReader_ReadSByte_m2E2A8A4735F89856AB2B4318702B4B65300D3916,
	BufferedBinaryReader_ReadUInt16_m5AEF216915781971C456484FAD34D2C133BDBFFD,
	BufferedBinaryReader_ReadInt16_mFE2F9EC3FC30E759A11E56E597CA3D41F11ECDCB,
	BufferedBinaryReader_ReadUInt32_m8DAE5258919A6A802F16F3DF54A59335E59E8FB3,
	BufferedBinaryReader_ReadInt32_m131C895FE68A737B3DBB7E522EC722C9A47CB403,
	BufferedBinaryReader_ReadSingle_m8F635100C180A0EB837A20389A6B5AE7F5E1C8D2,
	BufferedBinaryReader_Skip_m1246BA04F480EB9FD0BCB75B801479E4F4E28BFE,
	BufferedBinaryReader_Dispose_m19C080EFAFADF78061692EC4DDC076332D9255E4,
	Bit2Converter_Read_m6A9F724C42A23D7794F2B98393B19193CACDFF50,
	Bit4Converter_Read_m2E8694B24CE49956E94B493FF1544870F0478DC2,
	RuntimeTest_Start_m10C161C683779646859EBEC263332F46A71EC6D3,
	RuntimeTest_OnAvatarLoaded_mCD4A8D837AE479E6E7923863624A410D793B904D,
	RuntimeTest__ctor_m337FB724C20A0F9E603DEBA02C8E50BFD57A9361,
	DragRotate_OnDrag_m241F16E3337FB0DF2D81A24CC7ECDE8829D4DDE6,
	DragRotate__ctor_mB807CA7B51B3BE23E1718827DC15B9188C87A9F5,
	WebviewTest_Start_m964A2663ACE6EEDD8309B66FADE3895C658BC5B4,
	WebviewTest_DisplayWebView_mB759A88308464996744A2261D233C379AB381E56,
	WebviewTest_OnAvatarCreated_mA2F66DE56804FDB1537772E558C0794A1169D676,
	WebviewTest_OnAvatarLoaded_m7B5D0E39BC3FDB70BEDAD7068434F6C225501967,
	WebviewTest__ctor_m956F0DD42A57289950DCA18FAAD7C8572660AE53,
	AvatarLoader_get_Timeout_mE8CB3BCEC9F34E86871DE081D1A330578C69EF3C,
	AvatarLoader_set_Timeout_mD13FEA8E0424644DB952C51D62791745CDDB68CA,
	AvatarLoader_LoadAvatar_m7E97807F5051768CD371052088FF1C25A0234B85,
	AvatarLoader__ctor_mB620008D910565DF3B8DEE48579CC60E3A44DEE1,
	LoadOperation_LoadAvatarAsync_m1ECC417961699E00B7288CA0D359E51641B0177F,
	LoadOperation_DownloadAvatar_m8C41A534B940C0DB563C4C38D868D9E16FE14C4D,
	LoadOperation_OnImportFinished_mB0C9D1C7EFBD9399D4FE0FA06FB1DCC7A69C80AF,
	LoadOperation_PrepareAvatarAsync_mAFE7F9F3A13710DE1D43AF1133462C51F1518A76,
	LoadOperation__ctor_m68C5789147B330F82A5B16F4851DE558A0B1298F,
	U3CLoadAvatarAsyncU3Ed__2__ctor_mBD4F78947512C1C4071220D62833955E48D55EE7,
	U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_m0CBBA5987404C4EC2B71743C4BA0F0831EE98D9A,
	U3CLoadAvatarAsyncU3Ed__2_MoveNext_m5A8F2DA18C09C685B16B0F32B2F4E281CB05461D,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD133114924F55ADBDF3A6CAAE4061E50CAC7A9E4,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mB19A8FCBACFD78E2F5825DC6995D0723E30CBE85,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_mD85B71B8CD797CA29F0378FEED1261B7E8321080,
	U3CDownloadAvatarU3Ed__3__ctor_m68352DFBC6229C147598D5857B04E6ADEF630B85,
	U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mC275D51C7FF17A0C516114FF31C4E2D6CB73F551,
	U3CDownloadAvatarU3Ed__3_MoveNext_m0FBD65D03B0D3DC779B783869FBA672B9F20C50B,
	U3CDownloadAvatarU3Ed__3_U3CU3Em__Finally1_m7063558E183950E9ACFB5C5D5C2DE21853C42308,
	U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD843D5776566598C09ACDB97436644089FECE376,
	U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m8334C59C0F1A6BC4B759C04AD6760BC32EF6301A,
	U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_mFB2775F3A311FBB224A9179EF21700527E1B33D9,
	U3CPrepareAvatarAsyncU3Ed__5__ctor_mC4B4D228743E776D60E4E03F426C756904843118,
	U3CPrepareAvatarAsyncU3Ed__5_System_IDisposable_Dispose_mF77F6CDFC1C997744FB30300C5CE96795AE19ECC,
	U3CPrepareAvatarAsyncU3Ed__5_MoveNext_m30F07D5A0251A13329E5F555B2E3289B7A565152,
	U3CPrepareAvatarAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF636842F021B15398C2F08AF5EB996DC63C0425,
	U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m1FEAF518F8173872C6C8E22047A733575E8E9CC7,
	U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_m9BBD626EA4994CB9449CF0E46F2273F2E19E642B,
	AvatarLoaderBase_get_Timeout_m751F8141D184C514A2921289CA5C2D158925F2B1,
	AvatarLoaderBase_set_Timeout_m8037ABC73D784A1701B268248C1653CC73FD09AD,
	AvatarLoaderBase_LoadAvatar_mB3A3FB205BF0A074FAFFFF5BAA8B20526B17A917,
	NULL,
	NULL,
	AvatarLoaderBase_DownloadMetaData_m200441C7F7CE70C78D87747769B1CAEC7C60D74F,
	AvatarLoaderBase_get_AnimatorControllerName_mF9BB1E03049FC432DF909537BC5A5034F30D07FA,
	AvatarLoaderBase_get_AnimationAvatarSource_m8D255A218733FF89B9C16C952C6C5D3899676C2C,
	AvatarLoaderBase_RestructureAndSetAnimator_m83E0BC0403952A729293353A15473ABCBCC7E4D8,
	AvatarLoaderBase_OutfitVersion_m550FC6EB901AD6DF4D5D00D8FE1448C0A95B59A2,
	AvatarLoaderBase_SetMetaData_mFF38163711C06B971CC0F620B0C72BADF8F99E56,
	AvatarLoaderBase_SetAvatarAssetNames_mBE306862F0A883CF9BD844431BE95EC0FEE1664A,
	AvatarLoaderBase_SetTextureNames_m7ED78FE09ABC14262BC20A469D3AE03797CCE43E,
	AvatarLoaderBase_SetMeshName_m20388314C8E75CFCFA305893F1A5B9855483F678,
	AvatarLoaderBase__ctor_mF676DA3A64576376F8858711AB6B07754BE1C6C8,
	AvatarLoaderBase__cctor_m583C1C214966437DABC8A2378E3B8DFE6FD86D25,
	U3CLoadAvatarU3Ed__19_MoveNext_m9BD287FCF1FD3F252007B2DE8FDAE58D51FB2CF6,
	U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E,
	U3CDownloadMetaDataU3Ed__22__ctor_m28B2083136EB545C725ED009EBBFE7A5BB8BD813,
	U3CDownloadMetaDataU3Ed__22_System_IDisposable_Dispose_mB1728B9B8A43C8600AAD903B7BD21363587EDF66,
	U3CDownloadMetaDataU3Ed__22_MoveNext_m2842A64D0FEBA4FDF20FAA8FAF2D70EC77FE6DC7,
	U3CDownloadMetaDataU3Ed__22_U3CU3Em__Finally1_m0111D7C9C9407BE69C2410A3B24F2EB20D90D54A,
	U3CDownloadMetaDataU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A9228438F9EC5F8959B62258365983010A2483B,
	U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_Reset_mBC340BC3995F6746CAEC44E1096EF92C709DC67B,
	U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_get_Current_mCE4A69299659B9C8D5DCE6C126C1D33E7A6938C8,
	EditorAvatarLoader_LoadAvatarAsync_mAFCF3C276AA7B1B716C234BEF2CED8D28DACA28F,
	EditorAvatarLoader_DownloadAvatar_mD546367EBD7548B38CDE90EFB47032A424403882,
	EditorAvatarLoader_InstantiateAvatar_m61DD09B8820E0D696712B2F6384BC2A7D22C06C4,
	EditorAvatarLoader__ctor_mA35B40EA36ED34D3369BD349632A55F9612E2207,
	U3CLoadAvatarAsyncU3Ed__2__ctor_m33FA85ABA06CE6BB59613671CDD337BE19933C95,
	U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_mA359D6B2EDEC85F23AFF2C7A98B21AF9DE7F9EFE,
	U3CLoadAvatarAsyncU3Ed__2_MoveNext_mCCAC4AB969396E419F5A6F03D7BF7BE317BB8B2B,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AFFF62195D799E9059CEC04FD7ACF19988D46AC,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mF50263B27F63A1E65E26495BF41E351FD6011120,
	U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_m46EABEC84BC757D097187BBB5F759C594BFB78ED,
	U3CU3Ec__DisplayClass3_0__ctor_mDC5E05C8A139AAA010129DFC7FC63D1FBBE1470C,
	U3CU3Ec__DisplayClass3_0_U3CDownloadAvatarU3Eb__0_mD236682FDD6AB8CEB5AA9E4AF8EF4FA33F56B5F8,
	U3CDownloadAvatarU3Ed__3__ctor_mFDBEDB706E0A529EA40637D297209930BAC2ECE6,
	U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mB2353DD0FEC9E18048409BDB9C2D4CB6725B6D6D,
	U3CDownloadAvatarU3Ed__3_MoveNext_mF191D90C2A88125C7F71437DAAC13EB764ADD4C6,
	U3CDownloadAvatarU3Ed__3_U3CU3Em__Finally1_m49F2D0585294D8134BBC0D9D633B322B4FB7B807,
	U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43F2C2D1AA214B91EA1926994D2052EE57B8B5E2,
	U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m1B694D6F834ED62F52DB8CEA18448795201387CC,
	U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_m630C20BDF06581F5C703266612A613384F544080,
	EyeAnimationHandler_Start_m50592966FA0081A6D83DD0C66E2981193AAA6F7E,
	EyeAnimationHandler_AnimateEyes_mE8EEA814D4114198A201B9EAE5259C6329493701,
	EyeAnimationHandler_RotateEyes_mF100A0DCCACAE6D01BDCD130C2CC8084C513A7B2,
	EyeAnimationHandler_BlinkEyes_m4A936B47896A1360C3F64FCF080281F38E614556,
	EyeAnimationHandler__ctor_m82C4E997E334CF10787DA7CACDA98F28EF19F44E,
	U3CBlinkEyesU3Ed__22__ctor_m0E1712F32F0BF56CD99EDB68EAB6293E79A892EC,
	U3CBlinkEyesU3Ed__22_System_IDisposable_Dispose_mB7812B1354202F0649E708CB552E0913EDC7C169,
	U3CBlinkEyesU3Ed__22_MoveNext_m66A7FE2A9503EDCC65C8F2017259749812679428,
	U3CBlinkEyesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21A7148FA71D14491CF4D751C5253D5C7378B5E3,
	U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_Reset_m9190DCEEDA0E422FE8E5A55599E8BF390C1270A9,
	U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_get_Current_mD837E0FCA30EF9CF3DB882D895B6581C550E9BCE,
	VoiceHandler_Start_m0BF3C770C11C4639E70693E0509FB5F57B434397,
	VoiceHandler_Update_mA7995F722E7909275461DE17E13EA28E5AB1044B,
	VoiceHandler_InitializeAudio_m6E2B25C2165D0863D53B89C00DA38EF509E02759,
	VoiceHandler_SetMicrophoneSource_mCB61D3AA9104E80FDF57B3F0E28FD866FFCB39D8,
	VoiceHandler_SetAudioClipSource_m784611F6A7964B9BE8A058EA7D785D71C98052BD,
	VoiceHandler_PlayCurrentAudioClip_mE3652737AC6EA5F8A130DF88B92062A8365382E9,
	VoiceHandler_PlayAudioClip_mBDBE5530723FBEAFFF8EEB83FB91D8CE0A105B69,
	VoiceHandler_GetAmplitude_m4CA4F6618BD43577C7BA912E0F7B2783057FD43B,
	VoiceHandler_GetMeshAndSetIndex_m0D6335810257E0658E6F3E7D4740F7051C6CDDD2,
	VoiceHandler_SetBlendshapeWeights_mE7D5FF299BED1202F7B88D56DAE3A7E7A4D633A4,
	VoiceHandler_CheckIOSMicrophonePermission_m616B6A6A24D700321262DC6C3E9BFA6123BF79B2,
	VoiceHandler_OnDestroy_mD9BDB8CE7D4CFD13C3732F597CF4BEA32084C431,
	VoiceHandler__ctor_mA36B62D1EF5145F985AB61B2272CB794588D86FB,
	VoiceHandler_U3CSetBlendshapeWeightsU3Eg__SetBlendShapeWeightU7C22_0_m41B18EE5CDEBA498B926EBA87611CBACAB08439A,
	U3CCheckIOSMicrophonePermissionU3Ed__23__ctor_m264C205C0BE8F3073576CE32182AA5F0355C38A9,
	U3CCheckIOSMicrophonePermissionU3Ed__23_System_IDisposable_Dispose_m0B021614E8EEB33ED6F76C90E73991D7010EE85D,
	U3CCheckIOSMicrophonePermissionU3Ed__23_MoveNext_m87B979C5E3ACE387E54E7D7B9C8AADE9E619B07B,
	U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m932D7A3FFA7C4E9FBD405B0F5E6139FF797C0442,
	U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_Reset_mE4D19B2D8261B6BC433381F815813EA2A43CC4D7,
	U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_get_Current_m32974A9C230173BA61BD2D8F28060675019EE461,
	AvatarMetaData_IsOutfitMasculine_mA611D4EC8A43B4E1F23034EE091BF8D0CE46B8C8,
	AvatarMetaData_IsFullbody_m5CC81E8F9F1C73E9F01E50F5695D189B5EF19632,
	AvatarMetaData_ToString_m6EDCA59173AB0AB71DCAD7A611DB8FEF2FADF53A,
	AvatarMetaData__ctor_m6A93DA1FF95E250A8AC74236B5F4AAC0017AA906,
	AvatarUri_get_Extension_m7CB982F51D2BC6808595132919BDDAA41553C5C5,
	AvatarUri_set_Extension_mF323C6D000A26393118A08C8AB59B2D14DEA5898,
	AvatarUri_get_ModelName_m7C276606C508001D9F08A945A557E6AD32DD334F,
	AvatarUri_set_ModelName_m0B24DB53617858C7B3BFBA4CBD027B1C3B4F1284,
	AvatarUri_get_ModelPath_m6D3C07770079C06DB146CFE587697662049CD4B6,
	AvatarUri_set_ModelPath_m10161E6936F6A9B3BC4E7FADF82437E98167A36B,
	AvatarUri_get_AbsoluteUrl_mC941D4F13653D6E106E747785E35D8E37BFB854A,
	AvatarUri_set_AbsoluteUrl_m0F2DAA5A296AC51CCA4E93C61F08AEB09D76B732,
	AvatarUri_get_AbsolutePath_m9C4FAE3747B4467F9E049337A6A35A663E717CA0,
	AvatarUri_set_AbsolutePath_m0A0348B71E1A7438CB60BD36F4DB3FBB3616A828,
	AvatarUri_get_AbsoluteName_mB90E1916C3D359B671166D2104EF207EC8EEB487,
	AvatarUri_set_AbsoluteName_mB5FFDFE732D8E1F48AFA2A904AF29EAFEB7648F7,
	AvatarUri_get_MetaDataUrl_m409F1074743073E4118DD865A7232A46B3AF1702,
	AvatarUri_set_MetaDataUrl_m2BFE9FFD795BB7413C010C4287A633B198E3CF1F,
	AvatarUri_Create_m248A0429CF7D4306570B2808B051E1683814C184,
	AvatarUri_CreateFromURL_m13C5A5072DC706CE123440F0E6BB67A7392011CD,
	AvatarUri_GetUrlFromShortCode_m270A0D285ED4C8DA39692B922DFF3C20591BA134,
	AvatarUri__ctor_m86FB07D1ACAA0E7B998501B9EE8280B44C879AF0,
	U3CCreateU3Ed__30_MoveNext_m03A90E20C246E269C23775C030305659959B9521,
	U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E,
	U3CGetUrlFromShortCodeU3Ed__32_MoveNext_m9B9713B7096F62F82E7EA00CDD8D2F5C21C1BE89,
	U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C,
	ExtensionMethods_Run_mA83E080C59D109AD7966D664986900522591A182,
	ExtensionMethods_GetMeshRenderer_m7E058B376E70F57DF1238B386580D6E3BEC1F0E7,
	ExtensionMethods__cctor_mD221B82969735C9B14E9753F621630506417D471,
	ExtensionMethods_U3CGetMeshRendererU3Eg__GetMeshU7C7_3_m5C71F949054653625ECF6227E9D0B5F6F847200B,
	CoroutineRunner_Finalize_m164896F3BEEC1C0F678FAB766736B4EEACC72C06,
	CoroutineRunner__ctor_mF33E200941BFAB35CBA728FC5DD26605C8F7B039,
	U3CU3Ec__cctor_m4EE3EF58C42C9BB154C2900CB60D0BF2C53B2A32,
	U3CU3Ec__ctor_m3E5A3B189EA872FF4D056ED2E954F09A48F8141B,
	U3CU3Ec_U3CGetMeshRendererU3Eb__7_0_m2A4057BED3A5DFA2CE74E5DC4D0B9EDD998BAC28,
	U3CU3Ec_U3CGetMeshRendererU3Eb__7_1_mD9D11B7B4CA9E14239FD52B1DB92D1BC396C7C31,
	U3CU3Ec_U3CGetMeshRendererU3Eb__7_2_m7AE1735E1E30601EA8D312E98D52C693E6AC802F,
	EnumExtensions_ByteSize_m35DC2853AF8B37B64D323F8334391B278BDF9A5E,
	EnumExtensions_ComponentCount_m49267065A12FAA7BC7594663FDEDC27EF2B2A1DC,
	Exporter_ExportGLB_m43ECBEFF6CC882154E10A04AF8575C1F80C8FB33,
	Exporter_ExportGLTF_mFE586D17500DB7AFBB49B066570E9604C6AB54F1,
	Exporter_CreateGLTFObject_mAE3BE1E824FD2CE22BAB7BA978C87DA8F45ED8FB,
	Extensions_RunCoroutine_m0CA03A7C0B5CB0A6CB46D0F21EFED6DE5E3011B2,
	NULL,
	Extensions_UnpackTRS_mF30329BE0D6854C0EDEB2CFA0F03506703B25339,
	CoroutineRunner__ctor_m3D99F94CCC341D48667E8B778BD90517A4DD4F1E,
	KHR_texture_transform_Apply_m87448FE0C6C66F7A97191645B0204739A7AD90C5,
	KHR_texture_transform__ctor_m54EDD81C9E18B042A5C7D28259C0DA2BF2B0880D,
	Importer_LoadFromFile_m058C890E55F38451D516608EA6010B7A735DD666,
	Importer_LoadFromFile_m5277D0168E62F8752F6BDD8C8C2AEAA765B58276,
	Importer_LoadFromFile_m220179CD75F5AF7692E3C57F31343459DEC43473,
	Importer_LoadFromBytes_mE024166143950D6723FCC8955CB55410B0159AB7,
	Importer_LoadFromBytes_m9CFA768A7A9A6570B11E7BA8DB158E47E4BEFFF1,
	Importer_LoadFromFileAsync_mCDABFA0973FBE7F1AEF4065C891651801C6365F2,
	Importer_ImportGLB_m29FD3339910FF6610E6999AAAA49BD894BA09107,
	Importer_ImportGLB_mF5CE566623017A29C79E2DDC42D0196618F339EC,
	Importer_ImportGLBAsync_mF77E7F7950E28BC77CAC94EDE6BFB2FD24DF3B4E,
	Importer_ImportGLBAsync_m4F9C38158E5E900770F85E78FD5A5F761D89A19C,
	Importer_GetGLBJson_m44C120D96396FAFEAC9220B45A4B75C912604917,
	Importer_ImportGLTF_m76F7D953B4471423ED71158143608036E5C67BAB,
	Importer_ImportGLTFAsync_mBF7C7DEF9DDCE12991AFE74652902453F7994C90,
	Importer_LoadInternal_m5B17FBF2634BA38FBA7BAF2FE9B4479660E0A4DC,
	Importer_LoadAsync_m4FC08F596DD7774E818976E385460DD131E93398,
	Importer_TaskSupervisor_m7DD1809D6E78E40446C69FBDD60D87926EB76DF1,
	Importer_CheckExtensions_m650DA6F0BBE3B7318C947AD5F6F679DDB183A88D,
	NULL,
	NULL,
	ImportTask_get_IsReady_m33C7CC33C16D6A1D6899FCDAC0CA22340ED44D7C,
	ImportTask_get_IsCompleted_mF1257CE54E903AA412CB894E7F1BA17227353B4F,
	ImportTask_set_IsCompleted_m505F23568DDF7C554EE27016272CEE74A71F214B,
	ImportTask__ctor_mA8C2D32FF44B54CC145E63E941CDE524EF7E4A93,
	ImportTask_OnCoroutine_m728AA9D6F6B6E6C99C57A8B3FFE0BCFDDD317EED,
	U3CU3Ec__cctor_m401EEF16A480CBC39E8D3797084AEE544A31EB78,
	U3CU3Ec__ctor_m511CAF964CA511B97BBDF6A52F68D37240D5BB43,
	U3CU3Ec_U3Cget_IsReadyU3Eb__3_0_mC567962DCD9C4EDFE4C9B20C87D9AE827BE31728,
	U3COnCoroutineU3Ed__9__ctor_m48041C4FB14FB5782E074B0C495000063148026C,
	U3COnCoroutineU3Ed__9_System_IDisposable_Dispose_m67ABBF5C56A17B98AF1420671F6CDA4D7C9E5A1C,
	U3COnCoroutineU3Ed__9_MoveNext_m6A35C46A11D40191E98C1D30B52046EE16F9DDB1,
	U3COnCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9AB5731BBA2862D0FB21F18BC99898273B0BC0A5,
	U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m293618DFBDD8ECC5807A2634D4D9485B11229E8C,
	U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mCC55F658F9053F6137398A7A715B3B0F7BB823D5,
	U3CU3Ec__cctor_mDDC22C929EA7C127F8702D02401D14AF4063546A,
	U3CU3Ec__ctor_mB3B996537C243D877FEE21D4571312DE74CEFC0D,
	U3CU3Ec_U3CLoadInternalU3Eb__15_0_mF784F361A79029E4AB7EA9237F090E3EAFC017C8,
	U3CU3Ec_U3CLoadAsyncU3Eb__16_1_m7A054BED01419416666F59134E62C1BC592D70C7,
	U3CU3Ec__DisplayClass16_0__ctor_m1CFC7891B19B5B9F7F38DB0C0BADD3134C7F600A,
	U3CU3Ec__DisplayClass16_0_U3CLoadAsyncU3Eb__0_m13F496152C3596DE76320B7E4B9211781721BDE8,
	U3CLoadAsyncU3Ed__16__ctor_m76847F3E08DDF071B407B405D7ADABBC307726AF,
	U3CLoadAsyncU3Ed__16_System_IDisposable_Dispose_mD51CF56256EB3889DCA5538E6F7568F1EE24DD68,
	U3CLoadAsyncU3Ed__16_MoveNext_m5DC7799B30A27DABA23BD780523206929D43A919,
	U3CLoadAsyncU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C24BE0512ABFB360B2F421DEA952FD566645D14,
	U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_Reset_m0D9F51120F9B7E43A23C9DE624925382DA3160A5,
	U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_get_Current_m365D7F07B28C4C5807A32EB050BF876E6D85D55C,
	U3CTaskSupervisorU3Ed__17__ctor_mF99DE5C7C4C73E2783E7776108CA8B3ABE42645E,
	U3CTaskSupervisorU3Ed__17_System_IDisposable_Dispose_m07973E18B963E1230482ECB0589595E0DB9DF5AD,
	U3CTaskSupervisorU3Ed__17_MoveNext_mCCB43631E1D8059BFE73AE147FF2479EDA3D2424,
	U3CTaskSupervisorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49D10D59FCF34D9FDA6D7EAAAE8AE1B10FF12E29,
	U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_Reset_mF7742948AC166E4AABD25D9C2105F6B5519635E4,
	U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_get_Current_m219B092F317FA9E9F45B3319589854F628F5F065,
	ImportSettings__ctor_m34F786086FC674EC634567F897C8D5509276C2EA,
	ShaderSettings_get_Metallic_mD59F07C8A7DBD36167DAC0C875EDFB8AE43C0291,
	ShaderSettings_get_MetallicBlend_m0906331D0C9D57DE332AF2E44BC031CC3A786392,
	ShaderSettings_get_Specular_m71B0440D77958A7D0746F8534F058FAE65C3FF42,
	ShaderSettings_get_SpecularBlend_m49E435F053AF689777E7E0AD6D1893F31C9087DB,
	ShaderSettings_CacheDefaultShaders_m202173C22E952B5AFF80F043E35AA4D14611E39B,
	ShaderSettings_GetDefaultMetallic_m7DE9E691A400F9ECB1AB1C0951CBE17923CBA233,
	ShaderSettings_GetDefaultMetallicBlend_mB5A7E7BD2A65CE3F2C8FE2F05B9775BD66C84902,
	ShaderSettings_GetDefaultSpecular_mDB795CCB21E4B5F540500EE7B0FC2EB53BA752BA,
	ShaderSettings_GetDefaultSpecularBlend_m275BBE072EE5470DAB34B1D22366F39B1DD71690,
	ShaderSettings__ctor_m42A29BCD5922383ED0ADDD35455A80DC1CAA754B,
	GLTFAccessor_Import_m03BE7327C23901FA49A6C706BD6DF9BDF5D0E3FB,
	GLTFAccessor__ctor_m49FDFCF1EC231D1268A3AA35BA28A6E5EC2F1EF1,
	Sparse__ctor_m6197ABAFE9C8CAE6A777F72C299941CA223D9ADA,
	Values__ctor_mB4B3EABF34AB1F0E95E6B31DC5B3D7E8CA3B6766,
	Indices__ctor_m029072542F14F9914C1C9AD62239BB962F97B468,
	ImportResult_ReadMatrix4x4_m5C65684AEDCD8D87FD10A70D11E5C6C0F15D345C,
	ImportResult_ReadVec4_mFD19F92DC38075E2F815EF5A722831B8633B0831,
	ImportResult_ReadColor_m190921C7E32BD6A65A9E1FF5D9A1F7648D43E487,
	ImportResult_ReadVec3_m5B4DA9644EF368B9D828F59531CCAA81DE310D9D,
	ImportResult_ReadVec2_m5EB1C60CEE9143AE18571D4E009C6A81FC7747B6,
	ImportResult_ReadFloat_m99D81FC447786E9B9C7A93C20D2E3277ED335E63,
	ImportResult_ReadInt_mA9821C609234FBDCF4954FA6E8FBB52F7F7C432A,
	ImportResult_GetIntReader_m60E69F5F42DA7F9EE5C8695B1FC531998312F059,
	ImportResult_GetFloatReader_m78ED6CB1AEBC09F46651988560CC4E97ADC20B3E,
	ImportResult_GetComponentSize_mDECD61E46D694ECAADA96F0E9A850D58BE3F8D75,
	ImportResult_ValidateByteStride_m01087D76571B6AA5E55A1E733B58C2EE227AF532,
	ImportResult_ValidateAccessorType_m061737BC0198ECD54CB321700DF96424E58CDA31,
	ImportResult_ValidateAccessorTypeAny_m5CE190A5035881071F70FF155FB7FE9CF408BD72,
	ImportResult__ctor_m4F8C70D14809EC9C1DD66AE9EE6C0067CB84F669,
	Sparse__ctor_m4B42BCB08F4D361F87350BF71D056BE5E36C0B8A,
	Values__ctor_m797FE3FD346A7C05897F4ACD2006F8EFE2C8E45B,
	Indices__ctor_m45FC7D34D1DC715FD0D6F6BCE2ACE1B858CCC367,
	U3CU3Ec__cctor_mA58C424BCA709CFF8A8B5F3E5AB4F344FADDEA78,
	U3CU3Ec__ctor_mD9E00632150502572A482C375B346D4D5AB1630F,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_0_m5BA2B435FF5C01AFD0DDB48254D311E8E75CE525,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_1_m05336D4EF2293AFB4A21D116156F52B8605AD123,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_2_m368122150278BA7380B428DFB66E2D3C07000EB3,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_3_m82F59D82B4E9AA76C76550DCF0FC98D69D0CF4FD,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_4_mEB1C67A4FBF84D3550333A244D50791BED270031,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_5_m148BD797DCD0B3FCA6E2FF7C48A3A2896FF6E701,
	U3CU3Ec_U3CGetIntReaderU3Eb__15_6_m575F43E651AA139D2C48A91811083291AA0123D4,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_0_m75C31BDCB7059D735D6E65647C46EB4624FB53D3,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_1_mE7092443697BA7C1EDD28D0031C6C1BC2640A5E5,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_2_mE200DC4CFCE207BC55EA61EF5E2D936DB14EED93,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_3_m24BBD284075E0154C7407199D4955411597CF991,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_4_m912FD88EDD67D1EDDB0B4E760037F469D5D31814,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_5_m339242F45669729631D387F4A6137CB7321F1210,
	U3CU3Ec_U3CGetFloatReaderU3Eb__16_6_m8560F3A7B66AC0C7976DB2A3872BA11DB6F56408,
	ImportTask__ctor_m1686989991B0C4688310310A8B2A522798B4ABAC,
	U3CU3Ec__DisplayClass0_0__ctor_m6FB7AD07D3A16E030A3784BE079991E693A42060,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m635D56C7D5EC9685DA1204E8BA6BCC60DCF845A9,
	GLTFAnimation_Import_m8AED9E41BD1AF833636AFD853403B8C2B54B82B9,
	NULL,
	GLTFAnimation__ctor_m88B6A57FB663F69A39AA55E3A11575B984FD57F4,
	Sampler__ctor_mCD7EE8A365A6859DF230D28C357D194C3F51FC26,
	Channel__ctor_mEDEFF0FBA488E54C46624D52B21C538DD306BB2C,
	Target__ctor_mBD5255A5403EAC750557FAA4F71B4D286FD8378B,
	ImportResult__ctor_m5251D20424615BC9E43D79D8008D7FECA099411A,
	U3CU3Ec__cctor_m5A668E586775BAFD14D9F9393491EC2F57688C3C,
	U3CU3Ec__ctor_mBE71CDA27C7E14749A7BB1FD0E132D4D3109607F,
	U3CU3Ec_U3CImportU3Eb__7_0_m6CDC747F13A20E7F6226A62633A616561DFE0793,
	U3CU3Ec_U3CImportU3Eb__7_1_mE1054BB35DC586A532AACDE0A5DF54AC10D6C511,
	U3CU3Ec_U3CImportU3Eb__7_2_m75CEB549938D5A852DA01D7793475427BCB5CE93,
	U3CU3Ec_U3CImportU3Eb__7_3_m8D237D17638AAB3AE71BCE21B46FC60F476A65F5,
	U3CU3Ec_U3CImportU3Eb__7_4_mFE6FCDAAC2E650CDE2C78D53061C39552C5B9AA7,
	U3CU3Ec_U3CImportU3Eb__7_5_mC19AE5D84FAAEA35046A5282241B7ED8000A52DF,
	U3CU3Ec_U3CImportU3Eb__7_6_mD9F2E3AEA69B6B7D493E8D2390ACA2200647849A,
	U3CU3Ec_U3CImportU3Eb__7_7_m536020F1CCF02CF40DE3B20D7FA0CDA85E054889,
	U3CU3Ec_U3CImportU3Eb__7_8_m37786D7C3F398F4AA0894B3AFBCCF9220036EFFA,
	U3CU3Ec_U3CImportU3Eb__7_9_m690988444A65819269026F242B9A9BAC5D48B7F5,
	U3CU3Ec_U3CImportU3Eb__7_10_m68B1356BD8CA00B0883912527157E9D0025818E8,
	GLTFAnimationExtensions_Import_m3F291F7CC01854BE61DF4FE8617630A80E8D55F1,
	GLTFAsset__ctor_m2E5435A11B0564E6B54B0D1BF75C92703C3C7EE3,
	GLTFBuffer_Import_m2321E35D9E9735F3E5A608ECD2D4A2D1C80E5B62,
	GLTFBuffer__ctor_m904E8D9212E35FE97FE3AD4872F34CDF656168BC,
	ImportResult_Dispose_mAC75FAF48D450506451DB92E081AF037D9D465DF,
	ImportResult__ctor_mB56AA6B17C486D96553F1EC6952316155A480AD8,
	ImportTask__ctor_m290BAB1B23DD1936DF98B8419A9A47B61261BB7A,
	U3CU3Ec__DisplayClass0_0__ctor_m6C0EED70726AA554DC8CBD9200296DED0C4031C0,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mF1CCF067259D764F3D779F484B380E0B1048A154,
	GLTFBufferView__ctor_m813A1D91373C5FF8498692FAF84ECF9E891EBF20,
	ImportResult__ctor_m65F7CD3FCADED77304E1F469B6701A5D545235CF,
	ImportTask__ctor_m71EA93DB8C4CC26AA4FBBFEF47F1C7658AB06514,
	U3CU3Ec__DisplayClass0_0__ctor_m99A3EB6C97D1E36E3ADBBADABE44BE756B21F6F5,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m8C57D34460367647824ED366C9153B9AD8A35AFB,
	GLTFCamera__ctor_mDBE6527F9018BE1FD8FE2BB4F2115C5EA4E5F79E,
	Orthographic__ctor_mB6EA6BA991C32A3989337109579FC71471238880,
	Perspective__ctor_m353DEEACF172412C49693286FFD636C104758D14,
	GLTFImage__ctor_m6EB9AF82F8ED524A9FC6846FFC6E491C47EB125B,
	ImportResult__ctor_m4E1419C0874C38FF52CFF15A314CE79988DC9A5E,
	ImportResult_CreateTextureAsync_mE370E435B53F299305E45F88E0435DFF6DF0EE93,
	U3CCreateTextureAsyncU3Ed__3__ctor_m41C4795268B5A5FDBD09AAC759CB825B4E7ADDEC,
	U3CCreateTextureAsyncU3Ed__3_System_IDisposable_Dispose_m309F94D77BA47F75B8D25738544C71739FF6FE9E,
	U3CCreateTextureAsyncU3Ed__3_MoveNext_mE1D1DEA5A0EDAA8C199965EC1269EE7BDF35B144,
	U3CCreateTextureAsyncU3Ed__3_U3CU3Em__Finally1_mCFAFCB3B96460B1412E4849A5C249C74E520B136,
	U3CCreateTextureAsyncU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BA6DA399F310B5E0D0BE1275626D33722409938,
	U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_Reset_mC1E6231288207824BC54FEB4B4706F8AD9A2F8EE,
	U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_get_Current_m0581A5FAAE854B83BCF99A47E55DAF51A4F0387B,
	ImportTask__ctor_m1A726621D81C1A0BBA62D87565A43636502FCA60,
	U3CU3Ec__DisplayClass0_0__ctor_m015CC88C2A9EE8B523968039384B7F5CDA8CFF3E,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_m4E5339ADD7C9202FC24DC01D2B94F0ED3EB3E28E,
	GLTFMaterial_get_defaultMaterial_mEBF9532DF99E19839D892A8B64E00219EA681C3B,
	GLTFMaterial_CreateMaterial_m1BFA5DB15E037943CB2915DFB9B2001020343636,
	GLTFMaterial_TryGetTexture_m8BF1941ECA59813B13C2CD0DC135FC798F05043A,
	GLTFMaterial__ctor_m539C5E900EC8607C281A2FB3ABAC2C0EE07A04EB,
	ImportResult__ctor_mBA4D29E0CB83313D95F926BAEF5197D2A5A4B293,
	Extensions__ctor_m99E9A7AC1A4C37725E36EC5D5BA14BBB5C9CFFDC,
	PbrMetalRoughness_CreateMaterial_m510DD5D199D7102C9C43BBC3EB580743EBE52DAE,
	PbrMetalRoughness__ctor_mDBD457660EBAED214B9EE118AFBEB138C02842DA,
	U3CU3Ec__DisplayClass5_0__ctor_m2A8AE30EFB428AD0334E0259F5F36B7D91BC0C00,
	U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__0_m319D000D5BEA4D5641492C65200B577238C5C10D,
	U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__1_mBC7E8EAC107163F40648295A174D5A39A6D98F74,
	U3CCreateMaterialU3Ed__5__ctor_m0A51ABDB657E7515358686A7C789F8BBC753C944,
	U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m13CED19482847457E82EBDB9245B7CD31CB07240,
	U3CCreateMaterialU3Ed__5_MoveNext_mB20A36BCC15B15DED7B4F9291E01A376019AF20C,
	U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDDD4A652A44B7176224930CF73F79BD54250E8C,
	U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_mBC061664859BF0BCFDE8D797FFFC0B168990E006,
	U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_mFE01300D7830C2D0E963D4EB1C7BB3CF05891049,
	PbrSpecularGlossiness_CreateMaterial_m87F25CC94EAABAFC3DEAE2FC18C02FA6B3C3F53D,
	PbrSpecularGlossiness__ctor_m7978A0D2413E41A2CFFA3D27D90E05AE2C3E2C4B,
	U3CU3Ec__DisplayClass5_0__ctor_m8D558DCB6560F1D99E4F19A2CAB3EE1A03603FAF,
	U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__0_mFC38B15C8C304E70AC225BB0A52EAE2CF5286DBC,
	U3CU3Ec__DisplayClass5_0_U3CCreateMaterialU3Eb__1_mA49C86103AD35FA41520FFE97D603454550A64AA,
	U3CCreateMaterialU3Ed__5__ctor_m44A29489BC2A499F5B2F902E55565A4FBA517217,
	U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m12CE43689F6D8E80891D8B58C9734A3D34E2B981,
	U3CCreateMaterialU3Ed__5_MoveNext_mA6AFF0E5FCD6D8C4AC92CD06AEC7A1B76655763A,
	U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D6317ECF4FAC9564EDB508BF2C605119B8AC149,
	U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_m02FC2F8E28E73A773C1F110BF34CEAB125EAD61D,
	U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_m50CBFD918D48E2F4F72CBC1D965FFC4848C89846,
	TextureInfo__ctor_m9076A8B7BAC61017E1658F98C7D2A9B5F869CAFA,
	Extensions_Apply_mBC4E7987462D68D0704439134696870D65ECAF2B,
	Extensions__ctor_m26F2170661C93724EA5008D62C6270747163B295,
	NULL,
	ImportTask__ctor_m7E45F12979003FCDAC3D8AF1E41B43FA92AF1AA7,
	ImportTask_OnCoroutine_m126F3069CBD3CB5D2B22552C9654A993A7E4AB81,
	U3CU3Ec__DisplayClass3_0__ctor_m3CB9D5A4BD17576EBDBB230EA01EA1F51692265C,
	U3CU3Ec__DisplayClass3_0_U3C_ctorU3Eb__0_m38BE1BE63B61CC56EDD73B870B2E173789D35CA7,
	U3CU3Ec__DisplayClass4_0__ctor_mCAB600ABEBEEE55A01DE664954DDE1FD669FC86D,
	U3CU3Ec__DisplayClass4_0_U3COnCoroutineU3Eb__0_mADA04E150D9F97D231E6854DEC45B0F6B666B3DF,
	U3COnCoroutineU3Ed__4__ctor_mC000ADF62EDF90D65CCEF5F37D3B0545DA3969AD,
	U3COnCoroutineU3Ed__4_System_IDisposable_Dispose_m839B941EFE59407CAC6CFF970CE785DDCB4A525D,
	U3COnCoroutineU3Ed__4_MoveNext_m3BB863C14B661CD5BA9C6C9A23D9B337A931DB87,
	U3COnCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFE681C78510873E7CBC57648CD563EACCF5393,
	U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m63FF5A76FE5B7F03B362CECBF4B0CFDE61304E0A,
	U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m9EAB85B83380E6DBCED03E066A9EE672642ED63B,
	U3CU3Ec__DisplayClass13_0__ctor_mE66B3CC45A159086D47D8E9CC284D5EAC6A626A4,
	U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__0_mF275D9FE3D30DF29EDF1D0A43DD60C4EE5FF7894,
	U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__1_mA9880FC550C2A52F174B07A1192F7188ECAEE19D,
	U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__2_m10C0B25C17F8374F2AA9F4C2771530157592DB7A,
	U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__3_m228D0AEBC560D3AAF5E5C04367F7A27D2CD4478E,
	U3CU3Ec__DisplayClass13_0_U3CCreateMaterialU3Eb__4_m93DA3E7D3E95CD8332B2FFFA6C65D528C068D954,
	U3CCreateMaterialU3Ed__13__ctor_m906F8A69518299C3BE456AB85AD3D56E2CAB0E5E,
	U3CCreateMaterialU3Ed__13_System_IDisposable_Dispose_mB221ED8C778CB3745D04CF99F434E2FED1FBFE09,
	U3CCreateMaterialU3Ed__13_MoveNext_m2B43FB11A3B4657A587328A4157833A515A2863E,
	U3CCreateMaterialU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FE40E1FC57B8887B00B84B6167F1EA8C98A005D,
	U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_Reset_m42BEBD986CDAECFAC5912B7188178DF75B39F309,
	U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_get_Current_m9A6DBBE0CD322FC365D0EC8669134D2CF62C4C35,
	U3CTryGetTextureU3Ed__14__ctor_m49A3D3B99F86821FB6B1D9088E1FB565431BA72F,
	U3CTryGetTextureU3Ed__14_System_IDisposable_Dispose_m2C0609E384C9CA7CD0692B5FFDED45328ACE9E2F,
	U3CTryGetTextureU3Ed__14_MoveNext_m7FF2FE0A46723851BCEA5FB39E176EFFCAA4B6B8,
	U3CTryGetTextureU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AA5956C39B5D0E45D48C93B3B5F49395438AD99,
	U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_Reset_m6F5354F3EBCA5488FBFE81D513788587166AA037,
	U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_get_Current_mE2011302FAE1B32CC0672009985C052CA2ED8FFF,
	GLTFMesh_Export_m2E90C0D95740BDF062D300792C29CEE10D0E682B,
	GLTFMesh_Export_m653A9262512A09F2EF078495463B66B9FCE0E297,
	GLTFMesh__ctor_m201F15328639BEACC8C40804F68776175D1FBF6A,
	Extras__ctor_mE4A4FF1622A4779A305E12FF311A46FAFAF36C11,
	ImportResult__ctor_mBCA9F284C327351A9E5C23D67BB03A9C3F406F1D,
	ImportTask__ctor_mE3BF0B78FC7680E08663E80ABA3EA85DA265F615,
	ImportTask_OnCoroutine_mD6781BEBFC39FEA29FF35630A736F1D2148FC2F3,
	MeshData__ctor_mE2068B5E8E71B40AD3FAC60121960604E237692C,
	MeshData_GetMorphWeights_mB0510DD795C76C408DC0ED5C886FD49C88255F1F,
	MeshData_ToMesh_mCC0EE262937B0FF61D0C2F73B49B9B3ADE48767F,
	MeshData_FixBlendShapeNormals_mA348C32649DC4921D8A60D62AAFD01401E867194,
	MeshData_NormalizeWeights_m3DFCC671A03F738EA398948D5C31BDB87A865A94,
	MeshData_ReadUVs_mC877E0948DB9E701A8E86C724C87C88F71BA5C2D,
	MeshData_FlipY_m503D5D4F522BA76CF6E5B6F3F6A9F5C8CDD7DABD,
	BlendShape__ctor_mFA9F525133C3FFF92D57BAEEBDAA0DEB0D3AA75E,
	U3CU3Ec__DisplayClass19_0__ctor_mA49A482206787357DB8495F58C42CC099460331F,
	U3CU3Ec__DisplayClass19_0_U3C_ctorU3Eb__0_m9EF40A59B69FF56030297B76F5932CB26C5C2B6D,
	U3CU3Ec__DisplayClass19_1__ctor_m644CEF863082528137EAF7120F52F453BB4200F7,
	U3CU3Ec__DisplayClass19_1_U3C_ctorU3Eb__1_mDCB5EDE1BF36B2A1F09C926B478B2123DC8F7007,
	U3CU3Ec__cctor_m30B6AF535111D074CA46764E0E4E52CE4548E6F1,
	U3CU3Ec__ctor_m8F81E088321395ED6FF9E84B310AA5BAA3B5DF77,
	U3CU3Ec_U3C_ctorU3Eb__19_4_mE41E02C21D22A3D227FB2A4CC821315CE61D5E8C,
	U3CU3Ec_U3C_ctorU3Eb__19_2_mA6A7991CC93D9C82ED0D7DD1A4315D8C209FB1AC,
	U3CU3Ec_U3C_ctorU3Eb__19_3_m66A75DC4C4E5AF6D57C5E71F0AA805F99EC4F28B,
	U3CU3Ec_U3CGetMorphWeightsU3Eb__20_0_mCF098509AE1304701AAA2475E95A9E576D911DCB,
	U3CU3Ec__DisplayClass4_0__ctor_mEE7DA844CE4FF5A4AE67EB17E8BA1B4F18E703CA,
	U3CU3Ec__DisplayClass4_0_U3C_ctorU3Eb__0_mAAFC5917A6A627F2403E5E3C855B08C78FAE22D5,
	U3COnCoroutineU3Ed__5__ctor_m10BD190BCCD4CA5D3DE6E9E67C56D841671D3738,
	U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m519BC153E6FD02D1D79F068287499513399CCBAF,
	U3COnCoroutineU3Ed__5_MoveNext_m8A1FFCA3117503C3574ADC4B7B10401C9B544F90,
	U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CDE424A2B66B71D81B41A452ED5272CC3254BBE,
	U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCD839561719DB78CC9B400ABC9044F9D8D65A945,
	U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m27C3951830591BFBDD37468D99DFF91264FEAF9A,
	ExportResult__ctor_m0289C3735F27BF0157D4832DEB885EC1C97CCAD5,
	GLTFNode_set_matrix_m5021D50B20D9FC495434B1285D7B83307F5A76EB,
	GLTFNode_ShouldSerializetranslation_mA84C635C6EB142EBE31A52456728A866D0827386,
	GLTFNode_ShouldSerializerotation_m255418FA94A8C57291AA27AEF88409FC7A192451,
	GLTFNode_ShouldSerializescale_m304207CE77A2D451D74A91288E12D0709B09BCE2,
	GLTFNode_ApplyTRS_m2CA2377D8AD1ADD7E03EB4479ED8D32CFFC25639,
	GLTFNode_Export_mA3BE477CF76167DC1DD6D0208052FC94E065B57D,
	GLTFNode_CreateNodeListRecursive_mB89E4931C9485A37B1EA8C9FE047BCD7399A467E,
	GLTFNode__ctor_mD2392D42A509C53A90A2D1C2BF50A2F419FCC358,
	ImportResult_get_IsRoot_mE17F73630C3003C720D7C2CE2385F499CFB2CF40,
	ImportResult__ctor_mD715A3036A7B0BB5C348FD1C6A8829817412978A,
	ImportTask__ctor_m0F2061D9EFCCBD960AE1FD5DE4C429CF5CE8FFAA,
	ImportTask_OnCoroutine_m9F5F609BFEA83B8E95A3EFA0C47E70CBC0B506A2,
	U3CU3Ec__cctor_m7ABA7DD3B9857858D334FD8AD7A4CBCF895F55D3,
	U3CU3Ec__ctor_m6D93EA8A1C5CFC4B2BA5DE76D69E63AA012C97C0,
	U3CU3Ec_U3C_ctorU3Eb__4_0_m608491E7C34161ADFB68A93597FA4C86B80A06CF,
	U3COnCoroutineU3Ed__5__ctor_mA8A51B40E3A30D79F493009EAEFAA8487B2DB3FE,
	U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m858F98D21F6F66A0CBA1350D4CE8684CBE2BD24E,
	U3COnCoroutineU3Ed__5_MoveNext_m9A3A4A26BB83082AA2E09FBD4AAEEC3A2B3FCA64,
	U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDAFFD7EE3601FC7392DCE575962FB08F6247FF8,
	U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m488AE3FB7BA142292F30FA08BAEBF3FA87CB8FD7,
	U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF44DD69E2CE4C8639CC4D7D7A9011401805C3E1D,
	ExportResult__ctor_mE90844A3936BC12C95EFF1BAFE08AEE632E3AAD8,
	GLTFNodeExtensions_GetRoot_m16F36DAA9BC509FC186B083147EE9BBCEBEC1951,
	U3CU3Ec__cctor_m44F5837C79631E04A4A7A7B80764CA322AA95BB4,
	U3CU3Ec__ctor_m6287CCF554071E2C20BAF91502FBC2B7AF5727E9,
	U3CU3Ec_U3CGetRootU3Eb__0_0_m36A01DA28B5CB43244373B4611204DCA8B93B169,
	GLTFObject__ctor_mA0A38F7E8C513A8DE58549B1529B2D2E74626FB1,
	GLTFPrimitive__ctor_m6B988D08D05C3E98E6971212E33BEB2627EC590A,
	GLTFAttributes__ctor_mD4789A48EF7BAF7C8F1024125B244BDF57F4595C,
	GLTFProperty__ctor_mDDC6FB840932B7B0FBA52F720A7CDE13CC96DD3C,
	GLTFScene__ctor_m2955AEFDC17FFACDBFB0FAC8E2A546C8F6A837EF,
	GLTFSkin_Import_mE1A672644F9B89EDD670375B6679EE0A60700B86,
	GLTFSkin__ctor_m6DB48FE07EAF41283E9A3DAE8EFE4388D873FB1D,
	ImportResult_SetupSkinnedRenderer_m4638DC0973BA965AC04BBB879D5A2F29B9B8B6EE,
	ImportResult__ctor_m580EFB5BB14054DCDB6D22A8C8B5A3D1F79D3BA7,
	ImportTask__ctor_m8BB023A8C9DEBAAC8FC2222CE3C51C08EBA4AF36,
	U3CU3Ec__DisplayClass0_0__ctor_m24DDE1B93CAA568343C8BAEC5F5E9F3E05996E16,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mF0A61A668646DA96A0637A3BCBFB91F58E5B7F4C,
	GLTFTexture_Import_mA898C6D1F23331F4A25540655B10BC1687F67AA8,
	GLTFTexture__ctor_m1D35B0EC3C13E5DF89080CD90F88F9C388154D63,
	ImportResult__ctor_m6BDB049B8F514434D1E2A3B58B8541855567F8CB,
	ImportResult_GetTextureCached_mB90AC91183452AAC0FEE818967721C5674EEB90C,
	ImportResult_U3CGetTextureCachedU3Eb__3_0_mAC38EEC6CF9BF4CB79BADD9282751F7055D4B77E,
	U3CGetTextureCachedU3Ed__3__ctor_m08187E332D6D4CBF1F26C0854566D71C1B1481EC,
	U3CGetTextureCachedU3Ed__3_System_IDisposable_Dispose_mB8C460D10E2AC911E0BF6E36D837C16AB6A0BB39,
	U3CGetTextureCachedU3Ed__3_MoveNext_m78C5CE649F9526425B139A1046A4B1764F1DA534,
	U3CGetTextureCachedU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DB4C8C659FA598EA2FAB80A36EE08E786B081D,
	U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_Reset_m11B2A8AB90940DBA21D26137251ACF2F86E21FD8,
	U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_get_Current_m60485DC51BA051C07BC32A4FE37E6B3419AB0673,
	ImportTask__ctor_mF0750C69A56476A492446B29FF48197FA789A1DD,
	U3CU3Ec__DisplayClass0_0__ctor_mFEAF65681DCEFEA5FED8A93B52FD1D8116B6CE4B,
	U3CU3Ec__DisplayClass0_0_U3C_ctorU3Eb__0_mDC4FCD5BA4B22053C7B46EECE6460A817C698382,
	ColorRGBConverter_WriteJson_m1C28F266A54B68FC8187E70C4D96FE3410D5E0AD,
	ColorRGBConverter_ReadJson_m86088870AEF5A52477E1C3051B332884C0E58600,
	ColorRGBConverter_CanConvert_m6AF673F9E6658344F83B7C7A66C76611DBBB9C27,
	ColorRGBConverter__ctor_m1F339F8FB512F62B6ADC38F9422D5672D339ADFB,
	ColorRGBAConverter_WriteJson_mC5C32C6A22C8B6CB75ABD82BD0CF0B5C6A35646F,
	ColorRGBAConverter_ReadJson_m42368D4B50A055D01261C501CB408BF9FE427B37,
	ColorRGBAConverter_CanConvert_m87B2B7CB0F90F43E121E6ED7E0EFB87DE2456046,
	ColorRGBAConverter__ctor_mD26F98C66A8EA2E171193CED3F38542A43B663E2,
	EnumConverter_WriteJson_mCE2B439B5DCF94A4E4906551B82B2B18D88CF4F5,
	EnumConverter_ReadJson_m578A41050853AC55790EF219550665CCA879D1C2,
	EnumConverter_CanConvert_m3A551B8A9B03DF4B7E62992DCBEE9C2E60876AC7,
	EnumConverter__ctor_m81BE23CA91C829E2182090E4BF68E4BC800E8EDF,
	Matrix4x4Converter_WriteJson_mB78BAB9E1D5B79B15C9B6615D65753FCBDBC4A17,
	Matrix4x4Converter_ReadJson_mE2E93DCBD0B02F1A305758D36649172F85EE733C,
	Matrix4x4Converter_CanConvert_mB876817FF2135B0164DD06F1A3B703C29D5A42AB,
	Matrix4x4Converter__ctor_mD0BF9C90F2518295B43FA69E2E2014827A23D968,
	QuaternionConverter_WriteJson_m0C104DBB426A66A55135C227059C1AA67004AF63,
	QuaternionConverter_ReadJson_m2CCD8FD7E2D91F81F3DA8497B960B0FB74C2EE87,
	QuaternionConverter_CanConvert_m490A79FA7780312FF87AE9971946E8A5D045FF5F,
	QuaternionConverter__ctor_mD98DE4AE9A496B15C8113D6EDD512ED277E9B20D,
	TranslationConverter_WriteJson_m82D7FF22C853A69DD32730A35B68F5FEAEE585D9,
	TranslationConverter_ReadJson_mA106EA103EB9F66621B17FCBC7D960E61EA1231F,
	TranslationConverter_CanConvert_mBE936BB8122DDFF178B3759683C50AB454FF81C7,
	TranslationConverter__ctor_m1C1C3FB54A3FB2F6AFD6247DFFD0AB58CF127E60,
	Vector2Converter_WriteJson_mA9A1F9DBD58F1CB676BB78F9D858760874A9250B,
	Vector2Converter_ReadJson_m4FE619C78788AA70AF9476F996777150E039619B,
	Vector2Converter_CanConvert_m92F8EAC4287319D6EDF5C0AF1DE27504F605982D,
	Vector2Converter__ctor_m83A75A2959B7ECC04BD080321DC6E9F1BFA3D773,
	Vector3Converter_WriteJson_m9202133E7D3FAE8971632B284C255F12E7260484,
	Vector3Converter_ReadJson_m55E8FEF512C204E7E16460D167A42B9F6464D2F9,
	Vector3Converter_CanConvert_m1415AFFD6226BBC0F2F83E21BE5A9B8B20CB06C2,
	Vector3Converter__ctor_m184435212232544BE6EB6975977D3BB5F4BEB479,
};
extern void Bit2Converter_Read_m6A9F724C42A23D7794F2B98393B19193CACDFF50_AdjustorThunk (void);
extern void Bit4Converter_Read_m2E8694B24CE49956E94B493FF1544870F0478DC2_AdjustorThunk (void);
extern void U3CLoadAvatarU3Ed__19_MoveNext_m9BD287FCF1FD3F252007B2DE8FDAE58D51FB2CF6_AdjustorThunk (void);
extern void U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E_AdjustorThunk (void);
extern void U3CCreateU3Ed__30_MoveNext_m03A90E20C246E269C23775C030305659959B9521_AdjustorThunk (void);
extern void U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E_AdjustorThunk (void);
extern void U3CGetUrlFromShortCodeU3Ed__32_MoveNext_m9B9713B7096F62F82E7EA00CDD8D2F5C21C1BE89_AdjustorThunk (void);
extern void U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x0600000E, Bit2Converter_Read_m6A9F724C42A23D7794F2B98393B19193CACDFF50_AdjustorThunk },
	{ 0x0600000F, Bit4Converter_Read_m2E8694B24CE49956E94B493FF1544870F0478DC2_AdjustorThunk },
	{ 0x06000046, U3CLoadAvatarU3Ed__19_MoveNext_m9BD287FCF1FD3F252007B2DE8FDAE58D51FB2CF6_AdjustorThunk },
	{ 0x06000047, U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E_AdjustorThunk },
	{ 0x06000097, U3CCreateU3Ed__30_MoveNext_m03A90E20C246E269C23775C030305659959B9521_AdjustorThunk },
	{ 0x06000098, U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E_AdjustorThunk },
	{ 0x06000099, U3CGetUrlFromShortCodeU3Ed__32_MoveNext_m9B9713B7096F62F82E7EA00CDD8D2F5C21C1BE89_AdjustorThunk },
	{ 0x0600009A, U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C_AdjustorThunk },
};
static const int32_t s_InvokerIndices[509] = 
{
	4510,
	3654,
	1942,
	3653,
	4563,
	4563,
	4508,
	4508,
	4509,
	4509,
	4567,
	3653,
	4620,
	2016,
	2017,
	4620,
	1947,
	4620,
	3675,
	4620,
	4620,
	4620,
	3675,
	1947,
	4620,
	4509,
	3653,
	1947,
	4620,
	2684,
	2684,
	3675,
	2684,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	3653,
	4620,
	4563,
	4620,
	4533,
	4620,
	4533,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4509,
	3653,
	1947,
	2684,
	2684,
	1325,
	4533,
	4533,
	3675,
	2678,
	1947,
	3675,
	1947,
	1947,
	4620,
	7186,
	4620,
	3675,
	3653,
	4620,
	4563,
	4620,
	4533,
	4620,
	4533,
	2684,
	2684,
	2684,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	4563,
	3653,
	4620,
	4563,
	4620,
	4533,
	4620,
	4533,
	4620,
	4620,
	4620,
	4533,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	4620,
	4620,
	4620,
	4620,
	4620,
	3675,
	4567,
	982,
	3704,
	4533,
	4620,
	4620,
	5950,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4563,
	4563,
	4533,
	4620,
	4533,
	3675,
	4533,
	3675,
	4533,
	3675,
	4533,
	3675,
	4533,
	3675,
	4533,
	3675,
	4533,
	3675,
	2684,
	2684,
	2684,
	4620,
	4620,
	3675,
	4620,
	3675,
	6888,
	6168,
	7186,
	6505,
	4620,
	4620,
	7186,
	4620,
	3073,
	3073,
	3073,
	6767,
	6767,
	7066,
	7066,
	6888,
	6888,
	-1,
	5518,
	4620,
	1040,
	4620,
	6168,
	5706,
	5395,
	6172,
	5704,
	5540,
	5704,
	5704,
	5540,
	5540,
	6164,
	5704,
	5540,
	4866,
	4806,
	6172,
	7066,
	-1,
	-1,
	4563,
	4563,
	3701,
	3675,
	2684,
	7186,
	4620,
	3073,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	7186,
	4620,
	2684,
	3073,
	4620,
	4533,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	4533,
	4533,
	4533,
	4533,
	4620,
	4533,
	4533,
	4533,
	4533,
	4620,
	2684,
	4620,
	4620,
	4620,
	4620,
	4533,
	4533,
	4533,
	4533,
	4533,
	4533,
	4533,
	2678,
	2678,
	4509,
	6929,
	6278,
	6280,
	4620,
	4620,
	4620,
	4620,
	7186,
	4620,
	2449,
	2449,
	2449,
	2449,
	2449,
	2449,
	2449,
	3292,
	3292,
	3292,
	3292,
	3292,
	3292,
	3292,
	1947,
	4620,
	4620,
	853,
	-1,
	4620,
	4620,
	4620,
	4620,
	4620,
	7186,
	4620,
	3073,
	3297,
	3297,
	3297,
	3298,
	3298,
	3298,
	3298,
	3297,
	3297,
	3297,
	5401,
	4620,
	851,
	4620,
	4620,
	4620,
	729,
	4620,
	4620,
	4620,
	4620,
	1947,
	4620,
	4620,
	4620,
	4620,
	4620,
	4620,
	1947,
	866,
	3653,
	4620,
	4563,
	4620,
	4533,
	4620,
	4533,
	1040,
	4620,
	4620,
	7158,
	853,
	5054,
	4620,
	4620,
	4620,
	587,
	4620,
	4620,
	3675,
	3675,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	587,
	4620,
	4620,
	3675,
	3675,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	1040,
	4620,
	1040,
	1040,
	2684,
	4620,
	4620,
	4620,
	3675,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	3675,
	3675,
	3675,
	3675,
	3675,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	6888,
	6888,
	4620,
	4620,
	4620,
	355,
	2684,
	1040,
	571,
	4533,
	2684,
	3563,
	661,
	3563,
	4620,
	4620,
	3073,
	4620,
	2429,
	7186,
	4620,
	3370,
	3370,
	3374,
	3370,
	4620,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	3667,
	4563,
	4563,
	4563,
	3675,
	6888,
	6518,
	4620,
	4563,
	4620,
	730,
	2684,
	7186,
	4620,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	6888,
	7186,
	4620,
	3073,
	4620,
	4620,
	4620,
	4620,
	4620,
	2684,
	4620,
	853,
	4620,
	1947,
	4620,
	4620,
	2684,
	4620,
	3675,
	866,
	3675,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	1947,
	4620,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
	1040,
	596,
	3073,
	4620,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x060000AC, { 0, 1 } },
	{ 0x06000119, { 1, 1 } },
};
extern const uint32_t g_rgctx_TU5BU5D_tB36AA3801A957F1724DF1EFFAF571E0CB64BEF3D;
extern const uint32_t g_rgctx_Func_2_Invoke_m4158FC58B428EA9B481A3621397E392DF5CDEE51;
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_tB36AA3801A957F1724DF1EFFAF571E0CB64BEF3D },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Func_2_Invoke_m4158FC58B428EA9B481A3621397E392DF5CDEE51 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	509,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
