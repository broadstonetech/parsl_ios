﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtualActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// IOSWebViewWindow
struct IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// MessageCanvas
struct MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// NotSupportedWebviewWindow
struct NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Webview
struct Webview_t378970811F03E71B224E384A77472C00869DD7BF;
// WebviewOptions
struct WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75;
// WebviewWindowBase
struct WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// Webview/<LoadWebviewURL>d__13
struct U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral022FA5D881928CD213562A8ADFD309C0291B1DC1;
IL2CPP_EXTERN_C String_t* _stringLiteral218F5A08519088A96BE3C1074984C53EA49F1CCA;
IL2CPP_EXTERN_C String_t* _stringLiteral3BF35F255EAB6C8F0F4ACAE319B26008A7FC7263;
IL2CPP_EXTERN_C String_t* _stringLiteral5F4C20AD460EC51BE658DED3BFDD1A21D566B162;
IL2CPP_EXTERN_C String_t* _stringLiteral6B9666D6D571D2FF314955240409E0435EA33F8B;
IL2CPP_EXTERN_C String_t* _stringLiteral75D50314061E23E4B56389DAABD2C476AC3ECAE1;
IL2CPP_EXTERN_C String_t* _stringLiteralBFABCD6D77B45AD32AC9DABCBB5BCB368C08EDDC;
IL2CPP_EXTERN_C String_t* _stringLiteralCC757C1AF462C455DA292F07F8DC5D584F694ACF;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisIOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60_m22578968B2E08B2BA698515F87466AFCC2EBD326_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisNotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B_mF93551E8500D15D32FEAE9B324FFF994ACC3F118_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m0608DC9CE82BAF7D8F72BD9CAA44E9E1E0C31014_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m9B23620DA96F467C71976BA5721A4482EDDE6A8B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_Reset_m594D2E06A810AC4ECF48EF02997D6B304606049A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t5A983B30C1E6EE1D14B1BEF99A89ABD14B90C24B 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Webview
struct Webview_t378970811F03E71B224E384A77472C00869DD7BF  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour Webview::parent
	MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___parent_1;
	// WebviewWindowBase Webview::webViewObject
	WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * ___webViewObject_2;
	// System.Int32 Webview::left
	int32_t ___left_3;
	// System.Int32 Webview::top
	int32_t ___top_4;
	// System.Int32 Webview::right
	int32_t ___right_5;
	// System.Int32 Webview::bottom
	int32_t ___bottom_6;
	// System.Action`1<System.String> Webview::OnWebviewStarted
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnWebviewStarted_7;
	// System.Action`1<System.String> Webview::OnAvatarCreated
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnAvatarCreated_8;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___parent_1)); }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * get_parent_1() const { return ___parent_1; }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_1), (void*)value);
	}

	inline static int32_t get_offset_of_webViewObject_2() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___webViewObject_2)); }
	inline WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * get_webViewObject_2() const { return ___webViewObject_2; }
	inline WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 ** get_address_of_webViewObject_2() { return &___webViewObject_2; }
	inline void set_webViewObject_2(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * value)
	{
		___webViewObject_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webViewObject_2), (void*)value);
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___left_3)); }
	inline int32_t get_left_3() const { return ___left_3; }
	inline int32_t* get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(int32_t value)
	{
		___left_3 = value;
	}

	inline static int32_t get_offset_of_top_4() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___top_4)); }
	inline int32_t get_top_4() const { return ___top_4; }
	inline int32_t* get_address_of_top_4() { return &___top_4; }
	inline void set_top_4(int32_t value)
	{
		___top_4 = value;
	}

	inline static int32_t get_offset_of_right_5() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___right_5)); }
	inline int32_t get_right_5() const { return ___right_5; }
	inline int32_t* get_address_of_right_5() { return &___right_5; }
	inline void set_right_5(int32_t value)
	{
		___right_5 = value;
	}

	inline static int32_t get_offset_of_bottom_6() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___bottom_6)); }
	inline int32_t get_bottom_6() const { return ___bottom_6; }
	inline int32_t* get_address_of_bottom_6() { return &___bottom_6; }
	inline void set_bottom_6(int32_t value)
	{
		___bottom_6 = value;
	}

	inline static int32_t get_offset_of_OnWebviewStarted_7() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___OnWebviewStarted_7)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnWebviewStarted_7() const { return ___OnWebviewStarted_7; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnWebviewStarted_7() { return &___OnWebviewStarted_7; }
	inline void set_OnWebviewStarted_7(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnWebviewStarted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnWebviewStarted_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnAvatarCreated_8() { return static_cast<int32_t>(offsetof(Webview_t378970811F03E71B224E384A77472C00869DD7BF, ___OnAvatarCreated_8)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnAvatarCreated_8() const { return ___OnAvatarCreated_8; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnAvatarCreated_8() { return &___OnAvatarCreated_8; }
	inline void set_OnAvatarCreated_8(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnAvatarCreated_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAvatarCreated_8), (void*)value);
	}
};


// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// Webview/<LoadWebviewURL>d__13
struct U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3  : public RuntimeObject
{
public:
	// System.Int32 Webview/<LoadWebviewURL>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Webview/<LoadWebviewURL>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Webview Webview/<LoadWebviewURL>d__13::<>4__this
	Webview_t378970811F03E71B224E384A77472C00869DD7BF * ___U3CU3E4__this_2;
	// System.String Webview/<LoadWebviewURL>d__13::<destination>5__2
	String_t* ___U3CdestinationU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3, ___U3CU3E4__this_2)); }
	inline Webview_t378970811F03E71B224E384A77472C00869DD7BF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Webview_t378970811F03E71B224E384A77472C00869DD7BF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Webview_t378970811F03E71B224E384A77472C00869DD7BF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdestinationU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3, ___U3CdestinationU3E5__2_3)); }
	inline String_t* get_U3CdestinationU3E5__2_3() const { return ___U3CdestinationU3E5__2_3; }
	inline String_t** get_address_of_U3CdestinationU3E5__2_3() { return &___U3CdestinationU3E5__2_3; }
	inline void set_U3CdestinationU3E5__2_3(String_t* value)
	{
		___U3CdestinationU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdestinationU3E5__2_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_completeCallback_1)); }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_completeCallback_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// ColorMode
struct ColorMode_t84430DA786C5E5047A0CF677DE5454D452C3654F 
{
public:
	// System.Int32 ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_t84430DA786C5E5047A0CF677DE5454D452C3654F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.NetworkReachability
struct NetworkReachability_t8F15310A11943C1C3752844598A796FB8EE2BFBC 
{
public:
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkReachability_t8F15310A11943C1C3752844598A796FB8EE2BFBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UserAuthorization
struct UserAuthorization_tD0093A861C0EF8B1A1B921B19DBD7E7280C8B4DD 
{
public:
	// System.Int32 UnityEngine.UserAuthorization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserAuthorization_tD0093A861C0EF8B1A1B921B19DBD7E7280C8B4DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// WebkitContentMode
struct WebkitContentMode_t0DBB686B74FD0E45C22B9CE6DB38979BF7D12935 
{
public:
	// System.Int32 WebkitContentMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebkitContentMode_t0DBB686B74FD0E45C22B9CE6DB38979BF7D12935, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// WebviewOptions
struct WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75  : public RuntimeObject
{
public:
	// System.Boolean WebviewOptions::Transparent
	bool ___Transparent_0;
	// System.Boolean WebviewOptions::Zoom
	bool ___Zoom_1;
	// System.String WebviewOptions::UA
	String_t* ___UA_2;
	// ColorMode WebviewOptions::AndroidForceDarkMode
	int32_t ___AndroidForceDarkMode_3;
	// System.Boolean WebviewOptions::EnableWKWebView
	bool ___EnableWKWebView_4;
	// WebkitContentMode WebviewOptions::WKContentMode
	int32_t ___WKContentMode_5;

public:
	inline static int32_t get_offset_of_Transparent_0() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___Transparent_0)); }
	inline bool get_Transparent_0() const { return ___Transparent_0; }
	inline bool* get_address_of_Transparent_0() { return &___Transparent_0; }
	inline void set_Transparent_0(bool value)
	{
		___Transparent_0 = value;
	}

	inline static int32_t get_offset_of_Zoom_1() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___Zoom_1)); }
	inline bool get_Zoom_1() const { return ___Zoom_1; }
	inline bool* get_address_of_Zoom_1() { return &___Zoom_1; }
	inline void set_Zoom_1(bool value)
	{
		___Zoom_1 = value;
	}

	inline static int32_t get_offset_of_UA_2() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___UA_2)); }
	inline String_t* get_UA_2() const { return ___UA_2; }
	inline String_t** get_address_of_UA_2() { return &___UA_2; }
	inline void set_UA_2(String_t* value)
	{
		___UA_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UA_2), (void*)value);
	}

	inline static int32_t get_offset_of_AndroidForceDarkMode_3() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___AndroidForceDarkMode_3)); }
	inline int32_t get_AndroidForceDarkMode_3() const { return ___AndroidForceDarkMode_3; }
	inline int32_t* get_address_of_AndroidForceDarkMode_3() { return &___AndroidForceDarkMode_3; }
	inline void set_AndroidForceDarkMode_3(int32_t value)
	{
		___AndroidForceDarkMode_3 = value;
	}

	inline static int32_t get_offset_of_EnableWKWebView_4() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___EnableWKWebView_4)); }
	inline bool get_EnableWKWebView_4() const { return ___EnableWKWebView_4; }
	inline bool* get_address_of_EnableWKWebView_4() { return &___EnableWKWebView_4; }
	inline void set_EnableWKWebView_4(bool value)
	{
		___EnableWKWebView_4 = value;
	}

	inline static int32_t get_offset_of_WKContentMode_5() { return static_cast<int32_t>(offsetof(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75, ___WKContentMode_5)); }
	inline int32_t get_WKContentMode_5() const { return ___WKContentMode_5; }
	inline int32_t* get_address_of_WKContentMode_5() { return &___WKContentMode_5; }
	inline void set_WKContentMode_5(int32_t value)
	{
		___WKContentMode_5 = value;
	}
};


// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// MessageCanvas
struct MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform MessageCanvas::panel
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___panel_4;
	// UnityEngine.UI.Text MessageCanvas::messageLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___messageLabel_5;

public:
	inline static int32_t get_offset_of_panel_4() { return static_cast<int32_t>(offsetof(MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487, ___panel_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_panel_4() const { return ___panel_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_panel_4() { return &___panel_4; }
	inline void set_panel_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___panel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panel_4), (void*)value);
	}

	inline static int32_t get_offset_of_messageLabel_5() { return static_cast<int32_t>(offsetof(MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487, ___messageLabel_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_messageLabel_5() const { return ___messageLabel_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_messageLabel_5() { return &___messageLabel_5; }
	inline void set_messageLabel_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___messageLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___messageLabel_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// WebviewWindowBase
struct WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<System.String> WebviewWindowBase::OnJS
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnJS_4;
	// System.Action`1<System.String> WebviewWindowBase::OnError
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnError_5;
	// System.Action`1<System.String> WebviewWindowBase::OnHttpError
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnHttpError_6;
	// System.Action`1<System.String> WebviewWindowBase::OnStarted
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnStarted_7;
	// System.Action`1<System.String> WebviewWindowBase::OnLoaded
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnLoaded_8;
	// System.Action`1<System.String> WebviewWindowBase::OnHooked
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___OnHooked_9;
	// System.Int32 WebviewWindowBase::marginLeft
	int32_t ___marginLeft_10;
	// System.Int32 WebviewWindowBase::marginTop
	int32_t ___marginTop_11;
	// System.Int32 WebviewWindowBase::marginRight
	int32_t ___marginRight_12;
	// System.Int32 WebviewWindowBase::marginBottom
	int32_t ___marginBottom_13;
	// MessageCanvas WebviewWindowBase::messageCanvas
	MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * ___messageCanvas_14;
	// System.Int32 WebviewWindowBase::windowVisibleDisplayFrameHeight
	int32_t ___windowVisibleDisplayFrameHeight_15;
	// System.Boolean WebviewWindowBase::isVisible
	bool ___isVisible_16;
	// System.Boolean WebviewWindowBase::iskeyboardVisible
	bool ___iskeyboardVisible_17;
	// System.Boolean WebviewWindowBase::alertDialogEnabled
	bool ___alertDialogEnabled_18;
	// System.Boolean WebviewWindowBase::scrollBounceEnabled
	bool ___scrollBounceEnabled_19;

public:
	inline static int32_t get_offset_of_OnJS_4() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnJS_4)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnJS_4() const { return ___OnJS_4; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnJS_4() { return &___OnJS_4; }
	inline void set_OnJS_4(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnJS_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnJS_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnError_5() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnError_5)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnError_5() const { return ___OnError_5; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnError_5() { return &___OnError_5; }
	inline void set_OnError_5(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnError_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnError_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnHttpError_6() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnHttpError_6)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnHttpError_6() const { return ___OnHttpError_6; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnHttpError_6() { return &___OnHttpError_6; }
	inline void set_OnHttpError_6(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnHttpError_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHttpError_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnStarted_7() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnStarted_7)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnStarted_7() const { return ___OnStarted_7; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnStarted_7() { return &___OnStarted_7; }
	inline void set_OnStarted_7(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnStarted_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStarted_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnLoaded_8() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnLoaded_8)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnLoaded_8() const { return ___OnLoaded_8; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnLoaded_8() { return &___OnLoaded_8; }
	inline void set_OnLoaded_8(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnLoaded_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnLoaded_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnHooked_9() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___OnHooked_9)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_OnHooked_9() const { return ___OnHooked_9; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_OnHooked_9() { return &___OnHooked_9; }
	inline void set_OnHooked_9(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___OnHooked_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHooked_9), (void*)value);
	}

	inline static int32_t get_offset_of_marginLeft_10() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___marginLeft_10)); }
	inline int32_t get_marginLeft_10() const { return ___marginLeft_10; }
	inline int32_t* get_address_of_marginLeft_10() { return &___marginLeft_10; }
	inline void set_marginLeft_10(int32_t value)
	{
		___marginLeft_10 = value;
	}

	inline static int32_t get_offset_of_marginTop_11() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___marginTop_11)); }
	inline int32_t get_marginTop_11() const { return ___marginTop_11; }
	inline int32_t* get_address_of_marginTop_11() { return &___marginTop_11; }
	inline void set_marginTop_11(int32_t value)
	{
		___marginTop_11 = value;
	}

	inline static int32_t get_offset_of_marginRight_12() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___marginRight_12)); }
	inline int32_t get_marginRight_12() const { return ___marginRight_12; }
	inline int32_t* get_address_of_marginRight_12() { return &___marginRight_12; }
	inline void set_marginRight_12(int32_t value)
	{
		___marginRight_12 = value;
	}

	inline static int32_t get_offset_of_marginBottom_13() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___marginBottom_13)); }
	inline int32_t get_marginBottom_13() const { return ___marginBottom_13; }
	inline int32_t* get_address_of_marginBottom_13() { return &___marginBottom_13; }
	inline void set_marginBottom_13(int32_t value)
	{
		___marginBottom_13 = value;
	}

	inline static int32_t get_offset_of_messageCanvas_14() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___messageCanvas_14)); }
	inline MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * get_messageCanvas_14() const { return ___messageCanvas_14; }
	inline MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 ** get_address_of_messageCanvas_14() { return &___messageCanvas_14; }
	inline void set_messageCanvas_14(MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * value)
	{
		___messageCanvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___messageCanvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_windowVisibleDisplayFrameHeight_15() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___windowVisibleDisplayFrameHeight_15)); }
	inline int32_t get_windowVisibleDisplayFrameHeight_15() const { return ___windowVisibleDisplayFrameHeight_15; }
	inline int32_t* get_address_of_windowVisibleDisplayFrameHeight_15() { return &___windowVisibleDisplayFrameHeight_15; }
	inline void set_windowVisibleDisplayFrameHeight_15(int32_t value)
	{
		___windowVisibleDisplayFrameHeight_15 = value;
	}

	inline static int32_t get_offset_of_isVisible_16() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___isVisible_16)); }
	inline bool get_isVisible_16() const { return ___isVisible_16; }
	inline bool* get_address_of_isVisible_16() { return &___isVisible_16; }
	inline void set_isVisible_16(bool value)
	{
		___isVisible_16 = value;
	}

	inline static int32_t get_offset_of_iskeyboardVisible_17() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___iskeyboardVisible_17)); }
	inline bool get_iskeyboardVisible_17() const { return ___iskeyboardVisible_17; }
	inline bool* get_address_of_iskeyboardVisible_17() { return &___iskeyboardVisible_17; }
	inline void set_iskeyboardVisible_17(bool value)
	{
		___iskeyboardVisible_17 = value;
	}

	inline static int32_t get_offset_of_alertDialogEnabled_18() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___alertDialogEnabled_18)); }
	inline bool get_alertDialogEnabled_18() const { return ___alertDialogEnabled_18; }
	inline bool* get_address_of_alertDialogEnabled_18() { return &___alertDialogEnabled_18; }
	inline void set_alertDialogEnabled_18(bool value)
	{
		___alertDialogEnabled_18 = value;
	}

	inline static int32_t get_offset_of_scrollBounceEnabled_19() { return static_cast<int32_t>(offsetof(WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4, ___scrollBounceEnabled_19)); }
	inline bool get_scrollBounceEnabled_19() const { return ___scrollBounceEnabled_19; }
	inline bool* get_address_of_scrollBounceEnabled_19() { return &___scrollBounceEnabled_19; }
	inline void set_scrollBounceEnabled_19(bool value)
	{
		___scrollBounceEnabled_19 = value;
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// IOSWebViewWindow
struct IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60  : public WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4
{
public:
	// System.IntPtr IOSWebViewWindow::webView
	intptr_t ___webView_20;

public:
	inline static int32_t get_offset_of_webView_20() { return static_cast<int32_t>(offsetof(IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60, ___webView_20)); }
	inline intptr_t get_webView_20() const { return ___webView_20; }
	inline intptr_t* get_address_of_webView_20() { return &___webView_20; }
	inline void set_webView_20(intptr_t value)
	{
		___webView_20 = value;
	}
};


// NotSupportedWebviewWindow
struct NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B  : public WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4
{
public:

public:
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m83108B6D8808A0E83DE12FD220A87000D19AEE00_gshared (String_t* ___path0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m5E241FA6C20E2C54A2317FEE950354EB3BCE4454_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mF5562A0C81CEDFE1C295F7E16FC6904B5057CB2D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mAAE01A16F138CEC8E1965D322EFB6A7045FE76F2_gshared (Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);

// System.Boolean UnityEngine.Application::HasUserAuthorization(UnityEngine.UserAuthorization)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_HasUserAuthorization_mF2B71D5CC9F37FF5B600D495F9D57F9ABB77ECAC (int32_t ___mode0, const RuntimeMethod* method);
// UnityEngine.AsyncOperation UnityEngine.Application::RequestUserAuthorization(UnityEngine.UserAuthorization)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * Application_RequestUserAuthorization_mDA4EC05A984AC8CE9B7EB2B3038D0F2A124442B6 (int32_t ___mode0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.IntPtr IOSWebViewWindow::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IOSWebViewWindow__CWebViewPlugin_Init_mE1E39EEE1F883789A40087032C4B19384DAB2281 (String_t* ___gameObject0, bool ___transparent1, bool ___zoom2, String_t* ___ua3, bool ___enableWKWebView4, int32_t ___wkContentMode5, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetMargins_m013A342E92A766CEC45FF8880C789EB496543BD6 (intptr_t ___instance0, float ___left1, float ___top2, float ___right3, float ___bottom4, bool ___relative5, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetVisibility_m924F50E9C0DBC75E863D1BC3C605ABDE4C823F73 (intptr_t ___instance0, bool ___visibility1, const RuntimeMethod* method);
// System.Boolean UnityEngine.TouchScreenKeyboard::get_visible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TouchScreenKeyboard_get_visible_m9099838FFE020BEAC716B29F5F47FBC831710549 (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetAlertDialogEnabled_m47FC04E1BA1264D26FE53C9EA43335D4C57061C2 (intptr_t ___instance0, bool ___enabled1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetScrollBounceEnabled_mA523CE41F6B403C327699F835FB21090FA8DF42B (intptr_t ___instance0, bool ___enabled1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_LoadURL_m68F444DCD9141BC5BC30B39BF612361A3C316505 (intptr_t ___instance0, String_t* ___url1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_LoadHTML_mAC443A84AE23301083AEED3B6A9AF6B8294DFE3C (intptr_t ___instance0, String_t* ___html1, String_t* ___baseUrl2, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_EvaluateJS_mA62A6960E972C2AC108925F4D3F2B7C6806FD080 (intptr_t ___instance0, String_t* ___url1, const RuntimeMethod* method);
// System.Int32 IOSWebViewWindow::_CWebViewPlugin_Progress(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IOSWebViewWindow__CWebViewPlugin_Progress_mE0DC8F51E3F9CF631D5FBBDB51991F7A6544933F (intptr_t ___instance0, const RuntimeMethod* method);
// System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow__CWebViewPlugin_CanGoBack_mBC66CBFFD5AD414ACA1617325ED98A88CDC77F99 (intptr_t ___instance0, const RuntimeMethod* method);
// System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow__CWebViewPlugin_CanGoForward_m8649FD8A32D6FEC96E8C51B41301AAC8E3C08CBE (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_GoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_GoBack_m0E37FCED5F98A9DC60E941EB3E8FDC7796D53913 (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_GoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_GoForward_mE00507032C41464180A66CB8412728377A2681E9 (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_Reload(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_Reload_m415DA53D3BFAE3EBFBC83B601A86AC6C1467EBBC (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_AddCustomHeader_m5A5A19B2839F4A1D656C172275A7F1E87B94BFFC (intptr_t ___instance0, String_t* ___headerKey1, String_t* ___headerValue2, const RuntimeMethod* method);
// System.String IOSWebViewWindow::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow__CWebViewPlugin_GetCustomHeaderValue_mC7080B7CABE3A22EFA71AC6E404746EAC0F55DC4 (intptr_t ___instance0, String_t* ___headerKey1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_RemoveCustomHeader_m7C2B62BE9D3745212DF8335D3D2A1D2803205281 (intptr_t ___instance0, String_t* ___headerKey1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCustomHeader_m502CCBA08EC454578A4AD6DFC5C8AA2DFF14EFA3 (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCache_mC4E2792325D2F72F2BE263C530114D65D9EB5ECB (intptr_t ___instance0, bool ___includeDiskFiles1, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCookies_m970260FEB82B5D6B3ED3E5777D10B6C5BAD1B9E7 (const RuntimeMethod* method);
// System.String IOSWebViewWindow::_CWebViewPlugin_GetCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow__CWebViewPlugin_GetCookies_mAA0AE2F40B6B5498B8373F2FE2E5D8371350CC1A (String_t* ___url0, const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SaveCookies_mC9FBE134044F4D586E498E53B514FD6F865AE1E8 (const RuntimeMethod* method);
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetBasicAuthInfo_m5AA21D80878C30E7DA1FC8F25827E7D2191AB2B6 (intptr_t ___instance0, String_t* ___userName1, String_t* ___password2, const RuntimeMethod* method);
// System.Int32 IOSWebViewWindow::_CWebViewPlugin_Destroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IOSWebViewWindow__CWebViewPlugin_Destroy_m4186EDF719B0EACDF8B94F6D76467478B02FB3E6 (intptr_t ___instance0, const RuntimeMethod* method);
// System.Void WebviewWindowBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_offsetMax_m5FDE1063C8BA1EC98D3C57C58DD2A1B9B721A8BF (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_offsetMin_m86D7818770137C150B70A3842EBF03F494C34271 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_internetReachability_m039C30126BD989614BF2A4A3F33129177A95C61C (const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<MessageCanvas>(System.String)
inline MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * Resources_Load_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m9B23620DA96F467C71976BA5721A4482EDDE6A8B (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m83108B6D8808A0E83DE12FD220A87000D19AEE00_gshared)(___path0, method);
}
// !!0 UnityEngine.Object::Instantiate<MessageCanvas>(!!0)
inline MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * Object_Instantiate_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m0608DC9CE82BAF7D8F72BD9CAA44E9E1E0C31014 (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * ___original0, const RuntimeMethod* method)
{
	return ((  MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * (*) (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m5E241FA6C20E2C54A2317FEE950354EB3BCE4454_gshared)(___original0, method);
}
// System.Void MessageCanvas::SetMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageCanvas_SetMessage_mD62FB9E2C290DEF1513B72DDC41B40A26936F1AB (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void MessageCanvas::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageCanvas_SetMargins_m3CCCF18F11D17E1E76710B4643235D692BC395ED (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Boolean Webview::SetWebviewWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Webview_SetWebviewWindow_mFD05AC5E0E42860E1CA1DBF592235C3BD792A4D9 (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator Webview::LoadWebviewURL()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Webview_LoadWebviewURL_m2B9887C6D9EEA5A60E27A4E80B4CD5EB6F9883EB (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void Webview::SetScreenPadding(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_SetScreenPadding_m91D75A884E3768B63556B44FD3E2CE00581ECB88 (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Void WebviewOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewOptions__ctor_m37474B5636586B2F666B231344AAD3883F75270C (WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<IOSWebViewWindow>()
inline IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * GameObject_AddComponent_TisIOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60_m22578968B2E08B2BA698515F87466AFCC2EBD326 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mF5562A0C81CEDFE1C295F7E16FC6904B5057CB2D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<NotSupportedWebviewWindow>()
inline NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * GameObject_AddComponent_TisNotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B_mF93551E8500D15D32FEAE9B324FFF994ACC3F118 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mF5562A0C81CEDFE1C295F7E16FC6904B5057CB2D_gshared)(__this, method);
}
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161 (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mA671E933C9D3DAE4E3F71D34FDDA971739618158_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Webview/<LoadWebviewURL>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadWebviewURLU3Ed__13__ctor_m6F09FD7D1B3D0B3BEDA8BF2727183DE6C0EEFF03 (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::Invoke(!0)
inline void Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1 (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * __this, String_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *, String_t*, const RuntimeMethod*))Action_1_Invoke_mAAE01A16F138CEC8E1965D322EFB6A7045FE76F2_gshared)(__this, ___obj0, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_streamingAssetsPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1 (const RuntimeMethod* method);
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF (String_t* ___path10, String_t* ___path21, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_persistentDataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463 (const RuntimeMethod* method);
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* File_ReadAllBytes_mFB47FB50E938AE90CC822442D30E896441D95829 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.IO.File::WriteAllBytes(System.String,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_WriteAllBytes_m1E88860F73A6A2150FAB97D9BF3F44596F06036F (String_t* ___path0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL _CWebViewPlugin_Init(char*, int32_t, int32_t, char*, int32_t, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_Destroy(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetMargins(intptr_t, float, float, float, float, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetVisibility(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetAlertDialogEnabled(intptr_t, int32_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetScrollBounceEnabled(intptr_t, int32_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_SetURLPattern(intptr_t, char*, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_LoadURL(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_LoadHTML(intptr_t, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_EvaluateJS(intptr_t, char*);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_Progress(intptr_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_CanGoBack(intptr_t);
IL2CPP_EXTERN_C int32_t DEFAULT_CALL _CWebViewPlugin_CanGoForward(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_GoBack(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_GoForward(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_Reload(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_AddCustomHeader(intptr_t, char*, char*);
IL2CPP_EXTERN_C char* DEFAULT_CALL _CWebViewPlugin_GetCustomHeaderValue(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_RemoveCustomHeader(intptr_t, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCustomHeader(intptr_t);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCookies();
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SaveCookies();
IL2CPP_EXTERN_C char* DEFAULT_CALL _CWebViewPlugin_GetCookies(char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_SetBasicAuthInfo(intptr_t, char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL _CWebViewPlugin_ClearCache(intptr_t, int32_t);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr IOSWebViewWindow::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IOSWebViewWindow__CWebViewPlugin_Init_mE1E39EEE1F883789A40087032C4B19384DAB2281 (String_t* ___gameObject0, bool ___transparent1, bool ___zoom2, String_t* ___ua3, bool ___enableWKWebView4, int32_t ___wkContentMode5, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, char*, int32_t, int32_t);

	// Marshaling of parameter '___gameObject0' to native representation
	char* ____gameObject0_marshaled = NULL;
	____gameObject0_marshaled = il2cpp_codegen_marshal_string(___gameObject0);

	// Marshaling of parameter '___ua3' to native representation
	char* ____ua3_marshaled = NULL;
	____ua3_marshaled = il2cpp_codegen_marshal_string(___ua3);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Init)(____gameObject0_marshaled, static_cast<int32_t>(___transparent1), static_cast<int32_t>(___zoom2), ____ua3_marshaled, static_cast<int32_t>(___enableWKWebView4), ___wkContentMode5);

	// Marshaling cleanup of parameter '___gameObject0' native representation
	il2cpp_codegen_marshal_free(____gameObject0_marshaled);
	____gameObject0_marshaled = NULL;

	// Marshaling cleanup of parameter '___ua3' native representation
	il2cpp_codegen_marshal_free(____ua3_marshaled);
	____ua3_marshaled = NULL;

	return returnValue;
}
// System.Int32 IOSWebViewWindow::_CWebViewPlugin_Destroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IOSWebViewWindow__CWebViewPlugin_Destroy_m4186EDF719B0EACDF8B94F6D76467478B02FB3E6 (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Destroy)(___instance0);

	return returnValue;
}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetMargins_m013A342E92A766CEC45FF8880C789EB496543BD6 (intptr_t ___instance0, float ___left1, float ___top2, float ___right3, float ___bottom4, bool ___relative5, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float, float, float, float, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetMargins)(___instance0, ___left1, ___top2, ___right3, ___bottom4, static_cast<int32_t>(___relative5));

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetVisibility_m924F50E9C0DBC75E863D1BC3C605ABDE4C823F73 (intptr_t ___instance0, bool ___visibility1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetVisibility)(___instance0, static_cast<int32_t>(___visibility1));

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetAlertDialogEnabled_m47FC04E1BA1264D26FE53C9EA43335D4C57061C2 (intptr_t ___instance0, bool ___enabled1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetAlertDialogEnabled)(___instance0, static_cast<int32_t>(___enabled1));

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetScrollBounceEnabled_mA523CE41F6B403C327699F835FB21090FA8DF42B (intptr_t ___instance0, bool ___enabled1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetScrollBounceEnabled)(___instance0, static_cast<int32_t>(___enabled1));

}
// System.Boolean IOSWebViewWindow::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow__CWebViewPlugin_SetURLPattern_m9C7C68B4BE05F3151A1945739D4328AE83FD570C (intptr_t ___instance0, String_t* ___allowPattern1, String_t* ___denyPattern2, String_t* ___hookPattern3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, char*);

	// Marshaling of parameter '___allowPattern1' to native representation
	char* ____allowPattern1_marshaled = NULL;
	____allowPattern1_marshaled = il2cpp_codegen_marshal_string(___allowPattern1);

	// Marshaling of parameter '___denyPattern2' to native representation
	char* ____denyPattern2_marshaled = NULL;
	____denyPattern2_marshaled = il2cpp_codegen_marshal_string(___denyPattern2);

	// Marshaling of parameter '___hookPattern3' to native representation
	char* ____hookPattern3_marshaled = NULL;
	____hookPattern3_marshaled = il2cpp_codegen_marshal_string(___hookPattern3);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetURLPattern)(___instance0, ____allowPattern1_marshaled, ____denyPattern2_marshaled, ____hookPattern3_marshaled);

	// Marshaling cleanup of parameter '___allowPattern1' native representation
	il2cpp_codegen_marshal_free(____allowPattern1_marshaled);
	____allowPattern1_marshaled = NULL;

	// Marshaling cleanup of parameter '___denyPattern2' native representation
	il2cpp_codegen_marshal_free(____denyPattern2_marshaled);
	____denyPattern2_marshaled = NULL;

	// Marshaling cleanup of parameter '___hookPattern3' native representation
	il2cpp_codegen_marshal_free(____hookPattern3_marshaled);
	____hookPattern3_marshaled = NULL;

	return static_cast<bool>(returnValue);
}
// System.Void IOSWebViewWindow::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_LoadURL_m68F444DCD9141BC5BC30B39BF612361A3C316505 (intptr_t ___instance0, String_t* ___url1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___url1' to native representation
	char* ____url1_marshaled = NULL;
	____url1_marshaled = il2cpp_codegen_marshal_string(___url1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_LoadURL)(___instance0, ____url1_marshaled);

	// Marshaling cleanup of parameter '___url1' native representation
	il2cpp_codegen_marshal_free(____url1_marshaled);
	____url1_marshaled = NULL;

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_LoadHTML_mAC443A84AE23301083AEED3B6A9AF6B8294DFE3C (intptr_t ___instance0, String_t* ___html1, String_t* ___baseUrl2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___html1' to native representation
	char* ____html1_marshaled = NULL;
	____html1_marshaled = il2cpp_codegen_marshal_string(___html1);

	// Marshaling of parameter '___baseUrl2' to native representation
	char* ____baseUrl2_marshaled = NULL;
	____baseUrl2_marshaled = il2cpp_codegen_marshal_string(___baseUrl2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_LoadHTML)(___instance0, ____html1_marshaled, ____baseUrl2_marshaled);

	// Marshaling cleanup of parameter '___html1' native representation
	il2cpp_codegen_marshal_free(____html1_marshaled);
	____html1_marshaled = NULL;

	// Marshaling cleanup of parameter '___baseUrl2' native representation
	il2cpp_codegen_marshal_free(____baseUrl2_marshaled);
	____baseUrl2_marshaled = NULL;

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_EvaluateJS_mA62A6960E972C2AC108925F4D3F2B7C6806FD080 (intptr_t ___instance0, String_t* ___url1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___url1' to native representation
	char* ____url1_marshaled = NULL;
	____url1_marshaled = il2cpp_codegen_marshal_string(___url1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_EvaluateJS)(___instance0, ____url1_marshaled);

	// Marshaling cleanup of parameter '___url1' native representation
	il2cpp_codegen_marshal_free(____url1_marshaled);
	____url1_marshaled = NULL;

}
// System.Int32 IOSWebViewWindow::_CWebViewPlugin_Progress(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IOSWebViewWindow__CWebViewPlugin_Progress_mE0DC8F51E3F9CF631D5FBBDB51991F7A6544933F (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Progress)(___instance0);

	return returnValue;
}
// System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow__CWebViewPlugin_CanGoBack_mBC66CBFFD5AD414ACA1617325ED98A88CDC77F99 (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_CanGoBack)(___instance0);

	return static_cast<bool>(returnValue);
}
// System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow__CWebViewPlugin_CanGoForward_m8649FD8A32D6FEC96E8C51B41301AAC8E3C08CBE (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_CanGoForward)(___instance0);

	return static_cast<bool>(returnValue);
}
// System.Void IOSWebViewWindow::_CWebViewPlugin_GoBack(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_GoBack_m0E37FCED5F98A9DC60E941EB3E8FDC7796D53913 (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GoBack)(___instance0);

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_GoForward(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_GoForward_mE00507032C41464180A66CB8412728377A2681E9 (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GoForward)(___instance0);

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_Reload(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_Reload_m415DA53D3BFAE3EBFBC83B601A86AC6C1467EBBC (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_Reload)(___instance0);

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_AddCustomHeader_m5A5A19B2839F4A1D656C172275A7F1E87B94BFFC (intptr_t ___instance0, String_t* ___headerKey1, String_t* ___headerValue2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___headerKey1' to native representation
	char* ____headerKey1_marshaled = NULL;
	____headerKey1_marshaled = il2cpp_codegen_marshal_string(___headerKey1);

	// Marshaling of parameter '___headerValue2' to native representation
	char* ____headerValue2_marshaled = NULL;
	____headerValue2_marshaled = il2cpp_codegen_marshal_string(___headerValue2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_AddCustomHeader)(___instance0, ____headerKey1_marshaled, ____headerValue2_marshaled);

	// Marshaling cleanup of parameter '___headerKey1' native representation
	il2cpp_codegen_marshal_free(____headerKey1_marshaled);
	____headerKey1_marshaled = NULL;

	// Marshaling cleanup of parameter '___headerValue2' native representation
	il2cpp_codegen_marshal_free(____headerValue2_marshaled);
	____headerValue2_marshaled = NULL;

}
// System.String IOSWebViewWindow::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow__CWebViewPlugin_GetCustomHeaderValue_mC7080B7CABE3A22EFA71AC6E404746EAC0F55DC4 (intptr_t ___instance0, String_t* ___headerKey1, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___headerKey1' to native representation
	char* ____headerKey1_marshaled = NULL;
	____headerKey1_marshaled = il2cpp_codegen_marshal_string(___headerKey1);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GetCustomHeaderValue)(___instance0, ____headerKey1_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___headerKey1' native representation
	il2cpp_codegen_marshal_free(____headerKey1_marshaled);
	____headerKey1_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void IOSWebViewWindow::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_RemoveCustomHeader_m7C2B62BE9D3745212DF8335D3D2A1D2803205281 (intptr_t ___instance0, String_t* ___headerKey1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___headerKey1' to native representation
	char* ____headerKey1_marshaled = NULL;
	____headerKey1_marshaled = il2cpp_codegen_marshal_string(___headerKey1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_RemoveCustomHeader)(___instance0, ____headerKey1_marshaled);

	// Marshaling cleanup of parameter '___headerKey1' native representation
	il2cpp_codegen_marshal_free(____headerKey1_marshaled);
	____headerKey1_marshaled = NULL;

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCustomHeader_m502CCBA08EC454578A4AD6DFC5C8AA2DFF14EFA3 (intptr_t ___instance0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCustomHeader)(___instance0);

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCookies_m970260FEB82B5D6B3ED3E5777D10B6C5BAD1B9E7 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCookies)();

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SaveCookies_mC9FBE134044F4D586E498E53B514FD6F865AE1E8 (const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SaveCookies)();

}
// System.String IOSWebViewWindow::_CWebViewPlugin_GetCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow__CWebViewPlugin_GetCookies_mAA0AE2F40B6B5498B8373F2FE2E5D8371350CC1A (String_t* ___url0, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___url0' to native representation
	char* ____url0_marshaled = NULL;
	____url0_marshaled = il2cpp_codegen_marshal_string(___url0);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_GetCookies)(____url0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___url0' native representation
	il2cpp_codegen_marshal_free(____url0_marshaled);
	____url0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void IOSWebViewWindow::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_SetBasicAuthInfo_m5AA21D80878C30E7DA1FC8F25827E7D2191AB2B6 (intptr_t ___instance0, String_t* ___userName1, String_t* ___password2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___userName1' to native representation
	char* ____userName1_marshaled = NULL;
	____userName1_marshaled = il2cpp_codegen_marshal_string(___userName1);

	// Marshaling of parameter '___password2' to native representation
	char* ____password2_marshaled = NULL;
	____password2_marshaled = il2cpp_codegen_marshal_string(___password2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_SetBasicAuthInfo)(___instance0, ____userName1_marshaled, ____password2_marshaled);

	// Marshaling cleanup of parameter '___userName1' native representation
	il2cpp_codegen_marshal_free(____userName1_marshaled);
	____userName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___password2' native representation
	il2cpp_codegen_marshal_free(____password2_marshaled);
	____password2_marshaled = NULL;

}
// System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__CWebViewPlugin_ClearCache_mC4E2792325D2F72F2BE263C530114D65D9EB5ECB (intptr_t ___instance0, bool ___includeDiskFiles1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CWebViewPlugin_ClearCache)(___instance0, static_cast<int32_t>(___includeDiskFiles1));

}
// System.Void IOSWebViewWindow::Init(WebviewOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_Init_mCD581AE993D211CD8D460AAD73B90D6F35A923B3 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * ___options0, const RuntimeMethod* method)
{
	{
		// if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
		bool L_0;
		L_0 = Application_HasUserAuthorization_mF2B71D5CC9F37FF5B600D495F9D57F9ABB77ECAC(1, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// Application.RequestUserAuthorization(UserAuthorization.WebCam);
		AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * L_1;
		L_1 = Application_RequestUserAuthorization_mDA4EC05A984AC8CE9B7EB2B3038D0F2A124442B6(1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		// webView = _CWebViewPlugin_Init(name, options.Transparent, options.Zoom, options.UA, options.EnableWKWebView, (int)options.WKContentMode);
		String_t* L_2;
		L_2 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(__this, /*hidden argument*/NULL);
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_3 = ___options0;
		NullCheck(L_3);
		bool L_4 = L_3->get_Transparent_0();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_5 = ___options0;
		NullCheck(L_5);
		bool L_6 = L_5->get_Zoom_1();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_7 = ___options0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_UA_2();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_9 = ___options0;
		NullCheck(L_9);
		bool L_10 = L_9->get_EnableWKWebView_4();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_11 = ___options0;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_WKContentMode_5();
		intptr_t L_13;
		L_13 = IOSWebViewWindow__CWebViewPlugin_Init_mE1E39EEE1F883789A40087032C4B19384DAB2281(L_2, L_4, L_6, L_8, L_10, L_12, /*hidden argument*/NULL);
		__this->set_webView_20((intptr_t)L_13);
		// }
		return;
	}
}
// System.Void IOSWebViewWindow::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_SetMargins_m2D09A6CD9A361539B3D42F16E90543BE60EFA9BE (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method)
{
	{
		// _CWebViewPlugin_SetMargins(webView, left, top, right, bottom, false);
		intptr_t L_0 = __this->get_webView_20();
		int32_t L_1 = ___left0;
		int32_t L_2 = ___top1;
		int32_t L_3 = ___right2;
		int32_t L_4 = ___bottom3;
		IOSWebViewWindow__CWebViewPlugin_SetMargins_m013A342E92A766CEC45FF8880C789EB496543BD6((intptr_t)L_0, ((float)((float)L_1)), ((float)((float)L_2)), ((float)((float)L_3)), ((float)((float)L_4)), (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean IOSWebViewWindow::get_IsVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_get_IsVisible_mFA30DD1B5EC50C8398D2B84E4547433B80E683BD (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// return isVisible;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_isVisible_16();
		return L_0;
	}
}
// System.Void IOSWebViewWindow::set_IsVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_set_IsVisible_mDD59F0E928521D6E4C8B4995432E03970577621F (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// isVisible = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_isVisible_16(L_0);
		// _CWebViewPlugin_SetVisibility(webView, value);
		intptr_t L_1 = __this->get_webView_20();
		bool L_2 = ___value0;
		IOSWebViewWindow__CWebViewPlugin_SetVisibility_m924F50E9C0DBC75E863D1BC3C605ABDE4C823F73((intptr_t)L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean IOSWebViewWindow::get_IsKeyboardVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_get_IsKeyboardVisible_m0548E5139A1ECDC7803E5B5EA74A83D530D507AC (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// return  TouchScreenKeyboard.visible;
		bool L_0;
		L_0 = TouchScreenKeyboard_get_visible_m9099838FFE020BEAC716B29F5F47FBC831710549(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void IOSWebViewWindow::set_IsKeyboardVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_set_IsKeyboardVisible_m95F9A5A84319EA5CB06F1AEED5D2DF686FD82339 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75D50314061E23E4B56389DAABD2C476AC3ECAE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.LogWarning("Overwritten by [TouchScreenKeyboard.visible]");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7(_stringLiteral75D50314061E23E4B56389DAABD2C476AC3ECAE1, /*hidden argument*/NULL);
		// iskeyboardVisible = TouchScreenKeyboard.visible;
		bool L_0;
		L_0 = TouchScreenKeyboard_get_visible_m9099838FFE020BEAC716B29F5F47FBC831710549(/*hidden argument*/NULL);
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_iskeyboardVisible_17(L_0);
		// SetMargins(marginLeft, marginTop, marginRight, marginBottom);
		int32_t L_1 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginLeft_10();
		int32_t L_2 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginTop_11();
		int32_t L_3 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginRight_12();
		int32_t L_4 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginBottom_13();
		VirtualActionInvoker4< int32_t, int32_t, int32_t, int32_t >::Invoke(13 /* System.Void WebviewWindowBase::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32) */, __this, L_1, L_2, L_3, L_4);
		// }
		return;
	}
}
// System.Boolean IOSWebViewWindow::get_AlertDialogEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_get_AlertDialogEnabled_m7DF42FDAC56B43E948D22C209E4DE530E93AC2B5 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// return alertDialogEnabled;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_alertDialogEnabled_18();
		return L_0;
	}
}
// System.Void IOSWebViewWindow::set_AlertDialogEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_set_AlertDialogEnabled_m5059C70637C0123529E29CBE0B14247F2E235FC3 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// alertDialogEnabled = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_alertDialogEnabled_18(L_0);
		// _CWebViewPlugin_SetAlertDialogEnabled(webView, value);
		intptr_t L_1 = __this->get_webView_20();
		bool L_2 = ___value0;
		IOSWebViewWindow__CWebViewPlugin_SetAlertDialogEnabled_m47FC04E1BA1264D26FE53C9EA43335D4C57061C2((intptr_t)L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean IOSWebViewWindow::get_ScrollBounceEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_get_ScrollBounceEnabled_m43F24B2ADE3FD07ABC54A3F4A415A8A28EA1BCC3 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// return scrollBounceEnabled;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_scrollBounceEnabled_19();
		return L_0;
	}
}
// System.Void IOSWebViewWindow::set_ScrollBounceEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_set_ScrollBounceEnabled_m416820B44ACF05289D93285C80FEAFECAB81F9E6 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// scrollBounceEnabled = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_scrollBounceEnabled_19(L_0);
		// _CWebViewPlugin_SetScrollBounceEnabled(webView, value);
		intptr_t L_1 = __this->get_webView_20();
		bool L_2 = ___value0;
		IOSWebViewWindow__CWebViewPlugin_SetScrollBounceEnabled_mA523CE41F6B403C327699F835FB21090FA8DF42B((intptr_t)L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void IOSWebViewWindow::LoadURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_LoadURL_m995D6A3043ED0453223F56A90CC6347C0ABF6CC1 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		// _CWebViewPlugin_LoadURL(webView, url);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___url0;
		IOSWebViewWindow__CWebViewPlugin_LoadURL_m68F444DCD9141BC5BC30B39BF612361A3C316505((intptr_t)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void IOSWebViewWindow::LoadHTML(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_LoadHTML_m62EAB80A3C07D23504B219BEB884807B21D7916A (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___html0, String_t* ___baseUrl1, const RuntimeMethod* method)
{
	{
		// _CWebViewPlugin_LoadHTML(webView, html, baseUrl);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___html0;
		String_t* L_2 = ___baseUrl1;
		IOSWebViewWindow__CWebViewPlugin_LoadHTML_mAC443A84AE23301083AEED3B6A9AF6B8294DFE3C((intptr_t)L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void IOSWebViewWindow::EvaluateJS(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_EvaluateJS_m5D1916FD9D51EA349F98FC4D52D75AA3F5B57252 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___js0, const RuntimeMethod* method)
{
	{
		// _CWebViewPlugin_EvaluateJS(webView, js);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___js0;
		IOSWebViewWindow__CWebViewPlugin_EvaluateJS_mA62A6960E972C2AC108925F4D3F2B7C6806FD080((intptr_t)L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Int32 IOSWebViewWindow::get_Progress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IOSWebViewWindow_get_Progress_mD220D10E4827DDEFABBF0172ACB0742C009CD727 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override int Progress => _CWebViewPlugin_Progress(webView);
		intptr_t L_0 = __this->get_webView_20();
		int32_t L_1;
		L_1 = IOSWebViewWindow__CWebViewPlugin_Progress_mE0DC8F51E3F9CF631D5FBBDB51991F7A6544933F((intptr_t)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean IOSWebViewWindow::CanGoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_CanGoBack_m03BC351C2C0F81E413CD6F68DCF3C32078F06431 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override bool CanGoBack() => _CWebViewPlugin_CanGoBack(webView);
		intptr_t L_0 = __this->get_webView_20();
		bool L_1;
		L_1 = IOSWebViewWindow__CWebViewPlugin_CanGoBack_mBC66CBFFD5AD414ACA1617325ED98A88CDC77F99((intptr_t)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean IOSWebViewWindow::CanGoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IOSWebViewWindow_CanGoForward_mD91F1713B3559CEFB97913338D2072D20CD2FDF3 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override bool CanGoForward() => _CWebViewPlugin_CanGoForward(webView);
		intptr_t L_0 = __this->get_webView_20();
		bool L_1;
		L_1 = IOSWebViewWindow__CWebViewPlugin_CanGoForward_m8649FD8A32D6FEC96E8C51B41301AAC8E3C08CBE((intptr_t)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void IOSWebViewWindow::GoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_GoBack_mEF56ADD66A3FAA662BA23957425117E376756454 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void GoBack() => _CWebViewPlugin_GoBack(webView);
		intptr_t L_0 = __this->get_webView_20();
		IOSWebViewWindow__CWebViewPlugin_GoBack_m0E37FCED5F98A9DC60E941EB3E8FDC7796D53913((intptr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::GoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_GoForward_mBC90420F582903FFD7FD7CDD4BB2BC8B7182DD0B (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void GoForward() => _CWebViewPlugin_GoForward(webView);
		intptr_t L_0 = __this->get_webView_20();
		IOSWebViewWindow__CWebViewPlugin_GoForward_mE00507032C41464180A66CB8412728377A2681E9((intptr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::Reload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_Reload_mA5FC640D3CAEA08A1EDF5E9B350CEC06105F283F (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void Reload() => _CWebViewPlugin_Reload(webView);
		intptr_t L_0 = __this->get_webView_20();
		IOSWebViewWindow__CWebViewPlugin_Reload_m415DA53D3BFAE3EBFBC83B601A86AC6C1467EBBC((intptr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::AddCustomHeader(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_AddCustomHeader_m7F6993669F278A6E42618EBA9598FD2F10AE6518 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	{
		// public override void AddCustomHeader(string key, string value) => _CWebViewPlugin_AddCustomHeader(webView, key, value);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___key0;
		String_t* L_2 = ___value1;
		IOSWebViewWindow__CWebViewPlugin_AddCustomHeader_m5A5A19B2839F4A1D656C172275A7F1E87B94BFFC((intptr_t)L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String IOSWebViewWindow::GetCustomHeaderValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow_GetCustomHeaderValue_m70B535FB8AABFE81E7C149510030DCE904A96C34 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		// public override string GetCustomHeaderValue(string key) => _CWebViewPlugin_GetCustomHeaderValue(webView, key);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___key0;
		String_t* L_2;
		L_2 = IOSWebViewWindow__CWebViewPlugin_GetCustomHeaderValue_mC7080B7CABE3A22EFA71AC6E404746EAC0F55DC4((intptr_t)L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void IOSWebViewWindow::RemoveCustomHeader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_RemoveCustomHeader_mF3CF36589A6AC65E95285BBF902EBFCDDD4A2E09 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	{
		// public override void RemoveCustomHeader(string key) => _CWebViewPlugin_RemoveCustomHeader(webView, key);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___key0;
		IOSWebViewWindow__CWebViewPlugin_RemoveCustomHeader_m7C2B62BE9D3745212DF8335D3D2A1D2803205281((intptr_t)L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::ClearCustomHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_ClearCustomHeader_m60000FD500B764AEF2AA2810CDC117F0F3697AAA (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void ClearCustomHeader() => _CWebViewPlugin_ClearCustomHeader(webView);
		intptr_t L_0 = __this->get_webView_20();
		IOSWebViewWindow__CWebViewPlugin_ClearCustomHeader_m502CCBA08EC454578A4AD6DFC5C8AA2DFF14EFA3((intptr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::ClearCache(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_ClearCache_m901CFE7C80D47ED9F2A0E319A3E9759E7085DD6A (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, bool ___includeDiskFiles0, const RuntimeMethod* method)
{
	{
		// public override void ClearCache(bool includeDiskFiles) => _CWebViewPlugin_ClearCache(webView, includeDiskFiles);
		intptr_t L_0 = __this->get_webView_20();
		bool L_1 = ___includeDiskFiles0;
		IOSWebViewWindow__CWebViewPlugin_ClearCache_mC4E2792325D2F72F2BE263C530114D65D9EB5ECB((intptr_t)L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_ClearCookies_mC9E3E19377773037D974F092B8C97A9BD7C2352C (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void ClearCookies() => _CWebViewPlugin_ClearCookies();
		IOSWebViewWindow__CWebViewPlugin_ClearCookies_m970260FEB82B5D6B3ED3E5777D10B6C5BAD1B9E7(/*hidden argument*/NULL);
		return;
	}
}
// System.String IOSWebViewWindow::GetCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* IOSWebViewWindow_GetCookies_m5686C288DF256B6904E2643818DB8D81C023FFC7 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		// public override string GetCookies(string url) => _CWebViewPlugin_GetCookies(url);
		String_t* L_0 = ___url0;
		String_t* L_1;
		L_1 = IOSWebViewWindow__CWebViewPlugin_GetCookies_mAA0AE2F40B6B5498B8373F2FE2E5D8371350CC1A(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void IOSWebViewWindow::SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_SaveCookies_mF8467E8D2868AF0A473B5E3F22B3261D0F49AB4F (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		// public override void SaveCookies() => _CWebViewPlugin_SaveCookies();
		IOSWebViewWindow__CWebViewPlugin_SaveCookies_mC9FBE134044F4D586E498E53B514FD6F865AE1E8(/*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::SetBasicAuthInfo(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_SetBasicAuthInfo_mE2D13FBFA570498521EB6EAECDB7891FEA441996 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, String_t* ___userName0, String_t* ___password1, const RuntimeMethod* method)
{
	{
		// public override void SetBasicAuthInfo(string userName, string password) => _CWebViewPlugin_SetBasicAuthInfo(webView, userName, password);
		intptr_t L_0 = __this->get_webView_20();
		String_t* L_1 = ___userName0;
		String_t* L_2 = ___password1;
		IOSWebViewWindow__CWebViewPlugin_SetBasicAuthInfo_m5AA21D80878C30E7DA1FC8F25827E7D2191AB2B6((intptr_t)L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IOSWebViewWindow::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow_OnDestroy_mB747097FA32B03E96B790FC677512701A4E56077 (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _CWebViewPlugin_Destroy(webView);
		intptr_t L_0 = __this->get_webView_20();
		int32_t L_1;
		L_1 = IOSWebViewWindow__CWebViewPlugin_Destroy_m4186EDF719B0EACDF8B94F6D76467478B02FB3E6((intptr_t)L_0, /*hidden argument*/NULL);
		// webView = IntPtr.Zero;
		__this->set_webView_20((intptr_t)(0));
		// }
		return;
	}
}
// System.Void IOSWebViewWindow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOSWebViewWindow__ctor_m5C11C8EDBE14F137D9A4FDF618B1E80D8028BC2A (IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * __this, const RuntimeMethod* method)
{
	{
		WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MessageCanvas::SetMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageCanvas_SetMessage_mD62FB9E2C290DEF1513B72DDC41B40A26936F1AB (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		// messageLabel.text = message;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_messageLabel_5();
		String_t* L_1 = ___message0;
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		// }
		return;
	}
}
// System.Void MessageCanvas::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageCanvas_SetMargins_m3CCCF18F11D17E1E76710B4643235D692BC395ED (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method)
{
	{
		// panel.offsetMax = new Vector2(-right, -top);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_panel_4();
		int32_t L_1 = ___right2;
		int32_t L_2 = ___top1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_3), ((float)((float)((-L_1)))), ((float)((float)((-L_2)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_offsetMax_m5FDE1063C8BA1EC98D3C57C58DD2A1B9B721A8BF(L_0, L_3, /*hidden argument*/NULL);
		// panel.offsetMin = new Vector2(left, bottom);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4 = __this->get_panel_4();
		int32_t L_5 = ___left0;
		int32_t L_6 = ___bottom3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_7), ((float)((float)L_5)), ((float)((float)L_6)), /*hidden argument*/NULL);
		NullCheck(L_4);
		RectTransform_set_offsetMin_m86D7818770137C150B70A3842EBF03F494C34271(L_4, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MessageCanvas::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageCanvas__ctor_m7CFC0474D013EC20F1CF4BFA204AAAEB55117A9C (MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean NotSupportedWebviewWindow::get_IsWebViewAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_get_IsWebViewAvailable_m35630CA78767025DDFA7EDA7E799F10AC7D5D482 (const RuntimeMethod* method)
{
	{
		// public static bool IsWebViewAvailable => false;
		return (bool)0;
	}
}
// System.Void NotSupportedWebviewWindow::Init(WebviewOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_Init_m8546014FDC1A4BC18866FF4071DFBDE34F4BBF5F (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * ___options0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m0608DC9CE82BAF7D8F72BD9CAA44E9E1E0C31014_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m9B23620DA96F467C71976BA5721A4482EDDE6A8B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral022FA5D881928CD213562A8ADFD309C0291B1DC1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5F4C20AD460EC51BE658DED3BFDD1A21D566B162);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC757C1AF462C455DA292F07F8DC5D584F694ACF);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		// bool hasNetwork = Application.internetReachability != NetworkReachability.NotReachable;
		int32_t L_0;
		L_0 = Application_get_internetReachability_m039C30126BD989614BF2A4A3F33129177A95C61C(/*hidden argument*/NULL);
		// string message = hasNetwork ? "Current platform\ndoes not support Webview." : "Network is not reachable.";
		if (((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0))
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = _stringLiteralCC757C1AF462C455DA292F07F8DC5D584F694ACF;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral022FA5D881928CD213562A8ADFD309C0291B1DC1;
	}

IL_0016:
	{
		V_0 = G_B3_0;
		// MessageCanvas canvasPrefab = Resources.Load<MessageCanvas>("NotSupportedCanvas");
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_1;
		L_1 = Resources_Load_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m9B23620DA96F467C71976BA5721A4482EDDE6A8B(_stringLiteral5F4C20AD460EC51BE658DED3BFDD1A21D566B162, /*hidden argument*/Resources_Load_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m9B23620DA96F467C71976BA5721A4482EDDE6A8B_RuntimeMethod_var);
		V_1 = L_1;
		// messageCanvas = Instantiate(canvasPrefab);
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_3;
		L_3 = Object_Instantiate_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m0608DC9CE82BAF7D8F72BD9CAA44E9E1E0C31014(L_2, /*hidden argument*/Object_Instantiate_TisMessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487_m0608DC9CE82BAF7D8F72BD9CAA44E9E1E0C31014_RuntimeMethod_var);
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_messageCanvas_14(L_3);
		// messageCanvas.SetMessage(message);
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_4 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_messageCanvas_14();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		MessageCanvas_SetMessage_mD62FB9E2C290DEF1513B72DDC41B40A26936F1AB(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_SetMargins_mA23E53A33C24CE2E61DEC5980604A48FF0D16FE7 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method)
{
	{
		// messageCanvas.SetMargins(left, top, right, bottom);
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_messageCanvas_14();
		int32_t L_1 = ___left0;
		int32_t L_2 = ___top1;
		int32_t L_3 = ___right2;
		int32_t L_4 = ___bottom3;
		NullCheck(L_0);
		MessageCanvas_SetMargins_m3CCCF18F11D17E1E76710B4643235D692BC395ED(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean NotSupportedWebviewWindow::get_IsVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_get_IsVisible_m29351727A4C7901F5C6F02D3E6E3884055F257BA (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// return isVisible;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_isVisible_16();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::set_IsVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_set_IsVisible_m816574E59D63803E0E6CC63CB982012E3F2A7B81 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// isVisible = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_isVisible_16(L_0);
		// }
		return;
	}
}
// System.Boolean NotSupportedWebviewWindow::get_IsKeyboardVisible()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_get_IsKeyboardVisible_m5567E35450DD8E3BBFDBF5D208A3D65160949BEE (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// return iskeyboardVisible;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_iskeyboardVisible_17();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::set_IsKeyboardVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_set_IsKeyboardVisible_m6A26227A35241101EBCAFC47B247E9F4C78BC1A1 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// iskeyboardVisible = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_iskeyboardVisible_17(L_0);
		// SetMargins(marginLeft, marginTop, marginRight, marginBottom);
		int32_t L_1 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginLeft_10();
		int32_t L_2 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginTop_11();
		int32_t L_3 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginRight_12();
		int32_t L_4 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_marginBottom_13();
		VirtualActionInvoker4< int32_t, int32_t, int32_t, int32_t >::Invoke(13 /* System.Void WebviewWindowBase::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32) */, __this, L_1, L_2, L_3, L_4);
		// }
		return;
	}
}
// System.Boolean NotSupportedWebviewWindow::get_AlertDialogEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_get_AlertDialogEnabled_m9365C725ED784594CE69AE3BC260353C5A730135 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// return alertDialogEnabled;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_alertDialogEnabled_18();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::set_AlertDialogEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_set_AlertDialogEnabled_m0E69B062BA349C391461E01CB4E9B79BF7B4BF9D (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// alertDialogEnabled = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_alertDialogEnabled_18(L_0);
		// }
		return;
	}
}
// System.Boolean NotSupportedWebviewWindow::get_ScrollBounceEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_get_ScrollBounceEnabled_m98A8B6B0AF3ADCC9CC5855556B7FA2901A838AEE (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// return scrollBounceEnabled;
		bool L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_scrollBounceEnabled_19();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::set_ScrollBounceEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_set_ScrollBounceEnabled_mB92F1A3E2E2C7FBA29A53A822BDB0C5FE67D7E92 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// scrollBounceEnabled = value;
		bool L_0 = ___value0;
		((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->set_scrollBounceEnabled_19(L_0);
		// }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::LoadURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_LoadURL_m84D02B71C6A715976EC4BBA007289EA79D92DF83 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		// public override void LoadURL(string url) { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::LoadHTML(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_LoadHTML_mA7028455D5EB60FFAC6E3C902821F8473135AB36 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___html0, String_t* ___baseUrl1, const RuntimeMethod* method)
{
	{
		// public override void LoadHTML(string html, string baseUrl) { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::EvaluateJS(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_EvaluateJS_m2CAD79E7C6116A963E66F0C259BF7A4F93D3D526 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___js0, const RuntimeMethod* method)
{
	{
		// public override void EvaluateJS(string js) { }
		return;
	}
}
// System.Int32 NotSupportedWebviewWindow::get_Progress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NotSupportedWebviewWindow_get_Progress_m9C642D7F957E4B1AFABB3CFCB67EB8ABD597806D (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override int Progress => 0;
		return 0;
	}
}
// System.Boolean NotSupportedWebviewWindow::CanGoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_CanGoBack_m2E5CA3DB283BE1A611416A58B891C8DC815DA8DE (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override bool CanGoBack() => false;
		return (bool)0;
	}
}
// System.Boolean NotSupportedWebviewWindow::CanGoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotSupportedWebviewWindow_CanGoForward_m47B173FBF541870BEF102B7495761463FAFFFCE8 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override bool CanGoForward() => false;
		return (bool)0;
	}
}
// System.Void NotSupportedWebviewWindow::GoBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_GoBack_mB9C7617B4938DC9BFC8808EAC4EA305112C52E04 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void GoBack() { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::GoForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_GoForward_mE43534A5947CB75269DC75CBB7B6E3AF2FB09F06 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void GoForward() { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::Reload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_Reload_m4E522179FDB5B405AE94B2473F7D20BEAA597571 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void Reload() { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::AddCustomHeader(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_AddCustomHeader_mBFBB56F719E4840C65E6CDDE61C1834CADEB6DAF (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___headerKey0, String_t* ___headerValue1, const RuntimeMethod* method)
{
	{
		// public override void AddCustomHeader(string headerKey, string headerValue) { }
		return;
	}
}
// System.String NotSupportedWebviewWindow::GetCustomHeaderValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NotSupportedWebviewWindow_GetCustomHeaderValue_m3D436CF2D3C764724FE30978401DE5E8C6EE44EE (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___headerKey0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string GetCustomHeaderValue(string headerKey) => string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::RemoveCustomHeader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_RemoveCustomHeader_m320F2C1CE1C36673901DB84D61FA7E3E00C8AFDC (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___headerKey0, const RuntimeMethod* method)
{
	{
		// public override void RemoveCustomHeader(string headerKey) { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::ClearCustomHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_ClearCustomHeader_m941710B3CD640EDA564DC0EF0983F0D412BC01DA (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void ClearCustomHeader() { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::ClearCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_ClearCookies_m8ADA34AFB9FF102E5A5D9928B6713FE7E2F49C5A (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void ClearCookies() { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::SaveCookies()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_SaveCookies_mE27A26C91053BED73968ADDDF38450156DC28143 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		// public override void SaveCookies() { }
		return;
	}
}
// System.String NotSupportedWebviewWindow::GetCookies(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* NotSupportedWebviewWindow_GetCookies_m11EEC48D0359226F6A8878249B1D84F101C97FDE (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___url0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override string GetCookies(string url) => string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		return L_0;
	}
}
// System.Void NotSupportedWebviewWindow::SetBasicAuthInfo(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_SetBasicAuthInfo_mDA3140679255F0F1467475868A9A95BDCD170E6C (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, String_t* ___userName0, String_t* ___password1, const RuntimeMethod* method)
{
	{
		// public override void SetBasicAuthInfo(string userName, string password) { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::ClearCache(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_ClearCache_m3FAC9A600CFB1CDB65101E54DEFA771373576587 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, bool ___includeDiskFiles0, const RuntimeMethod* method)
{
	{
		// public override void ClearCache(bool includeDiskFiles) { }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow_OnDestroy_m4C139886C9076865F367300448372E0C85FF6A89 (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(messageCanvas);
		MessageCanvas_t89DB50F9323100B65E63E198A66ECC9972F93487 * L_0 = ((WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 *)__this)->get_messageCanvas_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void NotSupportedWebviewWindow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedWebviewWindow__ctor_m58D72F9FBF9D6617D4A38AF408CC080462D4FD4B (NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * __this, const RuntimeMethod* method)
{
	{
		WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Webview::CreateWebview(UnityEngine.MonoBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_CreateWebview_mD9F309341CA7018D0C99D01FE5D22CBC4A4DB81F (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___parent0, const RuntimeMethod* method)
{
	{
		// this.parent = parent;
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_0 = ___parent0;
		__this->set_parent_1(L_0);
		// if (SetWebviewWindow())
		bool L_1;
		L_1 = Webview_SetWebviewWindow_mFD05AC5E0E42860E1CA1DBF592235C3BD792A4D9(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		// parent.StartCoroutine(LoadWebviewURL());
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_2 = ___parent0;
		RuntimeObject* L_3;
		L_3 = Webview_LoadWebviewURL_m2B9887C6D9EEA5A60E27A4E80B4CD5EB6F9883EB(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_4;
		L_4 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(L_2, L_3, /*hidden argument*/NULL);
		// SetScreenPadding(left, top, right, bottom);
		int32_t L_5 = __this->get_left_3();
		int32_t L_6 = __this->get_top_4();
		int32_t L_7 = __this->get_right_5();
		int32_t L_8 = __this->get_bottom_6();
		Webview_SetScreenPadding_m91D75A884E3768B63556B44FD3E2CE00581ECB88(__this, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Void Webview::SetScreenPadding(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_SetScreenPadding_m91D75A884E3768B63556B44FD3E2CE00581ECB88 (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, int32_t ___left0, int32_t ___top1, int32_t ___right2, int32_t ___bottom3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.left = left;
		int32_t L_0 = ___left0;
		__this->set_left_3(L_0);
		// this.top = top;
		int32_t L_1 = ___top1;
		__this->set_top_4(L_1);
		// this.right = right;
		int32_t L_2 = ___right2;
		__this->set_right_5(L_2);
		// this.bottom = bottom;
		int32_t L_3 = ___bottom3;
		__this->set_bottom_6(L_3);
		// if (webViewObject)
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_4 = __this->get_webViewObject_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		// webViewObject.SetMargins(left, top, right, bottom);
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_6 = __this->get_webViewObject_2();
		int32_t L_7 = ___left0;
		int32_t L_8 = ___top1;
		int32_t L_9 = ___right2;
		int32_t L_10 = ___bottom3;
		NullCheck(L_6);
		VirtualActionInvoker4< int32_t, int32_t, int32_t, int32_t >::Invoke(13 /* System.Void WebviewWindowBase::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32) */, L_6, L_7, L_8, L_9, L_10);
	}

IL_003a:
	{
		// }
		return;
	}
}
// System.Void Webview::SetVisible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_SetVisible_mD43B53D29CA9573175561BA320B41B05C36987FF (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, bool ___visible0, const RuntimeMethod* method)
{
	{
		// webViewObject.IsVisible = visible;
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_0 = __this->get_webViewObject_2();
		bool L_1 = ___visible0;
		NullCheck(L_0);
		VirtualActionInvoker1< bool >::Invoke(5 /* System.Void WebviewWindowBase::set_IsVisible(System.Boolean) */, L_0, L_1);
		// }
		return;
	}
}
// System.Boolean Webview::SetWebviewWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Webview_SetWebviewWindow_mFD05AC5E0E42860E1CA1DBF592235C3BD792A4D9 (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisIOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60_m22578968B2E08B2BA698515F87466AFCC2EBD326_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisNotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B_mF93551E8500D15D32FEAE9B324FFF994ACC3F118_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * V_1 = NULL;
	{
		// bool hasNetwork = Application.internetReachability != NetworkReachability.NotReachable;
		int32_t L_0;
		L_0 = Application_get_internetReachability_m039C30126BD989614BF2A4A3F33129177A95C61C(/*hidden argument*/NULL);
		// bool supported = true;
		V_0 = (bool)1;
		// WebviewOptions options = new WebviewOptions();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_1 = (WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 *)il2cpp_codegen_object_new(WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75_il2cpp_TypeInfo_var);
		WebviewOptions__ctor_m37474B5636586B2F666B231344AAD3883F75270C(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		// if (hasNetwork)
		if (!((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0))
		{
			goto IL_002a;
		}
	}
	{
		// webViewObject = parent.gameObject.AddComponent<IOSWebViewWindow>();
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_2 = __this->get_parent_1();
		NullCheck(L_2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		IOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60 * L_4;
		L_4 = GameObject_AddComponent_TisIOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60_m22578968B2E08B2BA698515F87466AFCC2EBD326(L_3, /*hidden argument*/GameObject_AddComponent_TisIOSWebViewWindow_t790EA803C1350B45E5631D232D3E69AEE2E8BD60_m22578968B2E08B2BA698515F87466AFCC2EBD326_RuntimeMethod_var);
		__this->set_webViewObject_2(L_4);
		// }
		goto IL_0042;
	}

IL_002a:
	{
		// webViewObject = parent.gameObject.AddComponent<NotSupportedWebviewWindow>();
		MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * L_5 = __this->get_parent_1();
		NullCheck(L_5);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		NotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B * L_7;
		L_7 = GameObject_AddComponent_TisNotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B_mF93551E8500D15D32FEAE9B324FFF994ACC3F118(L_6, /*hidden argument*/GameObject_AddComponent_TisNotSupportedWebviewWindow_t05B468C590CF59BB7552C5A40B12A58E73E92F4B_mF93551E8500D15D32FEAE9B324FFF994ACC3F118_RuntimeMethod_var);
		__this->set_webViewObject_2(L_7);
		// supported = false;
		V_0 = (bool)0;
	}

IL_0042:
	{
		// webViewObject.OnLoaded = OnLoaded;
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_8 = __this->get_webViewObject_2();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_9 = (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)il2cpp_codegen_object_new(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161(L_9, __this, (intptr_t)((intptr_t)Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var);
		NullCheck(L_8);
		L_8->set_OnLoaded_8(L_9);
		// webViewObject.OnJS = OnWebMessageReceived;
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_10 = __this->get_webViewObject_2();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_11 = (Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 *)il2cpp_codegen_object_new(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3_il2cpp_TypeInfo_var);
		Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161(L_11, __this, (intptr_t)((intptr_t)Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m090CD607C7652B994D986F12CB18450A24FD8161_RuntimeMethod_var);
		NullCheck(L_10);
		L_10->set_OnJS_4(L_11);
		// webViewObject.Init(options);
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_12 = __this->get_webViewObject_2();
		WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * L_13 = V_1;
		NullCheck(L_12);
		VirtualActionInvoker1< WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * >::Invoke(12 /* System.Void WebviewWindowBase::Init(WebviewOptions) */, L_12, L_13);
		// return supported;
		bool L_14 = V_0;
		return L_14;
	}
}
// System.Collections.IEnumerator Webview::LoadWebviewURL()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Webview_LoadWebviewURL_m2B9887C6D9EEA5A60E27A4E80B4CD5EB6F9883EB (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * L_0 = (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 *)il2cpp_codegen_object_new(U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3_il2cpp_TypeInfo_var);
		U3CLoadWebviewURLU3Ed__13__ctor_m6F09FD7D1B3D0B3BEDA8BF2727183DE6C0EEFF03(L_0, 0, /*hidden argument*/NULL);
		U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void Webview::OnWebMessageReceived(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789 (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3BF35F255EAB6C8F0F4ACAE319B26008A7FC7263);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B3_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	{
		// Debug.Log(message);
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_0, /*hidden argument*/NULL);
		// if (message.Contains(".glb"))
		String_t* L_1 = ___message0;
		NullCheck(L_1);
		bool L_2;
		L_2 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_1, _stringLiteral3BF35F255EAB6C8F0F4ACAE319B26008A7FC7263, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		// webViewObject.IsVisible = false;
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_3 = __this->get_webViewObject_2();
		NullCheck(L_3);
		VirtualActionInvoker1< bool >::Invoke(5 /* System.Void WebviewWindowBase::set_IsVisible(System.Boolean) */, L_3, (bool)0);
		// OnAvatarCreated?.Invoke(message);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_4 = __this->get_OnAvatarCreated_8();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		String_t* L_6 = ___message0;
		NullCheck(G_B3_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B3_0, L_6, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void Webview::OnLoaded(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFABCD6D77B45AD32AC9DABCBB5BCB368C08EDDC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// webViewObject.EvaluateJS(@"
		//     if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
		//         window.Unity = {
		//             call: function(msg) {
		//                 window.webkit.messageHandlers.unityControl.postMessage(msg);
		//             }
		//         }
		//     }
		//     else {
		//         window.Unity = {
		//             call: function(msg) {
		//                 window.location = 'unity:' + msg;
		//             }
		//         }
		//     }
		// 
		//     document.getElementById('loading').innerHTML = '<center>Failed to load on current browser.<br/>Please update your Operating System.</center>'
		// ");
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_0 = __this->get_webViewObject_2();
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(16 /* System.Void WebviewWindowBase::EvaluateJS(System.String) */, L_0, _stringLiteralBFABCD6D77B45AD32AC9DABCBB5BCB368C08EDDC);
		// }
		return;
	}
}
// System.Void Webview::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Webview__ctor_mDE0CDB41C785A413BCD5F8A7C0CC4EB114EBDE5C (Webview_t378970811F03E71B224E384A77472C00869DD7BF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebviewOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewOptions__ctor_m37474B5636586B2F666B231344AAD3883F75270C (WebviewOptions_tAB0F6054D9DB4225A16159E60589E2ADDA9A4D75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string UA = "";
		__this->set_UA_2(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// public ColorMode AndroidForceDarkMode = ColorMode.DarkModeOff;
		__this->set_AndroidForceDarkMode_3(1);
		// public bool EnableWKWebView = true;
		__this->set_EnableWKWebView_4((bool)1);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebviewWindowBase::CallFromJS(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallFromJS_mED05D21C05D8AC77772FD86245C9852177A3FB27 (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallFromJS(string message) => OnJS?.Invoke(message);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnJS_4();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___message0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::CallOnHooked(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallOnHooked_m5563B5A45F54A266293FFB1F043C42E9045BA9C9 (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallOnHooked(string message) => OnHooked?.Invoke(message);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnHooked_9();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___message0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::CallOnLoaded(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallOnLoaded_mE21678B77304B10EEB125E0307D1E01DBFA205FC (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallOnLoaded(string url) => OnLoaded?.Invoke(url);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnLoaded_8();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___url0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::CallOnStarted(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallOnStarted_m24D400B89F460B76F8D5B56EBD781B95CD22E284 (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallOnStarted(string url) => OnStarted?.Invoke(url);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnStarted_7();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___url0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::CallOnError(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallOnError_mFE1903A1A4FA4B5DE5C7C376AB91FE7C693925C3 (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallOnError(string error) => OnError?.Invoke(error);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnError_5();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___error0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::CallOnHttpError(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase_CallOnHttpError_m8410C1021158F781C3B9504F406914D2C9B56045 (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B2_0 = NULL;
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * G_B1_0 = NULL;
	{
		// public void CallOnHttpError(string error) => OnHttpError?.Invoke(error);
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_0 = __this->get_OnHttpError_6();
		Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_2 = ___error0;
		NullCheck(G_B2_0);
		Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1(G_B2_0, L_2, /*hidden argument*/Action_1_Invoke_mBF157FC77ED7B1B47EE2F7C6E51DE0E9EFB197C1_RuntimeMethod_var);
		return;
	}
}
// System.Void WebviewWindowBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A (WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * __this, const RuntimeMethod* method)
{
	{
		// protected bool alertDialogEnabled = true;
		__this->set_alertDialogEnabled_18((bool)1);
		// protected bool scrollBounceEnabled = true;
		__this->set_scrollBounceEnabled_19((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Webview/<LoadWebviewURL>d__13::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadWebviewURLU3Ed__13__ctor_m6F09FD7D1B3D0B3BEDA8BF2727183DE6C0EEFF03 (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Webview/<LoadWebviewURL>d__13::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadWebviewURLU3Ed__13_System_IDisposable_Dispose_m404539BEB5F56A09EEEA13414B21AB00BB6C42FF (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Webview/<LoadWebviewURL>d__13::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadWebviewURLU3Ed__13_MoveNext_m1F4404D300B08B24E60BB3330DC8E371C3131472 (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral218F5A08519088A96BE3C1074984C53EA49F1CCA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B9666D6D571D2FF314955240409E0435EA33F8B);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Webview_t378970811F03E71B224E384A77472C00869DD7BF * V_1 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Webview_t378970811F03E71B224E384A77472C00869DD7BF * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0066;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// string source = Path.Combine(Application.streamingAssetsPath, WebViewFileName);
		String_t* L_4;
		L_4 = Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		String_t* L_5;
		L_5 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_4, _stringLiteral6B9666D6D571D2FF314955240409E0435EA33F8B, /*hidden argument*/NULL);
		// string destination = Path.Combine(Application.persistentDataPath, WebViewFileName);
		String_t* L_6;
		L_6 = Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463(/*hidden argument*/NULL);
		String_t* L_7;
		L_7 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_6, _stringLiteral6B9666D6D571D2FF314955240409E0435EA33F8B, /*hidden argument*/NULL);
		__this->set_U3CdestinationU3E5__2_3(L_7);
		// byte[] result = null;
		V_2 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		// result = File.ReadAllBytes(source);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
		L_8 = File_ReadAllBytes_mFB47FB50E938AE90CC822442D30E896441D95829(L_5, /*hidden argument*/NULL);
		V_2 = L_8;
		// File.WriteAllBytes(destination, result);
		String_t* L_9 = __this->get_U3CdestinationU3E5__2_3();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_2;
		File_WriteAllBytes_m1E88860F73A6A2150FAB97D9BF3F44596F06036F(L_9, L_10, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0066:
	{
		__this->set_U3CU3E1__state_0((-1));
		// webViewObject.LoadURL($"file://{ destination}");
		Webview_t378970811F03E71B224E384A77472C00869DD7BF * L_11 = V_1;
		NullCheck(L_11);
		WebviewWindowBase_t5CD1BDEA84ADF8A3947DF0EB5EC74C2B91BA97F4 * L_12 = L_11->get_webViewObject_2();
		String_t* L_13 = __this->get_U3CdestinationU3E5__2_3();
		String_t* L_14;
		L_14 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral218F5A08519088A96BE3C1074984C53EA49F1CCA, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtualActionInvoker1< String_t* >::Invoke(14 /* System.Void WebviewWindowBase::LoadURL(System.String) */, L_12, L_14);
		// }
		return (bool)0;
	}
}
// System.Object Webview/<LoadWebviewURL>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadWebviewURLU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234431268488DCC05F59E6F9315DDF3D66E56315 (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Webview/<LoadWebviewURL>d__13::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_Reset_m594D2E06A810AC4ECF48EF02997D6B304606049A (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_Reset_m594D2E06A810AC4ECF48EF02997D6B304606049A_RuntimeMethod_var)));
	}
}
// System.Object Webview/<LoadWebviewURL>d__13::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_get_Current_m190E3B7077895AEDA2FD63B7045232C154C225F4 (U3CLoadWebviewURLU3Ed__13_t848C0C40CB549EA23AA2869DD9AFF84EB1C11BE3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
