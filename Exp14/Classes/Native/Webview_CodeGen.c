﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MessageCanvas::SetMessage(System.String)
extern void MessageCanvas_SetMessage_mD62FB9E2C290DEF1513B72DDC41B40A26936F1AB (void);
// 0x00000002 System.Void MessageCanvas::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
extern void MessageCanvas_SetMargins_m3CCCF18F11D17E1E76710B4643235D692BC395ED (void);
// 0x00000003 System.Void MessageCanvas::.ctor()
extern void MessageCanvas__ctor_m7CFC0474D013EC20F1CF4BFA204AAAEB55117A9C (void);
// 0x00000004 System.Void Webview::CreateWebview(UnityEngine.MonoBehaviour)
extern void Webview_CreateWebview_mD9F309341CA7018D0C99D01FE5D22CBC4A4DB81F (void);
// 0x00000005 System.Void Webview::SetScreenPadding(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Webview_SetScreenPadding_m91D75A884E3768B63556B44FD3E2CE00581ECB88 (void);
// 0x00000006 System.Void Webview::SetVisible(System.Boolean)
extern void Webview_SetVisible_mD43B53D29CA9573175561BA320B41B05C36987FF (void);
// 0x00000007 System.Boolean Webview::SetWebviewWindow()
extern void Webview_SetWebviewWindow_mFD05AC5E0E42860E1CA1DBF592235C3BD792A4D9 (void);
// 0x00000008 System.Collections.IEnumerator Webview::LoadWebviewURL()
extern void Webview_LoadWebviewURL_m2B9887C6D9EEA5A60E27A4E80B4CD5EB6F9883EB (void);
// 0x00000009 System.Void Webview::OnWebMessageReceived(System.String)
extern void Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789 (void);
// 0x0000000A System.Void Webview::OnLoaded(System.String)
extern void Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A (void);
// 0x0000000B System.Void Webview::.ctor()
extern void Webview__ctor_mDE0CDB41C785A413BCD5F8A7C0CC4EB114EBDE5C (void);
// 0x0000000C System.Void Webview/<LoadWebviewURL>d__13::.ctor(System.Int32)
extern void U3CLoadWebviewURLU3Ed__13__ctor_m6F09FD7D1B3D0B3BEDA8BF2727183DE6C0EEFF03 (void);
// 0x0000000D System.Void Webview/<LoadWebviewURL>d__13::System.IDisposable.Dispose()
extern void U3CLoadWebviewURLU3Ed__13_System_IDisposable_Dispose_m404539BEB5F56A09EEEA13414B21AB00BB6C42FF (void);
// 0x0000000E System.Boolean Webview/<LoadWebviewURL>d__13::MoveNext()
extern void U3CLoadWebviewURLU3Ed__13_MoveNext_m1F4404D300B08B24E60BB3330DC8E371C3131472 (void);
// 0x0000000F System.Object Webview/<LoadWebviewURL>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadWebviewURLU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234431268488DCC05F59E6F9315DDF3D66E56315 (void);
// 0x00000010 System.Void Webview/<LoadWebviewURL>d__13::System.Collections.IEnumerator.Reset()
extern void U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_Reset_m594D2E06A810AC4ECF48EF02997D6B304606049A (void);
// 0x00000011 System.Object Webview/<LoadWebviewURL>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_get_Current_m190E3B7077895AEDA2FD63B7045232C154C225F4 (void);
// 0x00000012 System.Void WebviewOptions::.ctor()
extern void WebviewOptions__ctor_m37474B5636586B2F666B231344AAD3883F75270C (void);
// 0x00000013 System.IntPtr IOSWebViewWindow::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32)
extern void IOSWebViewWindow__CWebViewPlugin_Init_mE1E39EEE1F883789A40087032C4B19384DAB2281 (void);
// 0x00000014 System.Int32 IOSWebViewWindow::_CWebViewPlugin_Destroy(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_Destroy_m4186EDF719B0EACDF8B94F6D76467478B02FB3E6 (void);
// 0x00000015 System.Void IOSWebViewWindow::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void IOSWebViewWindow__CWebViewPlugin_SetMargins_m013A342E92A766CEC45FF8880C789EB496543BD6 (void);
// 0x00000016 System.Void IOSWebViewWindow::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
extern void IOSWebViewWindow__CWebViewPlugin_SetVisibility_m924F50E9C0DBC75E863D1BC3C605ABDE4C823F73 (void);
// 0x00000017 System.Void IOSWebViewWindow::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
extern void IOSWebViewWindow__CWebViewPlugin_SetAlertDialogEnabled_m47FC04E1BA1264D26FE53C9EA43335D4C57061C2 (void);
// 0x00000018 System.Void IOSWebViewWindow::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
extern void IOSWebViewWindow__CWebViewPlugin_SetScrollBounceEnabled_mA523CE41F6B403C327699F835FB21090FA8DF42B (void);
// 0x00000019 System.Boolean IOSWebViewWindow::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_SetURLPattern_m9C7C68B4BE05F3151A1945739D4328AE83FD570C (void);
// 0x0000001A System.Void IOSWebViewWindow::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_LoadURL_m68F444DCD9141BC5BC30B39BF612361A3C316505 (void);
// 0x0000001B System.Void IOSWebViewWindow::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_LoadHTML_mAC443A84AE23301083AEED3B6A9AF6B8294DFE3C (void);
// 0x0000001C System.Void IOSWebViewWindow::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_EvaluateJS_mA62A6960E972C2AC108925F4D3F2B7C6806FD080 (void);
// 0x0000001D System.Int32 IOSWebViewWindow::_CWebViewPlugin_Progress(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_Progress_mE0DC8F51E3F9CF631D5FBBDB51991F7A6544933F (void);
// 0x0000001E System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoBack(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_CanGoBack_mBC66CBFFD5AD414ACA1617325ED98A88CDC77F99 (void);
// 0x0000001F System.Boolean IOSWebViewWindow::_CWebViewPlugin_CanGoForward(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_CanGoForward_m8649FD8A32D6FEC96E8C51B41301AAC8E3C08CBE (void);
// 0x00000020 System.Void IOSWebViewWindow::_CWebViewPlugin_GoBack(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_GoBack_m0E37FCED5F98A9DC60E941EB3E8FDC7796D53913 (void);
// 0x00000021 System.Void IOSWebViewWindow::_CWebViewPlugin_GoForward(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_GoForward_mE00507032C41464180A66CB8412728377A2681E9 (void);
// 0x00000022 System.Void IOSWebViewWindow::_CWebViewPlugin_Reload(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_Reload_m415DA53D3BFAE3EBFBC83B601A86AC6C1467EBBC (void);
// 0x00000023 System.Void IOSWebViewWindow::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_AddCustomHeader_m5A5A19B2839F4A1D656C172275A7F1E87B94BFFC (void);
// 0x00000024 System.String IOSWebViewWindow::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_GetCustomHeaderValue_mC7080B7CABE3A22EFA71AC6E404746EAC0F55DC4 (void);
// 0x00000025 System.Void IOSWebViewWindow::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_RemoveCustomHeader_m7C2B62BE9D3745212DF8335D3D2A1D2803205281 (void);
// 0x00000026 System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
extern void IOSWebViewWindow__CWebViewPlugin_ClearCustomHeader_m502CCBA08EC454578A4AD6DFC5C8AA2DFF14EFA3 (void);
// 0x00000027 System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCookies()
extern void IOSWebViewWindow__CWebViewPlugin_ClearCookies_m970260FEB82B5D6B3ED3E5777D10B6C5BAD1B9E7 (void);
// 0x00000028 System.Void IOSWebViewWindow::_CWebViewPlugin_SaveCookies()
extern void IOSWebViewWindow__CWebViewPlugin_SaveCookies_mC9FBE134044F4D586E498E53B514FD6F865AE1E8 (void);
// 0x00000029 System.String IOSWebViewWindow::_CWebViewPlugin_GetCookies(System.String)
extern void IOSWebViewWindow__CWebViewPlugin_GetCookies_mAA0AE2F40B6B5498B8373F2FE2E5D8371350CC1A (void);
// 0x0000002A System.Void IOSWebViewWindow::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
extern void IOSWebViewWindow__CWebViewPlugin_SetBasicAuthInfo_m5AA21D80878C30E7DA1FC8F25827E7D2191AB2B6 (void);
// 0x0000002B System.Void IOSWebViewWindow::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
extern void IOSWebViewWindow__CWebViewPlugin_ClearCache_mC4E2792325D2F72F2BE263C530114D65D9EB5ECB (void);
// 0x0000002C System.Void IOSWebViewWindow::Init(WebviewOptions)
extern void IOSWebViewWindow_Init_mCD581AE993D211CD8D460AAD73B90D6F35A923B3 (void);
// 0x0000002D System.Void IOSWebViewWindow::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
extern void IOSWebViewWindow_SetMargins_m2D09A6CD9A361539B3D42F16E90543BE60EFA9BE (void);
// 0x0000002E System.Boolean IOSWebViewWindow::get_IsVisible()
extern void IOSWebViewWindow_get_IsVisible_mFA30DD1B5EC50C8398D2B84E4547433B80E683BD (void);
// 0x0000002F System.Void IOSWebViewWindow::set_IsVisible(System.Boolean)
extern void IOSWebViewWindow_set_IsVisible_mDD59F0E928521D6E4C8B4995432E03970577621F (void);
// 0x00000030 System.Boolean IOSWebViewWindow::get_IsKeyboardVisible()
extern void IOSWebViewWindow_get_IsKeyboardVisible_m0548E5139A1ECDC7803E5B5EA74A83D530D507AC (void);
// 0x00000031 System.Void IOSWebViewWindow::set_IsKeyboardVisible(System.Boolean)
extern void IOSWebViewWindow_set_IsKeyboardVisible_m95F9A5A84319EA5CB06F1AEED5D2DF686FD82339 (void);
// 0x00000032 System.Boolean IOSWebViewWindow::get_AlertDialogEnabled()
extern void IOSWebViewWindow_get_AlertDialogEnabled_m7DF42FDAC56B43E948D22C209E4DE530E93AC2B5 (void);
// 0x00000033 System.Void IOSWebViewWindow::set_AlertDialogEnabled(System.Boolean)
extern void IOSWebViewWindow_set_AlertDialogEnabled_m5059C70637C0123529E29CBE0B14247F2E235FC3 (void);
// 0x00000034 System.Boolean IOSWebViewWindow::get_ScrollBounceEnabled()
extern void IOSWebViewWindow_get_ScrollBounceEnabled_m43F24B2ADE3FD07ABC54A3F4A415A8A28EA1BCC3 (void);
// 0x00000035 System.Void IOSWebViewWindow::set_ScrollBounceEnabled(System.Boolean)
extern void IOSWebViewWindow_set_ScrollBounceEnabled_m416820B44ACF05289D93285C80FEAFECAB81F9E6 (void);
// 0x00000036 System.Void IOSWebViewWindow::LoadURL(System.String)
extern void IOSWebViewWindow_LoadURL_m995D6A3043ED0453223F56A90CC6347C0ABF6CC1 (void);
// 0x00000037 System.Void IOSWebViewWindow::LoadHTML(System.String,System.String)
extern void IOSWebViewWindow_LoadHTML_m62EAB80A3C07D23504B219BEB884807B21D7916A (void);
// 0x00000038 System.Void IOSWebViewWindow::EvaluateJS(System.String)
extern void IOSWebViewWindow_EvaluateJS_m5D1916FD9D51EA349F98FC4D52D75AA3F5B57252 (void);
// 0x00000039 System.Int32 IOSWebViewWindow::get_Progress()
extern void IOSWebViewWindow_get_Progress_mD220D10E4827DDEFABBF0172ACB0742C009CD727 (void);
// 0x0000003A System.Boolean IOSWebViewWindow::CanGoBack()
extern void IOSWebViewWindow_CanGoBack_m03BC351C2C0F81E413CD6F68DCF3C32078F06431 (void);
// 0x0000003B System.Boolean IOSWebViewWindow::CanGoForward()
extern void IOSWebViewWindow_CanGoForward_mD91F1713B3559CEFB97913338D2072D20CD2FDF3 (void);
// 0x0000003C System.Void IOSWebViewWindow::GoBack()
extern void IOSWebViewWindow_GoBack_mEF56ADD66A3FAA662BA23957425117E376756454 (void);
// 0x0000003D System.Void IOSWebViewWindow::GoForward()
extern void IOSWebViewWindow_GoForward_mBC90420F582903FFD7FD7CDD4BB2BC8B7182DD0B (void);
// 0x0000003E System.Void IOSWebViewWindow::Reload()
extern void IOSWebViewWindow_Reload_mA5FC640D3CAEA08A1EDF5E9B350CEC06105F283F (void);
// 0x0000003F System.Void IOSWebViewWindow::AddCustomHeader(System.String,System.String)
extern void IOSWebViewWindow_AddCustomHeader_m7F6993669F278A6E42618EBA9598FD2F10AE6518 (void);
// 0x00000040 System.String IOSWebViewWindow::GetCustomHeaderValue(System.String)
extern void IOSWebViewWindow_GetCustomHeaderValue_m70B535FB8AABFE81E7C149510030DCE904A96C34 (void);
// 0x00000041 System.Void IOSWebViewWindow::RemoveCustomHeader(System.String)
extern void IOSWebViewWindow_RemoveCustomHeader_mF3CF36589A6AC65E95285BBF902EBFCDDD4A2E09 (void);
// 0x00000042 System.Void IOSWebViewWindow::ClearCustomHeader()
extern void IOSWebViewWindow_ClearCustomHeader_m60000FD500B764AEF2AA2810CDC117F0F3697AAA (void);
// 0x00000043 System.Void IOSWebViewWindow::ClearCache(System.Boolean)
extern void IOSWebViewWindow_ClearCache_m901CFE7C80D47ED9F2A0E319A3E9759E7085DD6A (void);
// 0x00000044 System.Void IOSWebViewWindow::ClearCookies()
extern void IOSWebViewWindow_ClearCookies_mC9E3E19377773037D974F092B8C97A9BD7C2352C (void);
// 0x00000045 System.String IOSWebViewWindow::GetCookies(System.String)
extern void IOSWebViewWindow_GetCookies_m5686C288DF256B6904E2643818DB8D81C023FFC7 (void);
// 0x00000046 System.Void IOSWebViewWindow::SaveCookies()
extern void IOSWebViewWindow_SaveCookies_mF8467E8D2868AF0A473B5E3F22B3261D0F49AB4F (void);
// 0x00000047 System.Void IOSWebViewWindow::SetBasicAuthInfo(System.String,System.String)
extern void IOSWebViewWindow_SetBasicAuthInfo_mE2D13FBFA570498521EB6EAECDB7891FEA441996 (void);
// 0x00000048 System.Void IOSWebViewWindow::OnDestroy()
extern void IOSWebViewWindow_OnDestroy_mB747097FA32B03E96B790FC677512701A4E56077 (void);
// 0x00000049 System.Void IOSWebViewWindow::.ctor()
extern void IOSWebViewWindow__ctor_m5C11C8EDBE14F137D9A4FDF618B1E80D8028BC2A (void);
// 0x0000004A System.Boolean NotSupportedWebviewWindow::get_IsWebViewAvailable()
extern void NotSupportedWebviewWindow_get_IsWebViewAvailable_m35630CA78767025DDFA7EDA7E799F10AC7D5D482 (void);
// 0x0000004B System.Void NotSupportedWebviewWindow::Init(WebviewOptions)
extern void NotSupportedWebviewWindow_Init_m8546014FDC1A4BC18866FF4071DFBDE34F4BBF5F (void);
// 0x0000004C System.Void NotSupportedWebviewWindow::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
extern void NotSupportedWebviewWindow_SetMargins_mA23E53A33C24CE2E61DEC5980604A48FF0D16FE7 (void);
// 0x0000004D System.Boolean NotSupportedWebviewWindow::get_IsVisible()
extern void NotSupportedWebviewWindow_get_IsVisible_m29351727A4C7901F5C6F02D3E6E3884055F257BA (void);
// 0x0000004E System.Void NotSupportedWebviewWindow::set_IsVisible(System.Boolean)
extern void NotSupportedWebviewWindow_set_IsVisible_m816574E59D63803E0E6CC63CB982012E3F2A7B81 (void);
// 0x0000004F System.Boolean NotSupportedWebviewWindow::get_IsKeyboardVisible()
extern void NotSupportedWebviewWindow_get_IsKeyboardVisible_m5567E35450DD8E3BBFDBF5D208A3D65160949BEE (void);
// 0x00000050 System.Void NotSupportedWebviewWindow::set_IsKeyboardVisible(System.Boolean)
extern void NotSupportedWebviewWindow_set_IsKeyboardVisible_m6A26227A35241101EBCAFC47B247E9F4C78BC1A1 (void);
// 0x00000051 System.Boolean NotSupportedWebviewWindow::get_AlertDialogEnabled()
extern void NotSupportedWebviewWindow_get_AlertDialogEnabled_m9365C725ED784594CE69AE3BC260353C5A730135 (void);
// 0x00000052 System.Void NotSupportedWebviewWindow::set_AlertDialogEnabled(System.Boolean)
extern void NotSupportedWebviewWindow_set_AlertDialogEnabled_m0E69B062BA349C391461E01CB4E9B79BF7B4BF9D (void);
// 0x00000053 System.Boolean NotSupportedWebviewWindow::get_ScrollBounceEnabled()
extern void NotSupportedWebviewWindow_get_ScrollBounceEnabled_m98A8B6B0AF3ADCC9CC5855556B7FA2901A838AEE (void);
// 0x00000054 System.Void NotSupportedWebviewWindow::set_ScrollBounceEnabled(System.Boolean)
extern void NotSupportedWebviewWindow_set_ScrollBounceEnabled_mB92F1A3E2E2C7FBA29A53A822BDB0C5FE67D7E92 (void);
// 0x00000055 System.Void NotSupportedWebviewWindow::LoadURL(System.String)
extern void NotSupportedWebviewWindow_LoadURL_m84D02B71C6A715976EC4BBA007289EA79D92DF83 (void);
// 0x00000056 System.Void NotSupportedWebviewWindow::LoadHTML(System.String,System.String)
extern void NotSupportedWebviewWindow_LoadHTML_mA7028455D5EB60FFAC6E3C902821F8473135AB36 (void);
// 0x00000057 System.Void NotSupportedWebviewWindow::EvaluateJS(System.String)
extern void NotSupportedWebviewWindow_EvaluateJS_m2CAD79E7C6116A963E66F0C259BF7A4F93D3D526 (void);
// 0x00000058 System.Int32 NotSupportedWebviewWindow::get_Progress()
extern void NotSupportedWebviewWindow_get_Progress_m9C642D7F957E4B1AFABB3CFCB67EB8ABD597806D (void);
// 0x00000059 System.Boolean NotSupportedWebviewWindow::CanGoBack()
extern void NotSupportedWebviewWindow_CanGoBack_m2E5CA3DB283BE1A611416A58B891C8DC815DA8DE (void);
// 0x0000005A System.Boolean NotSupportedWebviewWindow::CanGoForward()
extern void NotSupportedWebviewWindow_CanGoForward_m47B173FBF541870BEF102B7495761463FAFFFCE8 (void);
// 0x0000005B System.Void NotSupportedWebviewWindow::GoBack()
extern void NotSupportedWebviewWindow_GoBack_mB9C7617B4938DC9BFC8808EAC4EA305112C52E04 (void);
// 0x0000005C System.Void NotSupportedWebviewWindow::GoForward()
extern void NotSupportedWebviewWindow_GoForward_mE43534A5947CB75269DC75CBB7B6E3AF2FB09F06 (void);
// 0x0000005D System.Void NotSupportedWebviewWindow::Reload()
extern void NotSupportedWebviewWindow_Reload_m4E522179FDB5B405AE94B2473F7D20BEAA597571 (void);
// 0x0000005E System.Void NotSupportedWebviewWindow::AddCustomHeader(System.String,System.String)
extern void NotSupportedWebviewWindow_AddCustomHeader_mBFBB56F719E4840C65E6CDDE61C1834CADEB6DAF (void);
// 0x0000005F System.String NotSupportedWebviewWindow::GetCustomHeaderValue(System.String)
extern void NotSupportedWebviewWindow_GetCustomHeaderValue_m3D436CF2D3C764724FE30978401DE5E8C6EE44EE (void);
// 0x00000060 System.Void NotSupportedWebviewWindow::RemoveCustomHeader(System.String)
extern void NotSupportedWebviewWindow_RemoveCustomHeader_m320F2C1CE1C36673901DB84D61FA7E3E00C8AFDC (void);
// 0x00000061 System.Void NotSupportedWebviewWindow::ClearCustomHeader()
extern void NotSupportedWebviewWindow_ClearCustomHeader_m941710B3CD640EDA564DC0EF0983F0D412BC01DA (void);
// 0x00000062 System.Void NotSupportedWebviewWindow::ClearCookies()
extern void NotSupportedWebviewWindow_ClearCookies_m8ADA34AFB9FF102E5A5D9928B6713FE7E2F49C5A (void);
// 0x00000063 System.Void NotSupportedWebviewWindow::SaveCookies()
extern void NotSupportedWebviewWindow_SaveCookies_mE27A26C91053BED73968ADDDF38450156DC28143 (void);
// 0x00000064 System.String NotSupportedWebviewWindow::GetCookies(System.String)
extern void NotSupportedWebviewWindow_GetCookies_m11EEC48D0359226F6A8878249B1D84F101C97FDE (void);
// 0x00000065 System.Void NotSupportedWebviewWindow::SetBasicAuthInfo(System.String,System.String)
extern void NotSupportedWebviewWindow_SetBasicAuthInfo_mDA3140679255F0F1467475868A9A95BDCD170E6C (void);
// 0x00000066 System.Void NotSupportedWebviewWindow::ClearCache(System.Boolean)
extern void NotSupportedWebviewWindow_ClearCache_m3FAC9A600CFB1CDB65101E54DEFA771373576587 (void);
// 0x00000067 System.Void NotSupportedWebviewWindow::OnDestroy()
extern void NotSupportedWebviewWindow_OnDestroy_m4C139886C9076865F367300448372E0C85FF6A89 (void);
// 0x00000068 System.Void NotSupportedWebviewWindow::.ctor()
extern void NotSupportedWebviewWindow__ctor_m58D72F9FBF9D6617D4A38AF408CC080462D4FD4B (void);
// 0x00000069 System.Boolean WebviewWindowBase::get_IsVisible()
// 0x0000006A System.Void WebviewWindowBase::set_IsVisible(System.Boolean)
// 0x0000006B System.Boolean WebviewWindowBase::get_IsKeyboardVisible()
// 0x0000006C System.Void WebviewWindowBase::set_IsKeyboardVisible(System.Boolean)
// 0x0000006D System.Boolean WebviewWindowBase::get_AlertDialogEnabled()
// 0x0000006E System.Void WebviewWindowBase::set_AlertDialogEnabled(System.Boolean)
// 0x0000006F System.Boolean WebviewWindowBase::get_ScrollBounceEnabled()
// 0x00000070 System.Void WebviewWindowBase::set_ScrollBounceEnabled(System.Boolean)
// 0x00000071 System.Void WebviewWindowBase::Init(WebviewOptions)
// 0x00000072 System.Void WebviewWindowBase::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32)
// 0x00000073 System.Void WebviewWindowBase::LoadURL(System.String)
// 0x00000074 System.Void WebviewWindowBase::LoadHTML(System.String,System.String)
// 0x00000075 System.Void WebviewWindowBase::EvaluateJS(System.String)
// 0x00000076 System.Int32 WebviewWindowBase::get_Progress()
// 0x00000077 System.Boolean WebviewWindowBase::CanGoBack()
// 0x00000078 System.Boolean WebviewWindowBase::CanGoForward()
// 0x00000079 System.Void WebviewWindowBase::GoBack()
// 0x0000007A System.Void WebviewWindowBase::GoForward()
// 0x0000007B System.Void WebviewWindowBase::Reload()
// 0x0000007C System.Void WebviewWindowBase::AddCustomHeader(System.String,System.String)
// 0x0000007D System.String WebviewWindowBase::GetCustomHeaderValue(System.String)
// 0x0000007E System.Void WebviewWindowBase::RemoveCustomHeader(System.String)
// 0x0000007F System.Void WebviewWindowBase::ClearCustomHeader()
// 0x00000080 System.Void WebviewWindowBase::ClearCookies()
// 0x00000081 System.Void WebviewWindowBase::SaveCookies()
// 0x00000082 System.String WebviewWindowBase::GetCookies(System.String)
// 0x00000083 System.Void WebviewWindowBase::SetBasicAuthInfo(System.String,System.String)
// 0x00000084 System.Void WebviewWindowBase::ClearCache(System.Boolean)
// 0x00000085 System.Void WebviewWindowBase::CallFromJS(System.String)
extern void WebviewWindowBase_CallFromJS_mED05D21C05D8AC77772FD86245C9852177A3FB27 (void);
// 0x00000086 System.Void WebviewWindowBase::CallOnHooked(System.String)
extern void WebviewWindowBase_CallOnHooked_m5563B5A45F54A266293FFB1F043C42E9045BA9C9 (void);
// 0x00000087 System.Void WebviewWindowBase::CallOnLoaded(System.String)
extern void WebviewWindowBase_CallOnLoaded_mE21678B77304B10EEB125E0307D1E01DBFA205FC (void);
// 0x00000088 System.Void WebviewWindowBase::CallOnStarted(System.String)
extern void WebviewWindowBase_CallOnStarted_m24D400B89F460B76F8D5B56EBD781B95CD22E284 (void);
// 0x00000089 System.Void WebviewWindowBase::CallOnError(System.String)
extern void WebviewWindowBase_CallOnError_mFE1903A1A4FA4B5DE5C7C376AB91FE7C693925C3 (void);
// 0x0000008A System.Void WebviewWindowBase::CallOnHttpError(System.String)
extern void WebviewWindowBase_CallOnHttpError_m8410C1021158F781C3B9504F406914D2C9B56045 (void);
// 0x0000008B System.Void WebviewWindowBase::.ctor()
extern void WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A (void);
static Il2CppMethodPointer s_methodPointers[139] = 
{
	MessageCanvas_SetMessage_mD62FB9E2C290DEF1513B72DDC41B40A26936F1AB,
	MessageCanvas_SetMargins_m3CCCF18F11D17E1E76710B4643235D692BC395ED,
	MessageCanvas__ctor_m7CFC0474D013EC20F1CF4BFA204AAAEB55117A9C,
	Webview_CreateWebview_mD9F309341CA7018D0C99D01FE5D22CBC4A4DB81F,
	Webview_SetScreenPadding_m91D75A884E3768B63556B44FD3E2CE00581ECB88,
	Webview_SetVisible_mD43B53D29CA9573175561BA320B41B05C36987FF,
	Webview_SetWebviewWindow_mFD05AC5E0E42860E1CA1DBF592235C3BD792A4D9,
	Webview_LoadWebviewURL_m2B9887C6D9EEA5A60E27A4E80B4CD5EB6F9883EB,
	Webview_OnWebMessageReceived_m251F62234A9DAE241FD84B44A9F1440D5DADB789,
	Webview_OnLoaded_m4033606B70CCCB344BC666EE554B9DEFB4E4202A,
	Webview__ctor_mDE0CDB41C785A413BCD5F8A7C0CC4EB114EBDE5C,
	U3CLoadWebviewURLU3Ed__13__ctor_m6F09FD7D1B3D0B3BEDA8BF2727183DE6C0EEFF03,
	U3CLoadWebviewURLU3Ed__13_System_IDisposable_Dispose_m404539BEB5F56A09EEEA13414B21AB00BB6C42FF,
	U3CLoadWebviewURLU3Ed__13_MoveNext_m1F4404D300B08B24E60BB3330DC8E371C3131472,
	U3CLoadWebviewURLU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m234431268488DCC05F59E6F9315DDF3D66E56315,
	U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_Reset_m594D2E06A810AC4ECF48EF02997D6B304606049A,
	U3CLoadWebviewURLU3Ed__13_System_Collections_IEnumerator_get_Current_m190E3B7077895AEDA2FD63B7045232C154C225F4,
	WebviewOptions__ctor_m37474B5636586B2F666B231344AAD3883F75270C,
	IOSWebViewWindow__CWebViewPlugin_Init_mE1E39EEE1F883789A40087032C4B19384DAB2281,
	IOSWebViewWindow__CWebViewPlugin_Destroy_m4186EDF719B0EACDF8B94F6D76467478B02FB3E6,
	IOSWebViewWindow__CWebViewPlugin_SetMargins_m013A342E92A766CEC45FF8880C789EB496543BD6,
	IOSWebViewWindow__CWebViewPlugin_SetVisibility_m924F50E9C0DBC75E863D1BC3C605ABDE4C823F73,
	IOSWebViewWindow__CWebViewPlugin_SetAlertDialogEnabled_m47FC04E1BA1264D26FE53C9EA43335D4C57061C2,
	IOSWebViewWindow__CWebViewPlugin_SetScrollBounceEnabled_mA523CE41F6B403C327699F835FB21090FA8DF42B,
	IOSWebViewWindow__CWebViewPlugin_SetURLPattern_m9C7C68B4BE05F3151A1945739D4328AE83FD570C,
	IOSWebViewWindow__CWebViewPlugin_LoadURL_m68F444DCD9141BC5BC30B39BF612361A3C316505,
	IOSWebViewWindow__CWebViewPlugin_LoadHTML_mAC443A84AE23301083AEED3B6A9AF6B8294DFE3C,
	IOSWebViewWindow__CWebViewPlugin_EvaluateJS_mA62A6960E972C2AC108925F4D3F2B7C6806FD080,
	IOSWebViewWindow__CWebViewPlugin_Progress_mE0DC8F51E3F9CF631D5FBBDB51991F7A6544933F,
	IOSWebViewWindow__CWebViewPlugin_CanGoBack_mBC66CBFFD5AD414ACA1617325ED98A88CDC77F99,
	IOSWebViewWindow__CWebViewPlugin_CanGoForward_m8649FD8A32D6FEC96E8C51B41301AAC8E3C08CBE,
	IOSWebViewWindow__CWebViewPlugin_GoBack_m0E37FCED5F98A9DC60E941EB3E8FDC7796D53913,
	IOSWebViewWindow__CWebViewPlugin_GoForward_mE00507032C41464180A66CB8412728377A2681E9,
	IOSWebViewWindow__CWebViewPlugin_Reload_m415DA53D3BFAE3EBFBC83B601A86AC6C1467EBBC,
	IOSWebViewWindow__CWebViewPlugin_AddCustomHeader_m5A5A19B2839F4A1D656C172275A7F1E87B94BFFC,
	IOSWebViewWindow__CWebViewPlugin_GetCustomHeaderValue_mC7080B7CABE3A22EFA71AC6E404746EAC0F55DC4,
	IOSWebViewWindow__CWebViewPlugin_RemoveCustomHeader_m7C2B62BE9D3745212DF8335D3D2A1D2803205281,
	IOSWebViewWindow__CWebViewPlugin_ClearCustomHeader_m502CCBA08EC454578A4AD6DFC5C8AA2DFF14EFA3,
	IOSWebViewWindow__CWebViewPlugin_ClearCookies_m970260FEB82B5D6B3ED3E5777D10B6C5BAD1B9E7,
	IOSWebViewWindow__CWebViewPlugin_SaveCookies_mC9FBE134044F4D586E498E53B514FD6F865AE1E8,
	IOSWebViewWindow__CWebViewPlugin_GetCookies_mAA0AE2F40B6B5498B8373F2FE2E5D8371350CC1A,
	IOSWebViewWindow__CWebViewPlugin_SetBasicAuthInfo_m5AA21D80878C30E7DA1FC8F25827E7D2191AB2B6,
	IOSWebViewWindow__CWebViewPlugin_ClearCache_mC4E2792325D2F72F2BE263C530114D65D9EB5ECB,
	IOSWebViewWindow_Init_mCD581AE993D211CD8D460AAD73B90D6F35A923B3,
	IOSWebViewWindow_SetMargins_m2D09A6CD9A361539B3D42F16E90543BE60EFA9BE,
	IOSWebViewWindow_get_IsVisible_mFA30DD1B5EC50C8398D2B84E4547433B80E683BD,
	IOSWebViewWindow_set_IsVisible_mDD59F0E928521D6E4C8B4995432E03970577621F,
	IOSWebViewWindow_get_IsKeyboardVisible_m0548E5139A1ECDC7803E5B5EA74A83D530D507AC,
	IOSWebViewWindow_set_IsKeyboardVisible_m95F9A5A84319EA5CB06F1AEED5D2DF686FD82339,
	IOSWebViewWindow_get_AlertDialogEnabled_m7DF42FDAC56B43E948D22C209E4DE530E93AC2B5,
	IOSWebViewWindow_set_AlertDialogEnabled_m5059C70637C0123529E29CBE0B14247F2E235FC3,
	IOSWebViewWindow_get_ScrollBounceEnabled_m43F24B2ADE3FD07ABC54A3F4A415A8A28EA1BCC3,
	IOSWebViewWindow_set_ScrollBounceEnabled_m416820B44ACF05289D93285C80FEAFECAB81F9E6,
	IOSWebViewWindow_LoadURL_m995D6A3043ED0453223F56A90CC6347C0ABF6CC1,
	IOSWebViewWindow_LoadHTML_m62EAB80A3C07D23504B219BEB884807B21D7916A,
	IOSWebViewWindow_EvaluateJS_m5D1916FD9D51EA349F98FC4D52D75AA3F5B57252,
	IOSWebViewWindow_get_Progress_mD220D10E4827DDEFABBF0172ACB0742C009CD727,
	IOSWebViewWindow_CanGoBack_m03BC351C2C0F81E413CD6F68DCF3C32078F06431,
	IOSWebViewWindow_CanGoForward_mD91F1713B3559CEFB97913338D2072D20CD2FDF3,
	IOSWebViewWindow_GoBack_mEF56ADD66A3FAA662BA23957425117E376756454,
	IOSWebViewWindow_GoForward_mBC90420F582903FFD7FD7CDD4BB2BC8B7182DD0B,
	IOSWebViewWindow_Reload_mA5FC640D3CAEA08A1EDF5E9B350CEC06105F283F,
	IOSWebViewWindow_AddCustomHeader_m7F6993669F278A6E42618EBA9598FD2F10AE6518,
	IOSWebViewWindow_GetCustomHeaderValue_m70B535FB8AABFE81E7C149510030DCE904A96C34,
	IOSWebViewWindow_RemoveCustomHeader_mF3CF36589A6AC65E95285BBF902EBFCDDD4A2E09,
	IOSWebViewWindow_ClearCustomHeader_m60000FD500B764AEF2AA2810CDC117F0F3697AAA,
	IOSWebViewWindow_ClearCache_m901CFE7C80D47ED9F2A0E319A3E9759E7085DD6A,
	IOSWebViewWindow_ClearCookies_mC9E3E19377773037D974F092B8C97A9BD7C2352C,
	IOSWebViewWindow_GetCookies_m5686C288DF256B6904E2643818DB8D81C023FFC7,
	IOSWebViewWindow_SaveCookies_mF8467E8D2868AF0A473B5E3F22B3261D0F49AB4F,
	IOSWebViewWindow_SetBasicAuthInfo_mE2D13FBFA570498521EB6EAECDB7891FEA441996,
	IOSWebViewWindow_OnDestroy_mB747097FA32B03E96B790FC677512701A4E56077,
	IOSWebViewWindow__ctor_m5C11C8EDBE14F137D9A4FDF618B1E80D8028BC2A,
	NotSupportedWebviewWindow_get_IsWebViewAvailable_m35630CA78767025DDFA7EDA7E799F10AC7D5D482,
	NotSupportedWebviewWindow_Init_m8546014FDC1A4BC18866FF4071DFBDE34F4BBF5F,
	NotSupportedWebviewWindow_SetMargins_mA23E53A33C24CE2E61DEC5980604A48FF0D16FE7,
	NotSupportedWebviewWindow_get_IsVisible_m29351727A4C7901F5C6F02D3E6E3884055F257BA,
	NotSupportedWebviewWindow_set_IsVisible_m816574E59D63803E0E6CC63CB982012E3F2A7B81,
	NotSupportedWebviewWindow_get_IsKeyboardVisible_m5567E35450DD8E3BBFDBF5D208A3D65160949BEE,
	NotSupportedWebviewWindow_set_IsKeyboardVisible_m6A26227A35241101EBCAFC47B247E9F4C78BC1A1,
	NotSupportedWebviewWindow_get_AlertDialogEnabled_m9365C725ED784594CE69AE3BC260353C5A730135,
	NotSupportedWebviewWindow_set_AlertDialogEnabled_m0E69B062BA349C391461E01CB4E9B79BF7B4BF9D,
	NotSupportedWebviewWindow_get_ScrollBounceEnabled_m98A8B6B0AF3ADCC9CC5855556B7FA2901A838AEE,
	NotSupportedWebviewWindow_set_ScrollBounceEnabled_mB92F1A3E2E2C7FBA29A53A822BDB0C5FE67D7E92,
	NotSupportedWebviewWindow_LoadURL_m84D02B71C6A715976EC4BBA007289EA79D92DF83,
	NotSupportedWebviewWindow_LoadHTML_mA7028455D5EB60FFAC6E3C902821F8473135AB36,
	NotSupportedWebviewWindow_EvaluateJS_m2CAD79E7C6116A963E66F0C259BF7A4F93D3D526,
	NotSupportedWebviewWindow_get_Progress_m9C642D7F957E4B1AFABB3CFCB67EB8ABD597806D,
	NotSupportedWebviewWindow_CanGoBack_m2E5CA3DB283BE1A611416A58B891C8DC815DA8DE,
	NotSupportedWebviewWindow_CanGoForward_m47B173FBF541870BEF102B7495761463FAFFFCE8,
	NotSupportedWebviewWindow_GoBack_mB9C7617B4938DC9BFC8808EAC4EA305112C52E04,
	NotSupportedWebviewWindow_GoForward_mE43534A5947CB75269DC75CBB7B6E3AF2FB09F06,
	NotSupportedWebviewWindow_Reload_m4E522179FDB5B405AE94B2473F7D20BEAA597571,
	NotSupportedWebviewWindow_AddCustomHeader_mBFBB56F719E4840C65E6CDDE61C1834CADEB6DAF,
	NotSupportedWebviewWindow_GetCustomHeaderValue_m3D436CF2D3C764724FE30978401DE5E8C6EE44EE,
	NotSupportedWebviewWindow_RemoveCustomHeader_m320F2C1CE1C36673901DB84D61FA7E3E00C8AFDC,
	NotSupportedWebviewWindow_ClearCustomHeader_m941710B3CD640EDA564DC0EF0983F0D412BC01DA,
	NotSupportedWebviewWindow_ClearCookies_m8ADA34AFB9FF102E5A5D9928B6713FE7E2F49C5A,
	NotSupportedWebviewWindow_SaveCookies_mE27A26C91053BED73968ADDDF38450156DC28143,
	NotSupportedWebviewWindow_GetCookies_m11EEC48D0359226F6A8878249B1D84F101C97FDE,
	NotSupportedWebviewWindow_SetBasicAuthInfo_mDA3140679255F0F1467475868A9A95BDCD170E6C,
	NotSupportedWebviewWindow_ClearCache_m3FAC9A600CFB1CDB65101E54DEFA771373576587,
	NotSupportedWebviewWindow_OnDestroy_m4C139886C9076865F367300448372E0C85FF6A89,
	NotSupportedWebviewWindow__ctor_m58D72F9FBF9D6617D4A38AF408CC080462D4FD4B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WebviewWindowBase_CallFromJS_mED05D21C05D8AC77772FD86245C9852177A3FB27,
	WebviewWindowBase_CallOnHooked_m5563B5A45F54A266293FFB1F043C42E9045BA9C9,
	WebviewWindowBase_CallOnLoaded_mE21678B77304B10EEB125E0307D1E01DBFA205FC,
	WebviewWindowBase_CallOnStarted_m24D400B89F460B76F8D5B56EBD781B95CD22E284,
	WebviewWindowBase_CallOnError_mFE1903A1A4FA4B5DE5C7C376AB91FE7C693925C3,
	WebviewWindowBase_CallOnHttpError_m8410C1021158F781C3B9504F406914D2C9B56045,
	WebviewWindowBase__ctor_m71BE8A0F29B3A115500C713AC322E8A0EA1AD26A,
};
static const int32_t s_InvokerIndices[139] = 
{
	3675,
	679,
	4620,
	3675,
	679,
	3701,
	4563,
	4533,
	3675,
	3675,
	4620,
	3653,
	4620,
	4563,
	4533,
	4620,
	4533,
	4620,
	4855,
	6769,
	4903,
	6499,
	6499,
	6499,
	5439,
	6498,
	5938,
	6498,
	6769,
	6931,
	6931,
	7064,
	7064,
	7064,
	5938,
	6161,
	6498,
	7064,
	7186,
	7186,
	6888,
	5938,
	6499,
	3675,
	679,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	3675,
	1947,
	3675,
	4509,
	4563,
	4563,
	4620,
	4620,
	4620,
	1947,
	2684,
	3675,
	4620,
	3701,
	4620,
	2684,
	4620,
	1947,
	4620,
	4620,
	7176,
	3675,
	679,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	3675,
	1947,
	3675,
	4509,
	4563,
	4563,
	4620,
	4620,
	4620,
	1947,
	2684,
	3675,
	4620,
	4620,
	4620,
	2684,
	1947,
	3701,
	4620,
	4620,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	4563,
	3701,
	3675,
	679,
	3675,
	1947,
	3675,
	4509,
	4563,
	4563,
	4620,
	4620,
	4620,
	1947,
	2684,
	3675,
	4620,
	4620,
	4620,
	2684,
	1947,
	3701,
	3675,
	3675,
	3675,
	3675,
	3675,
	3675,
	4620,
};
extern const CustomAttributesCacheGenerator g_Webview_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Webview_CodeGenModule;
const Il2CppCodeGenModule g_Webview_CodeGenModule = 
{
	"Webview.dll",
	139,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Webview_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
