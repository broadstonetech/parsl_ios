﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.ComponentModel.DescriptionAttribute
struct DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// Newtonsoft.Json.JsonConverterAttribute
struct JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4;
// Newtonsoft.Json.JsonIgnoreAttribute
struct JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114;
// Newtonsoft.Json.JsonPropertyAttribute
struct JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Matrix4x4Converter_tFE6BF2451296004AE1E081BA4AE71A32D46DD0A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* QuaternionConverter_t53B5C879445DDD3AE899ADFA15AF5FB7A942F96A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TranslationConverter_t1402C3F33EB927F093C58C349C5A49C4ED23BCED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3Converter_t3D3FD7050305E7E50C86FB28D72849A954D8D91A_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ComponentModel.DescriptionAttribute
struct DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::description
	String_t* ___description_1;

public:
	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___description_1), (void*)value);
	}
};

struct DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA_StaticFields, ___Default_0)); }
	inline DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * get_Default_0() const { return ___Default_0; }
	inline DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// Newtonsoft.Json.JsonConverterAttribute
struct JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Newtonsoft.Json.JsonConverterAttribute::_converterType
	Type_t * ____converterType_0;
	// System.Object[] Newtonsoft.Json.JsonConverterAttribute::<ConverterParameters>k__BackingField
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___U3CConverterParametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__converterType_0() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4, ____converterType_0)); }
	inline Type_t * get__converterType_0() const { return ____converterType_0; }
	inline Type_t ** get_address_of__converterType_0() { return &____converterType_0; }
	inline void set__converterType_0(Type_t * value)
	{
		____converterType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____converterType_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4, ___U3CConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_U3CConverterParametersU3Ek__BackingField_1() const { return ___U3CConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_U3CConverterParametersU3Ek__BackingField_1() { return &___U3CConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___U3CConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConverterParametersU3Ek__BackingField_1), (void*)value);
	}
};


// Newtonsoft.Json.JsonIgnoreAttribute
struct JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Newtonsoft.Json.DefaultValueHandling
struct DefaultValueHandling_t8C0F4C0B53CD53E979CCC30500D5E226619A2C1F 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t8C0F4C0B53CD53E979CCC30500D5E226619A2C1F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// Newtonsoft.Json.NullValueHandling
struct NullValueHandling_t6E78D075517457213329ADA45589150FAF5DDA82 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t6E78D075517457213329ADA45589150FAF5DDA82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Newtonsoft.Json.ObjectCreationHandling
struct ObjectCreationHandling_t9A8DEC546FF320F7CD2A9B2D6A6E4C46B70720A3 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t9A8DEC546FF320F7CD2A9B2D6A6E4C46B70720A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// Newtonsoft.Json.ReferenceLoopHandling
struct ReferenceLoopHandling_t86A953E12C98CC222AEE20C44996424CBE266D89 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t86A953E12C98CC222AEE20C44996424CBE266D89, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Newtonsoft.Json.Required
struct Required_tF6FF5ABC50DA127B2E300315C53E18C26E5B13B0 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_tF6FF5ABC50DA127B2E300315C53E18C26E5B13B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// Newtonsoft.Json.TypeNameHandling
struct TypeNameHandling_t7D26906BA4ED9E93294CDA393052F3C7989C3405 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t7D26906BA4ED9E93294CDA393052F3C7989C3405, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.Required>
struct Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Newtonsoft.Json.JsonPropertyAttribute
struct JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_nullValueHandling
	Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA  ____nullValueHandling_0;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_defaultValueHandling
	Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F  ____defaultValueHandling_1;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_referenceLoopHandling
	Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  ____referenceLoopHandling_2;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonPropertyAttribute::_objectCreationHandling
	Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2  ____objectCreationHandling_3;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_typeNameHandling
	Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  ____typeNameHandling_4;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_isReference
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ____isReference_5;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonPropertyAttribute::_order
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  ____order_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonPropertyAttribute::_required
	Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7  ____required_7;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_itemIsReference
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ____itemIsReference_8;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemReferenceLoopHandling
	Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  ____itemReferenceLoopHandling_9;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemTypeNameHandling
	Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  ____itemTypeNameHandling_10;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_11;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___U3CItemConverterParametersU3Ek__BackingField_12;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<NamingStrategyType>k__BackingField
	Type_t * ___U3CNamingStrategyTypeU3Ek__BackingField_13;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<NamingStrategyParameters>k__BackingField
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___U3CNamingStrategyParametersU3Ek__BackingField_14;
	// System.String Newtonsoft.Json.JsonPropertyAttribute::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of__nullValueHandling_0() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____nullValueHandling_0)); }
	inline Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA  get__nullValueHandling_0() const { return ____nullValueHandling_0; }
	inline Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA * get_address_of__nullValueHandling_0() { return &____nullValueHandling_0; }
	inline void set__nullValueHandling_0(Nullable_1_t3290292B4BADA5D4A183FF666136C62C9E4B36CA  value)
	{
		____nullValueHandling_0 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_1() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____defaultValueHandling_1)); }
	inline Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F  get__defaultValueHandling_1() const { return ____defaultValueHandling_1; }
	inline Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F * get_address_of__defaultValueHandling_1() { return &____defaultValueHandling_1; }
	inline void set__defaultValueHandling_1(Nullable_1_tF3FB4D4A8BEF8F50059DBE9C68FD78313513ED6F  value)
	{
		____defaultValueHandling_1 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_2() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____referenceLoopHandling_2)); }
	inline Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  get__referenceLoopHandling_2() const { return ____referenceLoopHandling_2; }
	inline Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89 * get_address_of__referenceLoopHandling_2() { return &____referenceLoopHandling_2; }
	inline void set__referenceLoopHandling_2(Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  value)
	{
		____referenceLoopHandling_2 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_3() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____objectCreationHandling_3)); }
	inline Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2  get__objectCreationHandling_3() const { return ____objectCreationHandling_3; }
	inline Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2 * get_address_of__objectCreationHandling_3() { return &____objectCreationHandling_3; }
	inline void set__objectCreationHandling_3(Nullable_1_t50AC324268CFB9729259727D985A90820D5F64F2  value)
	{
		____objectCreationHandling_3 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_4() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____typeNameHandling_4)); }
	inline Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  get__typeNameHandling_4() const { return ____typeNameHandling_4; }
	inline Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64 * get_address_of__typeNameHandling_4() { return &____typeNameHandling_4; }
	inline void set__typeNameHandling_4(Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  value)
	{
		____typeNameHandling_4 = value;
	}

	inline static int32_t get_offset_of__isReference_5() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____isReference_5)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get__isReference_5() const { return ____isReference_5; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of__isReference_5() { return &____isReference_5; }
	inline void set__isReference_5(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		____isReference_5 = value;
	}

	inline static int32_t get_offset_of__order_6() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____order_6)); }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  get__order_6() const { return ____order_6; }
	inline Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * get_address_of__order_6() { return &____order_6; }
	inline void set__order_6(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  value)
	{
		____order_6 = value;
	}

	inline static int32_t get_offset_of__required_7() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____required_7)); }
	inline Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7  get__required_7() const { return ____required_7; }
	inline Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7 * get_address_of__required_7() { return &____required_7; }
	inline void set__required_7(Nullable_1_tF4A54D2E6763EFD61DA565D5C7833E1EC15C63E7  value)
	{
		____required_7 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_8() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____itemIsReference_8)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get__itemIsReference_8() const { return ____itemIsReference_8; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of__itemIsReference_8() { return &____itemIsReference_8; }
	inline void set__itemIsReference_8(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		____itemIsReference_8 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_9() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____itemReferenceLoopHandling_9)); }
	inline Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  get__itemReferenceLoopHandling_9() const { return ____itemReferenceLoopHandling_9; }
	inline Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89 * get_address_of__itemReferenceLoopHandling_9() { return &____itemReferenceLoopHandling_9; }
	inline void set__itemReferenceLoopHandling_9(Nullable_1_t3F96A3D4FEBC0E4866061C7E7E4D1730817C5C89  value)
	{
		____itemReferenceLoopHandling_9 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_10() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ____itemTypeNameHandling_10)); }
	inline Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  get__itemTypeNameHandling_10() const { return ____itemTypeNameHandling_10; }
	inline Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64 * get_address_of__itemTypeNameHandling_10() { return &____itemTypeNameHandling_10; }
	inline void set__itemTypeNameHandling_10(Nullable_1_tE7FF89EEDCE61D545B930EC2293222CEE351ED64  value)
	{
		____itemTypeNameHandling_10 = value;
	}

	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ___U3CItemConverterTypeU3Ek__BackingField_11)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_11() const { return ___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_11() { return &___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_11(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CItemConverterTypeU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ___U3CItemConverterParametersU3Ek__BackingField_12)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_U3CItemConverterParametersU3Ek__BackingField_12() const { return ___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_U3CItemConverterParametersU3Ek__BackingField_12() { return &___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_12(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CItemConverterParametersU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNamingStrategyTypeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ___U3CNamingStrategyTypeU3Ek__BackingField_13)); }
	inline Type_t * get_U3CNamingStrategyTypeU3Ek__BackingField_13() const { return ___U3CNamingStrategyTypeU3Ek__BackingField_13; }
	inline Type_t ** get_address_of_U3CNamingStrategyTypeU3Ek__BackingField_13() { return &___U3CNamingStrategyTypeU3Ek__BackingField_13; }
	inline void set_U3CNamingStrategyTypeU3Ek__BackingField_13(Type_t * value)
	{
		___U3CNamingStrategyTypeU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNamingStrategyTypeU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNamingStrategyParametersU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ___U3CNamingStrategyParametersU3Ek__BackingField_14)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_U3CNamingStrategyParametersU3Ek__BackingField_14() const { return ___U3CNamingStrategyParametersU3Ek__BackingField_14; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_U3CNamingStrategyParametersU3Ek__BackingField_14() { return &___U3CNamingStrategyParametersU3Ek__BackingField_14; }
	inline void set_U3CNamingStrategyParametersU3Ek__BackingField_14(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___U3CNamingStrategyParametersU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNamingStrategyParametersU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F, ___U3CPropertyNameU3Ek__BackingField_15)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_15() const { return ___U3CPropertyNameU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_15() { return &___U3CPropertyNameU3Ek__BackingField_15; }
	inline void set_U3CPropertyNameU3Ek__BackingField_15(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPropertyNameU3Ek__BackingField_15), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.ComponentModel.DescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DescriptionAttribute__ctor_m88B8CF110270B3759BE4A9D17D6B1A4A17305D9E (DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * __this, const RuntimeMethod* method);
// System.Void Newtonsoft.Json.JsonConverterAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640 (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * __this, Type_t * ___converterType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void Newtonsoft.Json.JsonPropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * __this, const RuntimeMethod* method);
// System.Void Newtonsoft.Json.JsonPropertyAttribute::set_Required(Newtonsoft.Json.Required)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02 (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Newtonsoft.Json.JsonIgnoreAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void RuntimeTest_t8F75E56B9C4897AF129DDA399D9F24161C2C648B_CustomAttributesCacheGenerator_AvatarURL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WebviewTest_tF2A977D83543F0F0D00F0EA238DD572A4B879C82_CustomAttributesCacheGenerator_loadingLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_AvatarLoader_get_Timeout_mE8CB3BCEC9F34E86871DE081D1A330578C69EF3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_AvatarLoader_set_Timeout_mD13FEA8E0424644DB952C51D62791745CDDB68CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_LoadAvatarAsync_m1ECC417961699E00B7288CA0D359E51641B0177F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_0_0_0_var), NULL);
	}
}
static void LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_DownloadAvatar_m8C41A534B940C0DB563C4C38D868D9E16FE14C4D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_0_0_0_var), NULL);
	}
}
static void LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_PrepareAvatarAsync_mAFE7F9F3A13710DE1D43AF1133462C51F1518A76(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_0_0_0_var), NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2__ctor_mBD4F78947512C1C4071220D62833955E48D55EE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_m0CBBA5987404C4EC2B71743C4BA0F0831EE98D9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD133114924F55ADBDF3A6CAAE4061E50CAC7A9E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mB19A8FCBACFD78E2F5825DC6995D0723E30CBE85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_mD85B71B8CD797CA29F0378FEED1261B7E8321080(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3__ctor_m68352DFBC6229C147598D5857B04E6ADEF630B85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mC275D51C7FF17A0C516114FF31C4E2D6CB73F551(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD843D5776566598C09ACDB97436644089FECE376(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m8334C59C0F1A6BC4B759C04AD6760BC32EF6301A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_mFB2775F3A311FBB224A9179EF21700527E1B33D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5__ctor_mC4B4D228743E776D60E4E03F426C756904843118(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_IDisposable_Dispose_mF77F6CDFC1C997744FB30300C5CE96795AE19ECC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF636842F021B15398C2F08AF5EB996DC63C0425(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m1FEAF518F8173872C6C8E22047A733575E8E9CC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_m9BBD626EA4994CB9449CF0E46F2273F2E19E642B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_get_Timeout_m751F8141D184C514A2921289CA5C2D158925F2B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_set_Timeout_m8037ABC73D784A1701B268248C1653CC73FD09AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_LoadAvatar_mB3A3FB205BF0A074FAFFFF5BAA8B20526B17A917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_0_0_0_var), NULL);
	}
}
static void AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_DownloadMetaData_m200441C7F7CE70C78D87747769B1CAEC7C60D74F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_0_0_0_var), NULL);
	}
}
static void U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_CustomAttributesCacheGenerator_U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22__ctor_m28B2083136EB545C725ED009EBBFE7A5BB8BD813(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_IDisposable_Dispose_mB1728B9B8A43C8600AAD903B7BD21363587EDF66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A9228438F9EC5F8959B62258365983010A2483B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_Reset_mBC340BC3995F6746CAEC44E1096EF92C709DC67B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_get_Current_mCE4A69299659B9C8D5DCE6C126C1D33E7A6938C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EditorAvatarLoader_tFBB0776FACBE78338751996A640755C048B3AA64_CustomAttributesCacheGenerator_EditorAvatarLoader_LoadAvatarAsync_mAFCF3C276AA7B1B716C234BEF2CED8D28DACA28F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_0_0_0_var), NULL);
	}
}
static void EditorAvatarLoader_tFBB0776FACBE78338751996A640755C048B3AA64_CustomAttributesCacheGenerator_EditorAvatarLoader_DownloadAvatar_mD546367EBD7548B38CDE90EFB47032A424403882(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_0_0_0_var), NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2__ctor_m33FA85ABA06CE6BB59613671CDD337BE19933C95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_mA359D6B2EDEC85F23AFF2C7A98B21AF9DE7F9EFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AFFF62195D799E9059CEC04FD7ACF19988D46AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mF50263B27F63A1E65E26495BF41E351FD6011120(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_m46EABEC84BC757D097187BBB5F759C594BFB78ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t96D2C15A2E479823466C4DE24847CE9D1027268E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3__ctor_mFDBEDB706E0A529EA40637D297209930BAC2ECE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mB2353DD0FEC9E18048409BDB9C2D4CB6725B6D6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43F2C2D1AA214B91EA1926994D2052EE57B8B5E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m1B694D6F834ED62F52DB8CEA18448795201387CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_m630C20BDF06581F5C703266612A613384F544080(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x61\x64\x79\x20\x50\x6C\x61\x79\x65\x72\x20\x4D\x65\x2F\x45\x79\x65\x20\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x48\x61\x6E\x64\x6C\x65\x72"), 0LL, NULL);
	}
}
static void EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator_blinkSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator_EyeAnimationHandler_BlinkEyes_m4A936B47896A1360C3F64FCF080281F38E614556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_0_0_0_var), NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22__ctor_m0E1712F32F0BF56CD99EDB68EAB6293E79A892EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_IDisposable_Dispose_mB7812B1354202F0649E708CB552E0913EDC7C169(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21A7148FA71D14491CF4D751C5253D5C7378B5E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_Reset_m9190DCEEDA0E422FE8E5A55599E8BF390C1270A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_get_Current_mD837E0FCA30EF9CF3DB882D895B6581C550E9BCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x61\x64\x79\x20\x50\x6C\x61\x79\x65\x72\x20\x4D\x65\x2F\x56\x6F\x69\x63\x65\x20\x48\x61\x6E\x64\x6C\x65\x72"), 0LL, NULL);
	}
}
static void VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator_VoiceHandler_CheckIOSMicrophonePermission_m616B6A6A24D700321262DC6C3E9BFA6123BF79B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_0_0_0_var), NULL);
	}
}
static void VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator_VoiceHandler_U3CSetBlendshapeWeightsU3Eg__SetBlendShapeWeightU7C22_0_m41B18EE5CDEBA498B926EBA87611CBACAB08439A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tBF048089ECEF78F2711F6499F26A88AF5FFB544E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23__ctor_m264C205C0BE8F3073576CE32182AA5F0355C38A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_IDisposable_Dispose_m0B021614E8EEB33ED6F76C90E73991D7010EE85D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m932D7A3FFA7C4E9FBD405B0F5E6139FF797C0442(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_Reset_mE4D19B2D8261B6BC433381F815813EA2A43CC4D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_get_Current_m32974A9C230173BA61BD2D8F28060675019EE461(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CExtensionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CModelNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CModelPathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsoluteUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsolutePathU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsoluteNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CMetaDataUrlU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_Extension_m7CB982F51D2BC6808595132919BDDAA41553C5C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_Extension_mF323C6D000A26393118A08C8AB59B2D14DEA5898(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_ModelName_m7C276606C508001D9F08A945A557E6AD32DD334F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_ModelName_m0B24DB53617858C7B3BFBA4CBD027B1C3B4F1284(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_ModelPath_m6D3C07770079C06DB146CFE587697662049CD4B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_ModelPath_m10161E6936F6A9B3BC4E7FADF82437E98167A36B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsoluteUrl_mC941D4F13653D6E106E747785E35D8E37BFB854A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsoluteUrl_m0F2DAA5A296AC51CCA4E93C61F08AEB09D76B732(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsolutePath_m9C4FAE3747B4467F9E049337A6A35A663E717CA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsolutePath_m0A0348B71E1A7438CB60BD36F4DB3FBB3616A828(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsoluteName_mB90E1916C3D359B671166D2104EF207EC8EEB487(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsoluteName_mB5FFDFE732D8E1F48AFA2A904AF29EAFEB7648F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_MetaDataUrl_m409F1074743073E4118DD865A7232A46B3AF1702(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_MetaDataUrl_m2BFE9FFD795BB7413C010C4287A633B198E3CF1F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_Create_m248A0429CF7D4306570B2808B051E1683814C184(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_0_0_0_var), NULL);
	}
}
static void AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_GetUrlFromShortCode_m270A0D285ED4C8DA39692B922DFF3C20591BA134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_0_0_0_var), NULL);
	}
}
static void U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_CustomAttributesCacheGenerator_U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_CustomAttributesCacheGenerator_U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BodyType_tD4C6BAA11C5F73A89D682291DC21FDD9CFCE6A8E_CustomAttributesCacheGenerator_Fullbody(CustomAttributesCache* cache)
{
	{
		DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * tmp = (DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA *)cache->attributes[0];
		DescriptionAttribute__ctor_m88B8CF110270B3759BE4A9D17D6B1A4A17305D9E(tmp, il2cpp_codegen_string_new_wrapper("\x66\x75\x6C\x6C\x62\x6F\x64\x79"), NULL);
	}
}
static void BodyType_tD4C6BAA11C5F73A89D682291DC21FDD9CFCE6A8E_CustomAttributesCacheGenerator_Halfbody(CustomAttributesCache* cache)
{
	{
		DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * tmp = (DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA *)cache->attributes[0];
		DescriptionAttribute__ctor_m88B8CF110270B3759BE4A9D17D6B1A4A17305D9E(tmp, il2cpp_codegen_string_new_wrapper("\x68\x61\x6C\x66\x62\x6F\x64\x79"), NULL);
	}
}
static void ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_Run_mA83E080C59D109AD7966D664986900522591A182(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_GetMeshRenderer_m7E058B376E70F57DF1238B386580D6E3BEC1F0E7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_U3CGetMeshRendererU3Eg__GetMeshU7C7_3_m5C71F949054653625ECF6227E9D0B5F6F847200B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tF2780C26A5D7459DA62956B790E4242C580B8AD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tE595BC76471D8A6FB95EE31C87A2719737562EE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void OutfitGender_t7CA9120C0B5220645579E788B813E8CB7DC7FB9C_CustomAttributesCacheGenerator_Masculine(CustomAttributesCache* cache)
{
	{
		DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * tmp = (DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA *)cache->attributes[0];
		DescriptionAttribute__ctor_m88B8CF110270B3759BE4A9D17D6B1A4A17305D9E(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x61\x73\x63\x75\x6C\x69\x6E\x65"), NULL);
	}
}
static void OutfitGender_t7CA9120C0B5220645579E788B813E8CB7DC7FB9C_CustomAttributesCacheGenerator_Feminine(CustomAttributesCache* cache)
{
	{
		DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA * tmp = (DescriptionAttribute_tBDFA332A8D0CD36C090BA99110241E2A4A6F25DA *)cache->attributes[0];
		DescriptionAttribute__ctor_m88B8CF110270B3759BE4A9D17D6B1A4A17305D9E(tmp, il2cpp_codegen_string_new_wrapper("\x66\x65\x6D\x69\x6E\x69\x6E\x65"), NULL);
	}
}
static void EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator_EnumExtensions_ByteSize_m35DC2853AF8B37B64D323F8334391B278BDF9A5E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator_EnumExtensions_ComponentCount_m49267065A12FAA7BC7594663FDEDC27EF2B2A1DC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_RunCoroutine_m0CA03A7C0B5CB0A6CB46D0F21EFED6DE5E3011B2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_SubArray_m2548AEBAFB5AB8510EE1F1472B5CC1AB0DA28E54(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_UnpackTRS_mF30329BE0D6854C0EDEB2CFA0F03506703B25339(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator_offset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_0_0_0_var), NULL);
	}
}
static void KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator_scale(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_0_0_0_var), NULL);
	}
}
static void Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_LoadInternal_m5B17FBF2634BA38FBA7BAF2FE9B4479660E0A4DC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_LoadAsync_m4FC08F596DD7774E818976E385460DD131E93398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_0_0_0_var), NULL);
	}
}
static void Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_TaskSupervisor_m7DD1809D6E78E40446C69FBDD60D87926EB76DF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_0_0_0_var), NULL);
	}
}
static void ImportTask_1_tE4E9B791A65AB573A493962BE745CFA5FC732BBF_CustomAttributesCacheGenerator_ImportTask_1__ctor_mD1959FA5210950FF127E465E43445910C25A2432____waitFor0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_get_IsCompleted_mF1257CE54E903AA412CB894E7F1BA17227353B4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_set_IsCompleted_m505F23568DDF7C554EE27016272CEE74A71F214B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask__ctor_mA8C2D32FF44B54CC145E63E941CDE524EF7E4A93____waitFor0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m728AA9D6F6B6E6C99C57A8B3FFE0BCFDDD317EED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t34E4F3D9ED7FC530D8821E9C7B801ABE1220D0B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9__ctor_m48041C4FB14FB5782E074B0C495000063148026C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_IDisposable_Dispose_m67ABBF5C56A17B98AF1420671F6CDA4D7C9E5A1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9AB5731BBA2862D0FB21F18BC99898273B0BC0A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m293618DFBDD8ECC5807A2634D4D9485B11229E8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mCC55F658F9053F6137398A7A715B3B0F7BB823D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t665355825E495772964C636FD8E7BA8F793C6C38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t1F96567CB3BF5E4E8CDA260362F3A44B4994783A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16__ctor_m76847F3E08DDF071B407B405D7ADABBC307726AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_IDisposable_Dispose_mD51CF56256EB3889DCA5538E6F7568F1EE24DD68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C24BE0512ABFB360B2F421DEA952FD566645D14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_Reset_m0D9F51120F9B7E43A23C9DE624925382DA3160A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_get_Current_m365D7F07B28C4C5807A32EB050BF876E6D85D55C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17__ctor_mF99DE5C7C4C73E2783E7776108CA8B3ABE42645E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_IDisposable_Dispose_m07973E18B963E1230482ECB0589595E0DB9DF5AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49D10D59FCF34D9FDA6D7EAAAE8AE1B10FF12E29(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_Reset_mF7742948AC166E4AABD25D9C2105F6B5519635E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_get_Current_m219B092F317FA9E9F45B3319589854F628F5F065(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImportSettings_tD7535F5CF15688A065D6C18845C33748BA9941B4_CustomAttributesCacheGenerator_shaderOverrides(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x73\x68\x61\x64\x65\x72\x73"), NULL);
	}
}
static void ImportSettings_tD7535F5CF15688A065D6C18845C33748BA9941B4_CustomAttributesCacheGenerator_interpolationMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x69\x6F\x6E\x20\x6D\x6F\x64\x65\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x61\x6C\x6C\x20\x6B\x65\x79\x66\x72\x61\x6D\x65\x20\x74\x61\x6E\x67\x65\x6E\x74\x73\x2E\x20\x55\x73\x65\x20\x49\x6D\x70\x6F\x72\x74\x20\x46\x72\x6F\x6D\x20\x46\x69\x6C\x65\x20\x77\x68\x65\x6E\x20\x6D\x69\x78\x69\x6E\x67\x20\x6D\x6F\x64\x65\x73\x20\x77\x69\x74\x68\x69\x6E\x20\x61\x6E\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_metallic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_metallicBlend(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_specular(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_specularBlend(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[1];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var), NULL);
	}
}
static void GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_componentType(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_count(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_count(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_indices(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_values(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Values_tDADFFEB11BD5FF710A927F980DC2566F82EB7066_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Values_tDADFFEB11BD5FF710A927F980DC2566F82EB7066_CustomAttributesCacheGenerator_bufferView(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator_bufferView(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator_componentType(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void ImportResult_t603044289D118580E77ACEDE6B53D34C74FF6CDC_CustomAttributesCacheGenerator_ImportResult_ValidateAccessorTypeAny_m5CE190A5035881071F70FF155FB7FE9CF408BD72____expected1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec_t006EB07EA4E92958FC9DB4FAF4CB8426A4327553_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t37A50EA00661912DB378C3926023C66B4BA49933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator_channels(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator_samplers(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_interpolation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var), NULL);
	}
}
static void Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator_sampler(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Target_t823791F26903732C37CD778646D83823DA211878_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Target_t823791F26903732C37CD778646D83823DA211878_CustomAttributesCacheGenerator_path(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void ImportResult_t4BC85111F8A37459B2C2C8658EF37EAAD3005972_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void U3CU3Ec_tF6877DC6B82C85937019CE77BEF2F5C988A2F801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFAnimationExtensions_tC926EB1024229B2BD4B1654EE45DF7E627966BD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GLTFAnimationExtensions_tC926EB1024229B2BD4B1654EE45DF7E627966BD3_CustomAttributesCacheGenerator_GLTFAnimationExtensions_Import_m3F291F7CC01854BE61DF4FE8617630A80E8D55F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GLTFAsset_t7E6EB860C304A56344C0BC41A0E06E690B16F53B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFAsset_t7E6EB860C304A56344C0BC41A0E06E690B16F53B_CustomAttributesCacheGenerator_version(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_byteLength(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_embeddedPrefix(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_embeddedPrefix2(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t512B53CE276BE04FD898DA180CDCFAE12D68A8AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator_byteLength(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t3109B0A53E2C727EA7DE746045C8D91F4F406624_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFCamera_tD5DE3162635C4D5D1A3BE2943F57D7B67B3C1A6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFCamera_tD5DE3162635C4D5D1A3BE2943F57D7B67B3C1A6E_CustomAttributesCacheGenerator_type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[1];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var), NULL);
	}
}
static void Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_xmag(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_ymag(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_zfar(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_znear(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator_yfov(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator_znear(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFImage_t607BB5ADF8AD49B8C9DD2C3F25D726BD0E8921D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void ImportResult_tBCDEC5F4DFC38E35CE68A9860446C77418A3BFBE_CustomAttributesCacheGenerator_ImportResult_CreateTextureAsync_mE370E435B53F299305E45F88E0435DFF6DF0EE93(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_0_0_0_var), NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3__ctor_m41C4795268B5A5FDBD09AAC759CB825B4E7ADDEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_IDisposable_Dispose_m309F94D77BA47F75B8D25738544C71739FF6FE9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BA6DA399F310B5E0D0BE1275626D33722409938(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_Reset_mC1E6231288207824BC54FEB4B4706F8AD9A2F8EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_get_Current_m0581A5FAAE854B83BCF99A47E55DAF51A4F0387B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tF349D9620CD5F90C02833B72D36702355BD6C6F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_emissiveFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_0_0_0_var), NULL);
	}
}
static void GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_alphaMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_0_0_0_var), NULL);
	}
}
static void GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_GLTFMaterial_CreateMaterial_m1BFA5DB15E037943CB2915DFB9B2001020343636(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_0_0_0_var), NULL);
	}
}
static void GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_GLTFMaterial_TryGetTexture_m8BF1941ECA59813B13C2CD0DC135FC798F05043A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_0_0_0_var), NULL);
	}
}
static void Extensions_tA41E692D15632B96E5E96A35683856971BD20355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator_baseColorFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_0_0_0_var), NULL);
	}
}
static void PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator_PbrMetalRoughness_CreateMaterial_m510DD5D199D7102C9C43BBC3EB580743EBE52DAE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t7CFDBB40BE1615176D7C7CCDDD54C971271B4B00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5__ctor_m0A51ABDB657E7515358686A7C789F8BBC753C944(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m13CED19482847457E82EBDB9245B7CD31CB07240(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDDD4A652A44B7176224930CF73F79BD54250E8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_mBC061664859BF0BCFDE8D797FFFC0B168990E006(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_mFE01300D7830C2D0E963D4EB1C7BB3CF05891049(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_diffuseFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_0_0_0_var), NULL);
	}
}
static void PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_specularFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_0_0_0_var), NULL);
	}
}
static void PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_PbrSpecularGlossiness_CreateMaterial_m87F25CC94EAABAFC3DEAE2FC18C02FA6B3C3F53D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tCAA98420487ABC40FCBFA2A1BE900065C69836FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5__ctor_m44A29489BC2A499F5B2F902E55565A4FBA517217(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m12CE43689F6D8E80891D8B58C9734A3D34E2B981(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D6317ECF4FAC9564EDB508BF2C605119B8AC149(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_m02FC2F8E28E73A773C1F110BF34CEAB125EAD61D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_m50CBFD918D48E2F4F72CBC1D965FFC4848C89846(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextureInfo_tA7EAB793C527BE050263842A593DC13C912F0EA6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void TextureInfo_tA7EAB793C527BE050263842A593DC13C912F0EA6_CustomAttributesCacheGenerator_index(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void Extensions_t9C586AEDFF1C1A6B5AF887A5F573EB79EC91DCC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void ImportTask_t1DCBC2FE8300738DF3EBB9472642609F927F9A49_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m126F3069CBD3CB5D2B22552C9654A993A7E4AB81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tBFB1A418CE51A0BA22BD24D319F218A458DCFD23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t9812C6C85F9D4073BBC472078AEEB2CD321E777B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4__ctor_mC000ADF62EDF90D65CCEF5F37D3B0545DA3969AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_IDisposable_Dispose_m839B941EFE59407CAC6CFF970CE785DDCB4A525D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFE681C78510873E7CBC57648CD563EACCF5393(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m63FF5A76FE5B7F03B362CECBF4B0CFDE61304E0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m9EAB85B83380E6DBCED03E066A9EE672642ED63B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tE72A73A0AE0498ED22552C0EDD5CC6B664330602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13__ctor_m906F8A69518299C3BE456AB85AD3D56E2CAB0E5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_IDisposable_Dispose_mB221ED8C778CB3745D04CF99F434E2FED1FBFE09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FE40E1FC57B8887B00B84B6167F1EA8C98A005D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_Reset_m42BEBD986CDAECFAC5912B7188178DF75B39F309(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_get_Current_m9A6DBBE0CD322FC365D0EC8669134D2CF62C4C35(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14__ctor_m49A3D3B99F86821FB6B1D9088E1FB565431BA72F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_IDisposable_Dispose_m2C0609E384C9CA7CD0692B5FFDED45328ACE9E2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AA5956C39B5D0E45D48C93B3B5F49395438AD99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_Reset_m6F5354F3EBCA5488FBFE81D513788587166AA037(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_get_Current_mE2011302FAE1B32CC0672009985C052CA2ED8FFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GLTFMesh_t65AF1AB0F638BA6A3DC301A0D98AF05081258E8B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFMesh_t65AF1AB0F638BA6A3DC301A0D98AF05081258E8B_CustomAttributesCacheGenerator_primitives(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void ImportTask_tC0DBCD6E017843816ED108DB3B120997E96C04E6_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_mD6781BEBFC39FEA29FF35630A736F1D2148FC2F3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t01C67E2404F68AF83F0986FC4EC376F6D2CBD4EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_1_tF42B31809592552E7794C7AA606C1E0D3747361B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tE3DD77F3EE27CF1DB096B5E24ADC88BE7558D73B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t133C9ADFE61D76451B182ED3028A3E9D29593F4D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5__ctor_m10BD190BCCD4CA5D3DE6E9E67C56D841671D3738(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m519BC153E6FD02D1D79F068287499513399CCBAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CDE424A2B66B71D81B41A452ED5272CC3254BBE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCD839561719DB78CC9B400ABC9044F9D8D65A945(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m27C3951830591BFBDD37468D99DFF91264FEAF9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ExportResult_t38C487FE254355FCC75A555B609446DEE1DB9F46_CustomAttributesCacheGenerator_mesh(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_translation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TranslationConverter_t1402C3F33EB927F093C58C349C5A49C4ED23BCED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(TranslationConverter_t1402C3F33EB927F093C58C349C5A49C4ED23BCED_0_0_0_var), NULL);
	}
}
static void GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_rotation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&QuaternionConverter_t53B5C879445DDD3AE899ADFA15AF5FB7A942F96A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(QuaternionConverter_t53B5C879445DDD3AE899ADFA15AF5FB7A942F96A_0_0_0_var), NULL);
	}
}
static void GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_scale(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3Converter_t3D3FD7050305E7E50C86FB28D72849A954D8D91A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(Vector3Converter_t3D3FD7050305E7E50C86FB28D72849A954D8D91A_0_0_0_var), NULL);
	}
}
static void GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C____matrix_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Matrix4x4Converter_tFE6BF2451296004AE1E081BA4AE71A32D46DD0A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 * tmp = (JsonConverterAttribute_t2CE0A52816561E6F6ACAE6BB0FC37C23582713A4 *)cache->attributes[0];
		JsonConverterAttribute__ctor_m675FD83C6B5C76080F669CF77F7E31A4A20CD640(tmp, il2cpp_codegen_type_get_object(Matrix4x4Converter_tFE6BF2451296004AE1E081BA4AE71A32D46DD0A3_0_0_0_var), NULL);
	}
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[1];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
	}
}
static void ImportTask_t8367625A09A8B31BD2851B338307B9C847575D24_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m9F5F609BFEA83B8E95A3EFA0C47E70CBC0B506A2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tED7E73FB15B2C4E12598F62F64445EC535BFF6B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5__ctor_mA8A51B40E3A30D79F493009EAEFAA8487B2DB3FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m858F98D21F6F66A0CBA1350D4CE8684CBE2BD24E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDAFFD7EE3601FC7392DCE575962FB08F6247FF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m488AE3FB7BA142292F30FA08BAEBF3FA87CB8FD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF44DD69E2CE4C8639CC4D7D7A9011401805C3E1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_renderer(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_filter(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_skinnedRenderer(CustomAttributesCache* cache)
{
	{
		JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 * tmp = (JsonIgnoreAttribute_t983119A2A022475046CFFD04C8FBF8FBA3C91114 *)cache->attributes[0];
		JsonIgnoreAttribute__ctor_m1CC0226556458883EA15F583D236E032E26696EF(tmp, NULL);
	}
}
static void GLTFNodeExtensions_tB4B5EBDE856313878CDEABDDE33F706108F0387A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GLTFNodeExtensions_tB4B5EBDE856313878CDEABDDE33F706108F0387A_CustomAttributesCacheGenerator_GLTFNodeExtensions_GetRoot_m16F36DAA9BC509FC186B083147EE9BBCEBEC1951(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_t5B9868C22EEEFFD38A10F9915AD4526375912CD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFObject_t1A0A942ACCB7F183CD1E0980146A5661F29D8D09_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFObject_t1A0A942ACCB7F183CD1E0980146A5661F29D8D09_CustomAttributesCacheGenerator_asset(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFPrimitive_tE5D5897D771F30CE1A157F5BF66B66CB09468D57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFPrimitive_tE5D5897D771F30CE1A157F5BF66B66CB09468D57_CustomAttributesCacheGenerator_attributes(CustomAttributesCache* cache)
{
	{
		JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F * tmp = (JsonPropertyAttribute_t35C7337D1B0B52352D307B807B63E9BE6651C84F *)cache->attributes[0];
		JsonPropertyAttribute__ctor_m2B33AC3785B377311D7C6AD49D224B12ACC9333C(tmp, NULL);
		JsonPropertyAttribute_set_Required_m435E09041EC5B1647F7A5A9BEEE09FA67A8EDA02(tmp, 2LL, NULL);
	}
}
static void GLTFAttributes_t87ACC135A7EDEFDE7BAAC3337F44F14E7F216341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFProperty_t534A3EEEC455C71F4601A295D1694E507D0592C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFScene_tB66CF7671BAD06DF23541A7986AE871D0C994E7B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void GLTFSkin_t3A4E93997D1F4A0932078436C074B69EA8289EBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tCF85E9B16E772D485B2C73F01A12A94FF4F14F12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GLTFTexture_tF66215795806552AD402542275F41AB016B561FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void ImportResult_t86275B67075F847B7EF7D1DD77D72EAFA134A533_CustomAttributesCacheGenerator_ImportResult_GetTextureCached_mB90AC91183452AAC0FEE818967721C5674EEB90C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_0_0_0_var), NULL);
	}
}
static void ImportResult_t86275B67075F847B7EF7D1DD77D72EAFA134A533_CustomAttributesCacheGenerator_ImportResult_U3CGetTextureCachedU3Eb__3_0_mAC38EEC6CF9BF4CB79BADD9282751F7055D4B77E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3__ctor_m08187E332D6D4CBF1F26C0854566D71C1B1481EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_IDisposable_Dispose_mB8C460D10E2AC911E0BF6E36D837C16AB6A0BB39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DB4C8C659FA598EA2FAB80A36EE08E786B081D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_Reset_m11B2A8AB90940DBA21D26137251ACF2F86E21FD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_get_Current_m60485DC51BA051C07BC32A4FE37E6B3419AB0673(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t8B82F7471013DE9A6F5735F024CAA61929A51738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Matrix4x4Converter_tFE6BF2451296004AE1E081BA4AE71A32D46DD0A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void QuaternionConverter_t53B5C879445DDD3AE899ADFA15AF5FB7A942F96A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void TranslationConverter_t1402C3F33EB927F093C58C349C5A49C4ED23BCED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Vector3Converter_t3D3FD7050305E7E50C86FB28D72849A954D8D91A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[332] = 
{
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator,
	U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_CustomAttributesCacheGenerator,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t96D2C15A2E479823466C4DE24847CE9D1027268E_CustomAttributesCacheGenerator,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator,
	EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator,
	VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tBF048089ECEF78F2711F6499F26A88AF5FFB544E_CustomAttributesCacheGenerator,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator,
	U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_CustomAttributesCacheGenerator,
	U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_CustomAttributesCacheGenerator,
	ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tF2780C26A5D7459DA62956B790E4242C580B8AD8_CustomAttributesCacheGenerator,
	U3CU3Ec_tE595BC76471D8A6FB95EE31C87A2719737562EE0_CustomAttributesCacheGenerator,
	EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator,
	Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator,
	KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator,
	Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator,
	U3CU3Ec_t34E4F3D9ED7FC530D8821E9C7B801ABE1220D0B8_CustomAttributesCacheGenerator,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator,
	U3CU3Ec_t665355825E495772964C636FD8E7BA8F793C6C38_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t1F96567CB3BF5E4E8CDA260362F3A44B4994783A_CustomAttributesCacheGenerator,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator,
	GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator,
	Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator,
	Values_tDADFFEB11BD5FF710A927F980DC2566F82EB7066_CustomAttributesCacheGenerator,
	Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator,
	U3CU3Ec_t006EB07EA4E92958FC9DB4FAF4CB8426A4327553_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t37A50EA00661912DB378C3926023C66B4BA49933_CustomAttributesCacheGenerator,
	GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator,
	Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator,
	Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator,
	Target_t823791F26903732C37CD778646D83823DA211878_CustomAttributesCacheGenerator,
	ImportResult_t4BC85111F8A37459B2C2C8658EF37EAAD3005972_CustomAttributesCacheGenerator,
	U3CU3Ec_tF6877DC6B82C85937019CE77BEF2F5C988A2F801_CustomAttributesCacheGenerator,
	GLTFAnimationExtensions_tC926EB1024229B2BD4B1654EE45DF7E627966BD3_CustomAttributesCacheGenerator,
	GLTFAsset_t7E6EB860C304A56344C0BC41A0E06E690B16F53B_CustomAttributesCacheGenerator,
	GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t512B53CE276BE04FD898DA180CDCFAE12D68A8AA_CustomAttributesCacheGenerator,
	GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t3109B0A53E2C727EA7DE746045C8D91F4F406624_CustomAttributesCacheGenerator,
	GLTFCamera_tD5DE3162635C4D5D1A3BE2943F57D7B67B3C1A6E_CustomAttributesCacheGenerator,
	Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator,
	Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator,
	GLTFImage_t607BB5ADF8AD49B8C9DD2C3F25D726BD0E8921D4_CustomAttributesCacheGenerator,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tF349D9620CD5F90C02833B72D36702355BD6C6F4_CustomAttributesCacheGenerator,
	GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator,
	Extensions_tA41E692D15632B96E5E96A35683856971BD20355_CustomAttributesCacheGenerator,
	PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t7CFDBB40BE1615176D7C7CCDDD54C971271B4B00_CustomAttributesCacheGenerator,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator,
	PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tCAA98420487ABC40FCBFA2A1BE900065C69836FC_CustomAttributesCacheGenerator,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator,
	TextureInfo_tA7EAB793C527BE050263842A593DC13C912F0EA6_CustomAttributesCacheGenerator,
	Extensions_t9C586AEDFF1C1A6B5AF887A5F573EB79EC91DCC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tBFB1A418CE51A0BA22BD24D319F218A458DCFD23_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t9812C6C85F9D4073BBC472078AEEB2CD321E777B_CustomAttributesCacheGenerator,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tE72A73A0AE0498ED22552C0EDD5CC6B664330602_CustomAttributesCacheGenerator,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator,
	GLTFMesh_t65AF1AB0F638BA6A3DC301A0D98AF05081258E8B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t01C67E2404F68AF83F0986FC4EC376F6D2CBD4EF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_1_tF42B31809592552E7794C7AA606C1E0D3747361B_CustomAttributesCacheGenerator,
	U3CU3Ec_tE3DD77F3EE27CF1DB096B5E24ADC88BE7558D73B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t133C9ADFE61D76451B182ED3028A3E9D29593F4D_CustomAttributesCacheGenerator,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator,
	GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator,
	U3CU3Ec_tED7E73FB15B2C4E12598F62F64445EC535BFF6B1_CustomAttributesCacheGenerator,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator,
	GLTFNodeExtensions_tB4B5EBDE856313878CDEABDDE33F706108F0387A_CustomAttributesCacheGenerator,
	U3CU3Ec_t5B9868C22EEEFFD38A10F9915AD4526375912CD0_CustomAttributesCacheGenerator,
	GLTFObject_t1A0A942ACCB7F183CD1E0980146A5661F29D8D09_CustomAttributesCacheGenerator,
	GLTFPrimitive_tE5D5897D771F30CE1A157F5BF66B66CB09468D57_CustomAttributesCacheGenerator,
	GLTFAttributes_t87ACC135A7EDEFDE7BAAC3337F44F14E7F216341_CustomAttributesCacheGenerator,
	GLTFProperty_t534A3EEEC455C71F4601A295D1694E507D0592C1_CustomAttributesCacheGenerator,
	GLTFScene_tB66CF7671BAD06DF23541A7986AE871D0C994E7B_CustomAttributesCacheGenerator,
	GLTFSkin_t3A4E93997D1F4A0932078436C074B69EA8289EBF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tCF85E9B16E772D485B2C73F01A12A94FF4F14F12_CustomAttributesCacheGenerator,
	GLTFTexture_tF66215795806552AD402542275F41AB016B561FA_CustomAttributesCacheGenerator,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t8B82F7471013DE9A6F5735F024CAA61929A51738_CustomAttributesCacheGenerator,
	ColorRGBConverter_t6E4CA6B85C8B9E9FAF9111BDA9A858F2DF410500_CustomAttributesCacheGenerator,
	ColorRGBAConverter_t8EF78801EFF0B6F78CE33F59004A10813712ED7E_CustomAttributesCacheGenerator,
	EnumConverter_t701B3E2C8F55B00BECD1B6BFFF788F08F2DFFA26_CustomAttributesCacheGenerator,
	Matrix4x4Converter_tFE6BF2451296004AE1E081BA4AE71A32D46DD0A3_CustomAttributesCacheGenerator,
	QuaternionConverter_t53B5C879445DDD3AE899ADFA15AF5FB7A942F96A_CustomAttributesCacheGenerator,
	TranslationConverter_t1402C3F33EB927F093C58C349C5A49C4ED23BCED_CustomAttributesCacheGenerator,
	Vector2Converter_tAA11AC63C04FF96C91E339012FDE8C30B9330478_CustomAttributesCacheGenerator,
	Vector3Converter_t3D3FD7050305E7E50C86FB28D72849A954D8D91A_CustomAttributesCacheGenerator,
	RuntimeTest_t8F75E56B9C4897AF129DDA399D9F24161C2C648B_CustomAttributesCacheGenerator_AvatarURL,
	WebviewTest_tF2A977D83543F0F0D00F0EA238DD572A4B879C82_CustomAttributesCacheGenerator_loadingLabel,
	AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField,
	AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_U3CTimeoutU3Ek__BackingField,
	EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator_blinkSpeed,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CExtensionU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CModelNameU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CModelPathU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsoluteUrlU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsolutePathU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CAbsoluteNameU3Ek__BackingField,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_U3CMetaDataUrlU3Ek__BackingField,
	BodyType_tD4C6BAA11C5F73A89D682291DC21FDD9CFCE6A8E_CustomAttributesCacheGenerator_Fullbody,
	BodyType_tD4C6BAA11C5F73A89D682291DC21FDD9CFCE6A8E_CustomAttributesCacheGenerator_Halfbody,
	OutfitGender_t7CA9120C0B5220645579E788B813E8CB7DC7FB9C_CustomAttributesCacheGenerator_Masculine,
	OutfitGender_t7CA9120C0B5220645579E788B813E8CB7DC7FB9C_CustomAttributesCacheGenerator_Feminine,
	KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator_offset,
	KHR_texture_transform_tA8E803DEB57F113F927275F8BC28DDD72600D51D_CustomAttributesCacheGenerator_scale,
	ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_U3CIsCompletedU3Ek__BackingField,
	ImportSettings_tD7535F5CF15688A065D6C18845C33748BA9941B4_CustomAttributesCacheGenerator_shaderOverrides,
	ImportSettings_tD7535F5CF15688A065D6C18845C33748BA9941B4_CustomAttributesCacheGenerator_interpolationMode,
	ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_metallic,
	ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_metallicBlend,
	ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_specular,
	ShaderSettings_t8143211AF5E08F94CA887C4ABAA7A5A2C30A429C_CustomAttributesCacheGenerator_specularBlend,
	GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_type,
	GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_componentType,
	GLTFAccessor_t49406F6EEC1CC2AB5B07289C9DB4F65FF0AC91CA_CustomAttributesCacheGenerator_count,
	Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_count,
	Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_indices,
	Sparse_tE86D653BD351FEDEE65F5148E5205E4B0FC1DF7E_CustomAttributesCacheGenerator_values,
	Values_tDADFFEB11BD5FF710A927F980DC2566F82EB7066_CustomAttributesCacheGenerator_bufferView,
	Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator_bufferView,
	Indices_tA46F496EF02DF6A44EB3D615BCE124D13CAF3F8F_CustomAttributesCacheGenerator_componentType,
	GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator_channels,
	GLTFAnimation_t563D145592204DE4E18B19AB4689227CC7F51857_CustomAttributesCacheGenerator_samplers,
	Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_input,
	Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_output,
	Sampler_t27DA9A9601D044EAE324E5E129D5CFEE8563A881_CustomAttributesCacheGenerator_interpolation,
	Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator_sampler,
	Channel_t5F6BC5ABD2ABDE8EB564C60F4FCDAD2058BA31C4_CustomAttributesCacheGenerator_target,
	Target_t823791F26903732C37CD778646D83823DA211878_CustomAttributesCacheGenerator_path,
	GLTFAsset_t7E6EB860C304A56344C0BC41A0E06E690B16F53B_CustomAttributesCacheGenerator_version,
	GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_byteLength,
	GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_embeddedPrefix,
	GLTFBuffer_t6BB88C6EED83CB195DF60BDA6F471A56858B46CD_CustomAttributesCacheGenerator_embeddedPrefix2,
	GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator_buffer,
	GLTFBufferView_t4C70B2A6B27168009AB0CD7ACA100608F38CE216_CustomAttributesCacheGenerator_byteLength,
	GLTFCamera_tD5DE3162635C4D5D1A3BE2943F57D7B67B3C1A6E_CustomAttributesCacheGenerator_type,
	Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_xmag,
	Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_ymag,
	Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_zfar,
	Orthographic_tC0E6D7A389F44F80A0B111E645FBE034618CF1DF_CustomAttributesCacheGenerator_znear,
	Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator_yfov,
	Perspective_tCCC056F54403B55DEFD12C6D2E0ECEFD99ACB17B_CustomAttributesCacheGenerator_znear,
	GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_emissiveFactor,
	GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_alphaMode,
	PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator_baseColorFactor,
	PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_diffuseFactor,
	PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_specularFactor,
	TextureInfo_tA7EAB793C527BE050263842A593DC13C912F0EA6_CustomAttributesCacheGenerator_index,
	GLTFMesh_t65AF1AB0F638BA6A3DC301A0D98AF05081258E8B_CustomAttributesCacheGenerator_primitives,
	ExportResult_t38C487FE254355FCC75A555B609446DEE1DB9F46_CustomAttributesCacheGenerator_mesh,
	GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_translation,
	GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_rotation,
	GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_scale,
	ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_renderer,
	ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_filter,
	ExportResult_t1AAC1D2D9F4667FBCAB04431D0E7A0A8F968582E_CustomAttributesCacheGenerator_skinnedRenderer,
	GLTFObject_t1A0A942ACCB7F183CD1E0980146A5661F29D8D09_CustomAttributesCacheGenerator_asset,
	GLTFPrimitive_tE5D5897D771F30CE1A157F5BF66B66CB09468D57_CustomAttributesCacheGenerator_attributes,
	AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_AvatarLoader_get_Timeout_mE8CB3BCEC9F34E86871DE081D1A330578C69EF3C,
	AvatarLoader_t452F3F6AF6B76D6320C94090D105866EBE36D65A_CustomAttributesCacheGenerator_AvatarLoader_set_Timeout_mD13FEA8E0424644DB952C51D62791745CDDB68CA,
	LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_LoadAvatarAsync_m1ECC417961699E00B7288CA0D359E51641B0177F,
	LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_DownloadAvatar_m8C41A534B940C0DB563C4C38D868D9E16FE14C4D,
	LoadOperation_tE951069D037169A910A299FA2532207B187D4991_CustomAttributesCacheGenerator_LoadOperation_PrepareAvatarAsync_mAFE7F9F3A13710DE1D43AF1133462C51F1518A76,
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2__ctor_mBD4F78947512C1C4071220D62833955E48D55EE7,
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_m0CBBA5987404C4EC2B71743C4BA0F0831EE98D9A,
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD133114924F55ADBDF3A6CAAE4061E50CAC7A9E4,
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mB19A8FCBACFD78E2F5825DC6995D0723E30CBE85,
	U3CLoadAvatarAsyncU3Ed__2_t20C72A17A9D0DA0D842766CAC9D61B0329BB5FF3_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_mD85B71B8CD797CA29F0378FEED1261B7E8321080,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3__ctor_m68352DFBC6229C147598D5857B04E6ADEF630B85,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mC275D51C7FF17A0C516114FF31C4E2D6CB73F551,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD843D5776566598C09ACDB97436644089FECE376,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m8334C59C0F1A6BC4B759C04AD6760BC32EF6301A,
	U3CDownloadAvatarU3Ed__3_t32114C17D8B32732C6B9273E68809D29D5EC1B8D_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_mFB2775F3A311FBB224A9179EF21700527E1B33D9,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5__ctor_mC4B4D228743E776D60E4E03F426C756904843118,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_IDisposable_Dispose_mF77F6CDFC1C997744FB30300C5CE96795AE19ECC,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF636842F021B15398C2F08AF5EB996DC63C0425,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m1FEAF518F8173872C6C8E22047A733575E8E9CC7,
	U3CPrepareAvatarAsyncU3Ed__5_t01D785D42E378EC8F65566A97D3E424E347363B0_CustomAttributesCacheGenerator_U3CPrepareAvatarAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_m9BBD626EA4994CB9449CF0E46F2273F2E19E642B,
	AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_get_Timeout_m751F8141D184C514A2921289CA5C2D158925F2B1,
	AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_set_Timeout_m8037ABC73D784A1701B268248C1653CC73FD09AD,
	AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_LoadAvatar_mB3A3FB205BF0A074FAFFFF5BAA8B20526B17A917,
	AvatarLoaderBase_tA09AF6143195877239B4D81AC6509DA1E1A5AEAE_CustomAttributesCacheGenerator_AvatarLoaderBase_DownloadMetaData_m200441C7F7CE70C78D87747769B1CAEC7C60D74F,
	U3CLoadAvatarU3Ed__19_t17529A62D938CD00C4BAE2AF8092ABD9B93F4641_CustomAttributesCacheGenerator_U3CLoadAvatarU3Ed__19_SetStateMachine_mE7E5BB907C74894E459B20D6598C803A3826200E,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22__ctor_m28B2083136EB545C725ED009EBBFE7A5BB8BD813,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_IDisposable_Dispose_mB1728B9B8A43C8600AAD903B7BD21363587EDF66,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A9228438F9EC5F8959B62258365983010A2483B,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_Reset_mBC340BC3995F6746CAEC44E1096EF92C709DC67B,
	U3CDownloadMetaDataU3Ed__22_t40FDE6969A5BC3FFC4FBF8A87468A21670F948F3_CustomAttributesCacheGenerator_U3CDownloadMetaDataU3Ed__22_System_Collections_IEnumerator_get_Current_mCE4A69299659B9C8D5DCE6C126C1D33E7A6938C8,
	EditorAvatarLoader_tFBB0776FACBE78338751996A640755C048B3AA64_CustomAttributesCacheGenerator_EditorAvatarLoader_LoadAvatarAsync_mAFCF3C276AA7B1B716C234BEF2CED8D28DACA28F,
	EditorAvatarLoader_tFBB0776FACBE78338751996A640755C048B3AA64_CustomAttributesCacheGenerator_EditorAvatarLoader_DownloadAvatar_mD546367EBD7548B38CDE90EFB47032A424403882,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2__ctor_m33FA85ABA06CE6BB59613671CDD337BE19933C95,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_IDisposable_Dispose_mA359D6B2EDEC85F23AFF2C7A98B21AF9DE7F9EFE,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AFFF62195D799E9059CEC04FD7ACF19988D46AC,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_Reset_mF50263B27F63A1E65E26495BF41E351FD6011120,
	U3CLoadAvatarAsyncU3Ed__2_t03420A98C145EF5283E890B1B6CDDADB0ECF8F5F_CustomAttributesCacheGenerator_U3CLoadAvatarAsyncU3Ed__2_System_Collections_IEnumerator_get_Current_m46EABEC84BC757D097187BBB5F759C594BFB78ED,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3__ctor_mFDBEDB706E0A529EA40637D297209930BAC2ECE6,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_IDisposable_Dispose_mB2353DD0FEC9E18048409BDB9C2D4CB6725B6D6D,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43F2C2D1AA214B91EA1926994D2052EE57B8B5E2,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_Reset_m1B694D6F834ED62F52DB8CEA18448795201387CC,
	U3CDownloadAvatarU3Ed__3_tB80C53665F2DDB1438915B95E0DE70F16B9DFC40_CustomAttributesCacheGenerator_U3CDownloadAvatarU3Ed__3_System_Collections_IEnumerator_get_Current_m630C20BDF06581F5C703266612A613384F544080,
	EyeAnimationHandler_t9B901A673A3ABC496D4C76E10156A89368F3CF0E_CustomAttributesCacheGenerator_EyeAnimationHandler_BlinkEyes_m4A936B47896A1360C3F64FCF080281F38E614556,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22__ctor_m0E1712F32F0BF56CD99EDB68EAB6293E79A892EC,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_IDisposable_Dispose_mB7812B1354202F0649E708CB552E0913EDC7C169,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21A7148FA71D14491CF4D751C5253D5C7378B5E3,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_Reset_m9190DCEEDA0E422FE8E5A55599E8BF390C1270A9,
	U3CBlinkEyesU3Ed__22_tA17A73D50485E9331051394A7691CA6B77F2A6CB_CustomAttributesCacheGenerator_U3CBlinkEyesU3Ed__22_System_Collections_IEnumerator_get_Current_mD837E0FCA30EF9CF3DB882D895B6581C550E9BCE,
	VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator_VoiceHandler_CheckIOSMicrophonePermission_m616B6A6A24D700321262DC6C3E9BFA6123BF79B2,
	VoiceHandler_t1945E12D21AC01BD5D454C758471564CB8903E26_CustomAttributesCacheGenerator_VoiceHandler_U3CSetBlendshapeWeightsU3Eg__SetBlendShapeWeightU7C22_0_m41B18EE5CDEBA498B926EBA87611CBACAB08439A,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23__ctor_m264C205C0BE8F3073576CE32182AA5F0355C38A9,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_IDisposable_Dispose_m0B021614E8EEB33ED6F76C90E73991D7010EE85D,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m932D7A3FFA7C4E9FBD405B0F5E6139FF797C0442,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_Reset_mE4D19B2D8261B6BC433381F815813EA2A43CC4D7,
	U3CCheckIOSMicrophonePermissionU3Ed__23_tBE0FB7B5CEE84399BDF9629D0AA417F042048234_CustomAttributesCacheGenerator_U3CCheckIOSMicrophonePermissionU3Ed__23_System_Collections_IEnumerator_get_Current_m32974A9C230173BA61BD2D8F28060675019EE461,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_Extension_m7CB982F51D2BC6808595132919BDDAA41553C5C5,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_Extension_mF323C6D000A26393118A08C8AB59B2D14DEA5898,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_ModelName_m7C276606C508001D9F08A945A557E6AD32DD334F,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_ModelName_m0B24DB53617858C7B3BFBA4CBD027B1C3B4F1284,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_ModelPath_m6D3C07770079C06DB146CFE587697662049CD4B6,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_ModelPath_m10161E6936F6A9B3BC4E7FADF82437E98167A36B,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsoluteUrl_mC941D4F13653D6E106E747785E35D8E37BFB854A,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsoluteUrl_m0F2DAA5A296AC51CCA4E93C61F08AEB09D76B732,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsolutePath_m9C4FAE3747B4467F9E049337A6A35A663E717CA0,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsolutePath_m0A0348B71E1A7438CB60BD36F4DB3FBB3616A828,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_AbsoluteName_mB90E1916C3D359B671166D2104EF207EC8EEB487,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_AbsoluteName_mB5FFDFE732D8E1F48AFA2A904AF29EAFEB7648F7,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_get_MetaDataUrl_m409F1074743073E4118DD865A7232A46B3AF1702,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_set_MetaDataUrl_m2BFE9FFD795BB7413C010C4287A633B198E3CF1F,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_Create_m248A0429CF7D4306570B2808B051E1683814C184,
	AvatarUri_t73658BDCC0D648CB66DE63CC994DEA9BD113295D_CustomAttributesCacheGenerator_AvatarUri_GetUrlFromShortCode_m270A0D285ED4C8DA39692B922DFF3C20591BA134,
	U3CCreateU3Ed__30_tB88AF04F59007DA7D187039DD7D77F7AC63DC2EE_CustomAttributesCacheGenerator_U3CCreateU3Ed__30_SetStateMachine_m93AED8890CC8605A14D2D841D0FBC547AE8A1E1E,
	U3CGetUrlFromShortCodeU3Ed__32_t0D7130C052558BF8DCA7757B14C1E38C55C88C7D_CustomAttributesCacheGenerator_U3CGetUrlFromShortCodeU3Ed__32_SetStateMachine_mE13334FE94249EB951AB10F2050507E40158FA0C,
	ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_Run_mA83E080C59D109AD7966D664986900522591A182,
	ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_GetMeshRenderer_m7E058B376E70F57DF1238B386580D6E3BEC1F0E7,
	ExtensionMethods_t4E6C859420A4BB3C361081D8034EB8BC72152BC4_CustomAttributesCacheGenerator_ExtensionMethods_U3CGetMeshRendererU3Eg__GetMeshU7C7_3_m5C71F949054653625ECF6227E9D0B5F6F847200B,
	EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator_EnumExtensions_ByteSize_m35DC2853AF8B37B64D323F8334391B278BDF9A5E,
	EnumExtensions_t84D8E358F65CCECE69CB084CAC592AFC768EB99F_CustomAttributesCacheGenerator_EnumExtensions_ComponentCount_m49267065A12FAA7BC7594663FDEDC27EF2B2A1DC,
	Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_RunCoroutine_m0CA03A7C0B5CB0A6CB46D0F21EFED6DE5E3011B2,
	Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_SubArray_m2548AEBAFB5AB8510EE1F1472B5CC1AB0DA28E54,
	Extensions_t4DE435E6DA4A065BD66F460DCA6F40DFAADC723D_CustomAttributesCacheGenerator_Extensions_UnpackTRS_mF30329BE0D6854C0EDEB2CFA0F03506703B25339,
	Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_LoadInternal_m5B17FBF2634BA38FBA7BAF2FE9B4479660E0A4DC,
	Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_LoadAsync_m4FC08F596DD7774E818976E385460DD131E93398,
	Importer_t01E465898D917CC69EF33869DCC1DAF3E7B5EF3D_CustomAttributesCacheGenerator_Importer_TaskSupervisor_m7DD1809D6E78E40446C69FBDD60D87926EB76DF1,
	ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_get_IsCompleted_mF1257CE54E903AA412CB894E7F1BA17227353B4F,
	ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_set_IsCompleted_m505F23568DDF7C554EE27016272CEE74A71F214B,
	ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m728AA9D6F6B6E6C99C57A8B3FFE0BCFDDD317EED,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9__ctor_m48041C4FB14FB5782E074B0C495000063148026C,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_IDisposable_Dispose_m67ABBF5C56A17B98AF1420671F6CDA4D7C9E5A1C,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9AB5731BBA2862D0FB21F18BC99898273B0BC0A5,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m293618DFBDD8ECC5807A2634D4D9485B11229E8C,
	U3COnCoroutineU3Ed__9_t416549FDD55B59A29128447ECDBB852FF8E88B3D_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mCC55F658F9053F6137398A7A715B3B0F7BB823D5,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16__ctor_m76847F3E08DDF071B407B405D7ADABBC307726AF,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_IDisposable_Dispose_mD51CF56256EB3889DCA5538E6F7568F1EE24DD68,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C24BE0512ABFB360B2F421DEA952FD566645D14,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_Reset_m0D9F51120F9B7E43A23C9DE624925382DA3160A5,
	U3CLoadAsyncU3Ed__16_tD4D83C48AB3247ECA7ED8DCF2AE4568FEF05CC55_CustomAttributesCacheGenerator_U3CLoadAsyncU3Ed__16_System_Collections_IEnumerator_get_Current_m365D7F07B28C4C5807A32EB050BF876E6D85D55C,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17__ctor_mF99DE5C7C4C73E2783E7776108CA8B3ABE42645E,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_IDisposable_Dispose_m07973E18B963E1230482ECB0589595E0DB9DF5AD,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49D10D59FCF34D9FDA6D7EAAAE8AE1B10FF12E29,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_Reset_mF7742948AC166E4AABD25D9C2105F6B5519635E4,
	U3CTaskSupervisorU3Ed__17_t5A3AB22C12F41145ADC1C3D13C895101B3481F05_CustomAttributesCacheGenerator_U3CTaskSupervisorU3Ed__17_System_Collections_IEnumerator_get_Current_m219B092F317FA9E9F45B3319589854F628F5F065,
	GLTFAnimationExtensions_tC926EB1024229B2BD4B1654EE45DF7E627966BD3_CustomAttributesCacheGenerator_GLTFAnimationExtensions_Import_m3F291F7CC01854BE61DF4FE8617630A80E8D55F1,
	ImportResult_tBCDEC5F4DFC38E35CE68A9860446C77418A3BFBE_CustomAttributesCacheGenerator_ImportResult_CreateTextureAsync_mE370E435B53F299305E45F88E0435DFF6DF0EE93,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3__ctor_m41C4795268B5A5FDBD09AAC759CB825B4E7ADDEC,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_IDisposable_Dispose_m309F94D77BA47F75B8D25738544C71739FF6FE9E,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BA6DA399F310B5E0D0BE1275626D33722409938,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_Reset_mC1E6231288207824BC54FEB4B4706F8AD9A2F8EE,
	U3CCreateTextureAsyncU3Ed__3_tC3D65D4FB4E381DFBB2CD296E991324C608C11D6_CustomAttributesCacheGenerator_U3CCreateTextureAsyncU3Ed__3_System_Collections_IEnumerator_get_Current_m0581A5FAAE854B83BCF99A47E55DAF51A4F0387B,
	GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_GLTFMaterial_CreateMaterial_m1BFA5DB15E037943CB2915DFB9B2001020343636,
	GLTFMaterial_tD20EE2C80DB630A6EA79DB4F23AC6E21F0B1A6DE_CustomAttributesCacheGenerator_GLTFMaterial_TryGetTexture_m8BF1941ECA59813B13C2CD0DC135FC798F05043A,
	PbrMetalRoughness_t4871842E9929716E975C6A14E5F406810F7F39DC_CustomAttributesCacheGenerator_PbrMetalRoughness_CreateMaterial_m510DD5D199D7102C9C43BBC3EB580743EBE52DAE,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5__ctor_m0A51ABDB657E7515358686A7C789F8BBC753C944,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m13CED19482847457E82EBDB9245B7CD31CB07240,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDDD4A652A44B7176224930CF73F79BD54250E8C,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_mBC061664859BF0BCFDE8D797FFFC0B168990E006,
	U3CCreateMaterialU3Ed__5_tD5A5389F2FF3B00A541B239D143E3BCD486D6FE5_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_mFE01300D7830C2D0E963D4EB1C7BB3CF05891049,
	PbrSpecularGlossiness_tD18FB523CE9B4CE6F02CD2CD2C16286F6F7D51B9_CustomAttributesCacheGenerator_PbrSpecularGlossiness_CreateMaterial_m87F25CC94EAABAFC3DEAE2FC18C02FA6B3C3F53D,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5__ctor_m44A29489BC2A499F5B2F902E55565A4FBA517217,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_IDisposable_Dispose_m12CE43689F6D8E80891D8B58C9734A3D34E2B981,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D6317ECF4FAC9564EDB508BF2C605119B8AC149,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_Reset_m02FC2F8E28E73A773C1F110BF34CEAB125EAD61D,
	U3CCreateMaterialU3Ed__5_tBC3DD50A86BC468F060EADDE48AFD9499602639C_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__5_System_Collections_IEnumerator_get_Current_m50CBFD918D48E2F4F72CBC1D965FFC4848C89846,
	ImportTask_t1DCBC2FE8300738DF3EBB9472642609F927F9A49_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m126F3069CBD3CB5D2B22552C9654A993A7E4AB81,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4__ctor_mC000ADF62EDF90D65CCEF5F37D3B0545DA3969AD,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_IDisposable_Dispose_m839B941EFE59407CAC6CFF970CE785DDCB4A525D,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFE681C78510873E7CBC57648CD563EACCF5393,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m63FF5A76FE5B7F03B362CECBF4B0CFDE61304E0A,
	U3COnCoroutineU3Ed__4_tE8BE125478BACF1E6427CA93FFF6DE575FDBD6EA_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m9EAB85B83380E6DBCED03E066A9EE672642ED63B,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13__ctor_m906F8A69518299C3BE456AB85AD3D56E2CAB0E5E,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_IDisposable_Dispose_mB221ED8C778CB3745D04CF99F434E2FED1FBFE09,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FE40E1FC57B8887B00B84B6167F1EA8C98A005D,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_Reset_m42BEBD986CDAECFAC5912B7188178DF75B39F309,
	U3CCreateMaterialU3Ed__13_tEF9FF8C1E8BFC5E5FB99E564BB1B9B0287712949_CustomAttributesCacheGenerator_U3CCreateMaterialU3Ed__13_System_Collections_IEnumerator_get_Current_m9A6DBBE0CD322FC365D0EC8669134D2CF62C4C35,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14__ctor_m49A3D3B99F86821FB6B1D9088E1FB565431BA72F,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_IDisposable_Dispose_m2C0609E384C9CA7CD0692B5FFDED45328ACE9E2F,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7AA5956C39B5D0E45D48C93B3B5F49395438AD99,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_Reset_m6F5354F3EBCA5488FBFE81D513788587166AA037,
	U3CTryGetTextureU3Ed__14_tF5B2523B1B567A42A46CBF333ED3CB1A78DF5AE8_CustomAttributesCacheGenerator_U3CTryGetTextureU3Ed__14_System_Collections_IEnumerator_get_Current_mE2011302FAE1B32CC0672009985C052CA2ED8FFF,
	ImportTask_tC0DBCD6E017843816ED108DB3B120997E96C04E6_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_mD6781BEBFC39FEA29FF35630A736F1D2148FC2F3,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5__ctor_m10BD190BCCD4CA5D3DE6E9E67C56D841671D3738,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m519BC153E6FD02D1D79F068287499513399CCBAF,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CDE424A2B66B71D81B41A452ED5272CC3254BBE,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_mCD839561719DB78CC9B400ABC9044F9D8D65A945,
	U3COnCoroutineU3Ed__5_t4EDB7F4408CEEF18ED2187CFE73AEFB7DFB78BE2_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m27C3951830591BFBDD37468D99DFF91264FEAF9A,
	ImportTask_t8367625A09A8B31BD2851B338307B9C847575D24_CustomAttributesCacheGenerator_ImportTask_OnCoroutine_m9F5F609BFEA83B8E95A3EFA0C47E70CBC0B506A2,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5__ctor_mA8A51B40E3A30D79F493009EAEFAA8487B2DB3FE,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_IDisposable_Dispose_m858F98D21F6F66A0CBA1350D4CE8684CBE2BD24E,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFDAFFD7EE3601FC7392DCE575962FB08F6247FF8,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m488AE3FB7BA142292F30FA08BAEBF3FA87CB8FD7,
	U3COnCoroutineU3Ed__5_t2B5AAE9D4A4B208A2D59B29A78BC45E44F57814C_CustomAttributesCacheGenerator_U3COnCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF44DD69E2CE4C8639CC4D7D7A9011401805C3E1D,
	GLTFNodeExtensions_tB4B5EBDE856313878CDEABDDE33F706108F0387A_CustomAttributesCacheGenerator_GLTFNodeExtensions_GetRoot_m16F36DAA9BC509FC186B083147EE9BBCEBEC1951,
	ImportResult_t86275B67075F847B7EF7D1DD77D72EAFA134A533_CustomAttributesCacheGenerator_ImportResult_GetTextureCached_mB90AC91183452AAC0FEE818967721C5674EEB90C,
	ImportResult_t86275B67075F847B7EF7D1DD77D72EAFA134A533_CustomAttributesCacheGenerator_ImportResult_U3CGetTextureCachedU3Eb__3_0_mAC38EEC6CF9BF4CB79BADD9282751F7055D4B77E,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3__ctor_m08187E332D6D4CBF1F26C0854566D71C1B1481EC,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_IDisposable_Dispose_mB8C460D10E2AC911E0BF6E36D837C16AB6A0BB39,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78DB4C8C659FA598EA2FAB80A36EE08E786B081D,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_Reset_m11B2A8AB90940DBA21D26137251ACF2F86E21FD8,
	U3CGetTextureCachedU3Ed__3_tEF78CC3495ED643C39083E3ECA21A884DEC122CE_CustomAttributesCacheGenerator_U3CGetTextureCachedU3Ed__3_System_Collections_IEnumerator_get_Current_m60485DC51BA051C07BC32A4FE37E6B3419AB0673,
	ImportTask_1_tE4E9B791A65AB573A493962BE745CFA5FC732BBF_CustomAttributesCacheGenerator_ImportTask_1__ctor_mD1959FA5210950FF127E465E43445910C25A2432____waitFor0,
	ImportTask_t10DBE1A0F05A79DA32D709B38BE2DA8D5004B41B_CustomAttributesCacheGenerator_ImportTask__ctor_mA8C2D32FF44B54CC145E63E941CDE524EF7E4A93____waitFor0,
	ImportResult_t603044289D118580E77ACEDE6B53D34C74FF6CDC_CustomAttributesCacheGenerator_ImportResult_ValidateAccessorTypeAny_m5CE190A5035881071F70FF155FB7FE9CF408BD72____expected1,
	GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C_CustomAttributesCacheGenerator_GLTFNode_t1FD6F47DAD920AC979C877F07DE7C6C64A2E576C____matrix_PropertyInfo,
	AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
