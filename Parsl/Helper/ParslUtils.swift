//
//  ParslUtils.swift
//  Parsl
//
//  Created by broadstone on 20/04/2021.
//

import Foundation
import UIKit
class ParslUtils: NSObject {
    static let sharedInstance = ParslUtils()
    
    var DEVICE_TOKEN: String? = ""
    
    func getDateInUSLocale(timestamp: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func openUrl(url: String){
        if let Url = URL(string: url) {
            UIApplication.shared.open(Url)
        }
    }
    
    func doubleToRoundedOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = ","
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
    
    func doubleToPercentageOutput(doubleVal: Double) -> String {
        let roundedValue = (doubleVal * 100).rounded(.toNearestOrEven) / 100
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        formatter.decimalSeparator = "."
        formatter.groupingSeparator = .none
        return formatter.string(from: NSNumber(value: roundedValue))!
        
        //return String(roundedValue)
        
    }
 
    func parseDouble(strValue: String) -> Double {
        return Double(strValue.replacingOccurrences(of: ",", with: "")
                        .replacingOccurrences(of: "$", with: "")
                        .replacingOccurrences(of: " ", with: "")
                        .replacingOccurrences(of: "%", with: ""))!
    }
}
