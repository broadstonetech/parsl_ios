//
//  RoundButton.swift
//  Virtual Guardian
//
//  Created by MacUser on 11/04/2020.
//  Copyright © 2020 MacUser. All rights reserved.
//

import UIKit
class RoundButton: UIButton{
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
    super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        backgroundColor = UIColor(displayP3Red: 55/255, green: 77/255, blue: 176/255, alpha: 1)
    }
}
