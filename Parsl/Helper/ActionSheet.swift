//
//  AlertSheet.swift
//  Parsl
//
//  Created by Billal on 24/02/2021.
//

import Foundation
import AVKit

extension UIViewController{
    static func showActionsheet(viewController: UIViewController, title: String, message: String, actions: [(String, UIAlertAction.Style)], completion: @escaping (_ index: Int) -> Void) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for (index, (title, style)) in actions.enumerated() {
            let alertAction = UIAlertAction(title: title, style: style) { (_) in
                completion(index)
            }
            alertViewController.addAction(alertAction)
         }
         viewController.present(alertViewController, animated: true, completion: nil)
        }
}
