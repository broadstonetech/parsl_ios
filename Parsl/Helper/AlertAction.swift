//
//  AlertAction.swift
//  Parsl
//
//  Created by Billal on 25/02/2021.
//

import Foundation
import AVKit
extension UIViewController {
      func popupAlert(title: String?, message: String?, actionTitles:[String?], actionStyle:[UIAlertAction.Style], actions:[((UIAlertAction) -> Void)?], vc: UIViewController) {
             let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
             for (index, title) in actionTitles.enumerated() {
                  let action = UIAlertAction(title: title, style: actionStyle[index], handler: actions[index])
                  alert.addAction(action)
             }
             vc.present(alert, animated: true, completion: nil)
        }
   }
