//
//  DatabaseRepresentation.swift
//  BTech
//
//  Created by Mufaza Majeed on 5/26/21.
//  Copyright © 2021 Broadstone Technologies. All rights reserved.
//

import Foundation

protocol DatabaseRepresentation {
  var representation: [String: Any] { get }
}

