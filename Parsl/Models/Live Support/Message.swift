
import Firebase
import MessageKit
import FirebaseFirestore

struct Message: MessageType {
  
  let id: String?
  let content: String
  let sentDate: Date
  let sender: Sender
  
  var data: MessageData {
    if let image = image {
      return .photo(image)
    } else {
      return .text(content)
    }
  }
  
  var messageId: String {
    return id ?? UUID().uuidString
  }
  
  var image: UIImage? = nil
  var downloadURL: URL? = nil
  
  init(content: String) {
    
    let email = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
    sender = Sender(id:email, displayName: email)
    self.content = content
    sentDate = Date()
    id = nil
  }
    
    init(image: UIImage) {
      let email = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
      sender = Sender(id:email ,displayName: email)
      self.image = image
      content = ""
      sentDate = Date()
      id = nil
    }
  

  
  init?(document: QueryDocumentSnapshot) {
    let data = document.data()

    guard let senderID = data["sender_id"] as? String else {
      return nil
    }
    guard let senderName = data["sender_name"] as? String else {
      return nil
    }
    

    
    guard let sentDate = data["created"] as? TimeInterval else {
        return nil
    }

    self.sentDate = NSDate(timeIntervalSinceReferenceDate: sentDate) as Date
   // var sentDateConverted = sentDate.dateValue()
    // self.sentDate = sentDateConverted
    id = document.documentID
   // self.sentDate = sentDate
    
   
    sender = Sender(id: senderID, displayName: senderName)
    
    if let content = data["content"] as? String {
      self.content = content
      downloadURL = nil
    } else if let urlString = data["url"] as? String, let url = URL(string: urlString) {
      downloadURL = url
      content = ""
    } else {
      return nil
    }
  }
  
}

extension Message: DatabaseRepresentation {
  
  var representation: [String : Any] {
    
    
    var rep: [String : Any] = [
      "created": Int64((sentDate.timeIntervalSince1970).rounded()),
      "sender_id": sender.id,
      "sender_name": sender.displayName
    ]
    
    if let url = downloadURL {
      rep["url"] = url.absoluteString
    } else {
      rep["content"] = content
    }
    
    return rep
  }
  
}



extension Message: Comparable {
  
  static func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func < (lhs: Message, rhs: Message) -> Bool {
    return lhs.sentDate < rhs.sentDate
  }
  
}

