//
//  FaceModels.swift
//  Parsl
//
//  Created by Billal on 25/02/2021.
//

import Foundation

// MARK: - Welcome
struct FaceModels: Codable {
    let fullFaceMask, glasses, surgicalMask: [String]?
    let featuredModels: [FeaturedModel]?

    enum CodingKeys: String, CodingKey {
        case fullFaceMask = "full_face_mask"
        case glasses
        case surgicalMask = "surgical_mask"
        case featuredModels = "featured_models"
    }
}

// MARK: - FeaturedModel
struct FeaturedModel: Codable {
    let supportedApps: SupportedApps?
    let modelOff: ModelOff?
    let basicInfo: FaceModelBasicInfo?
    let testedStatus: Int?
    let category: Category?
    let manufacturingDate: String?
    let ownerID: OwnerID?
    let analytics: FaceModelAnalytics?
    let subCategory: SubCategory?
    let modelFiles: FaceModelFiles?
    let modelID: String?

    enum CodingKeys: String, CodingKey {
        case supportedApps = "supported_apps"
        case modelOff = "model_off"
        case basicInfo = "basic_info"
        case testedStatus = "tested_status"
        case category
        case manufacturingDate = "manufacturing_date"
        case ownerID = "owner_id"
        case analytics
        case subCategory = "sub_category"
        case modelFiles = "model_files"
        case modelID = "model_id"
    }
}

// MARK: - FaceModelAnalytics
struct FaceModelAnalytics: Codable {
    let sharesCount, downloadCount, searchAppearances: Int?

    enum CodingKeys: String, CodingKey {
        case sharesCount = "shares_count"
        case downloadCount = "download_count"
        case searchAppearances = "search_appearances"
    }
}

// MARK: - FaceModelBasicInfo
struct FaceModelBasicInfo: Codable {
    let name: String?
    let manufacturer: FaceModelManufacturer?
    let model: String?
}

enum FaceModelManufacturer: String, Codable {
    case locusAR = "LocusAR"
}

enum Category: String, Codable {
    case fashion = "fashion"
}

// MARK: - FaceModelFiles
struct FaceModelFiles: Codable {
    let thumbnail: String?
    let the3DModelFile: String?

    enum CodingKeys: String, CodingKey {
        case thumbnail
        case the3DModelFile = "3d_model_file"
    }
}

enum ModelOff: String, Codable {
    case common = "common"
}

enum OwnerID: String, Codable {
    case org6150 = "org6150"
}

enum SubCategory: String, Codable {
    case cheeksShades = "cheeks_shades"
    case eyeShades = "eye_shades"
    case faceShades = "face_shades"
    case lipstick = "lipstick"
}

// MARK: - SupportedApps
struct SupportedApps: Codable {
    let android, ios: Int?
}
