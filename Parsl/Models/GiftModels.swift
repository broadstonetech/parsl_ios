//
//  GiftModels.swift
//  Parsl
//
//  Created by Billal on 25/02/2021.
//

import Foundation

// MARK: - GiftModel
struct GiftModel: Codable {
    let message: String?
    var data: [GiftModelMessage]?
    var avatars: [AvatarModel]?
    var animations: [AnimationModel]
}

// MARK: - Gift
struct AvatarModel: Codable {
    var isAvatarSelected: Bool = false
    var chracter_key: String?
    var chracter_thumbnail: String?
    enum CodingKeys: String, CodingKey {
        case chracter_key = "chracter_key"
        case chracter_thumbnail = "chracter_thumbnail"
    }
}
struct AnimationModel: Codable {
    var isAnimationSelected: Bool = false
    var animation_key: String?
    var animation_thumbnail: String?
    enum CodingKeys: String, CodingKey {
        case animation_key = "animation_key"
        case animation_thumbnail = "animation_thumbnail"
    }
}
struct GiftModelMessage: Codable {
    var isModelSelected: Bool = false
    let basicInfo: GiftModelBasicInfo?
    let priceRelated: GiftModelPriceRelated?
    let scaling: GiftModelScaling?
    let hyperlink, document, sampleVideo, promotionalVideo: String?
    let images: [String]?
    let multiTexture: Int?
    let licenseSource: GiftModelLicenseSource?
    let actions: GiftModelActions?
    let languagesSupported: Annotations?
    let modelFiles: GiftModelFiles?
    let video: String?
    let cryptographicInfo: GiftModelCryptographicInfo?
    let analytics: GiftModelAnalytics?
    let category: String?
    let manufacturingDate: String?
    let ownerID: GiftModelOwnerID?
    let purchaseStatus: Int?
    let format: GiftModelFormat?
    let license: GiftModelLicense?
    let giftDescription: String?
    let additionalInfo: AdditionalInfo?
    let annotations: Annotations?
    let multiNode: Int?
    let questionnaire: [GiftModelQuestionnaire]?
    let commentFeed: [String]?
    let metaTags, visionMessage: String?
    let subCategory: [String]?
    let testedStatus, modelIndex: Int?
    let productID: String?
    let modelID: String?
    let isFavorite: Int?
    let textureFile: [GiftModelTextureFile]?

    enum CodingKeys: String, CodingKey {
        case basicInfo = "basic_info"
        case priceRelated = "price_related"
        case scaling, hyperlink, document
        case sampleVideo = "sample_video"
        case promotionalVideo = "promotional_video"
        case images
        case multiTexture = "multi_texture"
        case licenseSource = "license_source"
        case actions
        case languagesSupported = "languages_supported"
        case modelFiles = "model_files"
        case video
        case cryptographicInfo = "cryptographic_info"
        case analytics, category
        case manufacturingDate = "manufacturing_date"
        case ownerID = "owner_id"
        case purchaseStatus = "purchase_status"
        case format, license
        case giftDescription = "description"
        case additionalInfo = "additional_info"
        case annotations
        case multiNode = "multi_node"
        case questionnaire
        case commentFeed = "comment_feed"
        case metaTags = "meta_tags"
        case visionMessage = "vision_message"
        case subCategory = "sub_category"
        case testedStatus = "tested_status"
        case modelIndex = "model_index"
        case productID = "product_id"
        case modelID = "model_id"
        case isFavorite
        case textureFile = "texture_file"
    }
}

// MARK: - GiftModelActions
struct GiftModelActions: Codable {
    let actionType: String?
    let actionTitle: String?
    let actionData: String?

    enum CodingKeys: String, CodingKey {
        case actionType = "action_type"
        case actionTitle = "action_title"
        case actionData = "action_data"
    }
}

enum GiftModelActionTitle: String, Codable {
    case shopNow = "Shop Now"
}

enum GiftModelActionType: String, Codable {
    case link = "link"
}

enum AdditionalInfo: String, Codable {
    case empty = "{}"
}

// MARK: - GiftModelAnalytics
struct GiftModelAnalytics: Codable {
    let sharesCount, downloadCount, searchAppearances: Int?
    let survey: [GiftModelQuestionnaire]?

    enum CodingKeys: String, CodingKey {
        case sharesCount = "shares_count"
        case downloadCount = "download_count"
        case searchAppearances = "search_appearances"
        case survey
    }
}

enum GiftModelQuestionnaire: String, Codable {
    case howWouldYouLikeToRateOurServices = "How would you like to rate our services?"
    case whatYouWillRecommendUs = "What you will recommend us?"
}

// MARK: - Annotations
struct Annotations: Codable {
}

// MARK: - GiftModelBasicInfo
struct GiftModelBasicInfo: Codable {
    let name: String?
    let manufacturer: String?
    let model: String?
}

enum GiftModelLicenseSource: String, Codable {
    case locusAR = "LocusAR"
}

enum Model: String, Codable {
    case sunglasses = "Sunglasses"
}

enum GiftModelCategory: String, Codable {
    case fashion = "fashion"
}

// MARK: - GiftModelCryptographicInfo
struct GiftModelCryptographicInfo: Codable {
    let digitalSig, publicKey: [Int]?
    let modelSha1: String?
    let unzippedSize, zippedSize: Int?

    enum CodingKeys: String, CodingKey {
        case digitalSig = "digital_sig"
        case publicKey = "public_key"
        case modelSha1 = "model_sha1"
        case unzippedSize = "unzipped_size"
        case zippedSize = "zipped_size"
    }
}

enum GiftModelFormat: String, Codable {
    case usdz = ".usdz"
}

enum GiftModelLicense: String, Codable {
    case openSource = "Open source"
}

// MARK: - GiftModelFiles
struct GiftModelFiles: Codable {
    let audioFile: String?
    let texturesType: GiftModelTexturesType?
    let textures, multitextureModelsList: [String]?
    let the3DModelFile: String?
    let thumbnail: String?
    let animatedJsonFile: String?
    let previewVideo: String?

    enum CodingKeys: String, CodingKey {
        case audioFile = "audio_file"
        case texturesType = "textures_type"
        case textures
        case multitextureModelsList = "multitexture_models_list"
        case the3DModelFile = "3d_model_file"
        case thumbnail
        case animatedJsonFile = "animated_json_file"
        case previewVideo = "preview_video"
    }
}

enum GiftModelTexturesType: String, Codable {
    case single = "single"
}

enum GiftModelOwnerID: String, Codable {
    case org6150 = "org6150"
}

// MARK: - GiftModelPriceRelated
struct GiftModelPriceRelated: Codable {
    let price: Int?
    let currency: GiftModelCurrency?
    let priceStatus: GiftModelPriceStatus?

    enum CodingKeys: String, CodingKey {
        case price, currency
        case priceStatus = "price_status"
    }
}

enum GiftModelCurrency: String, Codable {
    case usd = "USD"
}

enum GiftModelPriceStatus: String, Codable {
    case free = "free"
}

// MARK: - GiftModelScaling
struct GiftModelScaling: Codable {
    let scaleUnit: String?
    let maxScale: MaxScale?
    let minScale: MinScale?
    enum CodingKeys: String, CodingKey {
        case scaleUnit = "scale_unit"
        case maxScale = "max_scale"
        case minScale = "min_scale"
    }
}

// MARK: - GiftModelScale
struct MaxScale: Codable {
    let xAxis, yAxis, zAxis: Int?

    enum CodingKeys: String, CodingKey {
        case xAxis = "x-axis"
        case yAxis = "y-axis"
        case zAxis = "z-axis"
    }
}
struct MinScale: Codable {
    let xAxis, yAxis, zAxis: Float?

    enum CodingKeys: String, CodingKey {
        case xAxis = "x-axis"
        case yAxis = "y-axis"
        case zAxis = "z-axis"
    }
}
enum ScaleUnit: String, Codable {
    case inches = "inches"
}

// MARK: - GiftModelTextureFile
struct GiftModelTextureFile: Codable {
    let title: FaceModelTitle?
    let textureFileDefault: Int?
    let texturesList: [String]?

    enum CodingKeys: String, CodingKey {
        case title
        case textureFileDefault = "default"
        case texturesList
    }
}

enum FaceModelTitle: String, Codable {
    case titleDefault = "default"
}
