//
//  Category.swift
//  Parsl
//
//  Created by Billal on 22/02/2021.
//

import Foundation

struct MainCategory: Codable {
    let message: String?
    let data: [MainCategoryResponse]?
}

struct MainCategoryResponse: Codable {
    let categoryTitle, categoryKey: String?
    let listPriority: Int?
    let thumbnails: String?

    enum CodingKeys: String, CodingKey {
        case categoryTitle = "category_title"
        case categoryKey = "category_key"
        case listPriority = "list_priority"
        case thumbnails
    }
}
