//
//  SaveParsl.swift
//  Parsl
//
//  Created by Billal on 26/02/2021.
//

import Foundation

struct SaveParsl: Codable {
    let message: String?
    let data: ParslInfo?
}

// MARK: - ParslInfo
struct ParslInfo: Codable {
    let transectionID:String?
    let cardNumber: String?
    let totalPrice:Double?
    let parslURL: String?
    let parslOtp: String?
    let parslID:String?
    
    enum CodingKeys: String, CodingKey {
        case transectionID = "transection_id"
        case cardNumber = "card_number"
        case totalPrice = "total_price"
        case parslURL = "parsl_url"
        case parslOtp = "parsl_otp"
        case parslID = "parsl_id"
    }
}
