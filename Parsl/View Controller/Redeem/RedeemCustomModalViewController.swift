//
//  RedeemCustomModalViewController.swift
//  Parsl
//
//  Created by Mufaza on 04/08/2021.
//

import Foundation
import UIKit
import FlagPhoneNumber



class RedeemCustomModalViewController: BaseViewController, UITextFieldDelegate,FPNTextFieldDelegate{
  
    var delegate : SendDataDelegate?
   

    @IBOutlet weak var receiverNameTF: UITextField!
    @IBOutlet weak var urlTF: UITextField!
    
    @IBOutlet weak var receiverContactTF: FPNTextField!
    
    var isValid = false
    
    
    //MARK: -ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        receiverNameTF.delegate = self
        receiverContactTF.delegate = self
        urlTF.delegate = self
       // receiverContactTF.setFlag(for: .US)
       
        receiverContactTF.setFlag(key: .US)
    }
    
    func fpnDisplayCountryList() {
//       let navigationViewController = UINavigationController(rootViewController: listController)
//
//       present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
       print(name, dialCode, code) // Output "France", "+33", "FR"
    }

    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            
          // Do something...
//          textField.getFormattedPhoneNumber(format: .E164),           // Output "+33600000001"
//          textField.getFormattedPhoneNumber(format: .International),  // Output "+33 6 00 00 00 01"
//          textField.getFormattedPhoneNumber(format: .National),       // Output "06 00 00 00 01"
//          textField.getFormattedPhoneNumber(format: .RFC3966),        // Output "tel:+33-6-00-00-00-01"
//          textField.getRawPhoneNumber()
            self.isValid = true
       } else {
        if textField.text != "" {
//            self.view.makeToast("Please enter valid number")
            self.isValid = false
        }
       
       }
    
 }

    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {

        
        if receiverNameTF.text == "" || receiverNameTF.text!.isEmpty {
            self.view.makeToast("Please enter receiver name")
            return
        }
        if receiverContactTF.text == "" || receiverContactTF.text!.isEmpty {
            self.view.makeToast("Please enter receiver contact number")
            return
        }

        if isValid{
            self.delegate?.sendRedeemData(self.receiverNameTF.text!,self.receiverContactTF.text!)
                dismiss(animated: true, completion: nil)
        }else{
            self.view.makeToast("Please enter valid number")
        }

    }
}

