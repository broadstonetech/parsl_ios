//
//  Drone.swift
//  Parsl
//
//  Created by Billal on 09/03/2021.
//
import ARKit
import SceneKit.ModelIO
class Drone: SCNNode {
    
    func loadModel(url : URL,modelFormat : String) {
        print("downloaded model local url== \(url)")
        if modelFormat == ".usdz" {
            
            print("USDZ format drone")
            let scene = try! SCNScene(url: url, options: nil)
            let wrapperNode = SCNNode()
            for child in scene.rootNode.childNodes {
                wrapperNode.addChildNode(child)
            }
            addChildNode(wrapperNode)
           // dict.add(node,annotations)
        } else {
            print("NOt a USDZ format drone")
            
            guard let scene = try? SCNScene(url: url, options: nil) else {return}
            let wrapperNode = SCNNode()
            for child in scene.rootNode.childNodes {
                wrapperNode.addChildNode(child)
            }
            addChildNode(wrapperNode)

        }
    }

    //for local models
    func loadModel(url:URL) {
        print(url)
        guard let virtualObjectScene = try? SCNScene(url: url, options: nil) else {
            print("file is crrupted")
            return }
        let wrapperNode = SCNNode()
        
        wrapperNode.position = SCNVector3(0, -0.5, -3)
        for child in virtualObjectScene.rootNode.childNodes {
         wrapperNode.addChildNode(child)
                }
            addChildNode(wrapperNode)
        }
}
