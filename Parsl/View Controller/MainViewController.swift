//
//  ViewController.swift
//  Parsl
//
//  Created by Billal on 19/02/2021.
//
import Foundation
import UIKit
import SDWebImage
import AVFoundation
import AVKit
import MediaPlayer
import AudioToolbox
import ARKit
import SSZipArchive
import Stripe
import Alamofire
import Toast_Swift
import JJFloatingActionButton
import StoreKit
import FirebaseAuth
import FirebaseFirestore
import UnityFramework
import Lottie


protocol SendDataDelegate: class{
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String)
    func showActionButton()
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String)
}

protocol CartEmptyDelegate {
    func cartHasBeenCleared()
}

let kStartingPosition = SCNVector3(0, -1, -2)
let kAnimationDurationMoving: TimeInterval = 0.2
let kMovingLengthPerLoop: CGFloat = 0.05
let kRotationRadianPerLoop: CGFloat = 0.2

var selectedCategoryDataList = [SubCategoryData]()
var giftModels: GiftModel!

var isOrderFlowCompleted = false
class MainViewController: BaseViewController, UITextFieldDelegate,AVPlayerViewControllerDelegate,SendDataDelegate,NativeCallsProtocol {
    
    func modelDownloadingDone(_ message: String!) {
        
        //only when avatar projection has complete
        print("projection done")
//        if message == "Avatar Successfully Downloaded"{
////            sleep(5)
////            sendRedeemMsg(ID: parslRedeemModel.data.giftModel.productId)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { [self] in
//                   //call any function
//                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
//               }
//        }
        
        if isRedeem{
            selectedModelForDownload.append(message)
            tableView.reloadData()
        }
    }
    
    func modelProjected(_ message: Bool) {
        print("model is projected from redeem screen")
        self.ControlTransferButtonActive()
        print(message)
        if isModelProjected == false{
            isModelProjected = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.loadAvatar()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.redeemAnimatingView.isHidden = false
                }
               }
        }
        
    }
    func modelDownloaded(_ message: Bool) {
        print(message)
        print("projection done in model downloaded")
        
//        self.actionButton.removeFromSuperview()
//        self.view.addSubview(self.actionButton)
//        self.view.addSubview(self.redeemAnimatingView)
//        self.actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
//        self.actionButton.bottomAnchor.constraint(equalTo: self.MainCategoryStack.topAnchor, constant: -8).isActive = true
//      //  RedeemAnimatingBackgroundView.isHidden = false
//        self.view.sendSubviewToBack(RedeemAnimatingBackgroundView)
        
        
        if !animatingCardView{
            self.view.makeToast("Tap on the screen to project the model")
        }
        
        
        if isRedeem{
           
            tableView.reloadData()
           
        }
//        else{
            self.ControlTransferButtonActive()
//            self.view.bringSubviewToFront(self.unityView!)
//        }
      
  
    }
    

    

//
//    func sendMessage(toMobileApp message: String!) {
//
//        DispatchQueue.main.async {
//            self.view.sendSubviewToBack(self.unityView!)
//            self.mainPresentedView.isHidden = false
//        }
//        print(message)
//    }
    
    
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String) {
        print(URLOTP)
        DispatchQueue.main.async {
           //self.addToWallet(name: receiverName, phone: receiverNumber, url: URLOTP)
            self.cartMainView.isHidden = true
//            self.reloadUnity()
            self.subcategoriesHeight.constant = 90
            self.actionButton.removeFromSuperview()
            self.view.addSubview(self.actionButton)
            self.actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            self.actionButton.bottomAnchor.constraint(equalTo: self.MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.URLOTP = URLOTP
            
            self.redeemGift(otp: URLOTP, name: receiverName, phoneNumber: receiverNumber)
            self.selectedModelForDownload.removeAll()
    }
}
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String) {
       
        self.getVirtualGiftCard(otp: self.URLOTP, receiverName: receiverName, phoneNumber: receiverNumber)
        
        
        
    }
    
    func showActionButton(){
        self.actionButton.isHidden = false
    }
    @IBOutlet weak var subcategoriesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var floatButtonView: UIView!
    var messageAR:String        = ""
    var quantity                = [Int]()
    var mainCategoryList:         MainCategory!
    var subCategoryList:          SubCategoryModel!
    var pricesList              = [Double]()
    var subCategoryArray        = [SubCategoryData]()
    var selectedItemsDataList   = [SubCategoryData]()
    var getParsl:                 GetParsl!
    var getParslList            = [ParslModels]()
    var getModelData:             ParslGiftsModel!
    var cartItemsArray          = [String]()
    var selectedIndex           = 0
    var cartCount :Int          = 0
    var searching               = false
    var isRedeem                = false
    var videoUrl:String         = ""
    // var used for model projections
    var playVideo: SCNPlane?
    var videoNode: SCNNode?
    var modelUrl = ""
    lazy var drone          = Drone()
    var droneList:[Drone]   = [Drone]()
    var format:                 String?
    var downloadedModelURL:   URL?
    var isMultiNode:          Int?
    var textueresDownloadedCount = 0
    var destPathforTextures: String?
    var isAlltextureDownloaded: Bool = false
    var player: AVPlayer!
    var pricesListofCart = [Double]()
    var URLOTP:String = ""
    var isFirstTime:Bool = true
    var nameofSender = " "
    
    var modelID:String?
    var selectedModelForDownload  = [String]()
    //add data to cart
    let configuration = ARWorldTrackingConfiguration()
    var dataList = [SubCategoryData]()
    var fileURL,modelTitle: String?
    //MARK: - Outlets
    //  For SceneKit
    @IBOutlet weak var showMessageButton        : UIButton!
   // @IBOutlet weak var playVideoButton          : UIButton!
//    @IBOutlet weak var sceneView                : ARSCNView!
    @IBOutlet weak var leftButtonsView          : UIView!
    @IBOutlet weak var rightButtonsView         : UIView!
    
    @IBOutlet weak var redeemVideoView: UIView!
    
    //outlets for view that is displayed when projected model is selected
    @IBOutlet weak var mainPresentedView: UIView!
    @IBOutlet weak var addToButton: RoundButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var presentedModelImage: UIImageView!
    @IBOutlet weak var presentedTextView: UITextView!
    
    
    //  For Basic View
    let actionButton = JJFloatingActionButton()
    @IBOutlet weak var MainCategoryStack:     UIStackView!
    @IBOutlet weak var cartMainView:         UIView!
    @IBOutlet weak var cartCountLabel:       UILabel!
    @IBOutlet weak var RedeemHalfView: UIView!
    //collection viewss
    @IBOutlet weak var mainCategoryCollectionView:      UICollectionView!
    @IBOutlet weak var subCategoryCollectionView:       UICollectionView!
    @IBOutlet weak var vendorsCollectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainCategoryView:        UIView!
    @IBOutlet weak var searchBar:               UISearchBar!
    @IBOutlet weak var subCategoryView:         UIView!
    
    @IBOutlet weak var messageView: UIView!
  //  @IBOutlet weak var messageTextView: UITextView!
    
    
    
    @IBOutlet weak var redeemAnimatingView: UIView!
    @IBOutlet weak var videoView: VideoView!
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var modelsBgView: UIView!
    
    @IBOutlet weak var vendorDetailDialog: VendorsDetailsDialog!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var addToWalletBtn: UIButton!
    
    @IBOutlet weak var showRedeemHalfViewBtn: UIButton!
    
    var isVideoCompleted = false
    
    var isModelDownloaded = false
    var isModelProjected = false
    
    var vendersList: GetVendorsList!
    
    var vendorSelectedIndex = -1
    var selectedModelIndex = 0
    var modelDataSturctArray = [ModelDataStruct]()
    var parslRedeemModel: RedeemModelStruct!
    
    var modelsInCart = [ModelDataStruct]()
    var modelsInView = [String : ModelDataStruct]()
    var downloadedModelsForRedeem = [ModelsDownloadedForRedeemStruct]()
    
    var searchResult = [ModelDataStruct]()
    var isSearching = false
    
    var selectedModelIndexForRedeem = -1
    
    var selectedFilter = ""
    
    var downloadedData = [DownloadedDataForCatAndVendors]()
    var downloadedVendors = [DownloadedDataForVendors]()
    var modelsDownloadedForSend = [ModelsDownloadedForSendStruct]()
    var isModelAvailableInSapce = false

    var isGiftModelDownloaded = false
    var isArVideoPlayed = false
    
    var isRedeemMessage = false
    var isVideoMessage = false
    var redeemMessage = ""
    
    var animatingCardView = false
    
    var tappedItem =  ModelDataStruct()
    
    var virtualCardModel: VirtualCard!
    
    enum DownloadStatus {
        case Downloading
        case Downloaded
        case NotDownloadable
    }
    
    var isSubcategoriesAPICalled = DownloadStatus.Downloading
    
    var userEmail :String? = ""
    var channelListener: ListenerRegistration?
    var documentID : String? = nil
    private let db = Firestore.firestore()
    
    var documentQueryObject: QueryDocumentSnapshot?
     private var messageReference: CollectionReference?
    
    private var channelReference: CollectionReference {
        return db.collection("channels")
    }
    
    @IBOutlet weak var BackgroundView: AnimationView!
    var unityView: UIView?
    
    @IBOutlet weak var firstCardTextView: UITextView!
    @IBOutlet weak var secondCardTextView: UITextView!
    @IBOutlet weak var firstCardView: UIView!
    @IBOutlet weak var secondCardView: UIView!
    @IBOutlet weak var RedeemAnimatingBackgroundView: UIView!
    //    @IBOutlet weak var redeemHalfViewLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var animatingHeight: NSLayoutConstraint!
    
    @IBOutlet weak var animatingWidth: NSLayoutConstraint!
    
    //MARK: - View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addToButton.layer.cornerRadius = 5
        addToButton.backgroundColor = UIColor(displayP3Red: 55/255, green: 77/255, blue: 176/255, alpha: 1)
        //stackViewHeight.constant -= 50
        hideViews()
        

        
        if isVideoCompleted {
           // setUpSceneView()
        }
//        addKeyboardListeners()
        
        if isOrderFlowCompleted {
            isOrderFlowCompleted = false
            modelsInCart.removeAll()
//            UnityEmbeddedSwift.unpauseUnity()
            self.setUpUnityView()
//            self.ControlTransferButtonActive()
//            self.sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
//                existingNode.removeFromParentNode()
//            }
        }
        cartCountLabel.text = "\(modelsInCart.count)"
       
    }
    func hideViews(){
        messageView.isHidden            = (messageView.isHidden == true)
       // addToWalletBtn.isHidden = (addToWalletBtn.isHidden == true)
        MainCategoryStack.isHidden      = (MainCategoryStack.isHidden == true)
        cartMainView.isHidden           = (cartMainView.isHidden == true)
        showMessageButton.isHidden      = true
      //  playVideoButton.isHidden        = (playVideoButton.isHidden == true)
        mainPresentedView.isHidden      = true
        searchBar.isHidden              = true
        leftButtonsView.isHidden        = true
        rightButtonsView.isHidden       = true
        subCategoryView.isHidden        = (subCategoryView.isHidden == true)
        modelsBgView.isHidden = true
        sortButton.isHidden = true
    }
    
    private func prepareVideoViewAndLoadData(){
        self.getCategory()
        isVideoCompleted = false
        videoView.isHidden = false
        guard let videoPath = Bundle.main.path(forResource: "parsl_video", ofType:"mp4") else {
            print("error return")
            return
        }
        let url = NSURL(fileURLWithPath: videoPath) as URL
        videoView.configure(url: url.absoluteString)
        videoView.isLoop = false
//        AVPlayerLayer *foregroundLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
//         self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
//         foregroundLayer.frame = self.view.frame;
//         foregroundLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

        videoView.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        print("VideoEnded")
        isVideoCompleted = true
        videoView.isHidden = true
       // sceneView.session.run(configuration)
        self.setUpFloatingActionbutton()
        self.setUpUnityView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
      //  NSClassFromString("FrameworkLibAPI")?.registerAPIforNativeCalls(self)
        FrameworkLibAPI.registerAPIforNativeCalls(self)
        
        //setUpFloatingActionbutton()
        getGuestDeviceDetail()
        configureLighting()
        searchBar.delegate = self
        searchBar.isHidden = true
        
        getAppVersionApi()
        
        if traitCollection.userInterfaceStyle == .dark {
           print("dark mode enabled")
        } else {
            print("light mode enabled")
        }
        
        
        self.tableView.tableFooterView = UIView()
        
        
        self.navigationItem.title = "PARSL"
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = UIColor.systemGray6
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
       
        
        mainCategoryCollectionView.isUserInteractionEnabled = true
        mainCategoryCollectionView.allowsMultipleSelection              = true
        if #available(iOS 14.0, *) {
            mainCategoryCollectionView.allowsMultipleSelectionDuringEditing = true
        } else {
            // Fallback on earlier versions
        }
        
        mainCategoryCollectionView.delegate     = self
        mainCategoryCollectionView.dataSource   = self
        
        vendorsCollectionView.delegate = self
        vendorsCollectionView.dataSource = self
        vendorsCollectionView.contentInsetAdjustmentBehavior = .never
        
        subCategoryCollectionView.dataSource = self
        subCategoryCollectionView.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        
        
        addTapGestureToScreenView()
        hideKeyboardTappedAround()
        
        DispatchQueue.main.async {
            self.getGiftModels()
        }
    
        prepareVideoViewAndLoadData()
        
        let blurViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(vendorDialogCloseBtnPressed))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(blurViewTapGesture)
        
        vendorDetailDialog.cancelBtn.addTarget(self, action: #selector(vendorDialogCloseBtnPressed), for: .touchUpInside)
        
        vendorDetailDialog.visitStore.addTarget(self, action: #selector(visitStoreBtnPressed), for: .touchUpInside)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTapped(withGestureRecognizer:)))
        doubleTapGesture.numberOfTapsRequired = 2
//       sceneView.addGestureRecognizer(doubleTapGesture)
        
    }
    
    @objc func vendorDialogCloseBtnPressed(){
        blurView.isHidden = true
        vendorDetailDialog.isHidden = true
    }
    
    @objc func visitStoreBtnPressed(){
        
        ParslUtils.sharedInstance.openUrl(url: vendersList.data.vendorsList[vendorSelectedIndex].otherInfo.website)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isVideoCompleted {
           // sceneView.session.pause()
        }
        NotificationCenter.default.removeObserver(self)

        if !isRedeem{
            self.reloadUnity()
            self.cartHasBeenCleared()
        }

    }
    
    func addKeyboardListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    var prices:Double?
    var productIDs:String?
    var completeList : SubCategoryData?
    
    //MARK: -Button Actions
    var isMessageShowing = false
    var isMenuShowing = false
    var downloadCount = 0
    var heightCount = 0
    var videoRepeatCount = 0
    var messageRepeatCount = 0
    
    @IBAction func sortButtonTapped(_ sender: Any) {
        openBottomActionSheetForDataSorting()
    }
    
    @IBAction func playVideoTapped(_ sender: Any) {
        let url = URL(string: parslRedeemModel.data.receiverData.videoMsg)!
        playServerVideo(url: url)
//        if (videoUrl != ""){
//
//            self.player.play()
//            //playARVideo(videoURL: self.videoUrl, modelURL: self.modelUrl!)
//            videoRepeatCount += 1
//        }
//        else {
//            showAlert(withTitle: "PARSL", withMessage: "No Video Message")
//        }
    }
    @IBAction func addToCartTapped(_ sender: Any) {
        //print(productIDs!)
//        if cartItemsArray.contains(productIDs!) == true{
//            showAlert(withTitle: "Item already added", withMessage: "Add others items to cart")
//        }
//        else{
//            pricesList.append(prices!)
//            quantity.append(1)
//            cartItemsArray.append(productIDs!)
         //   selectedItemsDataList.append(completeList!)
//            cartCount += 1
//            cartCountLabel.text = String(cartCount)
//        }
        if isItemAddedInCart(tappedItem){
            showAlertWithAction(withTitle: "PARSL", withMessage: "Item successfully added")
        }else{
            modelsInCart.append(tappedItem)
            cartCountLabel.text = String(modelsInCart.count)
            //
            self.ControlTransferButtonActive()
            //self.view.bringSubviewToFront(self.unityView!)
            mainPresentedView.isHidden = true
        }
    }
    
    private func isItemAddedInCart(_ selectedItem: ModelDataStruct) -> Bool {
        for item in modelsInCart {
            if item.productId == selectedItem.productId {
                return true
            }
        }
        return false
    }
    
    @IBAction func showMessageTapped(_ sender: Any) {
        showMessageAR(message: messageAR, modelURL: self.modelUrl)
        messageRepeatCount += 1
    }
    func showMessageAR(message:String,modelURL:String){
        ProgressHUD.dismiss()
        self.modelPathURL = URL(string: modelURL)!
        //self.view.isUserInteractionEnabled = true
        self.showMessageButton.isHidden = false
        self.messageView.isHidden = false
       // self.messageTextView.text = message
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.messageView.isHidden = true
            if self.messageRepeatCount == 0 {
                self.drone.loadModel(url: self.modelPathURL!)
//                self.sceneView.scene.rootNode.addChildNode(self.drone)
                self.subCategoryCollectionView.reloadData()
                self.subCategoryView.isHidden = false
                
                }
           }
    }
    @IBAction func cartTapped(_ sender: Any) {
        
        if modelsInCart.isEmpty {
            showAlert(withTitle:"PARSL", withMessage: "Please add items to cart")
        }
        else{
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartViewController") as? CartViewController
            isOrderFlowCompleted = true
            vc?.quantity = quantity
            vc?.selectedItemsArray = cartItemsArray
            vc?.selectedItemsDataList = selectedItemsDataList
            vc?.pricesList = pricesList
            vc?.modelsInCart = modelsInCart
            vc?.cartDelegate = self
            
            self.navigationController?.pushViewController(vc!, animated: true)
//            self.present(vc!, animated: true, completion: nil)
        }
    }
    //@IBAction func showSearch(_ sender: Any) {
      //  if searchBar.isHidden == true{
        //    searchBar.isHidden = false
        //}
        //else{
          //  searchBar.isHidden = true
        //}
    //}
    
    
    @IBAction func mainSearchBtnPressed(_ sender: Any) {
        
    }
    
    //MARK:- Floating Buttons
    func setUpFloatingActionbutton(){
        actionButton.buttonDiameter = 50
        actionButton.buttonImageColor = .black
        actionButton.buttonColor = .white
      //  actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: -160)
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 170)
      //correect one:
//        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 5, firstItemSpacing: 5)
        
      //  this popup with -170 is a hint
      //  actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: -170)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .bottom
            item.titleSpacing = .zero
            item.buttonColor = .white
            item.buttonImageColor = .black
           // item.layer.si
            
        }
        
        actionButton.addItem(title:"Help",image: UIImage(named: "support")?.withRenderingMode(.alwaysTemplate)) { item in
//            self.drone.removeFromParentNode()
//            self.hideViews()
            
            self.popupAlert(title: "Choose option", message: nil, actionTitles: ["Refund","General queries", "How to send PARSL?", "How to redeem PARSL?","Live Support","Cancel"], actionStyle: [.default,.default,.default,.default,.default,.cancel],
                actions: [
                {
                action in
                    let email =  "refund@parsl.io"
                    if let url = URL(string: "mailto:\(email)") {
                      if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                      } else {
                        UIApplication.shared.openURL(url)
                      }
                    }
                },
                {
                action in
                    let email = "support@parsl.io"
                    if let url = URL(string: "mailto:\(email)") {
                      if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                      } else {
                        UIApplication.shared.openURL(url)
                      }
                    }
                },
                {
                action in
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    vc.videoPath = "send_parsl"
                    self.navigationController!.present(vc, animated: true, completion: nil)
                        
                    },
                {
                action in
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                    vc.videoPath = "redeem_parsl"
                    self.navigationController!.present(vc, animated: true, completion: nil)
                    },
                    {
                action in
                        self.userEmail = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
                        print("email is",self.userEmail)
                        if self.userEmail == nil  || self.userEmail == "" {
                            
                            let alertController = UIAlertController(title: "Live Support", message: "Please enter your email to talk to our agent.", preferredStyle: .alert)

                            alertController.addTextField { (textField : UITextField!) -> Void in
                                textField.placeholder = "User Email"
                            }



                            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )

                            alertController.addAction(cancelAction)

                            let saveAction = UIAlertAction(title: "Start", style: .default, handler: { alert -> Void in
                                let firstTextField = alertController.textFields![0] as UITextField
                                if !self.isValidEmail(firstTextField.text!) {
                                    self.view.makeToast("Please enter valid email address")
                                    return
                                }else{
                                    UserDefaults.standard.set(firstTextField.text, forKey: "userEmail")
                                    self.userEmail = firstTextField.text
                                    self.openConversationWindow()
                                }

                            })
                            
                            
                            
                            alertController.addAction(saveAction)

                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            self.openConversationWindow()
                        }
                    },
                nil], vc: self)
        }
        actionButton.addItem(title: "Wallet", image: UIImage(named: "card")?.withRenderingMode(.alwaysTemplate)) { item in
            //self.homeFunctionality()
            
            let url = URL(string: "itms-apps://itunes.apple.com/app/id1160481993")
            
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!)
            }
        }
        actionButton.addItem(title: "Redeem", image: UIImage(named: "redeem")?.withRenderingMode(.alwaysTemplate)) { item in
            self.homeFunctionality()
            //self.showRedeemHalfViewBtn.isHidden = false
            self.actionButton.isHidden = true
            let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ReceiverModalViewController") as? ReceiverModalViewController
            vc?.delegate = self
            self.navigationController?.present(vc!, animated: true, completion: nil)
            
        }
        
        actionButton.addItem(title: "Send" ,image: UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)) { item in
            //self.homeFunctionality()
            self.redeemAnimatingView.isHidden = true
            self.reloadUnity()
           // self.playVideoButton.isHidden = true
           // self.addToWalletBtn.isHidden = true

            self.hideViews()
            //self.MainCategoryStack.isHidden = false
//            ProgressHUD.show("Please Wait")
            self.getCategory()
           // self.removeAllNodes()

            if self.isSubcategoriesAPICalled == .Downloading {
                ProgressHUD.show("Fetching data")
                return
            }else if self.isSubcategoriesAPICalled == .NotDownloadable {
                ProgressHUD.show("Fetching data")
                self.getCategory()
                return
            }

            self.showMessageButton.isHidden = true
          //  self.playVideoButton.isHidden = true
            self.cartMainView.isHidden = false
            self.MainCategoryStack.isHidden = false
            self.subCategoryView.isHidden  = false
            self.modelsBgView.isHidden = false
            self.vendorsCollectionView.isHidden = false
            self.sortButton.isHidden = false
            self.isRedeem = false

            DispatchQueue.main.async {
                self.mainCategoryCollectionView.reloadData()
                //self.vendorsCollectionView.reloadData()
                self.vendorsCollectionView.reloadSections(IndexSet(integer: 0))
                self.subCategoryCollectionView.reloadData()
            }
        }
        let imageHome = UIImage(systemName: "house.fill")
        actionButton.addItem(title: "Home", image: imageHome?.withRenderingMode(.alwaysTemplate)) { item in
            self.homeFunctionality()
            self.reloadUnity()
            self.cartHasBeenCleared()
            
           

        }


        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -98).isActive = true

        // last 4 lines can be replaced with
        // actionButton.display(inViewController: self)
    }
    
    
    //MARK: -Functions for Projected Object
    
    func homeFunctionality(){
//        self.sceneView.scene.rootNode.enumerateChildNodes { (node, _) in
//            node.removeFromParentNode()

        }
        
        
//        while let n = drone.childNodes.first {
//            n.removeFromParentNode()
//        }
        //self.drone.removeFromParentNode()
        self.subCategoryView.isHidden = true
       // self.playVideoButton.isHidden = true
        self.messageView.isHidden = true
       // self.addToWalletBtn.isHidden = true
        self.MainCategoryStack.isHidden = true
        self.isModelAvailableInSapce = false
        self.cartMainView.isHidden = true
        self.redeemAnimatingView.isHidden = true
        self.isRedeem = false
        self.hideViews()
        
    }
    
//    func configureLighting() {
//        sceneView.autoenablesDefaultLighting = true
//        sceneView.automaticallyUpdatesLighting = true
//    }
//    func setUpSceneView(){
//        configuration.planeDetection = .horizontal
//        sceneView.session.run(configuration)
//        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
//    }
    func addTapGestureToScreenView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.didTap(withGestureRecognizer:)))
//        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    func addModelInScene(url : URL) {
        self.drone = Drone()
        self.downloadedModelURL = url
//        DispatchQueue.main.async {
//            self.view.isUserInteractionEnabled = true
//        }
        self.drone.loadModel(url : self.downloadedModelURL!, modelFormat: self.format!)
    }
    func downloadModel(modelURL : String,modelTitle: String,format:String, modelIndex: Int) {
        
        let url = URL(string: modelURL)
        let modelTitle = modelTitle
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                print("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                print("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    print("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        if format == ".usdz" {
                            
                            let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(modelTitle).usdz"//.zip
                            let fileManager = FileManager.default
                            if (fileManager.fileExists(atPath: documentsDestPath)){
                                try! fileManager.removeItem(atPath: documentsDestPath)
                            }
                            try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                            //get stored file path and url
                            let fileName = "\(modelTitle).usdz" // your image name here .zip
                            let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                            let fileUrl: URL = URL(fileURLWithPath: filePath)
                            let destinationURL = fileUrl
                            print("destinatioURL is == \(destinationURL)")
                            self.modelUrl = String(describing: destinationURL)
                            DispatchQueue.main.async { [self] in
                                    ProgressHUD.dismiss()
                                //self.view.isUserInteractionEnabled = true
                                    if self.isRedeem == false{
                                        self.view.makeToast("Move your camera towards a flat surface and slowly hover around the surface and tap on surface to view object", duration: 5.0, position: .center, completion: nil)
                                    }
                                    else{
                                        //self.drone.loadModel(url: destinationURL)
                                        let modelDownloaded = ModelsDownloadedForRedeemStruct(_3dModelFile: modelURL, destinationUrl: destinationURL)
                                        self.downloadedModelsForRedeem.append(modelDownloaded)
                                        
                                        self.view.makeToast("Move your camera towards a flat surface and slowly hover around the surface and tap on surface to view object", duration: 5.0, position: .center, completion: nil)
                                        
                                        self.addModelInScene(url: destinationURL)
                                        if selectedModelIndexForRedeem == modelIndex{
                                            //self.drone.loadModel(url: destinationURL)
                                            
                                        }
                                    }
                            }
                        }
                        else {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: "PARSL", withMessage: "Model Not Available. Please select other items")
                            //self.view.isUserInteractionEnabled = true
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: "PARSL", withMessage: "Unable to download model, please try again later.")
                            //self.view.isUserInteractionEnabled = true
                        }
                    }
                }else {
                    //self.view.isUserInteractionEnabled = true
                    print("downlaoding failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    func downloadZippedModel(modelURL : String,modelTitle: String,format:String,videoURL:String,messageText:String) {
        ProgressHUD.show()
        let url = URL(string: modelURL)
        let modelTitle = modelTitle
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                print("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                print("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    print("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        if format == ".usdz" {
                            
                            let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(modelTitle).usdz"//.zip
                            let fileManager = FileManager.default
                            if (fileManager.fileExists(atPath: documentsDestPath)){
                                try! fileManager.removeItem(atPath: documentsDestPath)
                            }
                            try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                            //get stored file path and url
                            let fileName = "\(modelTitle).usdz" // your image name here .zip
                            let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                            let fileUrl: URL = URL(fileURLWithPath: filePath)
                            let destinationURL = fileUrl
                            print("destinatioURL is == \(destinationURL)")
                            self.modelUrl = String(describing: destinationURL)
                            DispatchQueue.main.async { [self] in
//                                    self.setUpSceneView()
                                    //self.view.isUserInteractionEnabled = true
                                    if self.isRedeem == false{
                                        let modelDownloaded = ModelsDownloadedForSendStruct(_3dModelFile: modelURL, destinationUrl: destinationURL)
                                        self.modelsDownloadedForSend.append(modelDownloaded)
                                        self.isModelAvailableInSapce = true
                                        
                                        self.addModelInScene(url: destinationURL)
                                        //self.addModelInScene(url: destinationURL)
                                        self.view.makeToast("Move your camera towards a flat surface and slowly hover around the surface and tap on surface to view object", duration: 5.0, position: .center, completion: nil)
                                    }
//                                    if videoURL != ""{
//                                        self.playARVideo(videoURL: videoURL, modelURL: self.modelUrl!)
//                                    }
//
//                                    else if messageText != ""{
//                                        self.showMessageAR(message: messageText, modelURL: self.modelUrl!)
//                                    }
                                    else{
                                       // self.addModelInScene(url: destinationURL)
                                        self.isGiftModelDownloaded = true
                                        self.view.makeToast("Move your camera towards a flat surface and slowly hover around the surface and tap on surface to view object", duration: 5.0, position: .center, completion: nil)
                                        //self.drone.loadModel(url: destinationURL)
                                        self.addModelInScene(url: destinationURL)
                                        if self.isArVideoPlayed {
                                            
                                            
                                        }
//                                        self.downloadModel(modelURL: parslRedeemModel.data.parslModels[0].modelFile._3dModelFile,
//                                                           modelTitle: parslRedeemModel.data.parslModels[0].basicInfo.name,
//                                                           format: parslRedeemModel.data.parslModels[0].format, modelIndex: 0)
                                    }
                                ProgressHUD.dismiss()
                                    //self.modelsInCart.append(self.modelDataSturctArray[self.selectedModelIndex])
                            }
                        }
                        else {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: "PARSL", withMessage: "Model not available. Please select other items")
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: "PARSL", withMessage: "Unable to download, please try again")
                            //self.view.isUserInteractionEnabled = true
                        }
                    }
                }else {
                    //self.view.isUserInteractionEnabled = true
                    print("downlaoding failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    var modelPathURL:URL?
    func playARVideo(videoURL: String,modelURL:String){
        ProgressHUD.dismiss()
        //self.view.isUserInteractionEnabled = true
        //self.modelPathURL = URL(string: modelURL)!
        let videoPath = URL(string: videoURL)!
        player = AVPlayer(url: videoPath)
        playVideo = SCNPlane(width: 0.6, height: 0.9)
        playVideo!.firstMaterial?.diffuse.contents = player
        playVideo!.firstMaterial?.isDoubleSided = true
        
        videoNode = SCNNode(geometry: playVideo)
        videoNode!.position.z = -2
        
//        sceneView.scene.rootNode.addChildNode(videoNode!)
        self.player.play()
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.videoDidEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
//        DispatchQueue.main.async {
//            self.playVideoButton.isHidden = false
//        }
    }
    
    var parentNode: String = ""
    
    @IBAction func messageDoneBtnPressed(_ sender: UIButton) {
        if messageView.isHidden == false {
            messageView.isHidden = true
        }
    }
    
    @objc func didTap(withGestureRecognizer recognizer: UIGestureRecognizer) {
        self.mainPresentedView.isHidden = false
        self.actionButton.isHidden = false
        self.RedeemHalfView.isHidden = true
        
        
//        if isRedeem{
//
//            self.redeemAnimatingView.isHidden = false
//           // self.RedeemHalfView.isHidden = false
//        }else{
//
//            self.redeemAnimatingView.isHidden = true
//        }
        
        //control back to unityview
//        self.ControlTransferButtonActive()
//        self.view.bringSubviewToFront(self.unityView!)
       

//            let tapLocation = recognizer.location(in: sceneView)
        print("Tap location \(tapLocation)")
//            let hitTestResults = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        guard let hitTestResult = hitTestResults.first else
            {
            if self.modelUrl != "" {
                self.view.makeToast("Not enough lighting", duration: 3.0, position: .center, completion: nil)
            }
            self.mainPresentedView.isHidden = true
            return
            }
        if self.modelUrl != "" && !self.modelUrl.isEmpty {
            let translation = hitTestResult.worldTransform.translation
            let x = translation.x
            let y = translation.y
            let z = translation.z
            //let z = Float(-0.80)
//            if z > -0.5 && z < 0{
//                z -= 0.1
//            }else {
//                z += 0.1
//            }
            
            
//            let action = SCNAction.moveBy(x: 0, y: kMovingLengthPerLoop, z: CGFloat(z), duration: kAnimationDurationMoving)
//            let loopAction = SCNAction.repeat(action, count: 3)
//            drone.runAction(loopAction)
            
            let action1 = SCNAction.moveBy(x: 0, y: 0, z: -deltas().cos, duration: kAnimationDurationMoving)
            let loopAction1 = SCNAction.repeat(action1, count: 5)
            drone.runAction(loopAction1)
            
//            let action2 = SCNAction.moveBy(x: 0, y: kMovingLengthPerLoop, z: 0, duration: kAnimationDurationMoving)
//            let loopAction2 = SCNAction.repeat(action2, count: 3)
//            drone.runAction(loopAction2)
            
            print("Z point: \(z)")
            drone.position = SCNVector3(x,y,z)
//            sceneView.scene.rootNode.addChildNode(drone)
            
            self.leftButtonsView.isHidden = false
            self.rightButtonsView.isHidden = false
        }
            
        //print("Hit test count \(hit.count)")
        //guard let node = getRoot(for: hit.first!.node) else {return}
            if isRedeem == false{
                addToWalletBtn.isHidden = true
            }else {
                addToWalletBtn.isHidden = false
            }
        self.modelUrl = ""
        }
    
    func getRoot(for node: SCNNode) -> SCNNode? {
        if var node = node.parent {
            node = getRoot(for: node)!
        }
        else {
            return node
        }
        return node
    }
    
    
    @objc func doubleTapped(withGestureRecognizer recognizer: UIGestureRecognizer) {
        if isRedeem == false {
//            let tapLocation = recognizer.location(in: sceneView)
            
//            let hit = sceneView.hitTest(tapLocation)
            guard hit.count > 0 else {return}
            
            if getRoot(for: hit.first!.node)?.name != nil {
                print(getRoot(for: hit.first!.node)?.name!)
                DispatchQueue.main.async {
                    self.mainPresentedView.isHidden = false
                }
            }
            else{
            }
        }
    }
    
    @IBAction func addToWalletBtnTapped(_ sender: UIButton) {
        
        let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
        
      //  self.getVirtualGiftCard(otp: otp, receiverName: name, phoneNumber: phoneNumber)
//        if virtualCardModel != nil && self.modelUrl == "" {
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VirtualCardViewController") as! VirtualCardViewController
//            vc.virtualCardModel = self.virtualCardModel
//            self.navigationController?.present(vc, animated: true, completion: nil)
//        }
    }
    
    @objc func videoDidEnd(note: NSNotification) {
        videoNode?.removeFromParentNode()
        if isRedeem {
            isArVideoPlayed = true
            if isGiftModelDownloaded {
                self.view.makeToast("Tap anywhere on screen to view PARSL", duration: 3.0, position: .center, completion: nil)
                self.addModelInScene(url: URL(string: self.modelUrl)!)
                //self.drone.loadModel(url: URL(string: self.modelUrl)!)
            }else {
                ProgressHUD.show()
            }
        }else {
            DispatchQueue.main.async {
                if self.videoRepeatCount == 0 {
                    self.drone.loadModel(url: self.modelPathURL!)
//                    self.sceneView.scene.rootNode.addChildNode(self.drone)
                    self.subCategoryCollectionView.reloadData()
                    self.subCategoryView.isHidden = false
                }
            }
        }
        
    }


    @IBAction func showRedeemCustomModal(_ sender: Any) {
        redeemAnimatingView.isHidden = true
        
        
        UIView.transition(with: RedeemHalfView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.RedeemHalfView.isHidden = false
                            self.view.bringSubviewToFront(self.RedeemHalfView!)
                            self.tableView.reloadData()
                            
                      })
        
        
      
    }
    
    //MARK: -Functions Performing API Calls
    func addToWallet(name:String,phone:String,url:String){
        if Reachability.isConnectedToNetwork(){
            let data = ["parsl":url,"reciever_name":name,"reciever_phone_no":phone]
            let parameters = ["data":data]
            
            let url = URL(string: "https://parsl.io/get/virtual/card/details")!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                    }
                }
                catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: "PARSL", withMessage: "No internet connection. Please try again later")
        }
        
    }
    func getGuestDeviceDetail(){
        if Reachability.isConnectedToNetwork(){
            let data = ["device_token":ParslUtils.sharedInstance.DEVICE_TOKEN,"device_id":deviceID] as [String : Any]
            let parameters = ["app_id":"parsl_ios",
                              "data":data] as [String : Any]
            
            let APIUrl = URL(string: "https://parsl.io/save/guest/device/details")!
            let session = URLSession.shared
            var request = URLRequest(url: APIUrl)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: "PARSL", withMessage: "No internet connection. Please try again later")
        }
        
    }
    func getCategory() {
        if Reachability.isConnectedToNetwork(){
            let parameters = ["app_id":"parsel_ios"]
            let url = URL(string: "https://parsl.io/get/categories/list")!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                ProgressHUD.dismiss()
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        self.mainCategoryList = try JSONDecoder().decode(MainCategory.self, from: data)
                        
                        if !(self.mainCategoryList.data!.isEmpty) {
                            let firstCategoryName = self.mainCategoryList.data![0].categoryKey!
                            
                            self.getCategoryVendors(categoryName: firstCategoryName)
                        }
//                        DispatchQueue.main.async {
//                            self.mainCategoryCollectionView.reloadData()
//                        }
                    }
                }
                catch let error {
                    print(error)
                    ProgressHUD.dismiss()
                }
            })
            task.resume()
        }else{
            //self.showAlert(withTitle: "No Internet Connection", withMessage: "Please try again later")
        }
        
    }
    
    func playServerVideo(url: URL) {
            let player = AVPlayer(url: url)
            
            let vc = AVPlayerViewController()
            vc.player = player
            
            self.present(vc, animated: true) {
                vc.player?.play()
                
            }
    }

//    func getSubCategories(key: String, vendor: String){
//        if Reachability.isConnectedToNetwork(){
//            var vendors = [String]()
//            vendors.append(vendor)
//            let data = ["category": "\(key)", "venders": vendors] as [String : Any]
//            let parameters = ["app_id": "parsl_ios", "namespace": "", "data": data] as [String : Any]
//            print(parameters)
//            let url = URL(string: "https://parsl.io/get/vendor/models")!
//            let session = URLSession.shared
//            var request = URLRequest(url: url)
//            request.httpMethod = "POST"
//
//            do {
//                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
//            } catch let error {
//                print(error.localizedDescription)
//            }
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
//
//                guard error == nil else {
//                    return
//                }
//                guard let data = data else {
//                    return
//                }
//                do {
//                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
//                        subCategoryList = try JSONDecoder().decode(SubCategoryModel.self, from: data)
//                        dump(subCategoryList)
//                        for i in 0..<subCategoryList!.data!.count{
//                            subCategoryList!.data![i].selectedCategories = key
//                            self.subCategoryArray.append(subCategoryList!.data![i])
//                            selectedCategoryDataList.append(subCategoryList!.data![i])
//
//                        }
//                        DispatchQueue.main.async {
//                            ProgressHUD.dismiss()
//                            self.subCategoryCollectionView.reloadData()
//                            self.view.isUserInteractionEnabled = true
//                        }
//                    }
//                } catch let error {
//                    print(error)
//                }
//            })
//            task.resume()
//        }else{
//            //self.showAlert(withTitle: "No Internet Connection", withMessage: "Please try again later")
//        }
//
//    }
    
    func getSubCategories(key: String, vendor: String){
        var vendors = [String]()
        vendors.append(vendor)
        let data: Dictionary<String, AnyObject> = ["category": key as AnyObject, "vendors": vendors as AnyObject]
        let parameters: Dictionary<String, AnyObject> = ["app_id": "parsl_ios" as AnyObject, "data": data as AnyObject]
        print(parameters)
        let url = "https://parsl.io/get/vendor/models"
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                fetchModelsApiData(data: json["data"] as? [AnyObject] ?? [AnyObject]())
                DispatchQueue.main.async {
                    self.isSubcategoriesAPICalled = .Downloaded
                    //self.view.isUserInteractionEnabled = true
                    self.mainCategoryCollectionView.reloadData()
                    self.vendorsCollectionView.reloadSections(IndexSet(integer: 0))
                    self.subCategoryCollectionView.reloadData()
                    //self.vendorsCollectionView.reloadData()
                    
                    ProgressHUD.dismiss()
                    
                    
                }
                
            }catch {
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    self.isSubcategoriesAPICalled = .NotDownloadable
                }
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self.isSubcategoriesAPICalled = .NotDownloadable
            }
        })
    }
    
    func getCategoryVendors(categoryName: String) {
        let url = "https://parsl.io/get/category/vendors"
        
        let data = ["category": categoryName]
        let params = ["app_id":"parsl_ios", "data": data] as [String : AnyObject]
        print("Vendors api parameter: \(params)")
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            print("Vendor api response \(response)")
            
            do {
                self.vendersList = try JSONDecoder().decode(GetVendorsList.self, from: response)
                print("Vendor api response data \(self.vendersList.data)")
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    self.downloadedVendors.append(DownloadedDataForVendors(categoryIndex: self.selectedIndex, vendorsList: self.vendersList))
                    let vendorId = self.vendersList.data.vendorsList[0].vendorID
                    self.getSubCategories(key: categoryName, vendor: vendorId)
                }

            }
                    
            catch{
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            print(data)
            ProgressHUD.dismiss()
        })
    }
    
    func redeemGift(otp: String, name: String, phoneNumber: String){
       // ProgressHUD.show("Please wait")
        self.view.isUserInteractionEnabled = true
        
//        self.showRedeemHalfViewBtn.frame.size.width = 5
//        self.showRedeemHalfViewBtn.frame.size.height = 5
        
        self.animatingWidth.constant = 35
        self.animatingHeight.constant = 35
        
        UIView.animate(withDuration: 5, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 5, animations: {
           // self.squareView.backgroundColor = .red
        }) { [weak self] _ in
            UIView.animate(withDuration:5, animations: {
                self!.animatingWidth.constant = 60
                self?.animatingHeight.constant = 60
            }) { [weak self] _ in
                UIView.animate(withDuration: 5, animations: {
                    self?.animatingWidth.constant = 60
                    self?.animatingHeight.constant = 60
                }) { [weak self] _ in
                    self?.animatingWidth.constant = 60
                    self?.animatingHeight.constant = 60
                }
            }
        }

        
 
       
        
        let data = ["parsl":otp as AnyObject,
                    "device_token":ParslUtils.sharedInstance.DEVICE_TOKEN as AnyObject,
                    "device_id":deviceID as AnyObject]
        let parameters = ["app_id":"parsl_ios" as AnyObject,
                          "data":data as AnyObject]
        
        
        print(parameters)
        let url = "https://parsl.io/get/parsl/details"
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: AnyObject]
                parslRedeemModel = fetchRedeemModelStruct(response: jsonResponse)
                
                if parslRedeemModel != nil {
                    if self.parslRedeemModel.message == "Success"{
                        
                        
                        nameofSender = parslRedeemModel.data.senderData.senderName
                        print("name of sender is \(nameofSender)")
                        self.format = parslRedeemModel.data.giftModel.format
                        
                       // self.getVirtualGiftCard(otp: otp, receiverName: name, phoneNumber: phoneNumber)
//                        if parslRedeemModel.data.giftModel.format == ".usdz" {
//                            DispatchQueue.main.async {
//                                    self.downloadZippedModel(modelURL: parslRedeemModel.data.giftModel.modelFile._3dModelFile,
//                                                             modelTitle: parslRedeemModel.data.giftModel.basicInfo.name,
//                                                             format: parslRedeemModel.data.giftModel.format, videoURL: parslRedeemModel.data.receiverData.videoMsg, messageText: parslRedeemModel.data.receiverData.textMsg)
//                            }
//                        }
                        
                        

                       
                        
            
                        if parslRedeemModel.data.receiverData.videoMsg != ""{
                            print("Video msg available")
                            DispatchQueue.main.async { [self] in
//                                self.playARVideo(videoURL: parslRedeemModel.data.receiverData.videoMsg, modelURL: "")

                               // playVideoButton.isHidden = false
                               // self.redeemVideoView.isHidden = false
                                isVideoMessage = true
                               // self.view.makeToast("Press on video icon to play video")
                            }
                        }else {
                            DispatchQueue.main.async {
                              //  self.playVideoButton.isHidden = true
                            //    self.redeemVideoView.isHidden = true
                                self.isArVideoPlayed = true
                                isVideoMessage = false
                            }
                        }
//                        if parslRedeemModel.data.receiverData.textMsg != ""  {
//
//                            self.isRedeemMessage = true
//
//
//                        }
//                        else {
//                           isRedeemMessage = false
//                        }
                        redeemMessage = parslRedeemModel.data.receiverData.textMsg
                        
                        print(redeemMessage)
                        DispatchQueue.main.async {
                            self.isRedeem = true
                            self.MainCategoryStack.isHidden = true
                            self.rightButtonsView.isHidden       = true
                            self.leftButtonsView.isHidden        = true
                            self.subCategoryCollectionView.reloadData()
                            self.subCategoryView.isHidden  = true
                            self.subCategoryCollectionView.isHidden = true
//                                                    self.Redeem(boxID: parslRedeemModel.data.giftModel.modelId, modelID: parslRedeemModel.data.parslModels[0].modelId)
                           // sendMSg(ID: parslRedeemModel.data.giftModel.productId)
                           
//                            self.loadRedeemAnimationBackGroundView()
                            var style = ToastStyle()
                            style.backgroundColor = .lightGray
                            style.messageColor = .black
                            self.view.makeToast(self.redeemMessage, duration: 10.0, position: .center, style: style)
                            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != ""{
//                                self.loadAvatar()
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }else{
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }
//                            sendRedeemMsg(ID: parslRedeemModel.data.giftModel.productId)
                            tableView.reloadData()

                        }
                        
                    }
                    else{
                        self.showAlert(withTitle: "PARSL", withMessage: "Invalid OTP/URL input. Please try again")
                        DispatchQueue.main.async {
                            //self.view.isUserInteractionEnabled = true
                            ProgressHUD.dismiss()
                        }
                    }
                }else {
                    self.showAlert(withTitle: "PARSL", withMessage: "Unable to fetch Parsl. Please try again later")
                    DispatchQueue.main.async {
                        //self.view.isUserInteractionEnabled = true
                        ProgressHUD.dismiss()
                    }
                }
//                DispatchQueue.main.async {
//                    ProgressHUD.dismiss()
//
//                }
                
            } catch let error {
                print(error)
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    //self.view.isUserInteractionEnabled = true
                }
            }
            
            
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                //self.view.isUserInteractionEnabled = true
                self.showAlert(withMessage: "Unable to fetch data. Please try again later")
            }
        })
    }
    
    func getGiftModels(){
        if Reachability.isConnectedToNetwork(){
//            let parameters = ["app_id":"parsel_ios",
//                              "namespace":""] as [String : Any]
            let parameters = ["app_id": "parsl_ios"] as [String : Any]
            let url = URL(string: "https://parsl.io/get/gift/models")!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        giftModels = try JSONDecoder().decode(GiftModel.self, from: data)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: "PARSL", withMessage: "No internet connection. Please try again later")
        }
        
    }
    func saveToServer(){
        var device_info : Dictionary <String, String> = [:]
        var receiver_info : Dictionary <String, String> = [:]

         device_info = [
          "device_name":UIDevice.current.name as String,
          "device_os":UIDevice.current.systemName as String,
            "device_version":UIDevice.current.systemVersion as String,
            "device_type":UIDevice.current.model,
            "device_manufacturar":"Apple"
        ]

        let data :Dictionary<String,Any> = ["creator" :userEmail! as String,"creator_name":userEmail! as String, "phone":"","creator_email":userEmail! as String,"creator_namespace":userEmail! as String,"creater_device_info":device_info as Any,"firebase_id":self.documentID! as String,"app_id":"parsl","creator_device_token":ParslUtils.sharedInstance.DEVICE_TOKEN! as String]
        
        
        print(data)
        
       postChatRequest(serviceName:"https://parsl.io/start/chat", sendData: data as [String : AnyObject], success: { (data) in
            print("data is",data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
        
        let status = data["status"] as! Bool
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    if status{
                        print("successfuly saved to server")
                    }else{
                        self.showAlert(withMessage: "All agents are busy at the moment.Try again later")
                    }
                   // self.showAlert("Success")
                    
                }
      
            }
  
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    //MARK: -Redeem Animation Methods
    
    func loadRedeemAnimationBackGroundView(){
        RedeemAnimatingBackgroundView.isHidden = false
        actionButton.removeFromSuperview()

        BackgroundView.contentMode = .scaleAspectFill
        BackgroundView.loopMode = .loop
        BackgroundView.play()
        self.view.sendSubviewToBack(self.unityView!)
        self.view.bringSubviewToFront(RedeemAnimatingBackgroundView)
        BackgroundView.bringSubviewToFront(firstCardView)
        BackgroundView.bringSubviewToFront(secondCardView)
        BackgroundView.bringSubviewToFront(buttonView)
        
        animatingCardView = true
        
        self.roundButtonView()
        
        
        self.firstCardView.alpha = 0
        self.buttonView.alpha = 0
        self.firstCardView.isHidden = false
        
        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.firstCardView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.firstCardView.alpha = 1
                self.firstCardTextView.text = "You received a parcel from your friend"
                self.firstCardView.transform = self.firstCardView.transform.translatedBy(x: self.view.frame.width/2-self.firstCardView.frame.width/2, y: 200)
               
               
            }, completion: nil)
        }
        
        
        UIView.animate(withDuration: 1.3, delay: 0.7, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
           
            self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            
            UIView.animate(withDuration: 1, delay: 0.9, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                self.buttonView.alpha = 1
                self.buttonView.transform = self.firstCardView.transform.translatedBy(x: -32, y: 64)

            }, completion: nil)
            
        }
        
        isFirstTime = true
        
        
    }
    
    
    
    @IBAction func nxtBtnTapped(_ sender: Any) {
        
        if isFirstTime{
            
            UIView.animate(withDuration: 0){
                self.firstCardView.alpha = 0
                self.buttonView.alpha = 0
                self.firstCardView.isHidden = true
               
            }
            
          
            
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                self.secondCardView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.secondCardView.alpha = 1
                    self.secondCardView.isHidden = false
                   // self.cardViewHeight.constant = 350
                    self.secondCardTextView.text = self.redeemMessage
                    self.secondCardView.transform = self.secondCardView.transform.translatedBy(x: self.view.frame.width/2-self.secondCardView.frame.width/2, y: 200)
                   
                   
                }, completion: nil)

            
            }
            
            
            UIView.animate(withDuration: 1.3, delay: 0.9, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
               
                self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                
                UIView.animate(withDuration: 1, delay: 0.7, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                    self.buttonView.alpha = 1
                    self.buttonView.transform = self.secondCardView.transform.translatedBy(x: -32, y: 264)

                }, completion: nil)
                
                
            }
            

            self.isFirstTime = false
    
        }else{
            
            RedeemAnimatingBackgroundView.isHidden = true
            self.secondCardView.isHidden = true
            view.addSubview(actionButton)
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.view.sendSubviewToBack(RedeemAnimatingBackgroundView)
            self.redeemAnimatingView.isHidden = false
            animatingCardView = false
            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != ""{
                self.loadAvatar()
            }else{
                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
            }
            
            
            self.view.makeToast("Tap on the screen to project the model")
            
            
            
        }
       
            
        }

        
func roundButtonView(){
    
    buttonView.layer.cornerRadius = buttonView.frame.size.width/2
    buttonView.clipsToBounds = true

    buttonView.layer.borderColor = UIColor.white.cgColor
    buttonView.layer.borderWidth = 5.0
    
    
    firstCardView.clipsToBounds = true
    firstCardView.layer.cornerRadius = 10.0
    
    secondCardView.clipsToBounds = true
    secondCardView.layer.cornerRadius = 10.0
    
    
}
    
    
    
    
    //MARK: -Unity Helper methods
    
    
    
    func destroyUnity(_ message: String!) {
        //destroying a model
        print("id is",message)
        if !isRedeem{
            let parsed = message.replacingOccurrences(of: "(Clone)", with: "")
            modelsInView.removeValue(forKey: parsed)
            
            for (index, element) in modelsInCart.enumerated() {
              
                if element.productId == message{
                    modelsInCart.remove(at: index)
                    cartCountLabel.text = String(modelsInCart.count)
                    
                }
            }
            
            
           
        }
        self.view.sendSubviewToBack(self.unityView!)

    }
    
    func Redeem(boxID:String,modelID:String)
    {
       // var ID = boxID
         let ID = boxID + "," + modelID
        print("redeem id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "RedeemBox", message: ID)
    }
    func sendMSg(ID:String) {
        print("model id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Activate", message: ID)
    }
    
    func sendRedeemMsg(ID:String) {
        print("model id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Redeem", message: ID)
    }
    
    func reloadUnity(){
        print("reloading ")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ReLoadUnity", message: "")
    }
    
    func  ControlTransferButtonActive(){
        print("add button")
        
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ControlTransferButtonActive", message: "")
    }
    
    // projects the avatar
    func loadAvatar(){
        
        let avatarString = parslRedeemModel.data.avatarCharacter + "," + parslRedeemModel.data.avatarAnimation
        print("avaatar string is ...\(avatarString)")
        self.view.makeToast("Tap on the screen to project the model")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "RedeemCharacter", message: avatarString)
//        self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
    }
    
    func openConversationWindow(){
        //check if email exists

            Auth.auth().signInAnonymously(completion: nil)

            print("chat button pressed")
        let filterString = userEmail
        channelListener = channelReference.whereField("name", isEqualTo: filterString).addSnapshotListener { [self] querySnapshot, error in
                
                if let error = error{
                    print("error is",error.localizedDescription)
                }else{
                    if let snapshot = querySnapshot {
                        
                        for document in snapshot.documents{
                            self.documentQueryObject = document
                        }
                        
                        if snapshot.documents.count != 0 {
                            self.documentQueryObject = snapshot.documents[0]
                            let channel = Channel(document: self.documentQueryObject!)
                            self.channelListener?.remove()
                            //store id in userdefaults
                            UserDefaults.standard.set((channel?.id!)! as String, forKey: "channelFirebaseID")

                            let vc = ChatViewController(channel: channel!)
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                           
                        }else{
                            // no thread ,create new one
                            
                            let channel = Channel(name:self.userEmail!,creator:self.userEmail!)
                            let ref = self.channelReference.document()
                            // ref is a DocumentReference
                            self.documentID = ref.documentID
                            // id contains the random ID
                            ref.setData(channel.representation){error in
                                if let e = error {
                                    print("Error saving channel: \(e.localizedDescription)")
                                }else{
                                    print("document created: \(self.documentID)")
                                    self.saveToServer()
                                }
                            }
                        }
                    }
                }
            }
        }
        
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
    }
    
  
    // load and initiliza when first video ends.
    func setUpUnityView(){
        
        //load unity
        UnityEmbeddedSwift.showUnity()
        unityView = UnityEmbeddedSwift.getUnityView()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//           self.setUpFloatingActionbutton()
            self.view.insertSubview(self.unityView!, at: 0)
      
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
////                self.view.sendSubviewToBack(self.unityView!)
//
//
//            })
        })
    }
    
    
    func showAlertWithAction(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
       
            self.mainPresentedView.isHidden = true
        self.ControlTransferButtonActive()
//        self.view.bringSubviewToFront(self.unityView!)
        
            
            
        })
        //        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        //        })
        alert.addAction(ok)

        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func closeUnityView(_ message: String!) {
        
        self.view.sendSubviewToBack(self.unityView!)
    }
    

    
    func sendMessage(toMobileApp message: String!) {
        //add to cart tapped
        if !isRedeem{
            self.view.sendSubviewToBack(self.unityView!)
            //populate main presented view with corresonding item
         //   print("recieved id is",message)
            let parsed = message.replacingOccurrences(of: "(Clone)", with: "")
            print("parsed message is,",parsed)
            self.mainPresentedView.isHidden = false
            tappedItem = modelsInView[parsed]!
            itemNameLabel.text = tappedItem.basicInfo.name
            let imagePath = tappedItem.modelFile.thumbnail
            let url = URL(string: imagePath!)

            presentedModelImage.sd_setImage(with: url)
            presentedTextView.text = tappedItem.description
        }

       
    }
    
    func showContainerView(_ message: String!) {
        self.mainPresentedView.isHidden = true
        self.view.sendSubviewToBack(self.unityView!)
      
    }
    
    func avatarModule(_ message1: String!,forRedeem message2: String!){
        print("Avatar module")
        print(message1)
        print(message2)
        self.view.sendSubviewToBack(self.unityView!)

        
  
    }
    
    func sendMessage(onReload message: String!) {
        modelsInCart.removeAll()
        modelsInView.removeAll()
        self.mainPresentedView.isHidden = true
        cartCountLabel.text = String(modelsInCart.count)
        self.view.sendSubviewToBack(self.unityView!)
        
    }
    
    func modelProjectionDone(_ message: String!){
        
       
        print("model projection done",message)
       // selectedModelForDownload.append(message)

        
    }
    
    
}





//MARK: -CollectionViews
extension MainViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainCategoryCollectionView {
            
            if mainCategoryList == nil {
                return 0
            }
            else {
                return mainCategoryList.data!.count
            }
        }else if collectionView == subCategoryCollectionView {
            if isRedeem == false {
                if isSearching {
                    return searchResult.count
                }else {
                    if vendorSelectedIndex == -1 {
                        return 0
                    }else {
                        return modelDataSturctArray.count
                    }
                    
                }
            }
            //for redeem process
            else{
                return parslRedeemModel.data.parslModels.count
            }
        }else if collectionView == vendorsCollectionView {
            if vendersList != nil {
                print("Vendors count \(vendersList.data.vendorsList.count)")
                return vendersList.data.vendorsList.count
            }else {
                return 0
            }
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainCategoryCollectionView {
            let cell = mainCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "MainCategoryCollectionViewCell", for: indexPath) as! MainCategoryCollectionViewCell
            cell.titleLabel.text = "  \(mainCategoryList.data![indexPath.row].categoryTitle!)  "
            
            cell.selectedView.isHidden = true
            
            if selectedIndex == indexPath.row
            {
                //cell.titleLabel.textColor = UIColor.white
//                cell.titleLabel.alpha = 1
//                cell.titleLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 16.0)!
                cell.parentView.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.6666666667, blue: 0.7647058824, alpha: 1)
                cell.titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            else
            {
                //cell.titleLabel.textColor = UIColor.lightGray
                //cell.selectedView.isHidden = true
//                cell.titleLabel.alpha = 0.6
//                cell.titleLabel.font = UIFont(name: "HelveticaNeue", size: 13.0)!
                cell.parentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            return cell
        }
        if collectionView == subCategoryCollectionView {
            let cell = subCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoriesCollectionViewCell", for: indexPath) as! SubCategoriesCollectionViewCell
    
            cell.sybCategoriesView.roundCorners(corners: [.layerMinXMaxYCorner, .layerMinXMinYCorner], radius: 8)
            cell.thumbnail.roundCorners(corners: [.layerMinXMaxYCorner, .layerMinXMinYCorner], radius: 5)
            if isRedeem == false{

                var item: ModelDataStruct
                if isSearching {
                    item = searchResult[indexPath.row]
                }else {
                    item = modelDataSturctArray[indexPath.row]
                }
                cell.imagePath = item.modelFile.thumbnail
                let url = URL(string: cell.imagePath)
                cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                cell.titleLabel.text = String(item.basicInfo.name)
                cell.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)
                
                return cell
            }
            else {
               
                let item = parslRedeemModel.data.parslModels[indexPath.row]
                cell.imagePath = item.modelFile.thumbnail
                let url = URL(string: cell.imagePath)
                cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
                cell.titleLabel.text = String(item.basicInfo.name)
                cell.selectedImage.isHidden = true
                cell.priceLabel.isHidden = true
                return cell
            }
        }
        if collectionView == vendorsCollectionView {
            let cell = vendorsCollectionView.dequeueReusableCell(withReuseIdentifier: "MainCategoryCollectionViewCell", for: indexPath) as! MainCategoryCollectionViewCell
            cell.titleLabel.text = "  \(vendersList.data.vendorsList[indexPath.row].vendorName)  "
            cell.selectedView.isHidden = true
            
            let url = URL(string: vendersList.data.vendorsList[indexPath.row].otherInfo.logoURL)
            cell.imageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            
            if vendorSelectedIndex == indexPath.row{
                //cell.selectedImage.isHidden = false
//                cell.titleLabel.alpha = 1
//                cell.titleLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
                cell.parentView.backgroundColor = #colorLiteral(red: 0.6505359792, green: 0.5087047048, blue: 1, alpha: 1)
                cell.titleLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            else{
                //cell.selectedImage.isHidden = true
//                cell.titleLabel.alpha = 0.6
//                cell.titleLabel.font = UIFont(name: "HelveticaNeue", size: 12.0)!
                cell.parentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                
                
            }
            let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(vendorLongTouched))
            cell.parentView.isUserInteractionEnabled = true
            cell.parentView.addGestureRecognizer(longTapGesture)
            return cell
        }
        return UICollectionViewCell()
    }
    
    
    @objc private func vendorLongTouched(){
        if vendorSelectedIndex == -1 {
            return
        }
        let vendorInfo = vendersList.data.vendorsList[vendorSelectedIndex].otherInfo
        vendorDetailDialog.vendorTitle.text = vendersList.data.vendorsList[vendorSelectedIndex].vendorName
        vendorDetailDialog.vendorSlogan.text = vendorInfo.slogan
        vendorDetailDialog.vendorDesc.text = vendorInfo.otherInfoDescription
        
        let url = URL(string: vendorInfo.logoURL)
        vendorDetailDialog.vendorLogo.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        
        
        blurView.isHidden = false
        vendorDetailDialog.isHidden = false
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == mainCategoryCollectionView{
            selectedIndex = indexPath.row
            getDownloadedVendorListOrCallAPI(categoryIndex: selectedIndex)
            
        }else if collectionView == vendorsCollectionView{
            vendorSelectedIndex = indexPath.row
           
//            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: 5).isActive = true
            getDownloadedModelsListOrCallAPI()
            self.subcategoriesHeight.constant = 90
            
            actionButton.removeFromSuperview()
            view.addSubview(actionButton)
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -8).isActive = true

            UIView.animate(withDuration: 1, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
                       
            
        } else if collectionView == subCategoryCollectionView{
        
           
            
            
       //     modelsInCart.append(modelDataSturctArray[indexPath.row])
//            return
            if isRedeem == false{
                self.mainPresentedView.isHidden = true


                var item: ModelDataStruct
                if isSearching {
                    item = searchResult[indexPath.row]
                }else {
                    item = modelDataSturctArray[indexPath.row]
                }
               
                prices = item.priceInfo.price
                selectedModelIndex = indexPath.row
                modelID = item.productId

                modelsInView[modelID!] = item
                self.sendMSg(ID: modelID!)
//                self.ControlTransferButtonActive()
//                self.view.bringSubviewToFront(self.unityView!)
                //completeList = item
//                itemNameLabel.text = item.basicInfo.name
//                let imagePath = item.modelFile.thumbnail
//                let url = URL(string: imagePath!)
//
//                presentedModelImage.sd_setImage(with: url)
                presentedTextView.text = item.description
//                self.format = item.format
//                self.isMultiNode = item.multiNode
//
//                let downloadedModelIndex = getIndexIfModelAlreadyDownloaded(modelId: item.modelFile._3dModelFile)
//                print(downloadedModelIndex)
//                if downloadedModelIndex != -1 {
//                    if isModelAvailableInSapce {
//                        self.view.makeToast("Model is already available in space!", duration: 3.0, position: .center, completion: nil)
//                    }else {
//
//                        self.addModelInScene(url: modelsDownloadedForSend[downloadedModelIndex].destinationUrl)
//                    }
//
//
//                }else {
////                    DispatchQueue.main.async {
////                        self.view.isUserInteractionEnabled = false
////                    }
//                    textueresDownloadedCount = 0
//                    ProgressHUD.show("Downloading")
//                    self.downloadZippedModel(modelURL: item.modelFile._3dModelFile, modelTitle: item.basicInfo.name,format:item.format, videoURL: self.videoUrl, messageText: messageAR)
//                }
//
//            }
//            else
//            {
//                let downloaedModelIndex = getDownloadedModelIndexForRedeem(parslRedeemModel.data.parslModels[indexPath.row].modelFile._3dModelFile)
//                if downloaedModelIndex != -1 {
//                    //self.drone.loadModel(url: downloadedModelUrl)
//                    self.addModelInScene(url: downloadedModelsForRedeem[downloaedModelIndex].destinationUrl)
//                }else {
////                    DispatchQueue.main.async {
////                        self.view.isUserInteractionEnabled = false
////                    }
//                    ProgressHUD.show("Downloading")
//                    selectedModelIndexForRedeem = indexPath.row
//                    self.downloadModel(modelURL: parslRedeemModel.data.parslModels[indexPath.row].modelFile._3dModelFile,
//                                       modelTitle: parslRedeemModel.data.parslModels[indexPath.row].basicInfo.name,
//                                       format: parslRedeemModel.data.parslModels[indexPath.row].format, modelIndex: selectedModelIndex)
//                }
            }else{
                selectedIndex = indexPath.row
                if selectedIndex != 0{
                    let redeemModelID = parslRedeemModel.data.parslModels[selectedIndex].productId
                    self.sendMSg(ID: redeemModelID!)

                    self.ControlTransferButtonActive()
                    self.view.bringSubviewToFront(self.unityView!)
                    
                }
                
                
                
                
            }
        }
    }
    
    private func getDownloadedModelIndexForRedeem(_ model: String) -> Int {
        var index = -1
        for i in 0..<downloadedModelsForRedeem.count {
            if downloadedModelsForRedeem[i]._3dModelFile == model {
                index = i
                break
            }
        }

        return index
    }
    
    
    private func getDownloadedVendorListOrCallAPI(categoryIndex: Int) {
        var isFound = false
        for i in 0..<downloadedVendors.count {
            if downloadedVendors[i].categoryIndex == categoryIndex {
                isFound = true
                self.vendersList = downloadedVendors[i].vendorsList
                vendorSelectedIndex = 0
                break
            }
        }
        
        if isFound {
            getDownloadedModelsListOrCallAPI()
        }else {
            ProgressHUD.animationType = .circleRotateChase
            ProgressHUD.show("Loading")
            
            let selectedItem = (mainCategoryList.data?[categoryIndex].categoryKey)!
            getCategoryVendors(categoryName: selectedItem)
            
//            DispatchQueue.main.async {
//                self.view.isUserInteractionEnabled = false
//            }
        }
    }
    
    private func getDownloadedModelsListOrCallAPI(){
        var isFound = false
        for i in 0..<downloadedData.count {
            if downloadedData[i].categoryIndex == selectedIndex && downloadedData[i].vendorIndex == vendorSelectedIndex {
                isFound = true
                self.modelDataSturctArray = downloadedData[i].modelsData
                break
            }
        }
        
        if isFound {
            DispatchQueue.main.async {
                self.mainCategoryCollectionView.reloadData()
                //self.vendorsCollectionView.reloadData()
                self.vendorsCollectionView.reloadSections(IndexSet(integer: 0))
                self.subCategoryCollectionView.reloadData()
            }
        }else {
            ProgressHUD.animationType = .circleRotateChase
            ProgressHUD.show("Loading")
            let categoryKey = (mainCategoryList.data?[selectedIndex].categoryKey)!
            let vendorKey = self.vendersList.data.vendorsList[vendorSelectedIndex].vendorID
            getSubCategories(key: categoryKey, vendor: vendorKey)
            
//            DispatchQueue.main.async {
//                self.view.isUserInteractionEnabled = false
//            }
        }
    }
    
    private func getIndexIfModelAlreadyDownloaded(modelId: String) -> Int{
        var index = -1
        for i in 0..<modelsDownloadedForSend.count {
            if modelId == modelsDownloadedForSend[i]._3dModelFile {
                index = i
                break
            }
        }
        
        return index
    }
}



//MARK: -TableViews
extension MainViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
    
        if isVideoMessage{
            return 5
        }else
        {

            return 4
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isRedeem && isVideoMessage{
            
            if section == 3{
                return parslRedeemModel.data.parslModels.count
            }else{
                return 1
            }
       
        }else if isRedeem && !isVideoMessage{
            
            if section == 2{
                return parslRedeemModel.data.parslModels.count
            }else{
     
                return 1
            }
    
        }
        else{
            return 0
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
        
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "redeemTitleCell", for: indexPath) as! RedeemTableViewCell
            
             cell.title.text = "Greetings from \(nameofSender)"
             //                            self.redeemHalfViewLabel.text = "Greetings from " + parslRedeemModel.data.senderData.senderEmail
             return cell
            
        }else if indexPath.section == 1{
            cell = tableView.dequeueReusableCell(withIdentifier: "redeemMessageCell", for: indexPath) as! RedeemTableViewCell
            cell.messageTextView.text =  redeemMessage
               
            return cell
            
        }
        else if indexPath.section == 2{
            
            if isVideoMessage{
                
                cell = tableView.dequeueReusableCell(withIdentifier: "redeemVideoCell", for: indexPath) as! RedeemTableViewCell
                cell.downloadedImage.addTarget(self, action: #selector(self.playRedeemVideoTapped(sender:)), for: .touchUpInside)
                cell.downloadedImage.tag = indexPath.row
                return cell
            }else{
                //model table view
                cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell
            }
        }
        else if indexPath.section == 3{
            
            
            
            if isVideoMessage{

                
                //model table view
                cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath) as! RedeemTableViewCell
               var item = ModelDataStruct()

               item = parslRedeemModel.data.parslModels[indexPath.row]
               let imagePath = item.modelFile.thumbnail
               let url = URL(string: imagePath!)
               cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
               cell.title.text = String(item.basicInfo.name)
               
               cell.downloadedImage.tag = indexPath.row
                cell.downloadedImage.addTarget(self, action: #selector(self.modelDownloadTapped(sender:)), for: .touchUpInside)
               
               
               for model in selectedModelForDownload{
                   print("models are",model)
                  
            
                   if model == item.productId as? String{
                       print("is equal ",item.productId)
                       cell.downloadedImage.setImage(UIImage(named: "model_downloaded.png"), for: .normal)
                       break
                       
                   }else{
                       cell.downloadedImage.setImage(UIImage(named: "download_icon.png"), for: .normal)
                   }
                   
               }
                return cell
            }
            else{
                
        
                //add to wallet
                cell = tableView.dequeueReusableCell(withIdentifier: "redeemAddWalletCell", for: indexPath) as! RedeemTableViewCell
                cell.downloadedImage.addTarget(self, action: #selector(self.addToWalletBtnTapped(sender:)), for: .touchUpInside)
                cell.downloadedImage.tag = indexPath.row
//                cell.downloadedImage.backgroundColor =  UIColor(named: "#35AAC3" )
//                    cell.downloadedImage.setTitle("Add to Walled", for: .normal)
                return cell
                
            }
        }else{
            
            //add to wallet
            cell = tableView.dequeueReusableCell(withIdentifier: "redeemAddWalletCell", for: indexPath) as! RedeemTableViewCell
            cell.downloadedImage.addTarget(self, action: #selector(self.addToWalletBtnTapped(sender:)), for: .touchUpInside)
            cell.downloadedImage.tag = indexPath.row
//            cell.downloadedImage.backgroundColor =  UIColor(named: "#35AAC3" )
//                cell.downloadedImage.setTitle("Add to Walled", for: .normal)
            return cell
        }
        
    }
                

    
    @objc func modelDownloadTapped(sender: UIButton){
        var isFound:Bool = false
        let index = sender.tag
        var buttonTag :String = ""
        
        buttonTag = parslRedeemModel.data.parslModels[index].productId
        for id in selectedModelForDownload{
            if id == buttonTag{
              //model already projected
                isFound = true
                
                break
            }else{
                isFound = false
                
            }

        }
        
        
        if isFound{
            self.view.makeToast("Model has already been  projected")
            self.RedeemHalfView.isHidden = true
            self.redeemAnimatingView.isHidden = false
        }else{
            self.RedeemHalfView.isHidden = true
            sendRedeemMsg(ID: buttonTag)
            self.redeemAnimatingView.isHidden = false
        }


    }
    
    @objc func playRedeemVideoTapped(sender: UIButton){
        let url = URL(string: parslRedeemModel.data.receiverData.videoMsg)!
        playServerVideo(url: url)
    }
    
    @objc func addToWalletBtnTapped(sender: UIButton){
    
    let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
    vc?.delegate = self
    self.navigationController?.present(vc!, animated: true, completion: nil)
    }
}




//MARK: -Extensions

extension MainViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search(searchBar)
        self.searchBar.endEditing(true)
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        search(searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchResult.removeAll()
        isSearching = false
        self.searchBar.isHidden = true
        DispatchQueue.main.async {
            self.subCategoryCollectionView.reloadData()
        }
    }
    
    func search(_ searchBar: UISearchBar){
        let searchText = searchBar.text?.lowercased()
        if searchText?.count ?? 0 > 2 {
            isSearching = true
            searchResult = modelDataSturctArray.filter({$0.basicInfo.name.lowercased().contains(searchText!) })
            print(searchResult)
            
            
        }else {
            isSearching = false
        }
        
        DispatchQueue.main.async {
            self.subCategoryCollectionView.reloadData()
        }
        
    }
}

extension float4x4 {
    var translation: simd_float3 {
        let translation = self.columns.3
        return simd_float3(translation.x, translation.y, translation.z)
    }
}
extension UIColor {
    open class var transparentLightBlue: UIColor {
        return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.50)
    }
}



extension MainViewController{
    //MARK: Move Projected Object
    private func deltas() -> (sin: CGFloat, cos: CGFloat) {
        return (sin: kMovingLengthPerLoop * CGFloat(sin(drone.eulerAngles.y)), cos: kMovingLengthPerLoop * CGFloat(cos(drone.eulerAngles.y)))
    }
    @IBAction func upLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("UP")
        let action = SCNAction.moveBy(x: 0, y: kMovingLengthPerLoop, z: 0, duration: kAnimationDurationMoving)
        execute(action: action, sender: sender)
    }
    @IBAction func downLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("Down")
        let action = SCNAction.moveBy(x: 0, y: -kMovingLengthPerLoop, z: 0, duration: kAnimationDurationMoving)
        execute(action: action, sender: sender)
    }
    @IBAction func moveLeftLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("Left")
        let x = -deltas().cos
        let z = deltas().sin
        moveDrone(x: x, z: z, sender: sender)
    }
    @IBAction func moveRightLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("Right")
        let x = deltas().cos
        let z = -deltas().sin
        moveDrone(x: x, z: z, sender: sender)
    }
    @IBAction func moveForwardLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("Forward")
        let x = -deltas().sin
        let z = -deltas().cos
        moveDrone(x: x, z: z, sender: sender)
    }
    @IBAction func moveBackLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("Back")
        let x = deltas().sin
        let z = deltas().cos
        moveDrone(x: x, z: z, sender: sender)
    }
    @IBAction func rotateLeftLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("RotateLeft")
        rotateDrone(yRadian: kRotationRadianPerLoop, sender: sender)
    }
    @IBAction func rotateRightLongPressed(_ sender: UILongPressGestureRecognizer) {
        print("RotateRight")
        rotateDrone(yRadian: -kRotationRadianPerLoop, sender: sender)
    }
    private func rotateDrone(yRadian: CGFloat, sender: UILongPressGestureRecognizer) {
        let action = SCNAction.rotateBy(x: 0, y: yRadian, z: 0, duration: kAnimationDurationMoving)
        execute(action: action, sender: sender)
    }
    private func moveDrone(x: CGFloat, z: CGFloat, sender: UILongPressGestureRecognizer) {
        let action = SCNAction.moveBy(x: x, y: 0, z: z, duration: kAnimationDurationMoving)
        execute(action: action, sender: sender)
    }
    private func execute(action: SCNAction, sender: UILongPressGestureRecognizer) {
        let loopAction = SCNAction.repeatForever(action)
        if sender.state == .began {
            drone.runAction(loopAction)
        } else if sender.state == .ended {
            drone.removeAllActions()
        }
    }
}
extension MainViewController {
    private func fetchModelsApiData(data: [AnyObject]){
        modelDataSturctArray.removeAll()
        for i in 0..<data.count {
            let dataObj = data[i] as! [String: AnyObject]
            let modelDataStruct = fetchSingleModel(dataObj: dataObj)
            modelDataSturctArray.append(modelDataStruct)
        }
        
        let downloadedDataStruct = DownloadedDataForCatAndVendors(categoryIndex: selectedIndex, vendorIndex: vendorSelectedIndex, modelsData: modelDataSturctArray)
        
        downloadedData.append(downloadedDataStruct)
    }
    
    private func fetchSingleModel(dataObj: [String: AnyObject]) -> ModelDataStruct {
        let modelFile = getModelFile(modelFile: dataObj["model_files"] as! [String: AnyObject])
        let basicInfo = getModelBasicInfo(basicInfo: dataObj["basic_info"] as! [String: AnyObject])
        let priceInfo = getModelPriceInfo(priceInfo: dataObj["price_related"] as! [String: AnyObject])
        let scalingInfo = getModelScalingInfo(scalingInfo: dataObj["scaling"] as! [String: AnyObject])
        
        let hyperlink = dataObj["hyperlink"] as! String
        let document = dataObj["document"] as! String
        let sampleVideo = dataObj["sample_video"] as! String
        let promoVideo = dataObj["promotional_video"] as! String
        let images = dataObj["images"] as! [String]
        let multiTexture = dataObj["multi_texture"] as! Int
        let licenceSource = dataObj["license_source"] as! String
        
        let action = getModelActions(actions: dataObj["actions"] as! [String: String])
        
        let video = dataObj["video"] as! String
        let analytics = getModelAnalytics(analytics: dataObj["analytics"] as! [String: AnyObject])
        let category = dataObj["category"] as! String
        let manufacturingDate = dataObj["manufacturing_date"] as! String
        let ownerId = dataObj["owner_id"] as! String
        let purchaseStatus = dataObj["purchase_status"] as! Int
        let license = dataObj["license"] as! String
        let description = dataObj["description"] as! String
        let addlInfo = dataObj["additional_info"] as! String
        let multinode = dataObj["multi_node"] as! Int
        let questionnaire = dataObj["questionnaire"] as! [String]
        let comments = dataObj["comment_feed"] as! [String]
        let metaTags = dataObj["meta_tags"] as! String
        let visionMessage = dataObj["vision_message"] as! String
        let subCategory = dataObj["sub_category"] as! [String]
        let testedStatus = dataObj["tested_status"] as! Double
        let modelIndex = dataObj["model_index"] as! Int
        let cryptoInfo = getModelCryptoInfo(cryptoInfo: dataObj["cryptographic_info"] as! [String: AnyObject])
        let format = dataObj["format"] as! String
        let productId = dataObj["product_id"] as! String
        let modelId = dataObj["model_id"] as! String
        let isFavourite = dataObj["isFavorite"] as! Int
        let textureFile = getModelTextureFile(textureFile: dataObj["texture_file"] as! [AnyObject])
        
        let modelDataStruct = ModelDataStruct(modelFile: modelFile, basicInfo: basicInfo, priceInfo: priceInfo, scalingInfo: scalingInfo, hyperlink: hyperlink, document: document, sampleVideo: sampleVideo, promoVideo: promoVideo, images: images, multiTexture: multiTexture, licenseSource: licenceSource, modelActions: action, video: video, analytics: analytics, category: category, manufacturingDate: manufacturingDate, ownerId: ownerId, purchaseStatus: purchaseStatus, license: license, description: description, additionalInfo: addlInfo, multiNode: multinode, questionnaire: questionnaire, commentFeed: comments, metaTags: metaTags, visionMessage: visionMessage, subCategory: subCategory, testedStatus: testedStatus, modelIndex: modelIndex, cryptographicsInfo: cryptoInfo, format: format, productId: productId, modelId: modelId, isFavourite: isFavourite, textureFiles: textureFile, isSelectedByUser: false)
        
        return modelDataStruct
    }
    
    private func getModelFile(modelFile: [String: AnyObject]) -> ModelFileStruct {
        let thumbnail = modelFile["thumbnail"] as! String
        let audioFile = modelFile["audio_file"] as! String
        let textureType = modelFile["textures_type"] as! String
        let textures = modelFile["textures"] as! [String]
        let multiModelTextureList = modelFile["multitexture_models_list"] as! [String]
        let _3dModelFile = modelFile["3d_model_file"] as! String
        
        return ModelFileStruct(thumbnail: thumbnail, audioFile: audioFile, textureType: textureType, textures: textures, multiTexturesModelFile: multiModelTextureList, _3dModelFile: _3dModelFile)
    }
    
    private func getModelBasicInfo(basicInfo: [String: AnyObject]) -> ModelBasicInfoStruct {
        let name = basicInfo["name"] as! String
        let manufacturer = basicInfo["manufacturer"] as! String
        let modelType = basicInfo["model"] as! String
        
        return ModelBasicInfoStruct(name: name, manufacturer: manufacturer, modelType: modelType)
    }
    
    private func getModelPriceInfo(priceInfo: [String: AnyObject]) -> ModelPriceInfoStruct {
        let price = priceInfo["price"] as! Double
        let currency = priceInfo["currency"] as! String
        let priceStatus = priceInfo["price_status"] as! String
        
        return ModelPriceInfoStruct(price: price, currency: currency, priceStatus: priceStatus)
    }
    
    private func getModelScalingInfo(scalingInfo: [String: AnyObject]) -> ModelScalingStruct {
        let scaleUnit = scalingInfo["scale_unit"] as! String
        let minScale = getScaling(scalingInfo: scalingInfo["min_scale"] as! [String: Double])
        let maxScale = getScaling(scalingInfo: scalingInfo["max_scale"] as! [String: Double])
        
        return ModelScalingStruct(scaleUnit: scaleUnit, minScale: minScale, maxScale: maxScale)
    }
    
    private func getScaling(scalingInfo: [String: Double]) -> ScalingAxisStruct {
        let xAxis = scalingInfo["x-axis"]
        let yAxis = scalingInfo["y-axis"]
        let zAxis = scalingInfo["z-axis"]
        
        return ScalingAxisStruct(xAxis: xAxis, yAxis: yAxis, zAxis: zAxis)
    }
    
    private func getModelActions(actions: [String: String]) -> ModelActionStruct {
        let actionType = actions["action_type"]
        let actionTitle = actions["action_title"]
        let actionData = actions["action_data"]
        
        return ModelActionStruct(actionType: actionType, actionTitle: actionTitle, actionData: actionData)
    }
    
    private func getModelAnalytics(analytics: [String: AnyObject]) -> ModelAnalyticsStruct {
        let shareCount = analytics["shares_count"] as! Int
        let downloadCount = analytics["download_count"] as! Int
        let searchCount = analytics["search_appearances"] as! Int
        let survey = analytics["survey"] as! [String]
        
        return ModelAnalyticsStruct(shareCount: shareCount, downloadCount: downloadCount, searchCount: searchCount, questions: survey)
    }
    
    private func  getModelCryptoInfo(cryptoInfo: [String: AnyObject]) -> ModelCryptoStruct {
        let digitalSig = cryptoInfo["digital_sig"] as! [Int]
        let publicKey = cryptoInfo["public_key"] as! [Int]
        let modelSha = cryptoInfo["model_sha1"] as! String
        let unzippedSize = cryptoInfo["unzipped_size"] as! Int64
        let zippedSize = cryptoInfo["zipped_size"] as! Int64
        
        return ModelCryptoStruct(digitalSig: digitalSig, publicKeys: publicKey, modelSHA: modelSha, unzippedSize: unzippedSize, zippedSize: zippedSize)
     }
    
    private func getModelTextureFile(textureFile: [AnyObject]) -> [ModelTextureFileStruct] {
        var textures = [ModelTextureFileStruct]()
        for i in 0..<textureFile.count {
            let texture = textureFile[i] as! [String: AnyObject]
            
            let title = texture["title"] as! String
            let defaultTexture = texture["default"] as! Int
            let texturesList = texture["texturesList"] as! [String]
            
            textures.append(ModelTextureFileStruct(title: title, defaultTextures: defaultTexture, texturesList: texturesList))
            
        }
        
        
        return textures
    }
    
    
    //Redeem Parsl API parser methods
    
    private func
    fetchRedeemModelStruct(response: [String: AnyObject]) -> RedeemModelStruct {
        let message = response["message"] as! String
        if message.contains("Success") {
            let data = fetchRedeemModelDataStruct(dataObj: response["data"] as! [String: AnyObject])
            
            return RedeemModelStruct(message: message, data: data)
        }
        
        return RedeemModelStruct(message: message, data: nil)
        
    }
    
    private func fetchRedeemModelDataStruct(dataObj: [String: AnyObject]) -> RedeemModelData {
        let giftModel = fetchSingleModel(dataObj: dataObj["gift_model"] as! [String: AnyObject])
        let stripeToken = dataObj["parsl_stripe_token"] as! String
        let senderData = fetchSenderData(senderData: dataObj["sender_data"] as! [String: String])
        let receiverData = fetchReceiverData(receiverData: dataObj["reciever_data"] as! [String: AnyObject])
        let timestamp = dataObj["parsl_timestamp"] as? Int64 ?? 0
        let otp = dataObj["parsl_otp"] as! String
        let parslAvailableFlag = dataObj["parsl_available_flag"] as! Bool
        //let parslRedeemed = dataObj["parsl_redeemed"] as! Bool
        let parslRedeemed = false
        let parslId = dataObj["parsl_id"] as! String
        
        let avatarCharacter = dataObj["avatar_chracter"] as! String
        let avatarAnimation = dataObj["avatar_animation"] as! String
        
        var parslModelsArray = [ModelDataStruct]()
        
        let parslModels = dataObj["parsl_models"] as! [AnyObject]
        
        for i in 0..<parslModels.count {
            let parslModel = fetchSingleModel(dataObj: parslModels[i] as! [String: AnyObject])
            parslModelsArray.append(parslModel)
        }
        
        return RedeemModelData(giftModel: giftModel, stripeToken: stripeToken, senderData: senderData, receiverData: receiverData, timestamp: timestamp, otp: otp, availableFlag: parslAvailableFlag, parslRedeemed: parslRedeemed, parslId: parslId, parslModels: parslModelsArray,avatarCharacter: avatarCharacter,avatarAnimation: avatarAnimation)
    }
    
    private func fetchSenderData(senderData: [String: String]) -> SenderDataStruct {
        let senderName = senderData["sender_name"]
        let cardNumber = senderData["card_number"]
        let cardType = senderData["card_type"]
        let senderEmail = senderData["sender_email"]
        let transactionId = senderData["transection_id"]
        
        return SenderDataStruct(senderName: senderName,cardNumber: cardNumber, cardType: cardType, senderEmail: senderEmail, transactionId: transactionId)
    }
    
    private func fetchReceiverData(receiverData: [String: AnyObject]) -> ReceiverDataStruct {
        let textMsg = receiverData["text_msg"] as? String
        let videoMsg = receiverData["video_msg"] as? String
        let isRedeemed = receiverData["redeemed_by_reciever"] as? Bool
        
        return ReceiverDataStruct(textMsg: textMsg, videoMsg: videoMsg, isRedeemedByUser: isRedeemed)
        
    }
}
extension MainViewController {
    private func openBottomActionSheetForDataSorting() {
        let optionMenu = UIAlertController(title: nil, message: "Sort by", preferredStyle: .actionSheet)
        
        
        let priceHighLow = UIAlertAction(title: "Price (High to Low)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "H_L"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.priceInfo.price > $1.priceInfo.price
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        
        
        let priceLowHigh = UIAlertAction(title: "Price (Low to High)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "L_H"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.priceInfo.price < $1.priceInfo.price
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        
        let alphaAZ = UIAlertAction(title: "Alphabetically (A - Z)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "A_Z"
            
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.basicInfo.name < $1.basicInfo.name
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        let alphaZA = UIAlertAction(title: "Alphabetically (Z - A)", style: .default, handler: { [self]
            (alert: UIAlertAction!) -> Void in
            selectedFilter = "Z_A"
            let sortedData = modelDataSturctArray.sorted(by: {
                $0.basicInfo.name > $1.basicInfo.name
            })
            modelDataSturctArray = sortedData
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        })
        
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dismiss(animated: true, completion: nil)
            
        })
        
        if selectedFilter == "H_L" {
            setSelectedCheckmark(action: priceHighLow)
        }else if selectedFilter == "L_H" {
            setSelectedCheckmark(action: priceLowHigh)
        } else if selectedFilter == "A_Z" {
            setSelectedCheckmark(action: alphaAZ)
        } else if selectedFilter == "Z_A" {
            setSelectedCheckmark(action: alphaZA)
        }
        
        
        optionMenu.addAction(priceHighLow)
        optionMenu.addAction(priceLowHigh)
        optionMenu.addAction(alphaAZ)
        optionMenu.addAction(alphaZA)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func setSelectedCheckmark(action: UIAlertAction){
        
        if #available(iOS 13.0, *) {
            action.setValue(UIImage(systemName: "checkmark"), forKey: "image")
        } else {
            // Fallback on earlier versions
        }
        
    }
}
@objc private extension MainViewController {

    func keyboardWillShow(_ notification: Notification) {
        collectionViewBottomConstraint.constant = 230
    }

    func keyboardWillHide(_ notification: Notification) {
        collectionViewBottomConstraint.constant = 20
    }
}

private extension MainViewController {
    
    func getVirtualGiftCard(otp: String, receiverName: String, phoneNumber: String) {
        let data = ["parsl":otp as AnyObject,
                    "reciever_name":receiverName as AnyObject,
                    "reciever_phone_no":phoneNumber as AnyObject,
                    "redeemed_timestamp": Date().currentTimeMillis() as AnyObject]
        let parameters = ["data":data as AnyObject]
        
        
        print(parameters)
        let url = "https://parsl.io/get/virtual/card/details"
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
//                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: AnyObject]
                self.virtualCardModel = try JSONDecoder().decode(VirtualCard.self, from: response)
                
                DispatchQueue.main.async {
                    
                    if virtualCardModel != nil && self.modelUrl == "" {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VirtualCardViewController") as! VirtualCardViewController
                        vc.virtualCardModel = self.virtualCardModel
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                }

                
            } catch let error {
                print(error)
            }
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                self.showAlert(withMessage: "Unable to fetch data. Please try again later")
            }
        })
    }
    
}
extension MainViewController: SKStoreProductViewControllerDelegate {
    private func getAppVersionApi() {
        let url = "https://parsl.io/check/version"
        postRequest(serviceName: url, sendData: AppVersionCheck.sharedInstance.getCheckVersionApiParams(), success: { (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: Any]
                
                let message = (jsonResponse["message"] ?? "") as! String
                let status = (jsonResponse["status"] ?? true) as!Bool
                print("VersionAPI response: \(jsonResponse)")
                if status == false {
                    if message.contains("App does not exisit") {
                        print("App does not exist")
                    }else if message.contains("Invalid app type") {
                        print("Invalid app type")
                    }
                    else{
                        DispatchQueue.main.async {
                            self.showAlertViewForVersionCheckAPI()
                        }
                    }
                }
                
                if message.contains("up to date") {
                    print("App is upto date")
                }else if message.contains("latest version available") {
                    print("App latest version available")
                    
                }else {
                    print("Undetermined result")
                    print(message)
                }
                
                
            } catch let error {
                print(error)
            }
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                print("Unable to fetch data of check version api. Please try again later")
            }
        })
    }
    
    private func showAlertViewForVersionCheckAPI(){
        let optionMenu = UIAlertController(title: "PARSL", message: "New version of app is available. Kindly update it from App Store.", preferredStyle: .alert)

        let update = UIAlertAction(title: "Update", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openStoreProductWithiTunesItemIdentifier("1558433040")   //Parsl identifier
        })

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.dismiss(animated: true, completion: nil)
        })




        optionMenu.addAction(update)
        optionMenu.addAction(cancel)
        self.navigationController?.present(optionMenu, animated: true, completion: nil)
    }

    private func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self

        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                // Parent class of self is UIViewContorller
                self?.navigationController?.present(storeViewController, animated: true, completion: nil)
            }
        }
    }
    private func productViewControllerDidFinish(viewController: SKStoreProductViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}
extension MainViewController: CartEmptyDelegate {
    func cartHasBeenCleared() {
        self.modelsInCart.removeAll()
        self.cartCountLabel.text = "\(modelsInCart.count)"
    }
}


