//
//  VirtualCardViewController.swift
//  Parsl
//
//  Created by broadstone on 02/05/2021.
//

import UIKit
import PassKit
class VirtualCardViewController: BaseViewController {

    @IBOutlet weak var nameOnCardLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cvcLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var numberOnCardLabel: UILabel!
    var virtualCardModel: VirtualCard!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nameOnCardLabel.text = virtualCardModel.data?.cardholder?.name

        self.setCardNumber()
        
        
        amountLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (virtualCardModel.data?.spendingControls?.spendingLimits?[0].amount)! / 100.0)
        
    }
    
    
    @IBAction func addToWalletTapped(_ sender: UIButton){
        let library = PKPassLibrary()
        library.openPaymentSetup()
    }
    
    @IBAction func saveCardBtnTapped(_ sender: UIButton){
        UIImageWriteToSavedPhotosAlbum(screenShotMethod()!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
            if let error = error {
                self.view.makeToast("Unable to save card in photos library")
                print(error.localizedDescription)
                
            } else {
                showAlertWithAction(title: "PARSL", message: "Card has been saved to your photos. Don't share this information with anyone.")
                
            }
        }

    func screenShotMethod() -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func setCardNumber(){
        let cardNumber = virtualCardModel.data?.number
       // cardNumber?.prefix(upTo: 4)
      let firstSet =   cardNumber!.prefix(4)
      var stripped = cardNumber!.dropFirst(4)
        
        let secondSet =   stripped.prefix(4)
        stripped = stripped.dropFirst(4)
        
        let thirdSet =   stripped.prefix(4)
        stripped = stripped.dropFirst(4)
        
        let fourthSet =   stripped.prefix(4)
        
        self.numberOnCardLabel.text = firstSet+"   "+secondSet+"   "+thirdSet+"   "+fourthSet
        
      
        
    }
}
