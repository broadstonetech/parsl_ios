//
//  VendorsDetailsDialog.swift
//  Parsl
//
//  Created by broadstone on 24/05/2021.
//

import UIKit

class VendorsDetailsDialog: UIView {
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var vendorLogo: UIImageView!
    @IBOutlet weak var vendorTitle: UILabel!
    @IBOutlet weak var vendorSlogan: UILabel!
    @IBOutlet weak var vendorDesc: UITextView!
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var visitStore: UIButton!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("VendorDetailsDialogView", owner: self, options: nil)
        addSubview(parentView)
        parentView.frame = self.bounds
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

}
