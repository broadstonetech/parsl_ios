//
//  CategoryTableViewCell.swift
//  Parsl
//
//  Created by Billal on 22/02/2021.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var plusButton:      UIButton!
    @IBOutlet weak var minusButton:     UIButton!
    @IBOutlet weak var titleLabel:      UILabel!
    @IBOutlet weak var priceLabel:      UILabel!
    @IBOutlet weak var itemsCountLabel: UILabel!
    @IBOutlet weak var discountedPrice: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var discountedCutView: UIView!
    var count = 0
    weak var delegate: CartCellClickDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
       // linedLabel.isHidden = true
    }

    @IBAction func deleteButtonAction(_ sender: Any) {
        self.delegate?.deleteButtonTapped(cell: self, sender: sender)
    }
    @IBAction func addButton(_ sender: Any) {
        self.delegate?.plusButtonTapped(cell: self, sender: sender)
    }
    
    @IBAction func minusButton(_ sender: Any) {
        self.delegate?.minusButtonTapped(cell: self, sender: sender)
    }
}
