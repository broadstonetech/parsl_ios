//
//  SubCategoriesCollectionViewCell.swift
//  Parsl
//
//  Created by Billal on 23/02/2021.
//

import UIKit

class SubCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sybCategoriesView: UIView!
    
   
    var imagePath = ""
    
    func isSelected (Bool: Bool)
    {
        if Bool == true{
            selectedImage.image = UIImage(named: "check")
        }
        else{
            selectedImage.image = UIImage(named: "")
        }
    }
}
