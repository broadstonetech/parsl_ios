//
//  VideoPlayerVC.swift
//  Parsl
//
//  Created by broadstone on 25/05/2021.
//

import UIKit
import AVFoundation
class VideoPlayerVC: BaseViewController {

    @IBOutlet weak var videoView: VideoView!
    
    var videoPath: String!
    var isArVideo = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareVideoView()
        
    }
    
    private func prepareVideoView(){
        var url: URL
        if isArVideo {
            url = URL(string: videoPath)!
            let player = AVPlayer(url: url)
        }
        else {
            guard let videoPath = Bundle.main.path(forResource: videoPath, ofType:"mp4") else {
                print("error return")
                return
            }
            url = NSURL(fileURLWithPath: videoPath) as URL
        }
        //url = NSURL(fileURLWithPath: videoPath) as URL
        videoView.configure(url: url.absoluteString)
        videoView.isLoop = false
        videoView.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        self.dismiss(animated: true, completion: nil)
    }

}
