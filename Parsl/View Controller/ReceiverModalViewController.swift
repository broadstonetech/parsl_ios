
import UIKit
import ContactsUI

class ReceiverModalViewController: BaseViewController,CNContactPickerDelegate, UITextFieldDelegate{
    private let contactPicker = CNContactPickerViewController()
    var delegate : SendDataDelegate?
    let peoplePicker = CNContactPickerViewController()

    @IBOutlet weak var receiverNameTF: UITextField!
    @IBOutlet weak var urlTF: UITextField!
    @IBOutlet weak var receiverContactTF: UITextField!
    
    
    //MARK: -ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        receiverNameTF.delegate = self
        receiverContactTF.delegate = self
        urlTF.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.delegate?.showActionButton()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if urlTF.text == "" || urlTF.text!.isEmpty {
            self.view.makeToast("Please enter Parsl URL or OTP")
            return
        }
        receiverNameTF.text = ""
        receiverContactTF.text = ""
        
//        if receiverNameTF.text == "" || receiverNameTF.text!.isEmpty {
//            self.view.makeToast("Please enter receiver name")
//            return
//        }
//        if receiverContactTF.text == "" || receiverContactTF.text!.isEmpty {
//            self.view.makeToast("Please enter receiver contact number")
//            return
//        }
        if !urlTF.text!.contains("parsl.io") {
            let uppercasedText = urlTF.text?.uppercased()
            urlTF.text = uppercasedText
        }
        self.delegate?.sendData(self.receiverNameTF.text!,self.receiverContactTF.text!,self.urlTF.text!)
            dismiss(animated: true, completion: nil)
    }
}

