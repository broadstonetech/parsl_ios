//
//  ShareViewController.swift
//  Parsl
//
//  Created by Billal on 08/03/2021.
//

import Foundation
import UIKit

class ShareViewController: PaymentsBaseClass{
    
    var parslURL,parslOTP,transaction,amount,cardNumber,transactionCost, msgText:String!
    var modelsInCart = [ModelDataStruct]()
    var modelsPrices = [ModelsQuantityStruct]()
    
    var isShareBtnClicked = false
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var customizeButton: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var msgForRecepient: UITextView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var transactionalCostLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var visitLinkLabel: UILabel!
    @IBOutlet weak var otpCodeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //msgForRecepient.text = "Hey, here is the Parsl visit link \(parslURL!) or use code \(parslOTP!) to unwrap your Parsl"
        transactionalCostLabel.text = transactionCost
        //msgLabel.text = "Visit link \(parslURL!) or use PARSL redeem code \(parslOTP!) to unwrap your PARSL."
        visitLinkLabel.text = parslURL
        otpCodeLabel.text = parslOTP
        transactionID.text = "\(transaction!)"
        amountLabel.text = "\(amount!)"

        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    @IBAction func shareButton(_ sender: Any) {
        if isShareBtnClicked {
            let alert = UIAlertController(title: "Share PARSL", message: "You have already shared. Do you want to share again?", preferredStyle: .alert)
            let share = UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.openShareActivity()
            })
            let cancel = UIAlertAction(title: "No", style: .cancel, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(share)
            alert.addAction(cancel)
            //        alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else {
            openShareActivity()
        }
        isShareBtnClicked = true
        
    }
    
    private func openShareActivity() {
        //let parslWebsite = "https://www.parsl.io"
        let parslWebsite = "https://testflight.apple.com/join/v9EZzYrO"
        let shareableText = msgText + "\n" + "Visit link \(parslWebsite) and use PARSL redeem code \(parslOTP!) to unwrap your PARSL."
        let activityVC = UIActivityViewController(activityItems: [shareableText], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    @IBAction func mainMenu(_ sender: Any) {
        print(parslOTP!)
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
        if let navigator = self.navigationController {
            navigator.popViewController(animated: false)
            navigator.popViewController(animated: true)
        }
    }
//    func trackMyParsl(){
//        let data = ["transection_id":transaction!,
//                    "card_number":cardNumber!,
//                    "total_price":amount!,
//                    "parsl_url":parslURL!,
//                    "parsl_otp":parslOTP!,
//                    "user_email":emailTF.text!,
//                    "email_condition":"send_transection_and_parsl_information_to_sender"] as [String:Any]
//        let parameters = ["data":data]
//        print(parameters)
//        let url = URL(string: "https://parsl.io/send/email")!
//        let session = URLSession.shared
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
//        } catch let error {
//            print(error)
//        }
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//
//            guard error == nil else {
//                return
//            }
//
//            guard let data = data else {
//                return
//            }
//
//            do {
//                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
//                    self.showAlert(withTitle: "Email Sent", withMessage: "Email with related info is sent successfully")
//                }
//            } catch let error {
//                print(error)
//            }
//        })
//        task.resume()
//    }
}
extension ShareViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + modelsInCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell")!
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductItemsCell") as! ProductItemsCell
            let index = indexPath.row - 1
            cell.name.text = modelsInCart[index].basicInfo.name
            let url = URL(string: modelsInCart[index].modelFile.thumbnail)
            cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            cell.qty.text = String(modelsPrices[index].modelQuantity)
            cell.subTotal.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
            return cell
        }
    }
}
