
import UIKit
import Photos
import Firebase
import MessageKit
import FirebaseFirestore

final class ChatViewController: MessagesViewController{
  
  private var isSendingPhoto = false {
    didSet {
      DispatchQueue.main.async {
        self.messageInputBar.leftStackViewItems.forEach { item in
          item.isEnabled = !self.isSendingPhoto
        }
      }
    }
  }
  
  private let db = Firestore.firestore()
  private var reference: CollectionReference?
  private let storage = Storage.storage().reference()

  private var messages: [Message] = []
  private var messageListener: ListenerRegistration?
  
  private let channel: Channel
    
  private var channelReference: CollectionReference {
    return db.collection("channels")
  }
  
  deinit {
    messageListener?.remove()
   // parent.channelReference.remove()
  }

  init(channel: Channel) {
    self.channel = channel
    super.init(nibName: nil, bundle: nil)
    
    title = "Live Support"
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let logoutBarButtonItem = UIBarButtonItem(title: "End Chat", style: .done, target: self, action: #selector(endChat))
    logoutBarButtonItem.tintColor = UIColor.white
    self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
    
    guard let id = channel.id else {
      navigationController?.popViewController(animated: true)
      return
    }

    reference = db.collection(["channels", id, "thread"].joined(separator: "/"))
    
    messageListener = reference?.addSnapshotListener { querySnapshot, error in
      guard let snapshot = querySnapshot else {
        print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
        return
      }
      
      snapshot.documentChanges.forEach { change in
        self.handleDocumentChange(change)
      }
    }
    
    self.navigationController?.navigationBar.isHidden = false
    self.navigationItem.hidesBackButton = false
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    self.navigationController?.navigationBar.barTintColor = UIColor.clear
    self.navigationController?.navigationBar.tintColor = UIColor.white
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    navigationItem.largeTitleDisplayMode = .never
    
    
    
    maintainPositionOnKeyboardFrameChanged = true
    messageInputBar.inputTextView.tintColor = .primary
    messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
    
    messageInputBar.delegate = self
    messagesCollectionView.messagesDataSource = self
    messagesCollectionView.messagesLayoutDelegate = self
    messagesCollectionView.messagesDisplayDelegate = self
    
//    let cameraItem = InputBarButtonItem(type: .system) // 1
//    cameraItem.tintColor = .primary
//    cameraItem.image = #imageLiteral(resourceName: "camera")
//    cameraItem.addTarget(
//      self,
//      action: #selector(cameraButtonPressed), // 2
//      for: .primaryActionTriggered
//    )
//    cameraItem.setSize(CGSize(width: 60, height: 30), animated: false)
    
//    messageInputBar.leftStackView.alignment = .center
//    messageInputBar.setLeftStackViewWidthConstant(to: 50, animated: false)
   // messageInputBar.setStackViewItems([cameraItem], forStack: .left, animated: false) // 3
  }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
  
  // MARK: - Actions
  
  @objc private func cameraButtonPressed() {
    let picker = UIImagePickerController()
    picker.delegate = self
    
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      picker.sourceType = .camera
    } else {
      picker.sourceType = .photoLibrary
    }
    
    present(picker, animated: true, completion: nil)
  }
  
  // MARK: - Helpers
  
    @objc private func endChat() {
      let ac = UIAlertController(title: nil, message: "All your chat will be deleted.Are you sure you want to continue?", preferredStyle: .alert)
      ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "End Chat", style: .destructive, handler: { _ in
             self.deleteChat()
        }))
        present(ac, animated: true, completion: nil)
        UserDefaults.standard.removeObject(forKey: "channelFirebaseID")
    }
    
    
    private func deleteChat() {
        //first server will save the chat then we will delete from firebase
        let data :Dictionary<String,Any> = ["firebase_id": channel.id! as String]
                                            
        postChatRequest(serviceName:"https://parsl.io/save/chat", sendData: data as [String : AnyObject], success: { (data) in
            print("data here is",data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            print("code is",statusCode)
            let status = data["status"] as! Bool
            
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    // self.showAlert("Success")
                    if status{
                        print("chat successfully saved to server from firebase and deleted")
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        print("something went wrong")
                    }

                }
                
            }
            
        }, failure: { (data) in
            print(data)
            
        })
        
   
    }
  
  private func save(_ message: Message) {
    reference?.addDocument(data: message.representation) { error in
      if let e = error {
        print("Error sending message: \(e.localizedDescription)")
        return
      }
    print("message saved successfully")
      self.messagesCollectionView.scrollToBottom()
    }
    
   channelReference.document(channel.id!).updateData(["status":"Unread"])
    
    
    
  }
  
  private func insertNewMessage(_ message: Message) {
    guard !messages.contains(message) else {
      return
    }
    
    messages.append(message)
    messages.sort()
   
    

    
    let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
    let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
    
    messagesCollectionView.reloadData()
    
    if shouldScrollToBottom {
      DispatchQueue.main.async {
        self.messagesCollectionView.scrollToBottom(animated: true)
      }
    }
  }
  
  private func handleDocumentChange(_ change: DocumentChange) {
   // print(Message(document:change.document))
    guard var message = Message(document:change.document) else {
      return
    }
    
    switch change.type {
    case .added:
      if let url = message.downloadURL {
        downloadImage(at: url) { [weak self] image in
          guard let `self` = self else {
            return
          }
          guard let image = image else {
            return
          }
          
          message.image = image
          self.insertNewMessage(message)
        }
      } else {
        insertNewMessage(message)
      }
      
    default:
      break
    }
  }
  
  private func uploadImage(_ image: UIImage, to channel: Channel, completion: @escaping (URL?) -> Void) {
    guard let channelID = channel.id else {
      completion(nil)
      return
    }
    
    guard let scaledImage = image.scaledToSafeUploadSize, let data = scaledImage.jpegData(compressionQuality: 0.4) else {
      completion(nil)
      return
    }
    
    let metadata = StorageMetadata()
    metadata.contentType = "image/jpeg"
    
    let imageName = [UUID().uuidString, String(Date().timeIntervalSince1970)].joined()
    storage.child(channelID).child(imageName).putData(data, metadata: metadata) { meta, error in
            if (error != nil) {

        } else {
           // let downloadURL = meta!.downloadURL
           // print(downloadURL)
        }
    }
  }

  private func sendPhoto(_ image: UIImage) {
    isSendingPhoto = true
    
    uploadImage(image, to: channel) { [weak self] url in
      guard let `self` = self else {
        return
      }
      self.isSendingPhoto = false
      
      guard let url = url else {
        return
      }
      
      var message = Message(image: image)
      message.downloadURL = url
      
      self.save(message)
      self.messagesCollectionView.scrollToBottom()
    }
  }
  
  private func downloadImage(at url: URL, completion: @escaping (UIImage?) -> Void) {
    let ref = Storage.storage().reference(forURL: url.absoluteString)
    let megaByte = Int64(1 * 1024 * 1024)
    
    ref.getData(maxSize: megaByte) { data, error in
      guard let imageData = data else {
        completion(nil)
        return
      }
      
      completion(UIImage(data: imageData))
    }
  }
  
}

//MARK: - PostRequest for chat module
func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {
 
    let serviceUrl =  serviceName
    print("url == \(serviceUrl)")
    let url = NSURL(string: serviceUrl)
    
    //create the session object
    let session = URLSession.shared
    
    //now create the NSMutableRequest object using the url object
    let request = NSMutableURLRequest(url: url! as URL)
    request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
    
    do {
        
        let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
        let dataString = String(data: data, encoding: .utf8)!
        print("data string is",dataString)
        
        request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
        
    } catch let error {
        print(error.localizedDescription)
    }
    
    //HTTP Headers
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    //create dataTask using the session object to send data to the server
    
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        
        print(" response is \(response)")
        
        //copied
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            // check for http errors
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            
            UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
            UserDefaults.standard.synchronize()
            
            
        }
        let httpStats = response as? HTTPURLResponse
        UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
        UserDefaults.standard.synchronize()
        //end copied
        
        guard error == nil else {
            print("this is error")
//            DispatchQueue.main.async {
////                    CozyLoadingActivity.hide()
////                    self.showFailureAlert(ConstantStrings.internet_off)
//            }
            return
        }
        guard let data = data else {
            return
        }
        
        do {
            //create json object from data
            
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                print("json is \(json)")
                success(json)
                
                //handle json...
     
            }
            
          
        } catch let error {
          
            print(error.localizedDescription)
            var errors = [String : AnyObject]()
            errors["Error"] = "Server Error" as AnyObject
            failure(errors)
        }
        
    })
    
    task.resume()
    
}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
  
  func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? .primary : .incomingMessage
  }
  
  func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
    return false
  }
  
  func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
  }
  
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
  
  func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return .zero
  }
  
  func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
  }
  
  func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    
    return 0
  }
  
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
  
  func currentSender() -> Sender {
    let email = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
    return Sender(id: email, displayName: email)
  }
  
  func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
    return messages.count
  }
  
  func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
    return messages[indexPath.section]
  }
  
  func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
    var name = message.sender.id
    
    if name.contains("agent"){
        name = "Support Agent"
    }
    
    return NSAttributedString(
      string: name,
      attributes: [
        .font: UIFont.preferredFont(forTextStyle: .caption1),
        .foregroundColor: UIColor(white: 0.3, alpha: 1)
      ]
    )
  }
  
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {
  
  func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
    let message = Message(content: text)

    save(message)
    inputBar.inputTextView.text = ""
  }
  
}

// MARK: - UIImagePickerControllerDelegate

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    picker.dismiss(animated: true, completion: nil)
    
    if let asset = info[.phAsset] as? PHAsset { // 1
      let size = CGSize(width: 500, height: 500)
      PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: nil) { result, info in
        guard let image = result else {
          return
        }
        
        self.sendPhoto(image)
      }
    } else if let image = info[.originalImage] as? UIImage { // 2
      sendPhoto(image)
    }
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
}

